# Server

## Installation

First, install the node_modules with:

```
yarn install
```

## Run

To run the server, execute:
```
yarn start
```

## Run - Watch

To run the server using Nodemon, use:

```
yarn watch
```

The server will automatically recompile on file change