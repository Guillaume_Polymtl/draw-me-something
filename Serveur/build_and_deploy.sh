#!/bin/bash

COMMIT_ID=$(git rev-parse --verify HEAD | cut -c1-8)
COMMIT_TIME=$(date '+%d.%m.%Y-%Hh%Mm%Ss-')
CONCATENATED_BUILD_TIME_AND_ID=$COMMIT_TIME$COMMIT_ID
docker build -t registry.gitlab.com/polytechnique-montr-al/log3900/21-1/equipe-110/log3900-110:$CONCATENATED_BUILD_TIME_AND_ID .
docker push registry.gitlab.com/polytechnique-montr-al/log3900/21-1/equipe-110/log3900-110:$CONCATENATED_BUILD_TIME_AND_ID
