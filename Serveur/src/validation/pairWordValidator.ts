import { body } from 'express-validator';

const POTRACE_OPTIONS = 'potraceOptions';
export const potraceOptionsValidator = [
  body(`${POTRACE_OPTIONS}.alphaMax`).optional().isNumeric(),
  body(`${POTRACE_OPTIONS}.background`).optional().isHexColor(),
  body(`${POTRACE_OPTIONS}.blackOnWhite`).optional().isBoolean(),
  body(`${POTRACE_OPTIONS}.color`).optional().isHexColor(),
  body(`${POTRACE_OPTIONS}.optCurve`).optional().isBoolean(),
  body(`${POTRACE_OPTIONS}.optTolerance`).optional().isNumeric(),
  body(`${POTRACE_OPTIONS}.threshold`).optional().isNumeric(),
  body(`${POTRACE_OPTIONS}.turdSize`).optional().isNumeric(),
];

export const drawingAttributesValidator = [
  body('word').exists().isString(),
  body('hints').exists().isArray(),
  body('difficulty').exists().isString(),
  body('creationMode').exists().isString(),
  body('drawingMode').optional().isString(),
  // TODO: Check if the validation below puts panoramicMode as optionnal
  body('panoramicMode').optional().isString(),
  body('centerMode').optional().isString(),

];

export const pairWordValidator = [
  body('word').exists().isString(),
  body('hints').exists().isArray(),
  body('difficulty').exists().isString(),
  body('creationMode').exists().isString(),
  body('drawingMode').optional().isString(),
  body('panoramicMode').optional().isString(),
  body('size').optional(),
  body('centerMode').optional().isString(),
  body('strokes').optional().isArray(),
  body('potraceOptions').optional().isObject(),
  body('imageBase64').optional().isString(),
];

export const potracePreviewValidator = [
  ...potraceOptionsValidator,
  body('imageBase64').exists().isBase64(),
  body('strokes').optional().isArray()];

export const pairWordPotraceValidator = [
  ...potraceOptionsValidator,
  ...drawingAttributesValidator,
  body('imageBase64').exists().isString(),
];
