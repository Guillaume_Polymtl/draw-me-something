import { body } from 'express-validator';

export const channelJoinValidator = [body('pseudo').isString().exists()];
export const channelCreationValidator = [
  body('channelName').isString().exists(),
  body('pseudo').isString().exists(),
  body('subscribedUsers').isArray().isString().optional(),
];
