import { body } from 'express-validator';

export const userPatchValidator = [
  body('newPseudo').optional().isString(),
  body('pseudo').exists().isString(),
  body('firstName').optional().isString(),
  body('lastName').optional().isString(),
  body('avatar').optional().isString(),
  body('description').optional().isString(),
];

export const userSignInValidator = [body('pseudo').exists().isString(), body('password').exists().isString()];

export const userSignUpValidator = [
  ...userSignInValidator,
  body('firstName').optional().isString(),
  body('lastName').optional().isString(),
];
