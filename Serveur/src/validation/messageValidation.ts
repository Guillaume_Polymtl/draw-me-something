import { body } from 'express-validator';

export const chatMessageValidator = [body('pseudo').isString().exists(), body('message').isString().exists()];
