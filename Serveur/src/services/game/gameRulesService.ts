import * as SocketIO from 'socket.io';
import { GameDifficulty } from '../../config/customTypes';
import { GroupGame, GroupGameSummary } from '../../models/game/groupGame.model';
import RoundModel from '../../models/game/round.model';
import Team from '../../models/game/team.model';
import { socketEV } from '../sockets/socketStr';
import GameService from './gameService';
import VPlayerService from './virtual-players/vplayerService';

export default class GameRulesService {
  static readonly baseGuessTimeMS = 60000;

  static gameChronometerEmit(gameSocket: SocketIO.Socket, gameUID: string, baseTimeToGuess: number) {
    return new Promise<void>((resolve, _) => {
      let timeToGuess = baseTimeToGuess;
      const gameClockInterval = setInterval(() => {
        timeToGuess = timeToGuess - 1;
        gameSocket.in(gameUID).emit(socketEV.GAME_CLOCK, timeToGuess * 1000);
        if (timeToGuess <= 0) {
          clearInterval(gameClockInterval);
          resolve();
        }
      }, 1000);
    });
  }

  static getEndOfGameTeam(teamsMap: Map<string, Team>, getWinning: boolean): Team {
    const teams = GameService.getTeamsAsArray(teamsMap);
    let endOfGameTeam = teams[0];
    if (getWinning) {
      if (teams[1].points >= teams[0].points) {
        endOfGameTeam = teams[1];
      }
    } else {
      if (teams[1].points <= teams[0].points) {
        endOfGameTeam = teams[1];
      }
    }

    return endOfGameTeam;
  }

  // Returns the next round state for classic mode (2v2)
  static getPlayingTeamFromRoundNum(roundCount: number, teamsMap: Map<string, Team>, isRightOfReply: boolean): Team {
    const teamKeysIterator = teamsMap.keys();
    const teamKeys = [teamKeysIterator.next().value, teamKeysIterator.next().value];
    let teamId = 0;
    // Game starts at round 1
    if ((roundCount % 2 === 0 && roundCount % 4 === 0) || roundCount - (3 % 4) === 0) {
      teamId = 1;
    }

    if (isRightOfReply && teamId === 1) {
      teamId = 0;
    } else if (isRightOfReply && teamId === 0) {
      teamId = 1;
    }

    return teamsMap.get(teamKeys[teamId]);
  }

  // Returns the next round state for classic mode (2v2)
  static getNextRoundState(roundCount: number, teamsMap: Map<string, Team>, baseTimeSec: number = 60): RoundModel {
    const guesserArtistTeam: Team = this.getPlayingTeamFromRoundNum(roundCount, teamsMap, false);

    let guesserArtistPair: { artist: string; guessers: string[] };

    const vplayer = VPlayerService.getVPlayerMember(guesserArtistTeam);
    if (vplayer) {
      guesserArtistPair = {
        artist: vplayer.pseudo,
        guessers: VPlayerService.getNonVirtualPlayers(guesserArtistTeam),
      };
    } else {
      const artistId = roundCount % 2;
      const guesserId = (roundCount + 1) % 2;
      guesserArtistPair = {
        artist: guesserArtistTeam.teamMembers[artistId]?.pseudo ?? '',
        guessers: [guesserArtistTeam.teamMembers[guesserId]?.pseudo] ?? [''],
      };
    }

    return {
      ...guesserArtistPair,
      roundNum: roundCount,
      timeToGuess: baseTimeSec,
      wordToGuess: '',
      vplayerDrawing: !!vplayer,
    };
  }

  static async endGame(game: GroupGame, teams: Map<string, Team>, playingSocket: SocketIO.Namespace): Promise<GroupGame> {
    const gameObject = game;
    gameObject.winner = this.getEndOfGameTeam(teams, true);
    gameObject.looser = this.getEndOfGameTeam(teams, false);
    const groupGameSumm: GroupGameSummary = {
      teams: GameService.getTeamsAsArray(teams),
      winner: gameObject.winner,
      elapsedTime: Date.now() - gameObject.startTime,
    };

    await GameService.saveEndedGroupGame(gameObject, gameObject.winner, gameObject.looser, groupGameSumm.elapsedTime)
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        // Emit to everyone that the game ended
        playingSocket.in(gameObject.gameUID).emit(socketEV.END_GAME, groupGameSumm);
        playingSocket._rooms.delete(gameObject.gameUID);
      });
    return gameObject;
  }

  /**
   * @returns -1 if game difficulty is incompatible, 0 if far from ans, 1 if close to ans, 2 if correct ans
   */
  static wordProximity(guessStr: string, answerStr: string, difficulty: GameDifficulty): number {
    const LONG_TRESHOLD = 8;
    if (difficulty !== 'Normale') {
      return -1;
    }

    const guessLen = guessStr.length;
    const lowerCaseGuess = guessStr.toLowerCase();

    const ansLen = answerStr.length;
    const lowerCaseAns = answerStr.toLowerCase();

    if (lowerCaseGuess === lowerCaseAns) {
      return 2;
    }

    let matchingChars = 0;

    for (let i = 0; i < ansLen && i < guessLen; i++) {
      if (lowerCaseAns[i] === lowerCaseGuess[i]) {
        matchingChars++;
      }
    }

    const deltaMatch = ansLen - matchingChars;
    const deltaLen = Math.abs(guessLen - ansLen);

    let promimityVal = 0;

    if (ansLen < LONG_TRESHOLD) {
      if (deltaMatch <= 1 && deltaLen <= 1) {
        promimityVal = 1;
      }
    } else {
      if (deltaMatch <= 2 && deltaLen <= 2) {
        promimityVal = 1;
      }
    }
    return promimityVal;
  }
}
