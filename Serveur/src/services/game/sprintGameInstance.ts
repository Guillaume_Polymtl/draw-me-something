import * as SocketIO from 'socket.io';
import { v4 as uuidv4 } from 'uuid';
import { GameModel } from '../../models/game/game.model';
import { GroupGameSummary, SoloGameSummary } from '../../models/game/groupGame.model';
import RoundModel from '../../models/game/round.model';
import SoloStats from '../../models/game/stats/soloStats.model';
import TeamMember from '../../models/game/teamMember.model';
import WordHints from '../../models/game/wordHints.model';
import { UserModel } from '../../models/user.model';
import AutoDrawingService from '../drawing/autoDrawingService';
import { SocketGameService } from '../sockets/socketGameService';
import { socketEV } from '../sockets/socketStr';
import { GameDifficulty, GameMode } from './../../config/customTypes';
import AbstractGame from './AbstractGame';
import GameService from './gameService';
import RandomWordService from './randomWordService';

const NORMAL_TIME_BONUS_S = 10;
const NORMAL_TRY_NUM = 3;
const HARD_TIME_BONUS_S = 5;
const HARD_TRY_NUM = 2;
const BASE_TIME_S = 120;
export default class SprintGameInstance extends AbstractGame {
  private timeToGuessSec = BASE_TIME_S;
  private bonusTimeSec = 0;
  private tryNum = 0;
  private score = 0;
  constructor(gameMode: GameMode, difficulty: GameDifficulty, name: string) {
    super();
    const gameInit: GameModel = {
      canBeJoined: true,
      gameUID: uuidv4(),
      difficulty,
      gameMode,

      name,
      players: [],
      roundNum: 0,
    };
    this.tryNum = difficulty === 'Normale' ? NORMAL_TRY_NUM : HARD_TRY_NUM;
    this.gameObject = gameInit;
    this.bonusTimeSec = difficulty === 'Normale' ? NORMAL_TIME_BONUS_S : HARD_TIME_BONUS_S;
  }
  /*
      Sprint solo : Dans un laps de temps prédéterminé, un joueur humain tente de deviner un
      maximum de mots ou expressions à partir de dessins réalisés par un joueur virtuel.
      En plus de pouvoir jouer en mode classique , l’utilisateur a l’option de jouer en mode

      Sprint solo, dans lequel il doit deviner un nombre maximum de mots ou expressions en
      un laps de temps limité.

      Le joueur a un nombre maximum d’essais pour deviner le mot
      ou l’expression représentés en fonction de la difficulté du dessin, défini à la création du
      jeu (section A.5).

      À tout moment dans la partie, le joueur doit voir à l’écran le temps
      restant, son nombre d’essais restants, et son score actuel.
      Pour chaque bonne réponse, il gagne un point et un temps bonus est ajouté à son
      compte à rebours en fonction de la difficulté choisie. Si le joueur ne réussit pas à deviner
      dans la limite d’essais lui étant allouée, il ne gagne aucun point et passe à l’image
      suivante sans bonus de temps.
  */

  joinGame(joinedPseudo: string, userSocket: SocketIO.Socket): Promise<GameModel> {
    // TODO
    console.log('JOIN GAME SPRINT');
    return new Promise<GameModel>((resolve, reject) => {
      if (this.gameObject.players.length >= 1) {
        reject();
      } else {
        this.gameObject.players.push(joinedPseudo);
        this.pseudoSocketMap.set(joinedPseudo, userSocket);
        console.log(joinedPseudo);
        userSocket.emit(socketEV.CAN_START, true);
        this.gameObject.canBeJoined = false;
        resolve(this.gameObject);
      }
    });
  }

  leaveGame(leavingPseudo: string): GameModel {
    this.pseudoSocketMap.delete(leavingPseudo);
    this.timeToGuessSec = 0;
    return this.gameObject;
  }

  joinTeam(_: TeamMember): GameModel {
    // implem not needed since the mode doesnt have team mates
    return this.gameObject;
  }

  async executeRound(playingSocket: SocketIO.Namespace, currentRound: RoundModel, wordWithHints: WordHints): Promise<void> {
    const { gameUID } = this.gameObject;
    playingSocket.in(gameUID).emit(socketEV.NEXT_ROUND, currentRound);
    console.log('=============== round (sprint solo) ==================');
    console.log(currentRound);

    // Drawing asynchronously
    AutoDrawingService.autoDrawAsync(this.gameObject.gameUID, playingSocket, wordWithHints.strokes);

    let goodGuess = false;
    try {
      const { isGoodGuess, timeLeftSec } = await SocketGameService.playerGuessing(
        gameUID,
        wordWithHints,
        this.timeToGuessSec,
        false,
        currentRound.guessers,
        this.tryNum,
      );
      goodGuess = isGoodGuess;
      this.timeToGuessSec = timeLeftSec;
    } catch (error) {
      console.error(error);
      goodGuess = false;
    }

    if (goodGuess) {
      const timeSum = this.timeToGuessSec + this.bonusTimeSec;
      const newTime = timeSum >= BASE_TIME_S ? BASE_TIME_S : timeSum;
      this.timeToGuessSec = newTime;
      this.score += 1;
    }
  }

  async runGame(playingSocket: SocketIO.Namespace): Promise<string> {
    this.gameObject.startTime = Date.now();
    while (this.timeToGuessSec > 0) {
      const { word, hints, strokes } = await RandomWordService.getRandomPairWordDrawing(this.gameObject.difficulty);
      this.gameObject.roundNum += 1;
      const nextRound: RoundModel = {
        artist: '',
        guessers: [this.gameObject.players[0]],
        roundNum: this.gameObject.roundNum,
        timeToGuess: this.timeToGuessSec,
        wordToGuess: word,
        vplayerDrawing: true,
        soloSprintScore: this.score,
      };

      await this.executeRound(playingSocket, nextRound, { word, hints, strokes });
    }
    const playTime = Date.now() - this.gameObject.startTime;
    const bestStreak = await GameService.getSoloSprintGameHistory(this.gameObject.players[0], true);
    const isPersonalRecord = bestStreak[0] ? this.gameObject.roundNum > bestStreak[0].roundNum : false;

    // Best overall
    const { soloStats } = await GameService.getRankingFromMode('soloSprint')[0] as UserModel ?? { soloStats: 0 };
    const bestOverallStats = soloStats as SoloStats;
    const isGlobalRecord = this.gameObject.roundNum > bestOverallStats.bestScore;

    const soloGameSumm: SoloGameSummary = {
      elapsedTime: playTime,
      score: this.score,
      isPersonalRecord,
      isGlobalRecord,
    };

    playingSocket.in(this.gameObject.gameUID).emit(socketEV.END_GAME, soloGameSumm);
    await GameService.saveEndedSoloSprint(this.gameObject, playTime, this.score);
    return this.gameObject.gameUID;
  }
}
