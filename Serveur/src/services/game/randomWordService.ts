import { GameDifficulty } from '../../config/customTypes';
import { ImageNotFoundError } from '../../errors/drawingErrors';
import { Drawing } from '../../models/drawing/drawing.model';
import { db } from '../firebaseInit';
import { difficultWords, normalWords } from './wordList';
export default class RandomWordService {

  static getRandomWordFromList(difficulty: GameDifficulty): { word: string, hints: string[] } {
    const wordList = difficulty === 'Normale' ? normalWords : difficultWords;
    const randomId = Math.floor(Math.random() * wordList.length);
    return wordList[randomId];
  }

  static async getRandomPairWordDrawing(difficulty: GameDifficulty): Promise<Drawing> {
    console.log(difficulty);
    const pairsFromDifficulty = await db.collection('drawings').where('difficulty', '==', difficulty.toString()).get();
    if (pairsFromDifficulty.empty) {
      throw new ImageNotFoundError();
    }
    const pairDocs = pairsFromDifficulty.docs;

    const randomPairDoc = pairDocs[Math.floor(Math.random() * pairDocs.length)];

    const pairWordDrawing = (randomPairDoc.data() as Drawing) ?? null;
    return pairWordDrawing;
  }

  static async getAnyRandomWord(difficulty: GameDifficulty): Promise<{ word: string, hints: string[] }> {
    const randId = Math.floor(Math.random() * 2);
    let randWordDrawing: { word: string, hints: string[] } | Drawing;
    if (randId === 0) {
      randWordDrawing = this.getRandomWordFromList(difficulty);
    } else {
      randWordDrawing = await this.getRandomPairWordDrawing(difficulty);
    }

    return { word: randWordDrawing.word, hints: randWordDrawing.hints };
  }

}
