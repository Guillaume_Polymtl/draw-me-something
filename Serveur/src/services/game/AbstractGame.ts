import SocketIO from 'socket.io';
import { GameModel } from '../../models/game/game.model';
import RoundModel from '../../models/game/round.model';
import Team from '../../models/game/team.model';
import TeamMember from '../../models/game/teamMember.model';
import WordHints from '../../models/game/wordHints.model';
import VirtualPlayerAlice from './virtual-players/virtualPlayerAlice';
import VirtualPlayerBob from './virtual-players/virtualPlayerBob';

abstract class AbstractGame {
  protected readonly baseTeam: Team = { points: 0, teamMembers: [], teamUID: '' };
  pseudoSocketMap = new Map<string, SocketIO.Socket>();
  gameObject: GameModel;
  humanVPlayerMap: Map<string, VirtualPlayerBob | VirtualPlayerAlice>;
  roundsCounter = 0;

  protected abstract executeRound(playingSocket: SocketIO.Namespace, round: RoundModel, wordWithHints: WordHints): void;
  abstract runGame(playingSocket: SocketIO.Namespace): Promise<string>;
  abstract leaveGame(pseudo: string): GameModel;
  abstract joinGame(joinedPseudo: string, userSocket: SocketIO.Socket): Promise<GameModel>;
  abstract joinTeam(newMember: TeamMember): GameModel;
}

export default AbstractGame;
