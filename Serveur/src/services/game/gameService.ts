import { DocRef, GameDifficulty, GameMode, GameStatsType } from '../../config/customTypes';
import { GameModeNotFoundError } from '../../errors/gameErrors';
import DatabaseGameFormat from '../../models/game/databaseGameFormat.model';
import { GameModel } from '../../models/game/game.model';
import { GroupGame } from '../../models/game/groupGame.model';
import GroupGameHistoryElem from '../../models/game/groupGameHistoryElem.model';
import ClassicStats from '../../models/game/stats/classicStats.model';
import GameStats from '../../models/game/stats/gameStats.model';
import SoloStats from '../../models/game/stats/soloStats.model';
import Team from '../../models/game/team.model';
import TeamMember from '../../models/game/teamMember.model';
import { publicInfo, UserModel } from '../../models/user.model';

import { db } from '../firebaseInit';
import GetterService from '../getterService';
import VPlayerService from './virtual-players/vplayerService';

export default class GameService {
  static async getTeamMemberRef(member: TeamMember): Promise<UserModel | DocRef | undefined> {
    return new Promise<UserModel | DocRef | undefined>(async (resolve, reject) => {
      try {
        resolve(await GetterService.getUserDoc(member.pseudo, true));
      } catch (error) {
        reject();
      }
    });
  }

  static getVirtualPlayers(team: Team): string[] {
    const virtualPlayersPseudos: string[] = [];
    team.teamMembers.forEach((member) => {
      if (member.isVirtual) {
        virtualPlayersPseudos.push(member.pseudo);
      }
    });
    return virtualPlayersPseudos;
  }

  static async saveEndedGroupGame(endedGame: GroupGame, winningTeam: Team, losingTeam: Team, elapsedTime: number): Promise<void> {
    const playersRef: DocRef[] = [];
    endedGame.teams.forEach((team: Team) => {
      team.teamMembers.forEach(async (member: TeamMember) => {
        const wonGame = winningTeam === team;
        playersRef.push(member.docRef);
        if (!member.isVirtual) {
          await this.addStatsToUser(member.docRef, team.points, elapsedTime, 'classicStats', wonGame);
        }
      });
    });
    // Format endedGroupGame
    const winningMembersRefs: DocRef[] = winningTeam.teamMembers.map((member) => {
      return member.docRef;
    });

    const losingMembersRefs: DocRef[] = losingTeam.teamMembers.map((member) => {
      return member.docRef;
    });

    // This allows a game to be saved in a more compact format in the db,
    // thus making fields easier to query
    const formattedGame: DatabaseGameFormat = {
      playersRef, // For query purposes
      startTime: endedGame.startTime,
      formattedWinningTeam: { ...winningTeam, teamMembers: winningMembersRefs },
      formattedLosingTeam: { ...losingTeam, teamMembers: losingMembersRefs },
      roundNum: endedGame.roundNum,
      name: endedGame.name,
      difficulty: endedGame.difficulty,
      gameMode: endedGame.gameMode,
      elapsedTime,
    };
    await db.collection('classicGames').doc(endedGame.gameUID).set(formattedGame);
  }

  static async addStatsToUser(playerRef: DocRef, score: number, playTime: number, statsMode: GameStatsType, won?: boolean): Promise<void> {
    const userData = (await playerRef.get()).data() as UserModel;

    let formerStats: GameStats;
    let updatedStats: GameStats;

    if (statsMode === 'classicStats') {
      formerStats = userData.classicStats as ClassicStats;
    } else {
      formerStats = userData.soloStats as SoloStats;
    }

    // No former stats, never played the game mode
    if (!formerStats) {
      updatedStats = { score, timePlayedMs: playTime, numberPlayed: 1 };
      formerStats = updatedStats;
    } else {
      updatedStats = {
        score: formerStats.score + score,
        timePlayedMs: formerStats.timePlayedMs + playTime,
        numberPlayed: formerStats.numberPlayed + 1,
      };
    }

    if (statsMode === 'soloStats') {
      const pseudo = await GetterService.getPseudoFromRef(playerRef);
      const formerSoloStats: SoloStats = { ...updatedStats, bestScore: formerStats.bestScore };
      const userSoloGames = await db.collection('soloSprintGames').where('pseudo', '==', pseudo).orderBy('score').get();
      const bestScore: number | undefined = userSoloGames.docs[0]?.data().score;
      if (!bestScore || score > bestScore) {
        updatedStats = { ...formerSoloStats, bestScore: score };
      }
    }

    if (statsMode === 'classicStats') {
      let winNum: number = formerStats?.wins ?? 0;
      // const formerClassicStats: ClassicStats = { ...formerStats, wins: winNum };
      if (won) {
        winNum++;
      }
      updatedStats = { ...updatedStats, wins: winNum };
    }

    playerRef.update({
      [statsMode]: updatedStats,
    });
  }

  static async getRankingFromMode(mode: string): Promise<UserModel[]> {
    let statsType = '';

    if (mode === 'classic') {
      statsType = 'classicStats';
    } else if (mode === 'soloSprint') {
      statsType = 'soloStats';
    } else {
      throw new GameModeNotFoundError();
    }
    const userDocs = await db.collection('userAuth').orderBy(`${statsType}.score`, 'desc').get();

    const userRanking: UserModel[] = [];
    userDocs.docs.forEach((user) => {
      const rankedUser = user.data() as UserModel;
      const infoToFilter = publicInfo.concat(statsType);

      Object.keys(rankedUser).forEach((keyAttribute) => {
        if (!infoToFilter.includes(keyAttribute)) {
          delete rankedUser[keyAttribute];
        }
      });

      if ((statsType === 'classicStats' && rankedUser?.classicStats.score) || (statsType === 'soloStats' && rankedUser?.soloStats.score)) {
        userRanking.push(rankedUser);
      }
    });
    return userRanking;
  }

  static getTeamsAsArray(teamsMap: Map<string, Team>): Team[] {
    const teamArr: Team[] = [];
    teamsMap.forEach((team) => teamArr.push(team));
    return teamArr;
  }

  static leaveTeam(leavingPseudo: string, teamsMap: Map<string, Team>, gameObject: GroupGame): GroupGame {
    const leavingMember = this.getMemberInTeam(leavingPseudo, this.getTeamsAsArray(teamsMap));
    if (leavingMember) {
      const teamLeft = teamsMap.get(leavingMember.teamUID);

      // Removing player
      const leavingMemberId = teamLeft.teamMembers.indexOf(leavingMember);
      teamsMap.get(leavingMember.teamUID).teamMembers.splice(leavingMemberId, 1);

      // Removing its virtual player
      if (VPlayerService.getVPlayerMember(teamLeft)) {
        teamsMap.get(leavingMember.teamUID).teamMembers = [];
      }

      // Updating
      gameObject.teams = [];
      teamsMap.forEach((team) => {
        gameObject.teams.push(team);
      });
      gameObject.canBeJoined = true;
    }
    return gameObject;
  }

  static getMemberInTeam(pseudoToCheck: string, teams: Team[], getVirtual?: boolean): TeamMember {
    let memberInTeam: TeamMember;

    teams.forEach((team) => {
      team.teamMembers.forEach((member) => {
        if (member.pseudo === pseudoToCheck && (!member.isVirtual || getVirtual)) {
          memberInTeam = member;
        }
      });
    });
    return memberInTeam;
  }

  static async getClassicGameHistory(searchPseudo: string, onlyWins?: boolean, withVPlayer?: string): Promise<GroupGameHistoryElem[]> {
    const pseudoRef = (await GetterService.getUserDoc(searchPseudo, true)) as DocRef;

    // Uses query index, see Firestore/indexes
    let gameHistRef: FirebaseFirestore.Query<FirebaseFirestore.DocumentData>;
    gameHistRef = db.collection('classicGames').where('playersRef', 'array-contains', pseudoRef).orderBy('startTime', 'desc');

    const gameHistSnapshot = await gameHistRef.get();

    const gameHistoryPromises: Promise<GroupGameHistoryElem>[] = gameHistSnapshot.docs.map(async (game) => {
      const gameElement = game.data() as DatabaseGameFormat;
      console.log(gameElement.formattedWinningTeam);
      // Extracting winners
      const winnersPromises = gameElement.formattedWinningTeam.teamMembers.map(async (memberRef) => {
        try {
          const { pseudo, avatar, isVirtual } = await GetterService.getUserDocFromRef(memberRef);
          return { pseudo, avatar, isVirtual };
        } catch (error) {
          return null;
        }
      });

      // Extracting losers
      const losersPromises = gameElement.formattedLosingTeam.teamMembers.map(async (memberRef) => {
        try {
          const { pseudo, avatar, isVirtual } = await GetterService.getUserDocFromRef(memberRef);
          return { pseudo, avatar, isVirtual };
        } catch (error) {
          return null;
        }
      });

      const winners = (await Promise.all(winnersPromises)).filter(Boolean);
      const losers = (await Promise.all(losersPromises)).filter(Boolean);
      // Constructing a game object displayable for the history
      const gameBase: GroupGameHistoryElem = {
        winningTeam: {
          players: winners,
          score: gameElement.formattedWinningTeam.points,
          name: gameElement.formattedWinningTeam.teamUID,
        },
        losingTeam: {
          players: losers,
          score: gameElement.formattedLosingTeam.points,
          name: gameElement.formattedLosingTeam.teamUID,
        },
        roundNum: gameElement.roundNum,
        startTime: gameElement.startTime,
        difficulty: gameElement.difficulty,
        gameMode: gameElement.gameMode,
        name: gameElement.name,
      };

      return gameBase;
    });

    let gameHistory = await Promise.all(gameHistoryPromises);

    // Filter to get only won games
    if (onlyWins) {
      gameHistory = gameHistory.filter((game) => {
        const winningPlayers = game.winningTeam.players;
        return winningPlayers.find((player: { pseudo: string; avatar: string }) => player.pseudo === searchPseudo);
      });
    }

    if (withVPlayer) {
      const vPlayerDoc = await db.collection('userAuth').where('pseudo', '==', withVPlayer).where('isVirtual', '==', true).get();
      const vPlayerData = vPlayerDoc.docs[0].data() as UserModel;
      gameHistory = gameHistory.filter((game) => {
        const inWinningGame = !!game.winningTeam.players.find((player) => player.isVirtual && (player.pseudo === vPlayerData.pseudo));
        const inLosingTeam = !!game.losingTeam.players.find((player) => player.isVirtual && (player.pseudo === vPlayerData.pseudo));

        return (inWinningGame || inLosingTeam);
      });
    }

    return gameHistory;
  }

  static async saveEndedSoloSprint(endedGame: GameModel, playTime: number, score: number): Promise<void> {
    const pseudo = endedGame.players[0];
    const playerRef = (await GetterService.getUserDoc(pseudo, true)) as DocRef;

    await db
      .collection('soloSprintGames')
      .doc(endedGame.gameUID)
      .set({
        difficulty: endedGame.difficulty,
        elapsedTime: playTime,
        gameMode: endedGame.gameMode,
        name: endedGame.name,
        playersRef: [playerRef],
        roundNum: score, // Equals the number of good guesses in a solo sprint game
        startTime: endedGame.startTime,
        gameUID: endedGame.gameUID,
      });
    await this.addStatsToUser(playerRef, score, playTime, 'soloStats');
  }

  static async getSoloSprintGameHistory(pseudo: string, getBestStreak?: boolean): Promise<GameModel[]> {
    const pseudoRef = (await GetterService.getUserDoc(pseudo, true)) as DocRef;
    const soloSprintDocs = await db
      .collection('soloSprintGames')
      .where('playersRef', 'array-contains', pseudoRef)
      .orderBy('startTime', 'desc')
      .get();

    let soloSprintHist = soloSprintDocs.docs.map((soloSprintDoc) => {
      const soloSprintDBElem = soloSprintDoc.data() as DatabaseGameFormat;
      const gameModel: GameModel = {
        gameMode: soloSprintDBElem.gameMode as GameMode,
        difficulty: soloSprintDBElem.difficulty as GameDifficulty,
        name: soloSprintDBElem.name,
        roundNum: soloSprintDBElem.roundNum,
        startTime: soloSprintDBElem.startTime,
        players: [pseudo], // In array for code reusability with classicGameHistory methods
        elapsedTime: soloSprintDBElem.elapsedTime,
      };
      return gameModel;
    });

    if (getBestStreak) {
      soloSprintHist = [soloSprintHist.sort((a, b) => (a.roundNum < b.roundNum ? 1 : -1))[0]];
    }
    return soloSprintHist;
  }
}
