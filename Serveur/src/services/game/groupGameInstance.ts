import SocketIO from 'socket.io';
import { v4 as uuidv4 } from 'uuid';
import { GameDifficulty, GameMode } from '../../config/customTypes';
import Stroke from '../../models/drawing/stroke.model';
import { GroupGame } from '../../models/game/groupGame.model';
import RoundModel from '../../models/game/round.model';
import Team from '../../models/game/team.model';
import TeamMember from '../../models/game/teamMember.model';
import WordHints from '../../models/game/wordHints.model';
import AutoDrawingService from '../drawing/autoDrawingService';
import GetterService from '../getterService';
import { SocketGameService } from '../sockets/socketGameService';
import { socketEV } from '../sockets/socketStr';
import AbstractGame from './AbstractGame';
import GameRulesService from './gameRulesService';
import GameService from './gameService';
import RandomWordService from './randomWordService';
import VirtualPlayerAlice from './virtual-players/virtualPlayerAlice';
import VirtualPlayerBob from './virtual-players/virtualPlayerBob';
import VPlayerService from './virtual-players/vplayerService';

const MAX_PLAYERS_PER_TEAM = 2;
const REPLY_TIME_S = 15;
const BLUE_TEAM = 'Bleue';
const PINK_TEAM = 'Rose';
export default class GroupGameInstance extends AbstractGame {

  teams = new Map<string, Team>();
  gameObject: GroupGame;

  constructor(gameMode: GameMode, difficulty: GameDifficulty, name: string, roundNum: number) {
    super();
    this.teams.set(PINK_TEAM, { teamUID: PINK_TEAM, teamMembers: [], points: 0 });
    this.teams.set(BLUE_TEAM, { teamUID: BLUE_TEAM, teamMembers: [], points: 0 });

    const gameInit: GroupGame = {
      canBeJoined: true,
      gameUID: uuidv4(),
      difficulty,
      gameMode,

      teams: [this.teams.get(PINK_TEAM), this.teams.get(BLUE_TEAM)],
      name,
      players: [],
      roundNum,
    };
    this.humanVPlayerMap = new Map<string, VirtualPlayerBob | VirtualPlayerAlice>();
    this.gameObject = gameInit;
  }

  joinGame(joinedPseudo: string, userSocket: SocketIO.Socket): Promise<GroupGame> {
    console.log('JOIN GAME GROUP');
    return new Promise<GroupGame>((resolve, reject) => {
      if (this.gameObject.players.length >= 4) {
        reject();
      } else {
        this.gameObject.players.push(joinedPseudo);
        if (!this.pseudoSocketMap.has(joinedPseudo)) {
          this.pseudoSocketMap.set(joinedPseudo, userSocket);
        }
        resolve(this.gameObject);
      }
    });
  }

  joinTeam(newMember: TeamMember): GroupGame {
    const { teamUID } = newMember;
    const playerArr: TeamMember[] = this.teams.get(teamUID).teamMembers;
    const teamMembers = this.teams.get(teamUID).teamMembers;

    // Player already in team
    let memberAlreadyInTeam = false;
    for (const member of teamMembers) {
      if (member.pseudo === newMember.pseudo) {
        memberAlreadyInTeam = true;
      }
    }

    // Player in the other team
    let memberInTheOtherTeam = false;
    let otherTeamUID = '';
    this.teams.forEach((team: Team, key: string) => {
      const members = team.teamMembers;
      for (const member of members) {
        if (member.pseudo === newMember.pseudo && key !== teamUID) {
          memberInTheOtherTeam = true;
          otherTeamUID = key;
        }
      }
    });

    if (memberAlreadyInTeam || (newMember.isVirtual && teamMembers.find((p) => p.isVirtual))) {
      console.log('Already in team!');
    } else if (memberInTheOtherTeam) {

      // wants to join the other team
      const otherTeam = this.teams.get(otherTeamUID);
      console.log(otherTeamUID);
      this.humanVPlayerMap.delete(newMember.pseudo);

      this.teams.set(otherTeamUID, {
        ...this.baseTeam,
        teamUID: otherTeamUID,
        teamMembers: otherTeam.teamMembers.filter((member: TeamMember) => member.pseudo !== newMember.pseudo && !member.isVirtual),
      });

      // Freshly joined team
      this.teams.set(teamUID, { ...this.baseTeam, teamUID, teamMembers: playerArr.concat(newMember) });
    }
    // Max 2 players per team
    else if (teamMembers.length < MAX_PLAYERS_PER_TEAM) {
      this.teams.set(teamUID, { ...this.baseTeam, teamUID, teamMembers: playerArr.concat(newMember) });
    }

    let numPlayers = 0;

    this.gameObject.teams = [];
    this.teams.forEach((team) => {
      this.gameObject.teams.push(team);
      numPlayers += team.teamMembers.length;
    });

    // Both teams are full
    if (newMember.isVirtual) {
      this.gameObject.players.push(newMember.pseudo);
    }

    if (numPlayers >= 2 * MAX_PLAYERS_PER_TEAM) {
      this.gameObject.canBeJoined = false;
    }

    return this.gameObject;
  }

  leaveGame(leavingPseudo: string): GroupGame {
    console.log('leave: ' + leavingPseudo);
    const gameObject = GameService.leaveTeam(leavingPseudo, this.teams, this.gameObject);
    this.gameObject = gameObject;
    this.gameObject.teams.forEach((team) => this.teams.set(team.teamUID, team));

    // Removing its vplayer if exists
    this.humanVPlayerMap.delete(leavingPseudo);

    const players: string[] = [];
    GetterService.getPlayersArrFromTeams(GameService.getTeamsAsArray(this.teams)).forEach((playerPseudo) => {
      if (playerPseudo !== leavingPseudo) {
        players.push(playerPseudo);
      }
    });
    this.gameObject.players = players;
    this.pseudoSocketMap.delete(leavingPseudo);

    // Game running
    if (this.roundsCounter >= 1) {
      // Provoke end of game
      this.roundsCounter = this.gameObject.roundNum + 1;
    }

    return this.gameObject;
  }

  // The round resolves once the timer is done or the player guessed right
  async executeRound(playingSocket: SocketIO.Namespace, currentRound: RoundModel, wordWithHints: WordHints): Promise<void> {
    const { gameUID } = this.gameObject;
    currentRound.wordToGuess = wordWithHints.word;
    playingSocket.in(gameUID).emit(socketEV.NEXT_ROUND, currentRound);

    console.log('===============round==================');
    console.log(currentRound);
    // Listen to drawing/guessing events and react to them
    let goodGuess = false;
    let badGuessNum = 0;
    do {
      const isRightOfReply = badGuessNum === 1;
      const baseTimeToGuess = isRightOfReply ? REPLY_TIME_S : currentRound.timeToGuess;
      try {
        const { isGoodGuess } = await SocketGameService.playerGuessing(
          gameUID,
          wordWithHints,
          baseTimeToGuess,
          isRightOfReply,
          currentRound.guessers,
        );
        goodGuess = isGoodGuess;
      } catch (error) {
        console.error(error);
        goodGuess = false;
      }

      playingSocket.in(gameUID).emit(socketEV.END_GUESS_DRAW, goodGuess);
      console.log('GOOD GUESS? : ' + goodGuess);

      if (!goodGuess) {
        badGuessNum++;
      }
      // If exists, emit message from virtual player
      this.humanVPlayerMap.get(currentRound.guessers[0])?.emitEndOfRoundMessage(goodGuess);

      if (badGuessNum === 1) {
        const replyingTeam: Team = GameRulesService.getPlayingTeamFromRoundNum(this.roundsCounter, this.teams, true);
        console.log(replyingTeam);
        const rightOfReply: RoundModel = {
          artist: '',
          guessers: VPlayerService.getNonVirtualPlayers(replyingTeam),
          timeToGuess: REPLY_TIME_S,
          roundNum: this.roundsCounter,
          wordToGuess: currentRound.wordToGuess,
        };
        playingSocket.in(gameUID).emit(socketEV.RIGHT_OF_REPLY, rightOfReply);
        console.log('===============right of reply==================');
        console.log(rightOfReply);
        currentRound = rightOfReply;
      }
    } while (!goodGuess && badGuessNum < 2);

    // Compute scores here
    // 1 bad guess: The playing team failed, but the other team won with a right of reply
    if (badGuessNum < 2) {
      const wonByReply = badGuessNum === 1;
      const goodGuessTeam = GameRulesService.getPlayingTeamFromRoundNum(this.roundsCounter, this.teams, wonByReply);
      goodGuessTeam.points += 1;
      playingSocket.in(gameUID).emit(socketEV.TEAM_POINT, { teamUID: goodGuessTeam.teamUID, points: goodGuessTeam.points });
      this.teams.set(goodGuessTeam.teamUID, goodGuessTeam);
    }
  }

  // Returns a promise that only resolves once the game ends
  async runGame(playingSocket: SocketIO.Namespace): Promise<string> {
    this.gameObject.startTime = Date.now();
    this.humanVPlayerMap = await VPlayerService.loadVPlayers(this.gameObject.gameUID, this.gameObject.name, this.teams, playingSocket);

    while (this.roundsCounter < this.gameObject.roundNum) {
      this.roundsCounter = this.roundsCounter + 1;

      // Until all rounds are finished (non-blocking)
      const nextRound = GameRulesService.getNextRoundState(this.roundsCounter, this.teams);

      let wordToGuess = '';
      let associatedHints: string[] = [];
      let strokeArr: Stroke[] = [];

      if (nextRound.vplayerDrawing) {
        const { word, hints, strokes } = await RandomWordService.getRandomPairWordDrawing(this.gameObject.difficulty);
        wordToGuess = word;
        associatedHints = hints;
        strokeArr = strokes;
      } else {
        const { word, hints } = await RandomWordService.getAnyRandomWord(this.gameObject.difficulty);
        wordToGuess = word;
        associatedHints = hints;
      }

      // 5 sec delay for animations
      const ROUND_TIMEOUT = this.roundsCounter === 1 ? 0 : 3000;
      const roundPromise = new Promise<void>((resolve, _) => {
        setTimeout(async () => {
          if (strokeArr.length > 0) {
            AutoDrawingService.autoDrawAsync(this.gameObject.gameUID, playingSocket, strokeArr);
          }
          await this.executeRound(playingSocket, nextRound, { word: wordToGuess, hints: associatedHints });
          resolve();
        }, ROUND_TIMEOUT);
      });
      await Promise.resolve(roundPromise);
    }

    // Game ended
    console.log('game ended');
    this.gameObject = await GameRulesService.endGame(this.gameObject, this.teams, playingSocket);
    return this.gameObject.gameUID;
  }
}
