import * as SocketIO from 'socket.io';
import TeamMember from '../../../models/game/teamMember.model';
import { bobEndOfRound, bobGameBeginning, bobHintResponse } from './bobSpeech';
import VirtualPlayer from './virtualPlayer';

export default class VirtualPlayerBob extends VirtualPlayer {
  constructor(gameUID: string, gameName: string, teamMate: TeamMember, playingSocket: SocketIO.Namespace) {
    super(gameUID, gameName, 'Bob', teamMate, playingSocket);
  }

  sendRandomWelcomeMessage(): void {
    super.sendRandomWelcomeMessage(bobGameBeginning);
  }

  emitEndOfRoundMessage(wasGoodGuess: boolean): void {
    super.sendEndOfRoundMessage(bobEndOfRound, wasGoodGuess);
  }

  emitHintResponse(hintNum: number): void {
    super.sendHintResponse(bobHintResponse, hintNum);
  }
}
