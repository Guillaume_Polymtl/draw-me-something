export const speechFillIdentifiers = {
  partnerIdentifier: 'Co',
  ptNumIdentifier: 'ptNum',
  minutesPlayedIdentifier: 'timePlayed',
  numGamesIdentifier: 'numGames',
  victoryPercent: 'victoryPercent',
  projectedVicPercent: 'projectedVicPercent',
  projectedVicNum: 'projectedVicNum',
  victoryNum: 'victoryNum',

  victoryDateIdentifier: 'victoryDate',
  lastGameDateIdentifier: 'lastGameDate',
};
