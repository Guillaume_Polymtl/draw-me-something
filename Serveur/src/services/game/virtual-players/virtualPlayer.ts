import SocketIO from 'socket.io';
import Stroke from '../../../models/drawing/stroke.model';
import GroupGameHistoryElem from '../../../models/game/groupGameHistoryElem.model';
import GameStats from '../../../models/game/stats/gameStats.model';
import TeamMember from '../../../models/game/teamMember.model';
import AutoDrawingService from '../../drawing/autoDrawingService';
import { SocketChannelService } from '../../sockets/socketChannelService';
import { socketEV } from '../../sockets/socketStr';
import UserService from '../../users/userService';
import GameService from '../gameService';
import SpeechService from './speechService';

/*
  Les joueurs virtuels poussent la conversation plus loin. Ils peuvent référencer des événements
  passés (parties jouées, joueurs rencontrés) ou utiliser les statistiques d’un joueur pour formuler
  des commentaires personnalisés.
  Les joueurs virtuels doivent acquérir une mémoire des anciennes parties jouées. Comme
  au niveau de difficulté précédent, les joueurs virtuels ont une personnalité distincte. Ils
  doivent utiliser leur nouvelle intelligence pour envoyer des messages personnalisés en
  fonction des statistiques des joueurs, leur historique de parties ensemble et leur personnalité
 */
export default class VirtualPlayer {
  private pseudo: string;
  private isVirtual = true;
  private playingSocket: SocketIO.Namespace;
  private teamUID: string;
  private gameUID: string;
  private gameName: string;
  private teamMate: TeamMember;
  private teamMateStats: GameStats;
  private teamMateWinHist: GroupGameHistoryElem;
  private teamMateVPlayerHist: GroupGameHistoryElem;
  private readonly baseHist: GroupGameHistoryElem = {
    difficulty: 'Normale',
    gameMode: 'Classique',
    name: '',
    losingTeam: { players: [], name: '', score: 0 },
    winningTeam: { players: [], name: '', score: 0 },
    roundNum: 0,
    startTime: 0,
  };
  constructor(gameUID: string, gameName: string, pseudo: string, teamMate: TeamMember, playingSocket: SocketIO.Namespace) {
    this.pseudo = pseudo;
    this.playingSocket = playingSocket;
    this.teamUID = teamMate.teamUID;
    this.gameUID = gameUID;
    this.gameName = gameName;
    this.teamMate = teamMate;
    this.teamMateStats = { numberPlayed: 0, score: 0, timePlayedMs: 0, wins: 0 };
    this.teamMateWinHist = this.baseHist;
    this.teamMateVPlayerHist = this.baseHist;
  }

  async initTeamMateData(): Promise<void> {
    try {
      this.teamMateStats = await UserService.getUserGameStatsFromMode(this.teamMate.pseudo, 'classicStats');
      const winHist = await GameService.getClassicGameHistory(this.teamMate.pseudo, true);

      if (winHist && winHist.length > 0) this.teamMateWinHist = winHist[0];

      const vplayerHist = await GameService.getClassicGameHistory(this.teamMate.pseudo, false, this.pseudo);
      if (vplayerHist && vplayerHist.length > 0) this.teamMateVPlayerHist = vplayerHist[0];
    } catch (error) {
      console.error(error);
    }
  }

  getVirtualTeamMember(): TeamMember {
    return {
      pseudo: this.pseudo,
      teamUID: this.teamUID,
      isVirtual: this.isVirtual,
    };
  }

  sendEndOfRoundMessage(endOfRoundDict: { wasGoodGuess: string[]; wasBadGuess: string[] }, wasGoodGuess: boolean): void {
    const responseChoices = wasGoodGuess ? endOfRoundDict.wasGoodGuess : endOfRoundDict.wasBadGuess;
    const endOfRoundMessage = SpeechService.getProcessedMessage(
      responseChoices,
      this.teamMate.pseudo,
      this.teamMateStats,
      this.teamMateWinHist,
      this.teamMateVPlayerHist,
    );
    console.log('End of round message from ' + this.pseudo + ': ' + endOfRoundMessage);
    const vplayerMessage = { message: endOfRoundMessage, channelName: this.gameName, pseudo: this.pseudo, date: Date.now() };
    this.playingSocket.in(this.gameUID).emit(socketEV.ROOM_MESSAGE, vplayerMessage);
  }

  // Is that the name of the famous actor?
  sendRandomWelcomeMessage(gameBeginningDict: {
    firstTimePlaying: string[];
    goodScore: string[];
    badScore: string[];
    alreadyWon: string[];
    playedTogether: string[];
  }): void {
    const DEFAULT_GOOD_SCORE = 10;
    let welcomeMessage = '';

    if (!this.teamMateStats.numberPlayed) {
      welcomeMessage = SpeechService.getProcessedMessage(
        gameBeginningDict.firstTimePlaying,
        this.teamMate.pseudo,
        this.teamMateStats,
        this.teamMateWinHist,
        this.teamMateVPlayerHist,
      );
    } else if (this.teamMateStats.score >= DEFAULT_GOOD_SCORE || this.teamMateVPlayerHist.startTime || this.teamMateWinHist.startTime) {
      let dict: string[];
      const randBase = Math.floor(Math.random() * 2);

      if (this.teamMateWinHist.startTime && randBase === 0) {
        dict = gameBeginningDict.alreadyWon;
      } else if (this.teamMateVPlayerHist.startTime) {
        dict = gameBeginningDict.playedTogether;
      } else {
        dict = gameBeginningDict.goodScore;
      }

      welcomeMessage = SpeechService.getProcessedMessage(
        dict,
        this.teamMate.pseudo,
        this.teamMateStats,
        this.teamMateWinHist,
        this.teamMateVPlayerHist,
      );
    } else {
      welcomeMessage = SpeechService.getProcessedMessage(
        gameBeginningDict.badScore,
        this.teamMate.pseudo,
        this.teamMateStats,
        this.teamMateWinHist,
        this.teamMateVPlayerHist,
      );
    }
    console.log('Welcome message from ' + this.pseudo + ': ' + welcomeMessage);
    const vplayerMessage = { channelName: this.gameName, pseudo: this.pseudo, message: welcomeMessage, date: Date.now() };
    this.playingSocket.in(this.gameUID).emit(socketEV.ROOM_MESSAGE, vplayerMessage);
  }

  sendHintResponse(hintDict: { firstHint: string[]; extraHint: string[] }, hintNum: number): void {
    let hintResponse = '';
    if (hintNum === 1) {
      hintResponse = SpeechService.getProcessedMessage(
        hintDict.firstHint,
        this.teamMate.pseudo,
        this.teamMateStats,
        this.teamMateWinHist,
        this.teamMateVPlayerHist,
      );
    } else {
      hintResponse = SpeechService.getProcessedMessage(
        hintDict.extraHint,
        this.teamMate.pseudo,
        this.teamMateStats,
        this.teamMateWinHist,
        this.teamMateVPlayerHist,
      );
    }
    console.log('Hint response from ' + this.pseudo + ': ' + hintResponse);
    const vplayerMessage = { channelName: this.gameName, pseudo: this.pseudo, message: hintResponse, date: Date.now() };
    this.playingSocket.in(this.gameUID).emit(socketEV.ROOM_MESSAGE, vplayerMessage);
  }
}
