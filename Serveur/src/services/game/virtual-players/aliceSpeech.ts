import { speechFillIdentifiers } from './virtualPlayerSpeech';

export const aliceGameBeginning = {
  firstTimePlaying: [
    `Oh, quelqu'un de nouveau🤩! Bien heureuse d'être en équipe avec toi, {${speechFillIdentifiers.partnerIdentifier}}!`,
    `Bonjour {${speechFillIdentifiers.partnerIdentifier}}🤙! Nous n'avons jamais joué ensemble. Heureuse de faire ta connaissance!`,
  ],
  badScore: [
    `Hey {${speechFillIdentifiers.partnerIdentifier}} je vois que tu as seulement {${speechFillIdentifiers.victoryPercent}} % de victoire dans ce mode, mais ne t’en fais pas, ça va bien aller cette fois-ci, fais de ton mieux :)`,
    `Salut {${speechFillIdentifiers.partnerIdentifier}}😄! Je vois que tu n'as que {${speechFillIdentifiers.victoryNum}} victoires. Tentons de t'en ajouter une supplémentaire!`,
    `Regardez! Je suis en équipe avec {${speechFillIdentifiers.partnerIdentifier}} qui a joué {${speechFillIdentifiers.minutesPlayedIdentifier}} minutes à ce mode!`,
  ],
  goodScore: [
    `WoW! {${speechFillIdentifiers.partnerIdentifier}} je vois que tu as {${speechFillIdentifiers.victoryNum}} victoires dans ce mode de jeu😄! Continue comme ça!`,
    `Youpi!🤩 Je suis avec {${speechFillIdentifiers.partnerIdentifier}}, qui cumule un total de {${speechFillIdentifiers.ptNumIdentifier}} points dans ce mode😄!`,
    `Mais voici {${speechFillIdentifiers.partnerIdentifier}}, ayant à son actif {${speechFillIdentifiers.victoryNum}} victoires! Mais quelle légende!`,
  ],
  alreadyWon: [`Salut! Tiens donc, ta dernière victoire remonte au {${speechFillIdentifiers.victoryDateIdentifier}}!🥳`],
  playedTogether: [
    `Hé, on se connaît, {${speechFillIdentifiers.partnerIdentifier}} ! Nous avons joué ensemble le {${speechFillIdentifiers.lastGameDateIdentifier}}:)`,
  ],
};

export const aliceEndOfRound = {
  wasGoodGuess: [
    `Bravo🤩! Je vois que tu joues vraiment bien! J’imagine que ce sont tes {${speechFillIdentifiers.numGamesIdentifier}} parties d’expérience qui t'ont rendues aussi pro!`,
    `Tu as deviné tellement vite😲! J’imagine mal comment as-tu pu cumuler {${speechFillIdentifiers.minutesPlayedIdentifier}} minutes de jeux alors que tu devines si vite!`,
  ],
  wasBadGuess: [
    `Tu y étais presque {${speechFillIdentifiers.partnerIdentifier}}! Ne t’inquiètes pas, c’est seulement 1 partie parmi les {${speechFillIdentifiers.numGamesIdentifier}} que tu as déjà joué! Même si on perd, on gagne en expérience :)`,
    `Vas-y, {${speechFillIdentifiers.partnerIdentifier}}, t’es capable🙌! Ne te décourages pas :)`,
  ],
};

export const aliceHintResponse = {
  firstHint: [`Bonne idée, l'indice💡! Je dois avouer que ce n'est pas évident!`, `Super, l'indice vas certainement aider:)`],
  extraHint: [`J'espère que cet autre indice te sera utile!💡`, `Génial, l'indice devra te mettre sur la bonne voie`],
};
