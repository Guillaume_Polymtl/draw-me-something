import SocketIO from 'socket.io';
import { GameMode } from '../../../config/customTypes';
import Team from '../../../models/game/team.model';
import TeamMember from '../../../models/game/teamMember.model';
import { socketEV } from '../../sockets/socketStr';
import GroupGameInstance from '../groupGameInstance';
import SprintGameInstance from '../sprintGameInstance';
import VirtualPlayerAlice from './virtualPlayerAlice';
import VirtualPlayerBob from './virtualPlayerBob';

export default class VPlayerService {
  static getNonVirtualPlayers(team: Team): string[] {
    const nonVirtualPseudos: string[] = [];
    team.teamMembers.forEach((member) => {
      if (!member.isVirtual) {
        nonVirtualPseudos.push(member.pseudo);
      }
    });
    return nonVirtualPseudos;
  }

  static getNonVirtualMembers(team: Team): TeamMember[] {
    const nonVirtualMembers: TeamMember[] = [];
    team.teamMembers.forEach((member) => {
      if (!member.isVirtual) {
        nonVirtualMembers.push(member);
      }
    });
    return nonVirtualMembers;
  }

  static getVPlayerMember(team: Team): TeamMember | undefined {
    let vTeamMember: TeamMember;
    team.teamMembers.forEach((member) => {
      if (member.isVirtual) {
        vTeamMember = member;
      }
    });
    return vTeamMember;
  }

  static generateVPlayerInstance(
    gameUID: string,
    gameName: string,
    vplayerPseudo: string,
    teamMate: TeamMember,
    playSoc: SocketIO.Namespace,
  ): VirtualPlayerBob | VirtualPlayerAlice {
    let vplayerInstance: VirtualPlayerBob | VirtualPlayerAlice;

    if (vplayerPseudo === 'Bob') {
      vplayerInstance = new VirtualPlayerBob(gameUID, gameName, teamMate, playSoc);
    } else if (vplayerPseudo === 'Alice') {
      vplayerInstance = new VirtualPlayerAlice(gameUID, gameName, teamMate, playSoc);
    }
    return vplayerInstance;
  }

  static async loadVPlayers(
    gameUID: string,
    gameName: string,
    teamsMap: Map<string, Team>,
    playSoc: SocketIO.Namespace,
  ): Promise<Map<string, VirtualPlayerBob | VirtualPlayerAlice>> {
    const humanVPlayerMap = new Map<string, VirtualPlayerBob | VirtualPlayerAlice>();

    for (const [_, team] of teamsMap) {
      const vplayer = this.getVPlayerMember(team);
      if (vplayer) {
        const teamMate = this.getNonVirtualMembers(team)[0];
        const vPlayerInstance = this.generateVPlayerInstance(gameUID, gameName, vplayer.pseudo, teamMate, playSoc);
        await vPlayerInstance.initTeamMateData();
        humanVPlayerMap.set(teamMate.pseudo, vPlayerInstance);
        vPlayerInstance.sendRandomWelcomeMessage();
      }
    }

    return humanVPlayerMap;
  }

  static vplayerHintResponse(concurrentGames: Map<string, GroupGameInstance | SprintGameInstance>, gamingSocket: SocketIO.Socket): void {
    gamingSocket.on(socketEV.HINT_RESPONSE, ({ gameUID, hintNum, pseudo }) => {

      const concurrentGame = concurrentGames.get(gameUID);

      const gameMode: GameMode | undefined = concurrentGame?.gameObject?.gameMode;

      if (gameMode && gameMode === 'Classique') {
        concurrentGame.humanVPlayerMap?.get(pseudo)?.emitHintResponse(hintNum);
      }
    });
  }
}
