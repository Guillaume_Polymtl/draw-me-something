import GroupGameHistoryElem from '../../../models/game/groupGameHistoryElem.model';
import GameStats from '../../../models/game/stats/gameStats.model';
import DateService from '../../dateService';
import { speechFillIdentifiers } from './virtualPlayerSpeech';

export default class SpeechService {
  static decomposeAndFill(
    randomString: string,
    teamMatePseudo: string,
    memberStats: GameStats,
    memberWinHist?: GroupGameHistoryElem,
    memberVPLayerHist?: GroupGameHistoryElem,
  ): string {
    let speechString = '';

    const decomposed = randomString.split(/\{(.*?)\}/);

    decomposed.forEach((speechSubstr) => {
      switch (speechSubstr) {
        case speechFillIdentifiers.partnerIdentifier:
          speechString += teamMatePseudo;
          break;

        case speechFillIdentifiers.ptNumIdentifier:
          speechString += memberStats.score.toString();
          break;

        case speechFillIdentifiers.minutesPlayedIdentifier:
          const minPlayed = memberStats.timePlayedMs / (1000 * 60);
          speechString += minPlayed.toString();
          break;

        case speechFillIdentifiers.numGamesIdentifier:
          speechString += memberStats.numberPlayed.toString();
          break;

        case speechFillIdentifiers.victoryPercent:
          // Avoids putting 'NaN' in the string
          const vicPercent = (memberStats.wins / (memberStats.numberPlayed ?? 1)) * 100;
          speechString += vicPercent.toString();
          break;

        case speechFillIdentifiers.victoryNum:
          speechString += memberStats.wins.toString();
          break;

        case speechFillIdentifiers.victoryDateIdentifier:
          speechString += DateService.getFormatedDateStr(memberWinHist.startTime);
          break;

        case speechFillIdentifiers.lastGameDateIdentifier:
          speechString += DateService.getFormatedDateStr(memberVPLayerHist.startTime);
          break;

        case speechFillIdentifiers.projectedVicPercent:
          speechString += ((memberStats.wins + 1) / (memberStats.numberPlayed ?? 1)) * 100;
          break;

        case speechFillIdentifiers.projectedVicNum:
          speechString += (memberStats.wins + 1).toString();
          break;

        default:
          speechString += speechSubstr;
      }
    });

    return speechString;
  }

  static getProcessedMessage(
    speechArray: string[],
    teamMatePseudo: string,
    memberStats: GameStats,
    memberWinHist?: GroupGameHistoryElem,
    memberVPLayerHist?: GroupGameHistoryElem,
  ): string {
    const randId = Math.floor(Math.random() * speechArray.length);
    const unprocessedMessage = speechArray[randId];
    return this.decomposeAndFill(unprocessedMessage, teamMatePseudo, memberStats, memberWinHist, memberVPLayerHist);
  }
}
