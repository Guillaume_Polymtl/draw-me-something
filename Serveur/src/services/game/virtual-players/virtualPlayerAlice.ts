import * as SocketIO from 'socket.io';
import TeamMember from '../../../models/game/teamMember.model';
import { aliceEndOfRound, aliceGameBeginning, aliceHintResponse } from './aliceSpeech';
import VirtualPlayer from './virtualPlayer';

export default class VirtualPlayerAlice extends VirtualPlayer {
  constructor(gameUID: string, gameName: string, teamMate: TeamMember, playingSocket: SocketIO.Namespace) {
    super(gameUID, gameName, 'Alice', teamMate, playingSocket);
  }

  sendRandomWelcomeMessage(): void {
    super.sendRandomWelcomeMessage(aliceGameBeginning);
  }

  emitEndOfRoundMessage(wasGoodGuess: boolean): void {
    super.sendEndOfRoundMessage(aliceEndOfRound, wasGoodGuess);
  }

  emitHintResponse(hintNum: number): void {
    super.sendHintResponse(aliceHintResponse, hintNum);
  }
}
