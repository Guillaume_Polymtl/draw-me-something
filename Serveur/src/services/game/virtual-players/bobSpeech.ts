import { speechFillIdentifiers } from './virtualPlayerSpeech';
export const bobGameBeginning = {
  firstTimePlaying: [
    `Zut!😭 Pourquoi je suis en équipe avec {${speechFillIdentifiers.partnerIdentifier}}, qui n\'a jamais joué à ce mode?!`,
    `Quoi!? 😓 Je suis avec {${speechFillIdentifiers.partnerIdentifier}} qui n\'a jamais joué.e à ce mode de jeu! Voyons voir de quoi tu es capable!`,
  ],

  badScore: [
    `Merde!!💩 Pourquoi je suis en équipe avec {${speechFillIdentifiers.partnerIdentifier}}, qui a seulement {${speechFillIdentifiers.ptNumIdentifier}} points dans ce mode de jeu -.- On va perdre c’est sûr!`,
    `Ben voyons?!😩 {${speechFillIdentifiers.partnerIdentifier}} n\'a que {${speechFillIdentifiers.ptNumIdentifier}} points dans ce mode de jeu, je doute qu\' on puisse gagner:(`,
    `Eh la la... 😑 {${speechFillIdentifiers.partnerIdentifier}} n'a que {${speechFillIdentifiers.victoryPercent}}% de victoires à ce jeu!`,
  ],

  goodScore: [
    `Yo!😝 {${speechFillIdentifiers.partnerIdentifier}} a déjà joué {${speechFillIdentifiers.numGamesIdentifier}} fois à ce mode de jeu! On va vous planter!`,
    `YES!👌 Je suis en équipe avec {${speechFillIdentifiers.partnerIdentifier}} qui a {${speechFillIdentifiers.victoryPercent}}% de victoire dans ce mode de jeu!`,
    `Mhmm... {${speechFillIdentifiers.partnerIdentifier}} a {${speechFillIdentifiers.minutesPlayedIdentifier}} minutes de jeu enregistrées pour ce mode🤯! Bien hâte de gagner!`,
  ],
  alreadyWon: [
    `Heyyyy, tu me dis quelque chose! J'ai entendu parler de toi le {${speechFillIdentifiers.victoryDateIdentifier}}. C'était ta dernière victoire!👾👾👾`,
  ],
  playedTogether: [
    `YESSS! J'ai déjà joué avec {${speechFillIdentifiers.partnerIdentifier}} le {${speechFillIdentifiers.lastGameDateIdentifier}}😎. C'est certain que nous allons gagner!`,
  ],
};

export const bobEndOfRound = {
  wasGoodGuess: [
    `Yes! Let’s go {${speechFillIdentifiers.partnerIdentifier}}, on doit continuer et gagner👊! Si on gagne, ton % de victoire va passer de {${speechFillIdentifiers.victoryPercent}}% à {${speechFillIdentifiers.projectedVicPercent}}%!`,
    `Wahooo! Je dessine tellement bien que tu devines rapidement. À ce rythme là, ton nombre de victoires totales va passer à {${speechFillIdentifiers.projectedVicNum}} après cette game!`,
  ],
  wasBadGuess: [
    `Même après {${speechFillIdentifiers.numGamesIdentifier}} partie.s jouées, tu es encore aussi pourri!😒`,
    `Ça fait {${speechFillIdentifiers.minutesPlayedIdentifier}} minutes que tu joues à ce mode! Tu devrais être bien meilleur que ça!`,
  ],
};

export const bobHintResponse = {
  firstHint: [`Comment ça, un indice!? Est-ce que je dessine mal?😳`, `Bon, un premier indice ne seras pas de refus!`],
  extraHint: [
    `Un autre indice? Mon coup de crayon est-il si pire que ça?😢`,
    `Encore un indice? J'ai déjà pris des cours de dessin pourtant:(`,
  ],
};
