export const normalWords = [
  {
    word: 'chat',
    hints: ['animal', 'felin'],
  },
  {
    word: 'chien',
    hints: ['animal', 'woof'],
  },
  {
    word: 'crayon',
    hints: ['ecrire', 'dessin'],
  },
  {
    word: 'bureau',
    hints: ['travail', 'lieu'],
  },
  {
    word: 'maison',
    hints: ['demeure', 'habiter'],
  },
  {
    word: 'chaise',
    hints: ['assis', 'confortable'],
  },
  {
    word: 'voiture',
    hints: ['vroum', 'transport'],
  },
  {
    word: 'lapin',
    hints: ['animal', 'oreilles'],
  },
  {
    word: 'nuage',
    hints: ['ciel', 'pluie'],
  },
  {
    word: 'plante',
    hints: ['vert', 'pousse'],
  },
  {
    word: 'cactus',
    hints: ['piquant', 'désert'],
  },
  {
    word: 'carotte',
    hints: ['orange', 'légume'],
  },
  {
    word: 'patate',
    hints: ['tubercule', 'rond'],
  },
  {
    word: 'ballon',
    hints: ['roule', 'sports'],
  },
  {
    word: 'lit',
    hints: ['dormir', 'chaud'],
  },
  {
    word: 'vélo',
    hints: ['transport', 'sport'],
  },
  {
    word: 'course',
    hints: ['courir', 'sport'],
  },
  {
    word: 'océan',
    hints: ['eau', 'atlantique'],
  },
  {
    word: 'montagne',
    hints: ['alpin', 'haut'],
  },
  {
    word: 'avion',
    hints: ['vole', 'voyage'],
  },
  {
    word: 'oiseau',
    hints: ['vole', 'animal'],
  },
  {
    word: 'guitare',
    hints: ['instrument', 'cordes'],
  },
  {
    word: 'piano',
    hints: ['musique', 'touches'],
  },
  {
    word: 'bière',
    hints: ['boisson', 'alcool'],
  },
];
export const difficultWords = [
  {
    word: 'polytechnique',
    hints: ['université', 'génie en première classe'],
  },
  {
    word: 'ordinateur',
    hints: ['électronique', 'programmation'],
  },
  {
    word: 'processeur',
    hints: ['calcul', 'ordinateur'],
  },
  {
    word: 'téléphone',
    hints: ['Appel', 'sonne'],
  },
  {
    word: 'photographie',
    hints: ['caméra', 'art'],
  },
  {
    word: 'zeppelin',
    hints: ['dirigeable', 'flotte'],
  },
  {
    word: 'arc-en-ciel',
    hints: ['ciel', 'couleurs'],
  },
  {
    word: 'étagère',
    hints: ['meuble', 'entreposer'],
  },
  {
    word: 'examen',
    hints: ['intra', 'couler'],
  },
  {
    word: 'dérailleur',
    hints: ['composante', 'vélo'],
  },
  {
    word: 'autobus',
    hints: ['transport', 'public'],
  },
  {
    word: 'tofu',
    hints: ['aliment', 'végétarien'],
  },
  {
    word: 'haut-parleurs',
    hints: ['son', 'électroniques'],
  },
  {
    word: 'cèdre',
    hints: ['haie', 'arbuste'],
  },
  {
    word: 'renard',
    hints: ['animal', 'mammifère'],
  },
  {
    word: 'amplificateur',
    hints: ['musique', 'guitare électrique'],
  },
  {
    word: 'vidéoconférence',
    hints: ['appel', 'vidéo'],
  },
  {
    word: 'métro',
    hints: ['sous-terrain', 'transport'],
  },
  {
    word: 'couverture',
    hints: ['chaud', 'dormir'],
  },
  {
    word: 'écouteurs',
    hints: ['son', 'musique'],
  },
  {
    word: 'hélicoptère',
    hints: ['vol', 'hélices'],
  },

];