import { ImageNotFoundError } from '../../errors/drawingErrors';
import { CenterMode, Drawing, DrawingMode, DrawingToPotrace, PanoramicMode } from '../../models/drawing/drawing.model';
import Stroke from '../../models/drawing/stroke.model';
import Vec2 from '../../models/drawing/vec2.model';
import { PotracePreviewModel } from '../../models/potracePreview.model';
import { db } from '../firebaseInit';
import PotraceService from '../potrace/potraceService';
import DrawingConverterService from './drawingConverterService';
import ImageService from './imageService';

const WHITE_HEX = '#ffffff';

export enum Anchor {
  NorthWest,
  North,
  NorthEast,
  West,
  SouthEast,
  South,
  SouthWest,
  East,
}

export default class PairWordService {

  static get drawingsDocRef(): FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData> {
    return db.collection('drawings').doc();
  }

  static async getPotracePreview(potraceData: PotracePreviewModel): Promise<Drawing> {
    const imgBuffer = ImageService.base64ToBuffer(potraceData.imageBase64);
    const svgStr = await PotraceService.traceFromImage(imgBuffer, potraceData.potraceOptions);
    // const outBuffer = await ImageService.svgToPng(svgStr);
    // const outBuffer = ImageService.base64ToBuffer(base64png);
    const strokes = DrawingConverterService.svgToStrokes(svgStr, potraceData.potraceOptions);
    const { drawing } = potraceData;

    (drawing.background = potraceData?.potraceOptions?.background ?? WHITE_HEX), (drawing.strokes = strokes);
    return drawing;
  }

  // TODO: Complete this request regarding the svg decomposition
  static async addPairWordDrawingFromImg(creatorPseudo: string, pairWordDrawing: DrawingToPotrace): Promise<void> {
    const pairWordDocRef = db.collection('drawings').doc();
    const imgBuffer = ImageService.base64ToBuffer(pairWordDrawing.imageBase64);
    const svgstr = await PotraceService.traceFromImage(imgBuffer, pairWordDrawing?.potraceOptions);
    const strokes = DrawingConverterService.svgToStrokes(svgstr, pairWordDrawing?.potraceOptions);

    delete pairWordDrawing.imageBase64;
    const newDrawing = {
      ...pairWordDrawing,
      strokes,
      creatorPseudo,
      creationTime: Date.now(),
      background: pairWordDrawing?.potraceOptions?.background ?? WHITE_HEX,
    };
    this.handleDrawingMode(newDrawing);
    await pairWordDocRef.set(newDrawing);
  }

  private static handleDrawingMode(drawing: Drawing): void {
    for (let i = 0; i < drawing.strokes.length; i++) {
      drawing.strokes[i].index = i;
    }
    switch (drawing.drawingMode) {
      case DrawingMode.CONVENTIONAL: {
        console.log('conventionnal');
        break;
      }
      case DrawingMode.RANDOM: {
        console.log('random');
        this.randomizeStrokes(drawing);
        break;
      }
      case DrawingMode.PANORAMIC: {
        console.log('panoramic');
        this.orderStrokesPanoramic(drawing);
        break;
      }
      case DrawingMode.CENTER: {
        console.log('center');
        this.orderStrokesCenter(drawing);
        break;
      }
      default: {
        break;
      }
    }
    this.setStrokesTimer(drawing);
    return;
  }

  private static randomizeStrokes(drawing: Drawing): void {
    for (let i = drawing.strokes.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * i);
      const temp = drawing.strokes[i];
      const stroke = drawing.strokes[j];
      drawing.strokes[i] = stroke;
      drawing.strokes[j] = temp;
    }
  }

  private static setStrokesTimer(drawing: Drawing): void {
    let timer = 0;
    for (const stroke of drawing.strokes) {
      for (let p = 0; p < stroke.points.length; p++) {
        const currentPoint = stroke.points[p];
        let pointsDeltaT = 0;
        if (stroke.points[p + 1]) {
          const nextPoint = stroke.points[p + 1];
          pointsDeltaT = nextPoint.time - currentPoint.time;
        }
        stroke.points[p].time = timer;
        timer += pointsDeltaT;
      }
    }
  }

  private static orderStrokesPanoramic(drawing: Drawing): void {
    drawing.strokes.sort((stroke1: Stroke, stroke2: Stroke) => {
      const stroke1Anchors = this.getStrokeAnchors(stroke1);
      const stroke2Anchors = this.getStrokeAnchors(stroke2);
      return this.orderPanoramic(drawing.panoramicMode, stroke1Anchors, stroke2Anchors);
    });
  }

  private static orderPanoramic(mode: PanoramicMode, anchors1: Map<Anchor, Vec2>, anchors2: Map<Anchor, Vec2>): number {
    switch (mode) {
      case PanoramicMode.BOTTOM_TOP: {
        return anchors2.get(Anchor.South).y - anchors1.get(Anchor.South).y;
      }
      case PanoramicMode.TOP_BOTTOM: {
        return anchors1.get(Anchor.North).y - anchors2.get(Anchor.North).y;
      }
      case PanoramicMode.LEFT_RIGHT: {
        return anchors1.get(Anchor.West).x - anchors2.get(Anchor.West).x;
      }
      case PanoramicMode.RIGHT_LEFT: {
        return anchors2.get(Anchor.East).x - anchors1.get(Anchor.East).x;
      }
      default: {
        return 0;
      }
    }
  }

  private static getStrokeAnchors(stroke: Stroke): Map<Anchor, Vec2> {
    const anchors = new Map();
    const width = stroke.width;
    let north: Vec2 = { x: 500, y: 500 };
    let east: Vec2 = { x: 0, y: 0 };
    let south: Vec2 = { x: 0, y: 0 };
    let west: Vec2 = { x: 500, y: 500 };

    for (const point of stroke.points) {
      if (point.position.y - width / 2 < north.y) north = point.position;
      if (point.position.y + width / 2 > south.y) south = point.position;
      if (point.position.x - width / 2 < west.x) west = point.position;
      if (point.position.x + width / 2 > east.x) east = point.position;
    }

    anchors.set(Anchor.North, north);
    anchors.set(Anchor.East, east);
    anchors.set(Anchor.South, south);
    anchors.set(Anchor.West, west);

    anchors.set(Anchor.NorthWest, { x: west.x, y: north.y });
    anchors.set(Anchor.NorthEast, { x: east.x, y: north.y });
    anchors.set(Anchor.SouthWest, { x: west.x, y: south.y });
    anchors.set(Anchor.SouthEast, { x: east.x, y: south.y });

    return anchors;
  }

  private static orderStrokesCenter(drawing: Drawing): void {
    const center: Vec2 = { x: drawing.size.x ?? 250, y: drawing.size.y ?? 250 };
    drawing.strokes.sort((stroke1: Stroke, stroke2: Stroke) => {
      const distance1 = this.getFurthestDistanceOnStrokeFromCenter(stroke1, center);
      const distance2 = this.getFurthestDistanceOnStrokeFromCenter(stroke2, center);
      return this.orderCenter(drawing.centerMode, distance1, distance2);
    });
  }

  private static orderCenter(mode: CenterMode, distance1: number, distance2: number): number {
    switch (mode) {
      case CenterMode.INWARDS: {
        return distance2 - distance1;
      }
      case CenterMode.OUTWARDS: {
        return distance1 - distance2;
      }
      default: {
        return distance1 - distance2;
      }
    }
  }

  private static getFurthestDistanceOnStrokeFromCenter(stroke: Stroke, center: Vec2): number {
    let furthestDistance = 0;
    for (const point of stroke.points) {
      const x = point.position.x - center.x;
      const y = point.position.y - center.y;
      let angle = Math.atan(x / y);
      if (x < 0) {
        angle += Math.PI;
      }
      const deltaX = Math.cos(angle);
      const deltaY = Math.sin(angle);

      const actualX = x + deltaX;
      const acutalY = y + deltaY;

      const distance = Math.sqrt(Math.pow(actualX, 2) + Math.pow(acutalY, 2));
      furthestDistance = Math.max(furthestDistance, distance);
    }
    return furthestDistance;
  }

  static async getRandomPairWordDrawing(difficulty: string): Promise<Drawing> {
    const pairsFromDifficulty = await db.collection('drawings').where('difficulty', '==', difficulty).get();
    if (pairsFromDifficulty.empty) {
      throw new ImageNotFoundError();
    }
    const pairDocs = pairsFromDifficulty.docs;

    const randomPairDoc = pairDocs[Math.floor(Math.random() * pairDocs.length)];

    const pairWordDrawing = (randomPairDoc.data() as Drawing) ?? null;
    return pairWordDrawing;
  }

  static async addPairWordFromStrokes(creatorPseudo: string, drawing: Drawing): Promise<void> {
    const drawingElement = {
      ...drawing,
      creatorPseudo,
      creationTime: Date.now(),
    };
    this.handleDrawingMode(drawingElement);
    // Generates a new document and sets its content
    await db.collection('drawings').doc().set(drawingElement);
  }

  static async getPairsPaginated(amount: number, pageNum: number): Promise<{ drawings: Drawing[]; numberOfPages: number }> {
    const startId = Math.max((pageNum - 1) * amount - 1, 0);

    const drawings: Drawing[] = [];
    // const drawingDocs = (await db.collection('drawings').orderBy('creationTime', 'desc').get()).docs;
    const docLength = (await db.collection('drawings').get()).docs.length;

    const drawingDocsRef = db.collection('drawings').orderBy('creationTime', 'desc').offset(startId).limit(amount);
    const drawingDocs = (await drawingDocsRef.get()).docs;

    drawingDocs.forEach((drawingDoc) => {
      const drawingDocData = drawingDoc.data() as Drawing;
      drawings.push(drawingDocData);
    });

    const numberOfPages = Math.ceil(docLength / amount);

    return { drawings, numberOfPages };
  }

  static async getStrokesFromWord(word: string): Promise<Stroke[]> {
    const foundPair = await db.collection('drawings').where('word', '==', word).get();
    let strokesFromFoundPair: Stroke[] = [];
    if (!foundPair.empty) {
      const { strokes } = foundPair.docs[0].data() as Drawing;
      strokesFromFoundPair = strokes;
    }
    return strokesFromFoundPair;
  }
}
