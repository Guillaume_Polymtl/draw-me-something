import SocketIO from 'socket.io';
import DrawingData from '../../models/drawing/drawingData';
import Stroke from '../../models/drawing/stroke.model';
import { SocketGameService } from '../sockets/socketGameService';
import { socketEV } from '../sockets/socketStr';
export default class AutoDrawingService {

  static async autoDrawAsync(gameUID: string, socket: SocketIO.Namespace, strokes: Stroke[]) {

    for (const stroke of strokes) {
      let deltaT = 0;

      for (let i = 0; i < stroke.points.length; i++) {
        const ptPromise = new Promise<number>((resolve, _) => {
          let data = new DrawingData();

          setTimeout(() => {
            const timeA = stroke.points[i]?.time;
            const timeB = stroke.points[i + 1]?.time;
            if (timeA && timeB) {
              deltaT = Math.abs(timeB - timeA);
            }

            if (i !== 0) {
              data = this.pushStrokePoint(data, stroke, i - 1, true, false);
            }
            data = this.pushStrokePoint(data, stroke, i, false, true);

            data.gameUID = gameUID;

            socket.in(gameUID).emit(socketEV.PLAYER_DRAWING, data);
            data = new DrawingData();

            resolve(deltaT);
          }, deltaT);
        });
        deltaT = await Promise.race([ptPromise, this.stopOnEndGuess(gameUID)]);

        if (deltaT === -1) {
          console.log('Stop!!!');
          return;
        }
      }
    }
  }

  static pushStrokePoint(data: DrawingData, stroke: Stroke, index: number, start: boolean, end: boolean): DrawingData {
    data.x.push(stroke.points[index].position.x);
    data.y.push(stroke.points[index].position.y);

    data.start.push(start);
    data.end.push(end);
    data.r.push(stroke.color.red);
    data.g.push(stroke.color.green);
    data.b.push(stroke.color.blue);
    data.a.push(stroke.color.alpha);

    data.width.push(stroke.width);
    data.toolType.push(stroke.tool);
    return data;
  }

  static stopOnEndGuess(gameUID: string) {
    return new Promise<number>((resolve) => {
      SocketGameService.endGuessEmitter.on(gameUID, () => {
        resolve(-1);
      });
    });
  }
}
