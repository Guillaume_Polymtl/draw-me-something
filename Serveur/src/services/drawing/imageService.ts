const START_STR = 'd="';
const END_STR = '" stroke';
export default class ImageService {

  static base64ToBuffer(base64Str: string): Buffer {
    return Buffer.from(base64Str, 'base64');
  }

  static getPathElementData(svgStr: string): string {
    const startIndex = svgStr.search(START_STR) + START_STR.length;
    const endIndex = svgStr.search(END_STR);
    return svgStr.substring(startIndex, endIndex);
  }
}
