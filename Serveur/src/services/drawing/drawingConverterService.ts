import Point from '../../models/drawing/point.model';
import Stroke from '../../models/drawing/stroke.model';
import PostraceSettingsModel from '../../models/potraceSettings.model';
import { Color } from './../../models/drawing/color';
import ImageService from './imageService';

const STROKE_SEPARATOR = 'M ';
const C_L_REGEX = /(\sL\s|\sC\s)/;

export default class DrawingConverterService {
  static svgToStrokes(svgStr: string, options?: PostraceSettingsModel): Stroke[] {
    const pathData = ImageService.getPathElementData(svgStr);
    const pathStrokes = this.getSvgStrokes(pathData, options);
    return pathStrokes;
  }

  private static getSvgStrokes(pathData: string, options?: PostraceSettingsModel) {
    const pointsArr: Point[][] = [];
    const pathStrokes = pathData.split(STROKE_SEPARATOR);
    pathStrokes.shift();
    pathStrokes.forEach((strokeStr) => {
      pointsArr.push(this.getPointsFromStroke(strokeStr));
    });

    let strokes: Stroke[] = [];

    pointsArr.forEach((points) => {
      // Creates a stroke based on hardcoded settings
      const colorObj: Color = options?.color ? new Color(options.color) : new Color('#ffffffff');
      const stroke: Stroke = {
        points,
        width: 1,
        tool: 'Pencil',
        color: colorObj.getObj(), // Black if undefined color
      };
      strokes.push(stroke);
    });

    strokes = this.filterSuperfluousStrokes(strokes, false);
    strokes = this.filterSuperfluousPoints(strokes);
    strokes = this.initTimeForStrokes(strokes);
    return strokes;
  }

  private static filterSuperfluousStrokes(strokes: Stroke[], hasBackground: boolean): Stroke[] {
    const filteredStrokes: Stroke[] = [];
    const PRECISION_RM = 10;
    if (hasBackground) {
      filteredStrokes.push(strokes.shift());
    }
    strokes.forEach((stroke) => {
      if (stroke.points.length >= PRECISION_RM) {
        filteredStrokes.push(stroke);
      }
    });
    return filteredStrokes;
  }

  private static filterSuperfluousPoints(strokes: Stroke[]): Stroke[] {
    const filteredStrokes: Stroke[] = [];
    const MAX_PT_LEN = 200;
    const PT_STEPS = 2;

    strokes.forEach((stroke) => {
      // To many points returned - init optimization
      if (stroke.points.length >= MAX_PT_LEN) {
        const strokePoints = [];

        // Keeping 1 point every PT_STEPS points
        stroke.points.forEach((_, i) => {
          if (i % PT_STEPS === 0) {
            strokePoints.push(stroke.points[i]);
          }
        });

        stroke.points = strokePoints;
      }
      filteredStrokes.push(stroke);
    });
    return filteredStrokes;
  }

  private static initTimeForStrokes(strokes: Stroke[]): Stroke[] {
    const TIME_PER_STROKE_MS = 25000;
    let totalTime = 0;
    let numPoints = 0;
    strokes.forEach((stroke) => {
      numPoints += stroke.points.length;
    });
    const timeStep = TIME_PER_STROKE_MS / numPoints;
    strokes.forEach((stroke) => {
      stroke.points = stroke.points.map((point, i) => {
        totalTime += timeStep;
        point.time = Math.round(totalTime);
        return point;
      });
    });
    return strokes;
  }

  private static getPointsFromStroke(pathStroke: string): Point[] {
    const ptStringArr = pathStroke.split(C_L_REGEX);

    const pointsStr: string[][] = [];
    ptStringArr.forEach((elementString) => {
      if (!elementString.match(C_L_REGEX)) {
        pointsStr.push(elementString.split(', '));
      }
    });

    const pointArr: Point[] = [];

    pointsStr.forEach((points) => {
      points.forEach((stringTuple) => {
        const coordsStr = stringTuple.split(' ');
        pointArr.push({
          position: {
            x: parseInt(coordsStr[0], 10),
            y: parseInt(coordsStr[1], 10),
          },
          time: 0,
        });
      });
    });

    return pointArr;
  }
}
