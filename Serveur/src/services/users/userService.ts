import { DocRef, GameStatsType } from '../../config/customTypes';
import { UserNotFoundError } from '../../errors/usersErrors';
import GameStats from '../../models/game/stats/gameStats.model';
import { privateInfo, publicInfo, UserModel } from '../../models/user.model';
import { db } from '../firebaseInit';
import GameService from '../game/gameService';
import GetterService from '../getterService';
import FriendsService from './friendsService';

export default class UserService {
  static async getUserByPseudo(searchpseudo: string, senderPseudo: string): Promise<UserModel> {
    const user = (await GetterService.getUserDoc(searchpseudo)) as UserModel;
    /**
     * Checks which information to send.
     * If the sender looks for his/her own account, send private informations.
     * Otherwise, send basic public information about the user.
     */
    const isFriend = await FriendsService.isInFriendsList(senderPseudo, searchpseudo);
    let propsArray = searchpseudo === senderPseudo || isFriend ? privateInfo : publicInfo;
    propsArray = propsArray.concat('classicStats', 'soloStats');
    Object.keys(user).forEach((keyAttribute) => {
      if (!propsArray.includes(keyAttribute)) {
        delete user[keyAttribute];
      }
    });
    return user;
  }

  /**
   * Returns all the channel names where a certain user is subscribed
   */
  static async getSubscribedChannels(userId: string): Promise<{ pseudo: string; channelName: string; isGameChannel?: boolean }[]> {
    let subscribedChannels: Promise<{ pseudo: string; channelName: string; isGameChannel?: boolean }>[] = [];

    const userRef = (await GetterService.getUserDoc(userId, true)) as DocRef;
    const channelsRef = db.collection('channels').where('subscribedUsers', 'array-contains', userRef);
    const channelDocs = await channelsRef.get();

    subscribedChannels = channelDocs.docs.map(async (channel) => {
      const { channelName, ownerRef, isGameChannel } = channel.data();
      const pseudo = await GetterService.getPseudoFromRef(ownerRef);
      return { pseudo, channelName, isGameChannel };
    });
    return await Promise.all(subscribedChannels);
  }

  /**
   * This method substracts the subscribed channels to the
   *
   */
  static async getUnsubscribedChannels(userId: string): Promise<{ pseudo: string; channelName: string }[]> {
    let unsubscribedChannels: { pseudo: string; channelName: string }[] = [];

    // Already checks if user exists
    const subscribedChannels = await this.getSubscribedChannels(userId);

    const allChannels = await db.collection('channels').get();

    const allChannelPairs: Promise<{ pseudo: string; channelName: string }>[] = allChannels.docs.map(async (channel) => {
      const { ownerRef, channelName } = channel.data();
      const pseudo = await GetterService.getPseudoFromRef(ownerRef);
      return { pseudo, channelName };
    });

    const channelPairs = await Promise.all(allChannelPairs);
    unsubscribedChannels = channelPairs.filter((channel) => {
      return !subscribedChannels.some(({ channelName }) => channelName === channel.channelName);
    });

    return unsubscribedChannels;
  }

  // TODO: Test
  static async patchUserAccount(user: UserModel): Promise<void> {
    const userAuthDocs = await db.collection('userAuth').where('pseudo', '==', user.pseudo).get();
    if (user.newPseudo) {
      user.pseudo = user.newPseudo;
      delete user.newPseudo;
    }
    if (userAuthDocs.empty) {
      throw new UserNotFoundError();
    }

    await userAuthDocs.docs[0].ref.update(user);
  }

  static async deleteUserAccount(pseudo: string): Promise<void> {
    const userRef = (await GetterService.getUserDoc(pseudo, true)) as DocRef;
    await userRef.delete();
  }

  // statsMode is either soloStats or classicStats. Otherwise the request will return empty
  static async getUserGameStatsFromMode(pseudo: string, statsMode: GameStatsType): Promise<GameStats> {
    const userAuthDocs = await db.collection('userAuth').where('pseudo', '==', pseudo).get();
    if (userAuthDocs.empty) {
      throw new UserNotFoundError();
    }

    let gameStats: GameStats;
    const stats: GameStats | undefined = userAuthDocs.docs[0].data()[statsMode.toString()];

    if (!stats) {
      gameStats = { numberPlayed: 0, score: 0, timePlayedMs: 0 };
      if (statsMode === 'classicStats') {
        gameStats = { ...gameStats, wins: 0 };
      } else if (statsMode === 'soloStats') {
        gameStats = { ...gameStats, bestScore: 0 };
      }
    } else {
      gameStats = stats;
    }
    return gameStats;
  }

  static async getAllOtherUsers(except: string): Promise<{ pseudo: string, avatar: string }[]> {
    const usersColl = await db.collection('userAuth').get();
    const allUsers = usersColl.docs.map((userDoc) => {
      const { pseudo, avatar } = userDoc.data() as UserModel;
      return { pseudo, avatar };
    });

    return allUsers.filter((user) => user.pseudo !== except);
  }

  static async getCompleteProfileData(pseudo: string): Promise<{ profile: UserModel, friendsList: { pseudo: string, avatar: string }[] }> {
    const { friends } = await FriendsService.getFriendsAndRequests(pseudo);
    const friendsList = friends.map((friend) => {
      return { pseudo: friend.pseudo, avatar: friend.avatar };
    });

    // Gettign private profile
    const profile = await UserService.getUserByPseudo(pseudo, pseudo);

    return {
      profile,
      friendsList,
    };
  }
}
