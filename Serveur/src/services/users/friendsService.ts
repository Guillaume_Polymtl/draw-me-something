import { firestore } from 'firebase-admin';
import { DocRef } from '../../config/customTypes';
import { FriendRequestConflictError } from '../../errors/usersErrors';
import FriendReq from '../../models/friendReq.model';
import FriendsAndRequests from '../../models/friendsAndRequests.model';
import { UserModel } from '../../models/user.model';
import GetterService from '../getterService';

export default class FriendsService {
  static async addPendingFriendRequest(friendReq: FriendReq): Promise<void> {
    const { from, to } = friendReq;
    const date = Date.now();

    const senderDocRef: DocRef = (await GetterService.getUserDoc(from, true)) as DocRef;
    const potentialFriendDocRef = (await GetterService.getUserDoc(to, true)) as DocRef;

    const { outbound, friends } = (await senderDocRef.get()).data() as UserModel;

    const isInFriendsList = !!(friends && friends.find((friendDocRef) => friendDocRef.path === potentialFriendDocRef.path));
    const hasAlreadyBeenSent = !!(outbound && outbound.find((outboundDocRef) => outboundDocRef.to.path === potentialFriendDocRef.path));

    if (isInFriendsList) {
      throw new FriendRequestConflictError('User is already in friends list');
    } else if (hasAlreadyBeenSent) {
      throw new FriendRequestConflictError('Friend request has already been sent');
    }

    await potentialFriendDocRef.update({
      inbound: firestore.FieldValue.arrayUnion({ from: senderDocRef, date }),
    });

    await senderDocRef.update({
      outbound: firestore.FieldValue.arrayUnion({ to: potentialFriendDocRef, date }),
    });
  }

  static async respondToFriendReq(friendReq: FriendReq): Promise<void> {
    const senderPseudo = friendReq.from;
    const potFriendPseudo = friendReq.to;
    const senderDocRef: DocRef = (await GetterService.getUserDoc(senderPseudo, true)) as DocRef;
    const potentialFriendDocRef: DocRef = (await GetterService.getUserDoc(potFriendPseudo, true)) as DocRef;

    const { inbound } = await GetterService.getUserDocFromRef(senderDocRef);
    await senderDocRef.update({
      inbound: inbound?.filter(({ from }) => from.path !== potentialFriendDocRef.path) ?? [],
    });

    const { outbound } = await GetterService.getUserDocFromRef(potentialFriendDocRef);
    await potentialFriendDocRef.update({
      outbound: outbound?.filter(({ to }) => to.path !== senderDocRef.path) ?? [],
    });

    if (friendReq.accepted) {
      await senderDocRef.update({
        friends: firestore.FieldValue.arrayUnion(potentialFriendDocRef),
      });

      await potentialFriendDocRef.update({
        friends: firestore.FieldValue.arrayUnion(senderDocRef),
      });
    }
  }

  static async getFriendsAndRequests(pseudo: string): Promise<FriendsAndRequests> {
    const userDoc = (await GetterService.getUserDoc(pseudo)) as UserModel;
    const { inbound, outbound, friends } = userDoc;

    const friendsPromises = friends?.map(async (friendRef) => {
      try {
        const friendDoc = await GetterService.getUserDocFromRef(friendRef);
        const filteredFriendDoc = GetterService.filterUserModelAttributes(friendDoc, true);
        return filteredFriendDoc;
      } catch (error) {
        return null;
      }
    });

    const inboundPromises = inbound?.map(async ({ from, date }) => {
      try {
        const inboundDoc = await GetterService.getUserDocFromRef(from);
        const filteredInboundDoc = GetterService.filterUserModelAttributes(inboundDoc, false);
        return { from: filteredInboundDoc, date };
      } catch (error) {
        return null;
      }
    });

    const outboundPromises = outbound?.map(async ({ to, date }) => {
      try {
        const outboundDoc = await GetterService.getUserDocFromRef(to);
        const filteredOutboundDoc = GetterService.filterUserModelAttributes(outboundDoc, false);
        return { to: filteredOutboundDoc, date };
      } catch (error) {
        return null;
      }
    });

    return {
      friends: friendsPromises ? (await Promise.all(friendsPromises)).filter(Boolean) : [],
      inbound: inboundPromises ? (await Promise.all(inboundPromises)).filter(Boolean) : [],
      outbound: outboundPromises ? (await Promise.all(outboundPromises)).filter(Boolean) : [],
    };
  }

  static async isInFriendsList(pseudo: string, userPseudoToCheck: string): Promise<boolean> {
    const friendRef = (await GetterService.getUserDoc(userPseudoToCheck, true)) as DocRef;
    const userDoc = (await GetterService.getUserDoc(pseudo)) as UserModel;
    const { friends } = userDoc;
    const isInFriendsList = !!(friends && friends.find((friend) => friend.path === friendRef.path));

    return isInFriendsList;
  }
}
