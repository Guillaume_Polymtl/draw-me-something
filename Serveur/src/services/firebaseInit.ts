import * as admin from 'firebase-admin';

const initFirebase = () => {
  // tslint:disable-next-line: no-require-imports
  const serviceAccount = require('../../secrets/projet3-log3900-firebase-adminsdk-1ex1c-9ba36b63e7.json');
  return admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://projet3-log3900-default-rtdb.firebaseio.com',
  });
};

const db = admin.firestore(initFirebase());
const rtdb = admin.database();
export { initFirebase, db, admin, rtdb };
