import * as SocketIO from 'socket.io';
import { db } from '../firebaseInit';
import SocketChatService from './socketChatService';
import { SocketManager } from './socketManager';
import { socketEV, socketNS } from './socketStr';

const MAX_CHANNEL_LISTENERS = 30;
export class SocketChannelService {
  /**
   *
   * @param serverSocket A server socket.
   * This method creates a unique rooms socket to manage
   * channel creation, joining and messages.
   *
   */
  static initChannel(serverSocket: SocketIO.Server): void {
    const channelSocket = serverSocket.of(socketNS.CHANNEL);
    channelSocket.setMaxListeners(MAX_CHANNEL_LISTENERS);
    this.initAllRooms(channelSocket);

    channelSocket.on('connection', (socket: SocketIO.Socket) => {
      SocketChatService.sendMessageToRoom(socket);

      this.createRoom(socket);
      this.deleteRoom(socket);
      this.joinRoom(socket);
      this.leaveRoom(socket);

      socket.on('disconnect', () => {
        socket.removeAllListeners();
        SocketManager.deleteSocket(channelSocket, socket.id);
      });
    });
  }

  /**
   *
   * @param roomSocket The unique room socket.
   */
  static createRoom(roomSocket: SocketIO.Socket): void {
    roomSocket.on(socketEV.CREATE_ROOM, (roomName: string) => {
      roomSocket.rooms.add(roomName);
      console.log(`Room ${roomName} was created`);
    });
  }

  private static deleteRoom(roomSocket: SocketIO.Socket): void {
    roomSocket.on(socketEV.DEL_ROOM, (roomName: string) => {
      roomSocket.rooms.delete(roomName);
      console.log(`Room ${roomName} was deleted`);
    });
  }

  private static joinRoom(roomSocket: SocketIO.Socket): void {
    roomSocket.on(socketEV.JOIN_ROOM, (roomName: string) => {
      roomSocket.join(roomName);
      console.log(`Room ${roomName} was joined by ${roomSocket.id}`);
    });
  }

  private static leaveRoom(roomSocket: SocketIO.Socket): void {
    roomSocket.on(socketEV.LEAVE_ROOM, (roomName: string) => {
      roomSocket.leave(roomName);
      console.log(`Room ${roomName} was left by ${roomSocket.id}`);
    });
  }

  /**
   *
   * @param roomSocket ->The room socket namespace for channels
   *
   * this method initializes the channels in the database
   * when the server starts, as the state is not preserved
   * in the server for SocketIO objects
   *
   * NOTE: SocketIO seems to add automatically all joined/left rooms.
   * An initAll method may not be needed...
   */
  private static async initAllRooms(roomSocket: SocketIO.Namespace) {
    const channelColls = await db.collection('channels').get();
    channelColls.docs.forEach((channelDoc) => {
      const { channelName } = channelDoc.data();
      roomSocket._rooms.add(channelName);
    });
  }
}
