import * as SocketIO from 'socket.io';
import AuthService from '../../controllers/auth/authService';
import { SocketManager } from './socketManager';

export default class SocketConnectionService {
  private static pseudoSocketIdMap = new Map<string, string>();
  static initSocketConnection(serverSocket: SocketIO.Server): void {
    serverSocket.on('connection', (connectedSocket: SocketIO.Socket) => {
      this.subscribeToSocketRegistry(connectedSocket);
      this.handleDisconnection(connectedSocket, serverSocket);
    });
  }

  static getPseudoSocketIdMap(): Map<string, string> {
    return this.pseudoSocketIdMap;
  }

  static subscribeToSocketRegistry(connectedSocket: SocketIO.Socket): void {
    connectedSocket.on('subscribe', (pseudo: string) => {
      this.pseudoSocketIdMap.set(pseudo, connectedSocket.id);
    });
  }

  static handleDisconnection(connectedSocket: SocketIO.Socket, serverSocket: SocketIO.Server): void {
    connectedSocket.on('disconnect', () => {
      let pseudoToDisconnect = '';
      let socIdToDisconnect = '';
      for (const [pseudo, socId] of this.pseudoSocketIdMap.entries()) {
        if (socId === connectedSocket.id) {
          pseudoToDisconnect = pseudo;
          socIdToDisconnect = socId;
        }
        pseudoToDisconnect = pseudo;
      }
      if (pseudoToDisconnect) {
        AuthService.logOutUser(pseudoToDisconnect).catch((error) => {
          console.error(error);
        });
        this.pseudoSocketIdMap.delete(pseudoToDisconnect);
        connectedSocket.disconnect(true);
        serverSocket.sockets.sockets.delete(socIdToDisconnect);
      }
    });
  }
}
