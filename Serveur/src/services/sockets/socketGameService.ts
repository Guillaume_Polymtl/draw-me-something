import EventEmitter from 'events';
import * as SocketIO from 'socket.io';
import { DocRef, GameMode } from '../../config/customTypes';
import { JoinLeaveGame, JoinLeaveTeam } from '../../models/game/gameAction.model';
import GuessModel from '../../models/game/guess.model';
import WordHints from '../../models/game/wordHints.model';
import { UserModel } from '../../models/user.model';
import ChannelService from '../chat/channelService';
import GameRulesService from '../game/gameRulesService';
import GameService from '../game/gameService';
import GroupGameInstance from '../game/groupGameInstance';
import SprintGameInstance from '../game/sprintGameInstance';
import VPlayerService from '../game/virtual-players/vplayerService';
import GetterService from '../getterService';
import { GameModel } from './../../models/game/game.model';
import SocketChatService from './socketChatService';
import { SocketManager } from './socketManager';
import { socketEV, socketNS } from './socketStr';

const GAME_MAX_LISTENERS = 50;
export class SocketGameService {
  static concurrentGames = new Map<string, GroupGameInstance | SprintGameInstance>();
  static socketIdsToGameUID = new Map<string, JoinLeaveGame>();

  static playingSocket: SocketIO.Namespace;

  static endGuessEmitter = new EventEmitter().setMaxListeners(GAME_MAX_LISTENERS);

  static initGame(serverSocket: SocketIO.Server): void {
    const gameSocket = serverSocket.of(socketNS.GAME);
    this.playingSocket = gameSocket;
    this.playingSocket.setMaxListeners(GAME_MAX_LISTENERS);

    gameSocket.on('connection', (connectedSocket: SocketIO.Socket) => {
      this.createGame(connectedSocket);
      this.startGame(connectedSocket);
      this.joinGame(connectedSocket);
      this.joinTeam(connectedSocket);
      this.leaveGame(connectedSocket);
      this.forceHallEmit(connectedSocket);
      this.forceLobbyEmit(connectedSocket);

      this.playerDrawing(connectedSocket);
      VPlayerService.vplayerHintResponse(this.concurrentGames, connectedSocket);
      console.log('connexion to game socket');

      // Sends all the concurrent games via socket
      gameSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));

      // If disconnects abruptly, handle memory cleaning
      connectedSocket.on('disconnect', (reason) => {
        const { id } = connectedSocket;
        const gamePseudoPair: JoinLeaveGame | undefined = this.socketIdsToGameUID.get(id);
        if (gamePseudoPair) {
          const { pseudo, gameUID } = gamePseudoPair;
          this.concurrentGames.get(gameUID)?.leaveGame(pseudo);
          this.emitLeaveGame(gameUID, pseudo);
          this.socketIdsToGameUID.delete(id);
        }
        connectedSocket.removeAllListeners();
        console.log(reason);
        SocketManager.deleteSocket(this.playingSocket, connectedSocket.id);
      });
    });
  }

  private static forceHallEmit(connectedSocket: SocketIO.Socket) {
    connectedSocket.on('force-hall-emit', () => {
      connectedSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
    });
  }

  private static forceLobbyEmit(connectedSocket: SocketIO.Socket) {
    connectedSocket.on('force-lobby-emit', (gameUID: string) => {
      connectedSocket.emit(socketEV.UPDATE_LOBBY, this.concurrentGames.get(gameUID).gameObject);
    });
  }

  private static createGame(userSocket: SocketIO.Socket): void {
    userSocket.on(socketEV.CREATE_GAME, async ({ gameMode, difficulty, name, roundNum }) => {
      let createdGame: GroupGameInstance | SprintGameInstance;
      console.log(gameMode);
      if ((gameMode as GameMode) === 'Classique') {
        createdGame = new GroupGameInstance(gameMode, difficulty, name, roundNum);

        // Creating new temporary channel
        await ChannelService.createNewChannel({ channelName: name, pseudo: 'Bob', isGameChannel: true }).catch((error) =>
          console.error(error),
        );
      } else {
        createdGame = new SprintGameInstance(gameMode, difficulty, name);
      }

      const { gameUID } = createdGame.gameObject;

      // Added to rooms and send useful feedback to user.
      userSocket.rooms.add(gameUID);

      this.concurrentGames.set(gameUID, createdGame);

      // On creation, sends the newly created game properties
      userSocket.emit(socketEV.CREATE_GAME, createdGame.gameObject);
      this.playingSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
    });
  }

  private static startGame(gameSocket: SocketIO.Socket): void {
    gameSocket.on(socketEV.START_GAME, (gameUID: string) => {
      this.playingSocket.in(gameUID).emit(socketEV.START_GAME, gameUID);

      this.concurrentGames
        .get(gameUID)
        .runGame(this.playingSocket)
        .then(async (gameUIDToStop) => {
          await this.cleanupTerminatedGame(gameUIDToStop);
        });
    });
  }

  private static joinGame(userSocket: SocketIO.Socket): void {
    userSocket.on(socketEV.JOIN_GAME, (joinGame: JoinLeaveGame) => {
      try {
        this.concurrentGames
          .get(joinGame.gameUID)
          .joinGame(joinGame.pseudo, userSocket)
          .then(async (gameObj: GameModel) => {
            userSocket.join(joinGame.gameUID);
            this.socketIdsToGameUID.set(userSocket.id, joinGame);
            this.playingSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
            // Chat is possible for classic mode only
            if (gameObj.gameMode === 'Classique') {
              await ChannelService.subscribeToChannel(gameObj.name, joinGame.pseudo).catch((error) => console.error(error));
              this.playingSocket.in(joinGame.gameUID).emit(socketEV.UPDATE_LOBBY, gameObj);

              // Successfully added to chat
              userSocket.emit(socketEV.GAME_CHAT_ACK, { update: true });
              SocketChatService.sendMessageToRoom(userSocket);
            }
          });
      } catch (error) {
        console.error(error);
      }
    });
  }

  private static joinTeam(userSocket: SocketIO.Socket): void {
    userSocket.on(socketEV.JOIN_TEAM, (joinGame: JoinLeaveTeam) => {
      GameService.getTeamMemberRef(joinGame.teamMember)
        .then(async (userRef: DocRef) => {
          try {
            joinGame.teamMember.docRef = userRef;

            const { avatar } = (await userRef.get()).data() as UserModel;
            joinGame.teamMember.avatar = avatar;

            const groupGame = this.concurrentGames.get(joinGame.gameUID).joinTeam(joinGame.teamMember);

            // Notifies that the game has been joined
            this.playingSocket.in(joinGame.gameUID).emit(socketEV.UPDATE_LOBBY, groupGame);

            if (joinGame.teamMember.isVirtual) {
              this.playingSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
            }

            // Game can be started
            if (!groupGame.canBeJoined) {
              this.playingSocket.in(joinGame.gameUID).emit(socketEV.CAN_START, true);
            }
          } catch (error) {
            console.error(error);
          }
        })
        .catch((error) => {
          console.error(error);
        });
    });
  }

  private static leaveGame(userSocket: SocketIO.Socket): void {
    userSocket.on(socketEV.LEAVE_GAME, async ({ gameUID, pseudo }) => {
      try {
        this.emitLeaveGame(gameUID, pseudo);
        const { gameObject } = this.concurrentGames.get(gameUID);

        await ChannelService.unsubscribeFromChannel(gameObject.name, pseudo).catch((error) => console.error(error));
        if (gameObject.players.length === 0) {
          await this.cleanupTerminatedGame(gameUID);
        }

        userSocket.emit(socketEV.GAME_CHAT_ACK, { update: true });
        userSocket.leave(gameUID);
      } catch (error) {
        console.error(error);
      }
    });
  }

  private static async cleanupTerminatedGame(gameUID: string) {
    try {
      const { gameObject, pseudoSocketMap } = this.concurrentGames.get(gameUID);
      if (gameObject.gameMode === 'Classique') {
        await ChannelService.deleteChannel('Bob', gameObject.name).catch((error) => console.error(error));
      }
      pseudoSocketMap.forEach((connectedSocket) => {
        connectedSocket.emit(socketEV.GAME_CHAT_ACK, { update: true });
        connectedSocket.leave(gameUID);
      });
      this.concurrentGames.delete(gameUID);
      this.playingSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
      this.endGuessEmitter.removeAllListeners(gameUID);
    } catch (error) {
      console.log('Error: Game already deleted');
    }

  }

  // Handles leaving a concurrent game
  private static emitLeaveGame(gameUID: string, pseudo: string) {
    const groupGame: GameModel | undefined = this.concurrentGames.get(gameUID)?.leaveGame(pseudo);

    if (!groupGame) {
      return;
    }

    this.playingSocket.emit(socketEV.UPDATE_LOBBY, groupGame);

    this.playingSocket.emit(socketEV.UPDATE_HALL, GetterService.getConcurrentGamesDisplay(this.concurrentGames));
    this.playingSocket.in(gameUID).emit(socketEV.CAN_START, false);

    if (groupGame.gameMode === 'Classique') {
      this.playingSocket.emit(socketEV.UPDATE_LOBBY, groupGame);
    }
  }

  static initPlayerGuessing(gameUID: string, word: string, guessers: string[], tryNum?: number): Promise<boolean>[] {
    const { pseudoSocketMap, gameObject } = this.concurrentGames.get(gameUID);
    const racePromises = guessers.map((guesser) => {
      return new Promise<boolean>((resolve, _) => {
        let tries = tryNum;
        pseudoSocketMap.get(guesser).on(socketEV.PLAYER_GUESSING, (guess: GuessModel) => {
          console.log('guess: ' + guess.guessContent + ', ans: ' + word);
          const isGoodGuess = word.toLowerCase() === guess.guessContent.toLowerCase();

          if (tryNum) {
            tries -= 1;
          }

          const guessPayload: GuessModel = {
            ...guess,
            isGoodGuess,
            proximity: GameRulesService.wordProximity(guess.guessContent, word, gameObject.difficulty),
            triesLeft: tries,
          };

          this.playingSocket.in(guess.gameUID).emit(socketEV.PLAYER_GUESSING, guessPayload);

          if (isGoodGuess || (tryNum && tries <= 0)) {
            resolve(isGoodGuess);
          }
        });
      });
    });
    return racePromises;
  }

  static playerGuessing(
    gameUID: string,
    wordWithHints: WordHints,
    baseTimeToGuess: number,
    isRightOfReply: boolean,
    guessers: string[],
    tryNum?: number,
  ): Promise<{ isGoodGuess: boolean; timeLeftSec: number }> {
    let isGoodGuess = false;
    const { word, hints } = wordWithHints;
    return new Promise(async (resolve, _) => {
      // Clock for guessing time
      let hasSentHints = false;
      let timeToGuess = baseTimeToGuess;

      const gameClockInterval = setInterval(() => {
        timeToGuess = timeToGuess - 1;
        this.playingSocket.in(gameUID).emit(socketEV.GAME_CLOCK, timeToGuess * 1000);

        if (!isRightOfReply && !hasSentHints) {
          this.playingSocket.in(gameUID).emit(socketEV.SEND_HINT, hints);
          console.log(hints);
          hasSentHints = true;
        }

        if (timeToGuess <= 0) {
          clearInterval(gameClockInterval);
          resolve({ isGoodGuess, timeLeftSec: 0 });
        }
      }, 1000);

      const guessPromises = this.initPlayerGuessing(gameUID, word, guessers, tryNum);

      // await until first promise to resolve
      isGoodGuess = await Promise.race(guessPromises);
      this.endGuessEmitter.emit(gameUID);

      // Remove player-guessing listeners for previous guessers
      this.removePlayerGuessingListeners(gameUID, guessers);
      clearInterval(gameClockInterval);
      resolve({ isGoodGuess, timeLeftSec: timeToGuess });
    });
  }

  static playerDrawing(gamingSocket: SocketIO.Socket): void {
    // tslint:disable-next-line:no-any
    gamingSocket.on(socketEV.PLAYER_DRAWING, (drawingData: any) => {
      gamingSocket.to(drawingData.gameUID).emit(socketEV.PLAYER_DRAWING, drawingData);
    });
  }

  // Once the guessers are done, remove their listeners
  static removePlayerGuessingListeners(gameUID: string, guessers: string[]) {
    guessers.forEach((guesser) => {
      this.concurrentGames.get(gameUID)?.pseudoSocketMap?.get(guesser)?.removeAllListeners(socketEV.PLAYER_GUESSING);
    });
  }
}
