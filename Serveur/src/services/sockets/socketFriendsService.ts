import SocketIO from 'socket.io';
import FriendReqModel from '../../models/friendReq.model';
import SocketConnectionService from './socketConnectionService';
import { socketEV, socketNS } from './socketStr';

export default class SocketFriendsService {
  static initFriends(serverSocket: SocketIO.Server): void {
    const friendsSocket = serverSocket.of(socketNS.FRIENDS);

    friendsSocket.on('connection', (connectedSocket: SocketIO.Socket) => {
      this.sendFriendRequest(connectedSocket);
      this.respondToFriendRequest(connectedSocket);
    });
  }

  static sendFriendRequest(connectedSocket: SocketIO.Socket): void {
    connectedSocket.on(socketEV.FRIEND_REQUEST, (friendReq: FriendReqModel) => {
      const socId = SocketConnectionService.getPseudoSocketIdMap().get(friendReq.to);
      if (socId) {
        // Sends to potential friend's socket
        connectedSocket.to(socId).emit(socketEV.FRIEND_REQUEST, friendReq.to);
      }
    });
  }

  static respondToFriendRequest(connectedSocket: SocketIO.Socket): void {
    connectedSocket.on(socketEV.REQ_RESPOND, (friendReq: FriendReqModel) => {
      const socId = SocketConnectionService.getPseudoSocketIdMap().get(friendReq.to);
      if (socId) {
        connectedSocket.to(socId).emit(socketEV.REQ_RESPOND, friendReq);
      }
    });
  }
}
