import * as http from 'http';
import { injectable } from 'inversify';
import * as SocketIO from 'socket.io';
import { SocketChannelService } from './socketChannelService';
import SocketConnectionService from './socketConnectionService';
import SocketFriendsService from './socketFriendsService';
import { SocketGameService } from './socketGameService';

@injectable()
export class SocketManager {
  private ioServer: SocketIO.Server;

  static deleteSocket(socketNamespace: SocketIO.Namespace, socIdToKill: string): void {
    socketNamespace.sockets.delete(socIdToKill);
  }

  getServerSocket(): SocketIO.Server {
    return this.ioServer;
  }

  initSocket(server: http.Server): void {
    this.ioServer = new SocketIO.Server(server, {
      cors: {
        origin: ['http://localhost:4200', 'http://log3990-110.canadaeast.cloudapp.azure.com:4200/'],
        methods: ['GET', 'POST'],
      },
      perMessageDeflate: false,
      pingInterval: 15 * 1000,
      pingTimeout: 90 * 1000,
    });

    SocketConnectionService.initSocketConnection(this.ioServer);
    SocketChannelService.initChannel(this.ioServer);
    SocketGameService.initGame(this.ioServer);
    SocketFriendsService.initFriends(this.ioServer);
  }
}
