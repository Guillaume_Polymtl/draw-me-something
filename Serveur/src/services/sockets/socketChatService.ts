import * as SocketIO from 'socket.io';
import SocketMessageModel from '../../models/socketMessage.model';
import { socketEV } from './socketStr';

export default class SocketChatService {
  /**
   *
   * @param roomSocket -> a socket associated to a room
   *
   * Sends a message to all users connected to a room socket
   */
  static sendMessageToRoom(roomSocket: SocketIO.Socket): void {
    roomSocket.on(socketEV.ROOM_MESSAGE, (data: SocketMessageModel) => {
      const date = Date.now();
      const messagePayload = {
        message: data.message,
        pseudo: data.pseudo,
        channelName: data.channelId,
        date,
      };
      console.log(data);
      roomSocket.to(data.channelId).emit(socketEV.ROOM_MESSAGE, messagePayload);
      roomSocket.emit(socketEV.ROOM_MESSAGE, { ...messagePayload, channelName: data.channelId });
    });
  }
}
