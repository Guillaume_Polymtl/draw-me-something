const socketNS = {
  CHAT: '/chat',
  GAME: '/game',
  CHANNEL: '/channel',
  FRIENDS: '/friends',
};

const socketEV = {
  // Channel events
  CREATE_ROOM: 'create-room',
  DEL_ROOM: 'delete-room',
  JOIN_ROOM: 'join-room',
  LEAVE_ROOM: 'leave-room',
  ROOM_MESSAGE: 'room-message',

  // Game events
  CREATE_GAME: 'create-game',
  JOIN_GAME: 'join-game',
  JOIN_TEAM: 'join-team',
  LEAVE_GAME: 'leave-game',
  LEAVE_TEAM: 'leave-team',
  START_GAME: 'start-game',
  CAN_START: 'can-start',
  END_GAME: 'end-game',
  HINT_RESPONSE: 'hint-response',
  PLAYER_DRAWING: 'player-drawing',
  PLAYER_GUESSING: 'player-guessing',
  NEXT_ROUND: 'next-round',
  END_GUESS_DRAW: 'end-guess-draw',
  UPDATE_HALL: 'update-hall',
  UPDATE_LOBBY: 'update-lobby',
  GAME_CLOCK: 'game-clock',
  RIGHT_OF_REPLY: 'right-of-reply',
  SEND_HINT: 'send-hint',
  TEAM_POINT: 'team-point',
  GAME_CHAT_ACK: 'game-chat-ack',

  // Friend request
  CONNECT_FRIENDS: 'connect-friends',
  FRIEND_REQUEST: 'friend-request',
  REQ_RESPOND: 'req-respond',
};
export { socketNS, socketEV };
