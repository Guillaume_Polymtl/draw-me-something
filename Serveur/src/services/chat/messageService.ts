import { firestore } from 'firebase-admin';
import { DocRef } from '../../config/customTypes';
import { ChannelNotFoundError } from '../../errors/channelErrors';
import { UserNotFoundError } from '../../errors/usersErrors';
import MessageModel from '../../models/message.model';
import { db } from '../firebaseInit';

export default class MessageService {
  static async addMessageToChannel(channelId: string, messageObj: MessageModel): Promise<void> {
    const userDocs = await db.collection('userAuth').where('pseudo', '==', messageObj.pseudo).get();
    const channelRef = db.collection('channels').where('channelName', '==', channelId);

    const channelDocs = await channelRef.get();

    if (channelDocs.empty) {
      throw new ChannelNotFoundError();
    }

    if (userDocs.empty) {
      throw new UserNotFoundError();
    }

    await channelDocs.docs[0].ref.update({
      messageList: firestore.FieldValue.arrayUnion({
        senderRef: userDocs.docs[0].ref,
        message: messageObj.message,

        date: Date.now(),
      }),
    });
  }

  static async getMessagesFromChannel(channelId: string): Promise<MessageModel[]> {
    let messagePromises: Promise<MessageModel>[] = [];
    const channelDocs = await db.collection('channels').where('channelName', '==', channelId).get();
    if (channelDocs.empty) {
      throw new ChannelNotFoundError();
    }

    const { messageList } = channelDocs.docs[0].data();
    messagePromises = messageList.map(async (message: { senderRef: DocRef; message: string; date: number }) => {
      let showPseudo = '';
      if ((await message.senderRef.get()).data()) {
        showPseudo = (await message.senderRef.get()).data().pseudo;
      }
      const messageModel: MessageModel = {
        pseudo: showPseudo,
        message: message.message,
        date: message.date,
      };
      return messageModel;
    });

    return await Promise.all(messagePromises);
  }
}
