import { DocRef } from '../../config/customTypes';
import { ChannelCollisionError, ChannelNotFoundError, UnauthorizedChannelDeletionError } from '../../errors/channelErrors';
import ChannelModel from '../../models/channel.model';
import { admin, db } from '../firebaseInit';
import GetterService from '../getterService';

export default class ChannelService {
  static async createNewChannel(newChannel: ChannelModel): Promise<void> {
    const docRef = db.collection('channels').where('channelName', '==', newChannel.channelName);
    if (!(await docRef.get()).empty) {
      throw new ChannelCollisionError();
    }
    // Existence validated in the middleware
    const creatorDocs = await db.collection('userAuth').where('pseudo', '==', newChannel.pseudo).get();
    delete newChannel.pseudo;
    await db
      .collection('channels')
      .doc()
      .set({ ...newChannel, ownerRef: creatorDocs.docs[0].ref, subscribedUsers: [], messageList: [] });
  }

  // TODO: Send creator
  static async subscribeToChannel(channelName: string, newUserPseudo: string): Promise<void> {
    const channelRef = (await GetterService.getChannelDoc(channelName, true)) as DocRef;
    const userRef = (await GetterService.getUserDoc(newUserPseudo, true)) as DocRef;

    // Set some new fields in the doc without overwriting its content
    await channelRef.update({
      subscribedUsers: admin.firestore.FieldValue.arrayUnion(userRef),
    });
  }

  static async unsubscribeFromChannel(channelName: string, pseudo: string): Promise<void> {
    const channelRef = (await GetterService.getChannelDoc(channelName, true)) as DocRef;
    const userRef = (await GetterService.getUserDoc(pseudo, true)) as DocRef;

    // Set some new fields in the doc without overwriting its content
    await channelRef.update({
      subscribedUsers: admin.firestore.FieldValue.arrayRemove(userRef),
    });
  }

  static async deleteChannel(deleterPseudo: string, channelId: string): Promise<void> {
    const channelDocs = await db.collection('channels').where('channelName', '==', channelId).get();
    if (channelDocs.empty) {
      throw new ChannelNotFoundError();
    }

    const { ownerRef } = channelDocs.docs[0].data() as ChannelModel;
    const pseudo = await GetterService.getPseudoFromRef(ownerRef);
    if (pseudo !== deleterPseudo) {
      throw new UnauthorizedChannelDeletionError();
    }

    await channelDocs.docs[0].ref.delete();
  }

  static async getSubscribedUsers(channelId: string): Promise<{ pseudo: string; avatar: string }[]> {
    const channelDoc = (await GetterService.getChannelDoc(channelId)) as ChannelModel;
    const { subscribedUsers } = channelDoc;
    const subscribedUsersPromises = subscribedUsers.map(async (userRef: DocRef) => {
      const userDoc = await GetterService.getUserDocFromRef(userRef);

      // The user account could have been deleted.
      if (userDoc) {
        const { pseudo, avatar } = userDoc;
        return { pseudo, avatar };
      }
    });
    let subs = await Promise.all(subscribedUsersPromises);

    // Returns false on falsy values using the Boolean constructor.
    subs = subs.filter(Boolean);
    return subs;
  }

}
