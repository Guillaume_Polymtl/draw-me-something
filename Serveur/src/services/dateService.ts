export default class DateService {

  static getFormatedDateStr(unixTimeMs: number): string {
    const date = new Date(unixTimeMs);
    const year = date.getUTCFullYear();
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    return `${day}/${month}/${year}`;
  }
}
