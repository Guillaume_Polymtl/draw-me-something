import potrace from 'potrace';
import { ImageConversionError } from '../../errors/drawingErrors';
import PotraceSettingsModel from '../../models/potraceSettings.model';

export default class PotraceService {
  static traceFromImage(imgBuffer: Buffer, potraceOptions?: PotraceSettingsModel): Promise<string> {
    const params = potraceOptions ?? {};

    return new Promise<string>((resolve, reject) => {
      potrace.trace(imgBuffer, params, (err: Error, svg: string) => {
        if (err) {
          reject(new ImageConversionError());
        }
        resolve(svg);
      });
    });
  }
}
