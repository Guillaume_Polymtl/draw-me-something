import { DocRef } from '../config/customTypes';
import { ChannelNotFoundError } from '../errors/channelErrors';
import { UserNotFoundError } from '../errors/usersErrors';
import ChannelModel from '../models/channel.model';
import { GroupGame, GroupGameDisplay } from '../models/game/groupGame.model';
import Team from '../models/game/team.model';
import TeamMember from '../models/game/teamMember.model';
import { publicInfo, UserModel } from '../models/user.model';
import { privateInfo } from './../models/user.model';
import { db } from './firebaseInit';
import AbstractGame from './game/AbstractGame';
export default class GetterService {
  // Returns a user document ref or model based on pseudo
  static async getUserDoc(pseudo: string, getRef?: boolean): Promise<UserModel | DocRef> {
    const userDocs = await db.collection('userAuth').where('pseudo', '==', pseudo).get();
    if (userDocs.empty) {
      throw new UserNotFoundError();
    }

    const user = userDocs.docs[0];
    if (getRef) {
      return user.ref;
    }
    return user.data() as UserModel;
  }

  // Returns a pseudo based on a given valid ref
  static async getPseudoFromRef(userRef: DocRef): Promise<string> {
    let pseudo = '';
    if ((await userRef?.get())?.data()) {
      pseudo = (await userRef.get()).data().pseudo;
    }
    return pseudo;
  }

  // Returns a user doc based on a given valid ref
  static async getUserDocFromRef(userRef: DocRef): Promise<UserModel> {
    const gotUserRef = await userRef.get();
    if (!gotUserRef.exists) {
      throw new UserNotFoundError(userRef.path);
    }

    return gotUserRef.data() as UserModel;
  }

  // Returns a channel doc or ref based on its id
  static async getChannelDoc(channelId: string, getRef?: boolean): Promise<ChannelModel | DocRef> {
    const channelDocs = await db.collection('channels').where('channelName', '==', channelId).get();
    if (channelDocs.empty) {
      throw new ChannelNotFoundError();
    }
    const channel = channelDocs.docs[0];

    return getRef ? channel.ref : (channel.data() as ChannelModel);
  }

  // Transforms the concurrent games map to a displayable array with filtered informations
  static getConcurrentGamesDisplay(concurrentGames: Map<string, AbstractGame>): GroupGameDisplay[] {
    const concurrentGamesDisplay: GroupGameDisplay[] = [];

    concurrentGames.forEach(({ gameObject }) => {
      const displayable: GroupGameDisplay = {
        canBeJoined: gameObject.canBeJoined,
        difficulty: gameObject.difficulty,
        gameMode: gameObject.gameMode,
        gameUID: gameObject.gameUID,
        players: gameObject.players,
        name: gameObject.name,
      };
      if (gameObject.canBeJoined) {
        concurrentGamesDisplay.push(displayable);
      }
    });

    return concurrentGamesDisplay;
  }

  // Transforms a single game object t to a displayable object with filtered informations
  static groupGameToDisplayable(game: GroupGame): GroupGameDisplay {
    const teams = game.teams ?? [];
    const players = this.getPlayersArrFromTeams(teams);
    const groupGameDisplayable = {
      canBeJoined: game.canBeJoined,
      difficulty: game.difficulty,
      gameMode: game.gameMode,
      gameUID: game.gameUID,
      name: game.name,
      players,
    };
    return groupGameDisplayable;
  }

  // Returns a pseudo array from a team
  static getPlayersArrFromTeams(teamsArr: Team[]): string[] {
    const playersPseudoArr: string[] = [];
    teamsArr.forEach((team: Team) => {
      team.teamMembers.forEach((member: TeamMember) => {
        playersPseudoArr.push(member.pseudo);
      });
    });

    return playersPseudoArr;
  }

  static filterUserModelAttributes(user: UserModel, showPrivate?: boolean) {
    const propsArray = showPrivate ? privateInfo : publicInfo;
    Object.keys(user).forEach((keyAttribute) => {
      if (!propsArray.includes(keyAttribute)) {
        delete user[keyAttribute];
      }
    });
    return user;
  }
}
