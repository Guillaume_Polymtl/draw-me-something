import 'reflect-metadata';

import dotenv from 'dotenv';
import { container } from './config/inversify.config';
import Types from './config/types';
import Server from './server';

dotenv.config();
const server: Server = container.get<Server>(Types.Server);

const PORT = process.env.PORT ?? 80;
// server.init(parseInt(PORT.toString(), 10));
server.init(80);
