import Vec2 from './vec2.model';

export default interface Point {
  position: Vec2;
  time: number;
}
