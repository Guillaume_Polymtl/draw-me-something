export class Color {
  red: number;
  green: number;
  blue: number;
  alpha: number;

  static colorToString(colorObj: { red: number; green: number; blue: number; alpha: number }) {
    return `rgb(${colorObj.red},${colorObj.green},${colorObj.blue},${colorObj.alpha})`;
  }

  constructor(hexString: string) {
    const colorData = hexString.split('#')[1];
    const r = parseInt(colorData.substring(0, 2), 16);
    const g = parseInt(colorData.substring(2, 4), 16);
    const b = parseInt(colorData.substring(4, 6), 16);
    const a = colorData.length >= 8 ? parseInt(colorData.substring(6, 8), 16) : 255;
    this.red = r;
    this.green = g;
    this.blue = b;
    this.alpha = a;
  }

  get value(): string {
    return `rgb(${this.red},${this.green},${this.blue})`;
  }

  getObj() {
    return { red: this.red, green: this.green, blue: this.blue, alpha: this.alpha };
  }
}
