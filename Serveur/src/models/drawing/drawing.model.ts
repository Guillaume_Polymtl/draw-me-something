import PotraceSettingsModel from '../potraceSettings.model';
import Stroke from './stroke.model';
import Vec2 from './vec2.model';

export enum DrawingMode {
  CONVENTIONAL = 'Conventionnel',
  RANDOM = 'Aléatoire',
  PANORAMIC = 'Panoramique',
  CENTER = 'Centré',
}

export enum CenterMode {
  INWARDS = `Vers l'intérieur`,
  OUTWARDS = `Vers l'extérieur`,
}

export enum PanoramicMode {
  LEFT_RIGHT = 'Gauche à droite',
  RIGHT_LEFT = 'Droite à gauche',
  TOP_BOTTOM = 'Haut en bas',
  BOTTOM_TOP = 'Bas en haut',
}

export enum CreationMode {
  MANUAL1 = 'Manuel 1',
  MANUAL2 = 'Manuel 2',
  ASSISTED1 = 'Assisté 1',
  ASSISTED2 = 'Assisté 2',
  // ASSISTED3 = 'Assisté 3'
}

export interface DrawingBase {
  word?: string;
  hints?: string[];
  difficulty?: string;
  creationMode?: CreationMode;
  centerMode?: CenterMode;
  panoramicMode?: PanoramicMode;
  drawingMode?: DrawingMode;
  size?: Vec2;
  background?: string;
}

export interface Drawing extends DrawingBase {
  strokes: Stroke[];
}

export interface DrawingToPotrace extends DrawingBase {
  potraceOptions?: PotraceSettingsModel;
  imageBase64: string;
}
