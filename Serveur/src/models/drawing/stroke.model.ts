import Point from './point.model';

export default interface Stroke {
  tool: string;
  points: Point[];
  color: { red: number; green: number; blue: number; alpha: number };
  width: number;
  index?: number;
}
