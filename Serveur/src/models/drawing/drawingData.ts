export default class DrawingData {
  x: number[];
  y: number[];
  start: boolean[];
  end: boolean[];
  r: number[];
  g: number[];
  b: number[];
  a: number[];
  width: number[];
  toolType: string[];
  gameUID: string;

  constructor() {
    this.x = [];
    this.y = [];
    this.start = [];
    this.end = [];
    this.r = [];
    this.g = [];
    this.b = [];
    this.a = [];
    this.width = [];
    this.toolType = [];
    this.gameUID = '';
  }
}
