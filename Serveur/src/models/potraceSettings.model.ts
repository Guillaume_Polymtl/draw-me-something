export default interface PostraceSettingsModel {
  alphaMax?: number;
  background?: string;
  blackOnWhite?: boolean;
  color?: string;
  optCurve?: boolean;
  optTolerance?: number;
  threshold?: number;
  turdSize?: number;
}
