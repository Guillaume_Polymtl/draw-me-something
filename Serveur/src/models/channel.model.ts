import { DocRef } from '../config/customTypes';
export default interface ChannelModel {
  pseudo: string; // channel owner (creator)
  channelName: string;
  subscribedUsers?: DocRef[];
  messageList?: string[];
  ownerRef?: DocRef;
  isGameChannel?: boolean;
}
