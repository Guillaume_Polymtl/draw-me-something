import { DocRef } from './../config/customTypes';
import ClassicStats from './game/stats/classicStats.model';
import SoloStats from './game/stats/soloStats.model';

export interface UserModel {
  pseudo: string;
  password: string;

  // Optionnal parameters
  firstName?: string;
  lastName?: string;
  logged?: boolean;
  loginHistory?: number[];
  logoutHistory?: number[];
  avatar?: string;
  description?: string;
  isVirtual?: boolean;

  // Only used for user patching
  newPseudo?: string;
  classicStats?: ClassicStats;
  soloStats?: SoloStats;

  inbound?: { from: DocRef; date: number }[];
  outbound?: { to: DocRef; date: number }[];
  friends?: DocRef[];
}

export const publicInfo = ['pseudo', 'avatar', 'description'];

export const privateInfo = [...publicInfo, 'firstName', 'lastName', 'loginHistory', 'logoutHistory'];
