export default interface MessageModel {
  pseudo: string;
  message: string;
  date?: number;
}
