export default interface SocketMessageModel {
  message: string;
  pseudo: string;
  channelId: string;
}
