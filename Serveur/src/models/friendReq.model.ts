export default interface FriendReqModel {
  from: string;
  to: string;

  date?: number;
  accepted?: boolean;
}
