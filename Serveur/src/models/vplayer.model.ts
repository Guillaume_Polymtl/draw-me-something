export default interface VPlayerModel {
  pseudo: string;
  avatar: string;
  isVirtual: boolean;
}
