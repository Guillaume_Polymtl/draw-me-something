import { Drawing } from './drawing/drawing.model';
import PostraceSettingsModel from './potraceSettings.model';

export interface PotracePreviewModel {
  potraceOptions?: PostraceSettingsModel;
  imageBase64: string;
  drawing: Drawing;
}
