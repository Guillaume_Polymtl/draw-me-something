import { DocRef, GameDifficulty } from '../../config/customTypes';
import { GameMode } from './../../config/customTypes';
import { GameModel } from './game.model';
import Team from './team.model';

export interface GroupGame extends GameModel {
  canBeJoined: boolean;
  gameUID: string;
  winner?: Team;
  looser?: Team;
  teams?: Team[];
}

export interface GroupGameSummary {
  teams: Team[];
  winner: Team;
  elapsedTime: number;
}

export interface SoloGameSummary {
  elapsedTime: number;
  score: number;
  isPersonalRecord: boolean;
  isGlobalRecord: boolean;
}

export interface GroupGameDisplay {
  gameUID: string;
  difficulty: string;
  gameMode: string;
  canBeJoined: boolean;
  players: string[];
  name: string;
}
