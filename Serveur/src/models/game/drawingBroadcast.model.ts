interface PointBroadcast {
  x: number;
  y: number;
}

export default interface DrawingBroadcast {
  gameUID: string;

  toolSize: number;
  toolType?: string;
  // Right now, the drawing is transmitted point-by-point
  position: PointBroadcast;
  color: string;
}
