import Team from './team.model';

export default interface RoundModel {
  artist: string; // empty string if none
  guessers: string[];
  roundNum: number;
  timeToGuess: number;
  wordToGuess: string;
  vplayerDrawing?: boolean;
  soloSprintScore?: number;
}
