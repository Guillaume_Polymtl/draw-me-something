import { DocRef } from '../../config/customTypes';

export default interface TeamMember {
  pseudo: string;
  teamUID: string;

  isVirtual?: boolean;
  avatar?: string;
  docRef?: DocRef | undefined;
}
