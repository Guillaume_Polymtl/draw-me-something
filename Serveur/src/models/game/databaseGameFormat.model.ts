import { DocRef, GameDifficulty, GameMode } from '../../config/customTypes';

export default interface DatabaseGameFormat {
  playersRef: DocRef[];
  startTime: number;
  formattedWinningTeam: {
    teamMembers: DocRef[]
    teamUID: string
    points: number
  };
  formattedLosingTeam: {
    teamMembers: DocRef[]
    teamUID: string
    points: number
  };
  roundNum: number;
  name: string;
  difficulty: GameDifficulty;
  gameMode: GameMode;
  elapsedTime: number;
}