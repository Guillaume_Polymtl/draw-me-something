import Stroke from '../drawing/stroke.model';

export default interface WordHints {
  word: string;
  hints: string[];
  strokes?: Stroke[];
}