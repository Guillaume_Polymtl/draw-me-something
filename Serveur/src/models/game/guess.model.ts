export default interface GuessModel {
  guessContent: string;
  gameUID: string;
  pseudo: string;
  proximity: number; // See rules in GameRulesService.ts
  isGoodGuess?: boolean;
  triesLeft?: number;
}
