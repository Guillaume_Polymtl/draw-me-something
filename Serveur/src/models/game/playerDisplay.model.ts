export default interface PlayerDisplayModel {
  pseudo: string;
  avatar: string;
}
