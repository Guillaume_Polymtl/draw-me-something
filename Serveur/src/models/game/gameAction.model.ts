import TeamMember from './teamMember.model';

export interface JoinLeaveGame {
  gameUID: string;
  pseudo: string;
}

export interface JoinLeaveTeam {
  gameUID: string;
  teamMember: TeamMember;
}