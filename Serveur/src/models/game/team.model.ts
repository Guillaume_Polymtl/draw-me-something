import TeamMember from './teamMember.model';

export default interface Team {
  teamMembers: TeamMember[];
  teamUID: string;
  points: number;
}
