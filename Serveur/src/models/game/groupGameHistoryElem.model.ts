export default interface GroupGameHistoryElem {
  winningTeam: {
    players: { pseudo: string; avatar: string, isVirtual?: boolean }[];
    score: number;
    name: string;
  };
  losingTeam: {
    players: { pseudo: string; avatar: string, isVirtual?: boolean }[];
    score: number;
    name: string;
  };
  roundNum: number;
  startTime: number;
  difficulty: string;
  gameMode: string;
  name: string;
}
