import GameStats from './gameStats.model';

export default interface ClassicStats extends GameStats {
    wins: number;
}