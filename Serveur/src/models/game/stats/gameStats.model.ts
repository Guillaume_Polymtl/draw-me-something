export default interface GameStats {
  score: number;
  timePlayedMs: number;
  numberPlayed: number;

  wins?: number;
  bestScore?: number;
}
