import GameStats from './gameStats.model';

export default interface SoloStats extends GameStats {
  bestScore: number;
}