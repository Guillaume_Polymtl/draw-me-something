import { GameDifficulty, GameMode } from '../../config/customTypes';
import Team from './team.model';

export interface GameModel {
  difficulty: GameDifficulty;
  gameMode: GameMode;
  name: string;
  players: string[];

  gameUID?: string;
  canBeJoined?: boolean;
  roundNum?: number;
  startTime?: number;
  winner?: Team | string[];
  elapsedTime?: number;
}
