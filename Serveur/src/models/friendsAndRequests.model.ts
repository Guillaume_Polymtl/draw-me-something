import { UserModel } from './user.model';

export default interface FriendsAndRequests {
  inbound: { from: UserModel; date: number }[];
  outbound: { to: UserModel; date: number }[];

  friends: UserModel[];
}
