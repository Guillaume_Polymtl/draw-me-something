import { Request, Response } from 'express';
import { matchedData, validationResult } from 'express-validator';
import { ChannelNotFoundError } from '../errors/channelErrors';
import { UserNotFoundError } from '../errors/usersErrors';
import ChannelModel from '../models/channel.model';
import MessageModel from '../models/message.model';
import ChannelService from '../services/chat/channelService';
import MessageService from '../services/chat/messageService';
import { ChannelCollisionError, UnauthorizedChannelDeletionError } from './../errors/channelErrors';
export default class ChannelController {
  /**
   * Creates a new channel in the database if it doesnt exists
   */
  async createNewChannel(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }
    try {
      const newChannel = matchedData(req, { locations: ['body'] }) as ChannelModel;
      await ChannelService.createNewChannel(newChannel);
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof ChannelCollisionError) {
        return res.status(409).send({
          message: 'channelCollisionError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async postMessage(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    const { channelId } = req.params;

    try {
      const message = matchedData(req, { locations: ['body'] }) as MessageModel;

      await MessageService.addMessageToChannel(channelId, message);
      res.send(true);
    } catch (error) {
      console.error(error);

      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getMessagesFromChannel(req: Request, res: Response) {
    const { channelId } = req.params;

    try {
      const messageList = await MessageService.getMessagesFromChannel(channelId);
      res.send(messageList);
    } catch (error) {
      console.error(error);

      if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async deleteExistingChannel(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }
    const { channelId } = req.params;
    const { pseudo } = req.headers;
    try {
      await ChannelService.deleteChannel(pseudo.toString(), channelId);
      return res.send(true);
    } catch (error) {
      if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else if (error instanceof UnauthorizedChannelDeletionError) {
        return res.status(401).send({
          message: 'UnauthorizedChannelDeletion',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async joinChannel(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    const { channelId } = req.params;

    try {
      const { pseudo } = matchedData(req, { locations: ['body'] });
      await ChannelService.subscribeToChannel(channelId, pseudo);
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async leaveChannel(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }
    const { channelId } = req.params;

    try {
      const { pseudo } = matchedData(req, { locations: ['body'] });
      await ChannelService.unsubscribeFromChannel(channelId, pseudo);
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getSubscribedUsers(req: Request, res: Response) {
    const { channelId } = req.params;
    try {
      const subscribedPseudos = await ChannelService.getSubscribedUsers(channelId);
      return res.send(subscribedPseudos);
    } catch (error) {
      console.error(error);
      if (error instanceof ChannelNotFoundError) {
        return res.status(404).send({
          message: 'channelNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }
}
