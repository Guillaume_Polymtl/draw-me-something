import { Request, Response } from 'express';
import { matchedData, validationResult } from 'express-validator';
import { GameDifficulty } from '../config/customTypes';
import { PotracePreviewModel } from '../models/potracePreview.model';
import PairWordService from '../services/drawing/pairWordService';
import RandomWordService from '../services/game/randomWordService';
import { ImageConversionError, ImageTooVoluminousError } from './../errors/drawingErrors';
import { CreationMode, Drawing, DrawingBase, DrawingToPotrace } from './../models/drawing/drawing.model';

export default class PairWordController {
  async addPairWordDrawing(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }
    try {
      const drawing = matchedData(req, { locations: ['body'] });
      const { pseudo } = req.body;

      if (drawing.creationMode === CreationMode.ASSISTED1) {
        await PairWordService.addPairWordDrawingFromImg(pseudo, drawing as DrawingToPotrace);
      } else {
        await PairWordService.addPairWordFromStrokes(pseudo, drawing as Drawing);
      }
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof ImageConversionError) {
        return res.status(500).send({
          message: 'imageConversionError',
          error,
        });
      } else if (error instanceof ImageTooVoluminousError) {
        return res.status(413).send({
          message: 'imageTooVoluminousError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getPairsPaginated(req: Request, res: Response) {
    const { amount, pageNum } = req.params;

    try {
      const drawings = await PairWordService.getPairsPaginated(parseInt(amount, 10), parseInt(pageNum, 10));
      return res.send(drawings);
    } catch (error) {
      return res.status(500).send({
        message: 'internalError',
        error,
      });
    }
  }

  async getPotracePreview(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    const potraceData = req.body as PotracePreviewModel;
    try {
      const drawing = await PairWordService.getPotracePreview(potraceData);

      return res.send(drawing);
    } catch (error) {
      console.error(error);
      if (error instanceof ImageConversionError) {
        return res.status(500).send({
          message: 'imageConversionError',
          error,
        });
      } else if (error instanceof ImageTooVoluminousError) {
        return res.status(413).send({
          message: 'imageTooVoluminousError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getRandomPairWordDrawing(req: Request, res: Response) {
    const { difficulty } = req.params;
    try {
      const pair = await RandomWordService.getRandomPairWordDrawing(difficulty as GameDifficulty);
      return res.send(pair);
    } catch (error) {
      return res.status(500).send({
        message: 'internalError',
        error,
      });
    }
  }
}
