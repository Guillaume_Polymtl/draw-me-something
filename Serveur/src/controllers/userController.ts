import { Request, Response } from 'express';
import { matchedData, validationResult } from 'express-validator';
import { GameStatsType } from '../config/customTypes';
import { UserNotFoundError } from '../errors/usersErrors';
import UserService from '../services/users/userService';
import { UserAlreadyExistingError } from './../errors/usersErrors';
import { UserModel } from './../models/user.model';

export default class UserController {
  /**
   * Gets an existing user in the database by using
   * his/her pseudo
   */
  async getUserByPseudo(req: Request, res: Response) {
    const { searchpseudo } = req.params;
    const { pseudo } = req.headers;

    try {
      const user = await UserService.getUserByPseudo(searchpseudo, pseudo.toString());
      return res.send(user);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async patchUserAccount(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    try {
      const user = matchedData(req, { locations: ['body'] }) as UserModel;
      await UserService.patchUserAccount(user);
      res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }
  async getAllSubscribedChannels(req: Request, res: Response) {
    const { pseudo } = req.headers;
    try {
      const channels = await UserService.getSubscribedChannels(pseudo.toString());
      console.log(channels);
      return res.send(channels);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async deleteUserAccount(req: Request, res: Response) {
    const { pseudo } = req.params;
    try {
      await UserService.deleteUserAccount(pseudo);
      res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getAllUnsubscribedChannels(req: Request, res: Response) {
    const { pseudo } = req.headers;
    try {
      const channels = await UserService.getUnsubscribedChannels(pseudo.toString());
      return res.send(channels);
    } catch (error) {
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getUserGameStatsFromMode(req: Request, res: Response) {
    const { statsMode, pseudo } = req.params;

    try {
      const gameStats = await UserService.getUserGameStatsFromMode(pseudo, statsMode as GameStatsType);
      return res.send(gameStats);
    } catch (error) {
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getAllUsers(req: Request, res: Response) {
    try {
      const { pseudo } = req.body;
      const allUsers: { pseudo: string, avatar: string }[] = await UserService.getAllOtherUsers(pseudo);
      return res.send(allUsers);
    } catch (error) {
      return res.status(500).send({
        message: 'internalError',
        error,
      });
    }
  }

  async getProfileData(req: Request, res: Response) {
    const { searchpseudo } = req.params;
    try {
      const completeProfileData = await UserService.getCompleteProfileData(searchpseudo);
      return res.send(completeProfileData);
    } catch (error) {
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

}
