import { Request, Response } from 'express';
import FriendReqModel from '../models/friendReq.model';
import FriendsAndRequests from '../models/friendsAndRequests.model';
import FriendsService from '../services/users/friendsService';
import { FriendRequestConflictError, UserNotFoundError } from './../errors/usersErrors';

export default class FriendsController {
  async sendFriendRequest(req: Request, res: Response) {
    const { pseudo } = req.headers;
    const { to } = req.body;
    try {
      await FriendsService.addPendingFriendRequest({ from: pseudo.toString(), to });
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof FriendRequestConflictError) {
        return res.status(409).send({
          message: 'friendRequestCollisionError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async answerToFriendReq(req: Request, res: Response) {
    const { pseudo } = req.headers;
    const { to, accepted } = req.body as FriendReqModel;

    try {
      await FriendsService.respondToFriendReq({ from: pseudo.toString(), to, accepted });
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getFriendsAndRequests(req: Request, res: Response) {
    const { pseudo } = req.headers;

    try {
      const friendsAndReqs: FriendsAndRequests = await FriendsService.getFriendsAndRequests(pseudo.toString());
      return res.send(friendsAndReqs);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }
}
