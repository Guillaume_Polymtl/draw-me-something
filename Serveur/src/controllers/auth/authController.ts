import { Request, Response } from 'express';
import { matchedData, validationResult } from 'express-validator';
import { BadCredentialsError, UserAlreadyExistingError, UserAlreadyLoggedError, UserNotFoundError } from '../../errors/usersErrors';
import { UserModel } from '../../models/user.model';
import AuthService from './authService';

export default class AuthController {
  async userLogIn(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    const { pseudo, password } = matchedData(req, { locations: ['body'] });

    try {
      const token = await AuthService.loginUser(pseudo, password);
      return res.send(token);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof UserAlreadyLoggedError) {
        return res.status(409).send({
          message: 'userAlreadyLoggedError',
          error,
        });
      } else if (error instanceof BadCredentialsError) {
        return res.status(400).send({
          message: 'badCredentialsError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async userLogOut(req: Request, res: Response) {
    const { pseudo } = req.body;

    try {
      await AuthService.logOutUser(pseudo);
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async userSignUp(req: Request, res: Response) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send(errors);
    }

    const newUser = matchedData(req, { locations: ['body'] }) as UserModel;

    try {
      console.log();
      await AuthService.signUpUser(newUser);
      return res.send(true);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else if (error instanceof UserAlreadyExistingError) {
        return res.status(409).send({
          message: 'userConflictError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }
}
