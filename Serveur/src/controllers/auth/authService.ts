import * as bcrypt from 'bcrypt';
import { UserAlreadyExistingError, UserAlreadyLoggedError, UserNotFoundError } from '../../errors/usersErrors';
import { UserModel } from '../../models/user.model';
import { admin, db } from '../../services/firebaseInit';

const SALT_ROUNDS = 10;
const DEFAULT_AVATAR =
  'https://firebasestorage.googleapis.com/v0/b/projet3-log3900.appspot.com/o/images%2Favatar.png?alt=media&token=1451ad30-0d4a-4c91-827c-5eb0b66e12ee';

export default class AuthService {
  // We could add login timestamps here
  static async loginUser(pseudo: string, password: string) {
    let authToken = '';
    const userCollRef = db.collection('userAuth');
    const userDocs = await userCollRef.where('pseudo', '==', pseudo).get();

    if (userDocs.empty) {
      throw new UserNotFoundError();
    }
    const userDoc = userDocs.docs[0];
    const { logged, hash } = userDoc.data();

    if (logged) {
      throw new UserAlreadyLoggedError();
    }

    const isValid = await bcrypt.compare(password, hash);

    if (isValid) {
      authToken = await admin.auth().createCustomToken(pseudo);
      userDoc.ref.set(
        {
          token: authToken,
          loginHistory: admin.firestore.FieldValue.arrayUnion(Date.now()),
          logged: true,
        },
        { merge: true },
      );
    } else {
      throw new UserNotFoundError();
    }
    return { authToken };
  }

  static async signUpUser(newUser: UserModel): Promise<void> {
    const userCollRef = db.collection('userAuth');

    const userDocs = await userCollRef.where('pseudo', '==', newUser.pseudo).get();

    // Query returned  an element: the user already exists
    if (!userDocs.empty) {
      throw new UserAlreadyExistingError();
    }

    const salt = bcrypt.genSaltSync(SALT_ROUNDS);

    const hashed = await bcrypt.hash(newUser.password, salt);
    await db
      .collection('userAuth')
      .doc()
      .set({
        pseudo: newUser.pseudo,
        hash: hashed,
        firstName: newUser.firstName ?? '',
        lastName: newUser.lastName ?? '',
        description: newUser.description ?? '',
        avatar: DEFAULT_AVATAR,
      });
  }

  static async logOutUser(pseudo: string): Promise<void> {
    const userCollRef = db.collection('userAuth');
    const userDocs = await userCollRef.where('pseudo', '==', pseudo).get();

    if (userDocs.empty) {
      throw new UserNotFoundError();
    }
    const userDoc = userDocs.docs[0];
    await userDoc.ref.set(
      {
        logoutHistory: admin.firestore.FieldValue.arrayUnion(Date.now()),
        logged: false,
        token: '',
      },
      { merge: true },
    );
  }
}
