import { Request, Response } from 'express';
import { UserNotFoundError } from '../errors/usersErrors';
import { GameModel } from '../models/game/game.model';
import GroupGameHistoryElem from '../models/game/groupGameHistoryElem.model';
import { UserModel } from '../models/user.model';
import GameService from '../services/game/gameService';
import { GameModeNotFoundError } from './../errors/gameErrors';

export default class GameController {
  async getGameRankingFromMode(req: Request, res: Response) {
    const { mode } = req.params;

    try {
      const rankInDescOrder: UserModel[] = await GameService.getRankingFromMode(mode);
      return res.send(rankInDescOrder);
    } catch (error) {
      if (error instanceof GameModeNotFoundError) {
        return res.status(404).send({
          message: 'gameModeNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getClassicGameHistory(req: Request, res: Response) {
    const { onlywins, searchpseudo, vplayer } = req.query;
    const pseudoToSearch = searchpseudo ?? req.body.pseudo;
    try {
      const gameHist: GroupGameHistoryElem[] = await GameService.getClassicGameHistory(pseudoToSearch, !!onlywins, vplayer?.toString());
      return res.send(gameHist);
    } catch (error) {
      console.error(error);
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }

  async getSoloSprintGameHistory(req: Request, res: Response) {
    const { getbeststreak, searchpseudo } = req.query;
    const pseudoToSearch = searchpseudo ?? req.body.pseudo;

    try {
      const gameHist: GameModel[] = await GameService.getSoloSprintGameHistory(pseudoToSearch, Boolean(getbeststreak));
      return res.send(gameHist);
    } catch (error) {
      if (error instanceof UserNotFoundError) {
        return res.status(404).send({
          message: 'userNotFoundError',
          error,
        });
      } else {
        return res.status(500).send({
          message: 'internalError',
          error,
        });
      }
    }
  }
}
