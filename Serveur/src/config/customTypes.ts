export type DocRef = FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;
export type GameDifficulty = 'Normale' | 'Difficile';
export type GameStatsType = 'classicStats' | 'soloStats';
export type GameMode = 'Classique' | 'Sprint solo';
