export default {
  App: Symbol('App'),
  Server: Symbol('Server'),
  SocketManager: Symbol('SocketManager'),
  SocketChannelService: Symbol('SocketChannelService'),
};
