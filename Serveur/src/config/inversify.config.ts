import { Container } from 'inversify';
import App from '../app';
import Server from '../server';
import { SocketChannelService } from '../services/sockets/socketChannelService';
import { SocketManager } from '../services/sockets/socketManager';
import Types from './types';

const container: Container = new Container();

container.bind(Types.App).to(App);
container.bind(Types.Server).to(Server);
container.bind(Types.SocketManager).to(SocketManager);
container.bind(Types.SocketChannelService).to(SocketChannelService);

export { container };
