import * as http from 'http';
import { inject, injectable } from 'inversify';
import App from './app';
import Types from './config/types';
import { bobGameBeginning } from './services/game/virtual-players/bobSpeech';
import SpeechService from './services/game/virtual-players/speechService';
import { SocketManager } from './services/sockets/socketManager';
@injectable()
export default class Server {
  private server: http.Server;

  constructor(@inject(Types.App) private application: App, @inject(Types.SocketManager) private socketManager: SocketManager) {}

  init(port: number): void {
    this.application.app.set('port', port);

    this.server = http.createServer(this.application.app);
    this.server.listen(port);
    this.server.on('listening', () => this.onListening());

    this.socketManager.initSocket(this.server);
  }

  private onListening(): void {
    const addr = this.server.address();
    // tslint:disable-next-line:no-non-null-assertion
    const bind: string = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr!.port}`;
    // tslint:disable-next-line:no-console
    console.log(`Listening on ${bind}`);
  }
}
