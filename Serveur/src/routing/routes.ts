import Router from 'express';
import AuthController from '../controllers/auth/authController';
import ChannelController from '../controllers/channelController';
import FriendsController from '../controllers/friendsController';
import GameController from '../controllers/gameController';
import PairWordController from '../controllers/pairWordController';
import UserController from '../controllers/userController';
import { channelCreationValidator, channelJoinValidator } from '../validation/channelValidation';
import { chatMessageValidator } from '../validation/messageValidation';
import { pairWordValidator, potracePreviewValidator } from '../validation/pairWordValidator';
import { userPatchValidator, userSignInValidator, userSignUpValidator } from '../validation/usersValidation';
import { isAuthenticated } from './middlewares';

const router = Router();
const authController = new AuthController();
router.post('/users/signup', userSignUpValidator, authController.userSignUp);
router.patch('/users/login', userSignInValidator, authController.userLogIn);
router.patch('/users/logout', isAuthenticated, authController.userLogOut);

const userController = new UserController();
router.get('/users/account/:searchpseudo', isAuthenticated, userController.getUserByPseudo);
router.get('/users/subscribedChannels', userController.getAllSubscribedChannels);
router.get('/users/unsubscribedChannels', userController.getAllUnsubscribedChannels);
router.get('/users/gamestats/:statsMode/:pseudo', isAuthenticated, userController.getUserGameStatsFromMode);
router.get('/users', isAuthenticated, userController.getAllUsers);
router.get('/users/profiledata/:searchpseudo', isAuthenticated, userController.getProfileData);

router.patch('/users/account', isAuthenticated, userPatchValidator, userController.patchUserAccount);
router.delete('/users/account', isAuthenticated, userController.deleteUserAccount);

const channelController = new ChannelController();
router.post('/channels', isAuthenticated, channelCreationValidator, channelController.createNewChannel);

router.patch('/channels/join/:channelId', isAuthenticated, channelJoinValidator, channelController.joinChannel);
router.patch('/channels/chat/:channelId', isAuthenticated, chatMessageValidator, channelController.postMessage);
router.patch('/channels/leave/:channelId', isAuthenticated, channelJoinValidator, channelController.leaveChannel);
router.get('/channels/chat/:channelId', isAuthenticated, channelController.getMessagesFromChannel);
router.get('/channels/subscribed/:channelId', isAuthenticated, channelController.getSubscribedUsers);
router.delete('/channels/:channelId', isAuthenticated, channelController.deleteExistingChannel);

const pairWordController = new PairWordController();
router.post('/pair-word/potrace-preview', isAuthenticated, potracePreviewValidator, pairWordController.getPotracePreview);
router.post('/pair-word/create', isAuthenticated, pairWordValidator, pairWordController.addPairWordDrawing);
router.get('/pair-word/random/:difficulty', isAuthenticated, pairWordController.getRandomPairWordDrawing);
router.get('/pair-word/page/:amount/:pageNum', isAuthenticated, pairWordController.getPairsPaginated);

const gameController = new GameController();
router.get('/game/ranking/:mode', isAuthenticated, gameController.getGameRankingFromMode);
router.get('/game/history/classic', isAuthenticated, gameController.getClassicGameHistory);
router.get('/game/history/solosprint', isAuthenticated, gameController.getSoloSprintGameHistory);

const friendsController = new FriendsController();
router.patch('/friends/request', isAuthenticated, friendsController.sendFriendRequest);
router.patch('/friends/acknowledge', isAuthenticated, friendsController.answerToFriendReq);
router.get('/friends', isAuthenticated, friendsController.getFriendsAndRequests);

export default router;
