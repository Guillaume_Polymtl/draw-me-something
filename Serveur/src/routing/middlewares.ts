import { NextFunction, Request, Response } from 'express';
import { InvalidTokenError } from '../errors/tokenErrors';
import { db } from '../services/firebaseInit';

const senderEndpointChecker = (req: Request, res: Response, next: NextFunction) => {
  const { host } = req.headers;
  if (host.includes('localhost')) {
    res.status(403).send({
      hasSent: false,
      message: 'Cannot add message from localhost',
    });
  } else {
    next();
  }
};

// tslint:disable-next-line: no-any
const getAuthToken = (req: any, res: Response, next: NextFunction) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    req.body.authToken = req.headers.authorization.split(' ')[1];
  } else {
    req.body.authToken = null;
  }

  req.body.pseudo = req.headers.pseudo ? req.headers.pseudo.split()[0] : null;

  next();
};

// tslint:disable-next-line: no-any
const isAuthenticated = async (req: any, res: Response, next: NextFunction) => {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = req.body;
      const { pseudo } = req.body;
      const userAuthDocs = await db.collection('userAuth').where('pseudo', '==', pseudo).get();
      if (userAuthDocs.empty) {
        throw new InvalidTokenError();
      }
      const token = await userAuthDocs.docs[0].get('token');
      if (authToken === token) {
        return next();
      } else {
        throw new InvalidTokenError();
      }
    } catch (error) {
      console.error(error);
      return res.status(401).send({ error: 'You are not authorized to make this request.' });
    }
  });
};

export { senderEndpointChecker, isAuthenticated };
