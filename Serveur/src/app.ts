import cors from 'cors';
import express, { Request, Response } from 'express';
import { injectable } from 'inversify';
import morgan from 'morgan';
import router from './routing/routes';
@injectable()
export default class App {
  app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.bindRoutes();
  }

  // Middlewares config
  private config(): void {
    // tslint:disable-next-line:no-require-imports
    require('events').EventEmitter.defaultMaxListeners = 20;
    this.app.use(morgan('combined'));
    this.app.use(cors());
    this.app.use(express.json({ limit: '2mb' }));
    this.app.use(express.urlencoded({ extended: true, limit: '2mb' }));
  }

  bindRoutes(): void {
    this.app.use('/', router);
    this.pingServer();
  }

  private pingServer(): void {
    // Waits for a GET request on route [serverEndpoint]/ping
    this.app.get('/ping', (_: Request, res: Response) => {
      res.send({ responseTime: Date.now() });
    });
  }
}
