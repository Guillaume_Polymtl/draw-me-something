// tslint:disable: max-classes-per-file
// Reason: Declaration of granular error types

export class ChannelNotFoundError extends Error {
  constructor() {
    super('Channel not found');
  }
}

export class ChannelCollisionError extends Error {
  constructor() {
    super('Channel already exists');
  }
}

export class UnauthorizedChannelDeletionError extends Error {
  constructor() {
    super('Unauthorized to delete channel');
  }
}
