// tslint:disable: max-classes-per-file
// Reason: Declaration of granular error types
export class UserNotFoundError extends Error {
  constructor(message?: string) {
    super('User not found');
    this.message = message;
  }
}

export class BadCredentialsError extends Error {
  constructor() {
    super('Bad credentials');
  }
}

export class UserAlreadyExistingError extends Error {
  constructor() {
    super('User already exists');
  }
}

export class UserAlreadyLoggedError extends Error {
  constructor() {
    super('User already logged in');
  }
}

export class FriendRequestConflictError extends Error {
  message: string;
  constructor(message?: string) {
    super(message);
    this.message = message ?? 'Friend request already exists';
  }
}
