// tslint:disable:max-classes-per-file

export class ImageConversionError extends Error {
  constructor() {
    super('Image conversion error');
  }
}

export class ImageNotFoundError extends Error {
  constructor() {
    super('Image not found');
  }
}

export class ImageTooVoluminousError extends Error {
  constructor() {
    super('Image too voluminous');
  }
}
