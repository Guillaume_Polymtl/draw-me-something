export class GameModeNotFoundError extends Error {
  constructor() {
    super('Game mode not found');
  }
}
