const { app, BrowserWindow, ipcMain, remote } = require("electron");
const url = require("url");
const path = require("path");

let mainWindow;
let childWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
    icon: __dirname + '/guessit.ico'
  });
  mainWindow.menuBarVisible = false;
  mainWindow.maximize();

  // run `ng serve` && `yarn start:electron` during development, use the commented mainWindow.loadURL for production
  // mainWindow.loadURL('http://localhost:4200/');

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/index.html`),
      protocol: "file:",
      slashes: true,
    })
  );
  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  mainWindow.on("closed", function () {
    mainWindow = null;
  });

  childWindow = new BrowserWindow({
    width: 800,
    height: 300,
    parent: mainWindow,
    frame: false,
    maximizable: false,
    webPreferences: {
      nodeIntegration: true,
    },
    alwaysOnTop: false,
    resizable: false,
  });

  // childWindow.loadURL('http://localhost:4200/index.html/#/windowed-chat');
  // childWindow.loadURL(`file://${__dirname}/dist/index.html`)
  childWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/index.html`),
      protocol: "file:",
      slashes: true,
      hash: "/windowed-chat",
    })
  );

  ipcMain.on("attach-chat", (event, arg) => {
    childWindow.hide();
    mainWindow.webContents.send("attach-chat-complete");
    return event.sender.send("attach-chat-complete");
  });

  ipcMain.on("detatch-chat", (event, arg) => {
    childWindow.show();
    mainWindow.webContents.send("detatch-chat-complete");
    return event.sender.send("detatch-chat-complete");
  });

  childWindow.hide();

  /*ipcMain.on("attach-chat", (event, arg) => {
    childWindow.hide();
    mainWindow.webContents.send("attach-chat-complete");
    return event.sender.send("attach-chat-complete");
  });*/
}

app.on("ready", createWindow);

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", function () {
  if (mainWindow === null) createWindow();
});
