# Local development

1. Install dependencies with `npm install`
2. Run the application locally with `npm start`


# App architecture

```
angular-electron
|__ e2e                             # end-to-end testing (not used currently)
|__ node_modules                    # contains all external modules
|__ src                             # contains the source code
|   |__ app                         # contains the app
|       |__ core                    # Contains all services, universal components and any features that are singletons
|           |__ authentication
|           |__ header
|           |__ http
|           |__ interceptors
|           |__ mocks
|           |__ services
|       |__ modules                 # Contains all modules, split by components and pages
|       |__ services
|       |__ shared                  # SharedModule, contains all shared components, pipes and filters. 
|           |__components           # The shared module is self-contained
|           |__ directives
|           |__ pipes
|       app-routing.module.ts   
|       app.module.ts
|   |__ assets                      # contains assets such as icons, images, fonts used throughout the app
|   |__ environments                # contains environment specific variables, not track by git for obvious reasons
|   |__ style                       # contains global styling
|   favicon.ico                     # application icon
|   index.html                      
|   main.ts
|   polyfill.ts
|   test.ts
| .browserlistrc
| .gitignore
| angular.json
| index.js
| karma.conf.js
| package-lock.json
| package.json
| README.md
| tsconfig.app.json
| tsconfig.json
| tsconfig.spec.json
| tslint.json
```


# Production build





# AngularElectron

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
