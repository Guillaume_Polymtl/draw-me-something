import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AudioService } from '@app/services/audio/audio.service';
import { RouterInterceptorService } from '@app/services/router-interceptor/router-interceptor.service';
import { AudioType, audioTypes } from '@models/audio/audio-type';

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss']
})
export class AudioComponent implements AfterViewInit {

  private audioPlayers: Map<AudioType, HTMLAudioElement> = new Map<AudioType, HTMLAudioElement>();
  private volume = 0.1;

  constructor(
    private routerInterceptorService: RouterInterceptorService,
    private audioService: AudioService,
    private changeDetector: ChangeDetectorRef) { }


  audioEnabled: boolean = false;

  private handleAudioPlayers(): void {
    this.audioService.currentMusic.subscribe((music: string) => {
      this.handleMusicPlayer(music);
    });
    this.audioService.currentEffect.subscribe((effect: string) => {
      this.handleEffectsPlayer(effect);
    });
    this.audioService.currentClock.subscribe((clock: string) => {
      this.handleClockPlayer(clock);
    });
  }

  private get clockPlayer(): HTMLAudioElement {
    return this.audioPlayers.get(AudioType.CLOCK) as HTMLAudioElement;
  }

  private get musicPlayer(): HTMLAudioElement {
    return this.audioPlayers.get(AudioType.MUSIC) as HTMLAudioElement;
  }

  private get effectsPlayer(): HTMLAudioElement {
    return this.audioPlayers.get(AudioType.EFFECTS) as HTMLAudioElement;
  }

  private handleClockPlayer(clock: string): void {
    if (!this.audioEnabled) {
      return;
    }
    this.clockPlayer.src = clock;
    this.increaseMusicVolume();
    this.clockPlayer.onloadeddata = () => {
      if (clock !== '') {
        this.clockPlayer.play();
        this.reduceMusicVolume();
      }
    };
  }

  private handleEffectsPlayer(effect: string): void {
    if (!this.audioEnabled) {
      return;
    }
    this.effectsPlayer.src = effect;
    this.increaseMusicVolume();
    this.effectsPlayer.onloadeddata = () => {
      if (effect !== '') {
        this.effectsPlayer.play();
        this.reduceMusicVolume();
      }
    };
  }

  private handleMusicPlayer(music: string): void {
    this.musicPlayer.src = music;
    this.musicPlayer.onloadeddata = () => {
      this.musicPlayer.play();
    };
  }

  private increaseMusicVolume(): void {
    this.musicPlayer.volume = this.volume;
  }

  private reduceMusicVolume(): void {
    this.musicPlayer.volume = this.volume / 3;
  }

  ngAfterViewInit(): void {
    audioTypes.forEach((audioType: AudioType) => {
      this.audioPlayers.set(audioType, new Audio(''));
    });
    this.audioPlayers.forEach((player: HTMLAudioElement) => {
      player.autoplay = true;
      player.loop = true;
    });

    this.audioService.audioEnabled.subscribe((enabled: boolean) => {
      this.handleAudioChange(enabled);
    });
    this.handleAudioPlayers();
    this.audioService.disableAudio();
    this.audioService.startBackgroundMusic();
    setTimeout(() => {
      this.audioService.disableAudio();
    }, 100);
  }

  get isWindowedChat(): boolean {
    return this.routerInterceptorService.isWindowedChat();
  }

  toggleAudio(): void {
    this.audioService.toggleAudio();
  }

  private handleAudioChange(enabled: boolean): void {
    this.audioEnabled = enabled;
    this.clockPlayer.volume = this.volume;
    this.musicPlayer.volume = this.volume;
    this.effectsPlayer.volume = this.volume;
    if (!this.audioEnabled) {
      this.clockPlayer.volume = 0;
      this.musicPlayer.volume = 0;
      this.effectsPlayer.volume = 0;
    }
    this.changeDetector.detectChanges();
  }
}
