import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Channel } from '@app/models/chat/channel';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { ChatIntegrationService } from '@app/services/chat/chat-integration/chat-integration.service';
import { ChatService } from '@app/services/chat/chat.service';
import { RouterInterceptorService } from '@app/services/router-interceptor/router-interceptor.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CreateChannelComponent } from '../create-channel/create-channel.component';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  subscribedChannels: Observable<Channel[]> = of([]);
  unsubscribedChannels: Observable<Channel[]> = of([]);

  currentChannelName: string;

  userIsLoggedIn: boolean = false;
  isIntegrated: boolean = false;
  isOpened: boolean = false;

  constructor(
    private chatService: ChatService,
    private changeDetector: ChangeDetectorRef,
    private dialog: MatDialog,
    private routerInterceptor: RouterInterceptorService,
    private chatIntegrationService: ChatIntegrationService,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.subscribedChannels = this.chatService.getSubscribedChannels().pipe(
      map((subscribedChannels: Channel[]) => this.chatService.orderChannelsByName(subscribedChannels))
    );
    this.unsubscribedChannels = this.chatService.getUnsubscribedChannels().pipe(
      map((unsubscribedChannels: Channel[]) => this.chatService.orderChannelsByName(unsubscribedChannels))
    );
    this.chatIntegrationService.getIsAttached().subscribe((isAttached: boolean) => {
      this.isIntegrated = isAttached;
      this.changeDetector.detectChanges();
    });
    this.chatService.getCurrentChannelName().subscribe((currentChannelName: string) => {
      this.currentChannelName = currentChannelName;
    });
    this.authenticationService.connected.subscribe((userIsConnected: boolean) => {
      this.userIsLoggedIn = userIsConnected;
      if (!userIsConnected) {
        this.isOpened = false;
      }
    }
    );
  }

  create(): void {
    const createChannelDialogRef = this.dialog.open(CreateChannelComponent);
    createChannelDialogRef.afterClosed().subscribe((newChannelName: string) => {
      if (newChannelName.length > 0) {
        this.createChannel(newChannelName);
      }
    });
  }

  displayToggleChat(): boolean {
    if (this.isIntegrated && this.routerInterceptor.canDisplayToggleChat()) {
      return true;
    }
    return false;
  }

  toggleChat(): void {
    this.isOpened = !this.isOpened;
  }

  private createChannel(channelId: string): void {
    this.chatService.createChannel(channelId);
  }

  selectChannel(channelId: string): void {
    this.chatService.selectChannel(channelId);
  }

  joinChannel(channelId: string): void {
    this.chatService.joinChannel(channelId);
  }

  leaveChannel(channelId: string): void {
    this.chatService.leaveChannel(channelId);
  }

  deleteChannel(channelId: string): void {
    this.chatService.deleteChannel(channelId);
  }

  sendMessage(channelId: string, message: string): void {
    this.chatService.sendMessage(message, channelId);
  }

  getHistory(channelId: string): void {
    this.chatService.getChannelHistory(channelId);
  }

  detatch(): void {
    this.chatIntegrationService.detatch();
    this.isIntegrated = false;
  }

  attach(): void {
    this.chatIntegrationService.attach();
    this.isIntegrated = true;
  }

  // Parses the user's email to display it as a username (this should be temporary)
  trackChannels(index: number, channel: Channel): any {
    return channel.channelName;
  }
}
