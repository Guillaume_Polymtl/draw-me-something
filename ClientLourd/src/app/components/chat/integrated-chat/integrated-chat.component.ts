import { Component } from '@angular/core';
import { ChatComponent } from '../chat/chat.component';

@Component({
  selector: 'app-integrated-chat',
  templateUrl: './integrated-chat.component.html',
  styleUrls: ['./integrated-chat.component.scss']
})
export class IntegratedChatComponent extends ChatComponent {}
