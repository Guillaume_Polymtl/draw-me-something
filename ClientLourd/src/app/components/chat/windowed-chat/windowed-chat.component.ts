import { Component, ViewEncapsulation } from '@angular/core';
import { ChatComponent } from '../chat/chat.component';

@Component({
  selector: 'app-windowed-chat',
  templateUrl: './windowed-chat.component.html',
  styleUrls: ['./windowed-chat.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WindowedChatComponent extends ChatComponent {}
