import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WindowedChatComponent } from './windowed-chat.component';

describe('WindowedChatComponent', () => {
  let component: WindowedChatComponent;
  let fixture: ComponentFixture<WindowedChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WindowedChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WindowedChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
