import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-channel',
  templateUrl: './create-channel.component.html',
  styleUrls: ['./create-channel.component.scss']
})
export class CreateChannelComponent {
  constructor(public dialogRef: MatDialogRef<CreateChannelComponent>) { }
  name: string = '';

  createChannel(): void {
    this.dialogRef.close(this.name);
  }
}
