import {
  Component, Input, Output, EventEmitter, OnChanges, ViewChild,
  ElementRef, OnInit, SimpleChange, AfterViewInit, ViewChildren,
  QueryList, HostListener, ViewContainerRef, AfterViewChecked,
  AfterContentInit, OnDestroy, ChangeDetectorRef
} from '@angular/core';
import { Channel } from '@app/models/chat/channel';
import { Message } from '@app/models/chat/message';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements AfterViewInit {

  constructor(private changeDetector: ChangeDetectorRef) { }

  @ViewChild('chat') private chat: ElementRef;
  @ViewChildren('message')
  private messageElements: QueryList<ElementRef>;

  private chatContainer: any;

  missedMessage: Message | undefined = undefined;

  message: string = '';

  @Input() channel: Channel;

  @Output() newMessage: EventEmitter<string> = new EventEmitter();


  ngAfterViewInit(): void {
    this.chatContainer = this.chat.nativeElement;
    this.messageElements.changes.subscribe(() => {
      this.onMessagesChanged();
    });
  }

  sendMessage(): void {
    this.newMessage.next(this.message);
    this.message = '';
    this.scrollToBottom();
  }

  private onMessagesChanged(): void {
    if (this.isUserNearBottom()) {
      this.scrollToBottom();
    } else if (!this.missedMessage) {
      const length = this.channel.messages.length;
      const lastMessage = this.channel.messages[length - 1];

      // dont show notification for my own message
      if (!lastMessage.fromCurrent) {
        this.missedMessage = lastMessage;
        setTimeout(() => {
          this.missedMessage = undefined;
        }, 1000);
      } else{
        this.scrollToBottom();
      }
      this.changeDetector.detectChanges();
    }
  }

  private scrollToBottom(): void {
    this.chatContainer.scroll({
      top: this.chatContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }

  private isUserNearBottom(): boolean {
    const threshold = 250;
    const position = this.chatContainer.scrollTop + this.chatContainer.offsetHeight;
    const height = this.chatContainer.scrollHeight;
    return position > height - threshold;
  }
}
