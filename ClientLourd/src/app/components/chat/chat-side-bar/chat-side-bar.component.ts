import { viewClassName } from '@angular/compiler';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChatComponent } from '../chat/chat.component';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-chat-side-bar',
  templateUrl: './chat-side-bar.component.html',
  styleUrls: ['./chat-side-bar.component.scss'],
})
export class ChatSideBarComponent extends ChatComponent {

  subscribedChannelsExpanded: boolean = true;
  unsubscribedChannelsExpanded: boolean = true;

  toggleSubscribedChannels(): void {
    this.subscribedChannelsExpanded = !this.subscribedChannelsExpanded;
  }

  toggleUnsubscribedChannels(): void {
    this.unsubscribedChannelsExpanded = !this.unsubscribedChannelsExpanded;
  }

}
