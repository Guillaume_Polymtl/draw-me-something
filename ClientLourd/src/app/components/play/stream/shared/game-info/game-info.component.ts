import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Game } from '@models/game/game';
import { GameMode } from '@models/game/game-mode';
import RoundModel from '@models/game/round';
import { GameService } from '@services/play/game/game.service';

@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.scss']
})
export class GameInfoComponent implements OnChanges, OnInit {

  @Input() game: Game;

  @Input() team1Score: number = 0;
  @Input() team2Score: number = 0;
  @Input() triesLeft: number = 0;

  @Input() totalRoundTime: number = 60000;
  @Input() roundTimeLeft: number = 60000;

  @Input() roundsLeft: number = 4;
  @Input() totalRounds: number = 10;

  @Input() word: string = '';
  @Input() round: RoundModel;

  @Input() isGuesser: boolean;
  @Input() isArtist: boolean;

  timeLeftInPercent: number = 100;
  previousRound: number = 0;
  playerRole: string;

  constructor(private gameService: GameService) { }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
    if (this.game.gameMode === GameMode.SPRINT_SOLO) {
      this.isGuesser = true;
    }
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      if (nextRound.roundNum !== this.previousRound) {
        this.team1Score = 0;
        this.team2Score = 0;
        this.previousRound = nextRound.roundNum ?? 0;
      }
    });
  }

  ngOnChanges(): void {
    this.timeLeftInPercent = this.roundTimeLeft / this.totalRoundTime * 100;
  }

  public displayHangman(): string {
    // tslint:disable-next-line: prefer-for-of
    let hangman = '';
    // tslint:disable-next-line: no-non-null-assertion
    for (let i = 0; i < this.round.wordToGuess?.length!; i++) {
      hangman += '_ ';
    }
    return hangman;
  }
}
