import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GameDifficulty } from '@app/models/game/difficulty';
import { Game } from '@app/models/game/game';
import { GameMode } from '@app/models/game/game-mode';
import { Guess } from '@app/models/game/streaming/guess';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { GameService } from '@app/services/play/game/game.service';
import RoundModel from '@models/game/round';
@Component({
  selector: 'app-guess',
  templateUrl: './guess.component.html',
  styleUrls: ['./guess.component.scss']
})
export class GuessComponent implements OnInit, AfterViewInit {

  @ViewChild('guesses') private guessesElement: ElementRef;
  private guessesContainer: any;
  isGuesser: boolean;

  currentGame: Game = {gameMode: GameMode.CLASSIC, difficulty: GameDifficulty.NORMAL};
  previousGuesses: Guess[] = [];
  word: string;
  isGoodGuess: boolean;
  previousRound: number = 0;
  currentTeam: string = '';

  constructor(private gameService: GameService,
              private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.gameService.currentGame.subscribe((g: Game) => {
      this.currentGame = g;
    }).unsubscribe();
    this.gameService.currentGameStatus.subscribe({
      next: status => {
        this.previousGuesses = status.guesses as Guess[];
        this.scrollDown();
      }
    });
    this.gameService.endGuessDraw().then((isGoodGuess) => {
      this.isGoodGuess = isGoodGuess;
    });
    this.gameService.isGuesser.subscribe((isGuesser: boolean) => {
      this.isGuesser = isGuesser;
    });
    this.gameService.isRoundEnded.subscribe((isEnded: boolean) => {
      if (isEnded) {
        this.previousGuesses = [];
      }
    });
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      if (nextRound.roundNum !== this.previousRound) {
        this.previousGuesses = [];
        this.previousRound = nextRound.roundNum ?? 0;
      }
    });
    this.gameService.currentTeam.subscribe((teamUID: string) => {
      this.currentTeam = teamUID;
      console.log(this.currentTeam);
    });
  }

  ngAfterViewInit(): void {
    this.guessesContainer = this.guessesElement.nativeElement;
    this.scrollDown();
  }

  guess(): void {
    const guess: Guess = {
      guessContent: this.word ?? '',
      gameUID: this.currentGame.gameUID,
      pseudo: this.authService.currentUserPseudo
    };
    this.gameService.guess(guess);
    this.word = '';
  }

  private scrollDown(): void {
    if (this.guessesContainer) {
      this.guessesContainer.scroll({
        top: this.guessesContainer.scrollHeight,
        left: 0,
        behavior: 'smooth'
      });
    }

  }

}
