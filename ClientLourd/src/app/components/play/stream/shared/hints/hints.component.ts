import { Component, Input, OnInit } from '@angular/core';
import { Game } from '@models/game/game';
import { GameMode } from '@models/game/game-mode';
import RoundModel from '@models/game/round';
import { GameService } from '@services/play/game/game.service';

@Component({
  selector: 'app-hints',
  templateUrl: './hints.component.html',
  styleUrls: ['./hints.component.scss']
})
export class HintsComponent implements OnInit {

  hints: string[] = [];
  gameMode: GameMode;
  previousRound: number = 0;
  isShowHint: boolean[] = [];
  hintCount: number = 0;
  constructor(private gameService: GameService) { }

  ngOnInit(): void {
    this.gameService.hints.subscribe((hints: string[]) => {
      this.hints = hints;
    });
    this.gameService.isRoundEnded.subscribe((isEnded: boolean) => {
      if (isEnded) {
        this.hints = [''];
        this.isShowHint = [];
      }
    });
    this.gameService.currentGame.subscribe((g: Game) => {
      this.gameMode = g.gameMode as GameMode;
    });
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      if (nextRound.roundNum !== this.previousRound) {
        this.hintCount = 0;
        this.hints = [''];
        this.isShowHint = [];
        this.previousRound = nextRound.roundNum ?? 0;
      }
    });
  }

  showHint(): void {
    this.isShowHint[this.hintCount] = true;
    this.gameService.hintResponse(this.hintCount);
    this.hintCount++;
  }
}
