import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamTeamsInfoComponent } from './stream-teams-info.component';

describe('StreamTeamsInfoComponent', () => {
  let component: StreamTeamsInfoComponent;
  let fixture: ComponentFixture<StreamTeamsInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamTeamsInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamTeamsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
