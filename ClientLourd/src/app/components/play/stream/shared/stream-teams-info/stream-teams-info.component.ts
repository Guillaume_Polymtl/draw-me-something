import { Component, Input, OnInit } from '@angular/core';
import { Team } from '@models/game/team';

@Component({
  selector: 'app-stream-teams-info',
  templateUrl: './stream-teams-info.component.html',
  styleUrls: ['./stream-teams-info.component.scss']
})
export class StreamTeamsInfoComponent{
  @Input() teams: Team[] = [];
}
