import { Component, OnInit, ViewChild } from '@angular/core';
import { CanvasComponent } from '@app/components/pair-word-drawing/canvas/canvas.component';
import { Team } from '@app/models/game/team';
import { GameService } from '@services/play/game/game.service';
import { StreamingService } from '@app/services/play/streaming/streaming.service';
import { Game } from '@models/game/game';
import RoundModel from '@models/game/round';

@Component({
  selector: 'app-watch-stream',
  templateUrl: './watch-stream.component.html',
  styleUrls: ['./watch-stream.component.scss']
})
export class WatchStreamComponent implements OnInit {
  canvasSize: string = '500';
  previousRound: number = 0;

  @ViewChild('canvas') canvas: CanvasComponent;

  constructor(
    private streamingService: StreamingService,
    private gameService: GameService) {
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit(): void {
    this.streamingService.context = this.canvas.context;
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      if (nextRound.roundNum !== this.previousRound) {
        this.canvas.context.clearRect(0, 0, 500, 500);
        this.previousRound = nextRound.roundNum ?? 0;
      }
    });
  }

  ngOnInit(): void {
  }
}
