/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WatchStreamComponent } from './watch-stream.component';

describe('WatchStreamComponent', () => {
  let component: WatchStreamComponent;
  let fixture: ComponentFixture<WatchStreamComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchStreamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
