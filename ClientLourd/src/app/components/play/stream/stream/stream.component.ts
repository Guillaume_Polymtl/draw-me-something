import { Component, HostListener, OnInit } from '@angular/core';
import { EditorComponent } from '@app/components/editor/editor/editor.component';
import { Drawing } from '@app/models/drawing/drawing';
import { Game } from '@app/models/game/game';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { StreamingService } from '@app/services/play/streaming/streaming.service';
import { UndoRedoService } from '@services/editor/undo-redo/undo-redo.service';
import { GameService } from '@services/play/game/game.service';

@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.scss']
})
export class StreamComponent implements OnInit {

  editor: EditorComponent;

  gameUID: string;

  constructor(private drawingService: DrawingService,
              private streamingService: StreamingService,
              private gameService: GameService,
              private undoRedoService: UndoRedoService) { }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: MouseEvent): void {

    this.drawingService.getCurrentDrawing().subscribe((drawing: Drawing) => {
      const numberOfStrokes = drawing.strokes.length;
      const lastStroke = drawing.strokes[numberOfStrokes - 1];

      const sentColor = [lastStroke.color.red, lastStroke.color.green, lastStroke.color.blue, lastStroke.color.alpha];
      const lastPoint = lastStroke.points.length - 1;

      for (const [index, point] of lastStroke?.points.entries()) {
        let isStart = false;
        let isEnd = false;
        switch (index) {
          case 0: {
            isStart = true;
            break;
          }
          case lastPoint: {
            isEnd = true;
            break;
          }
          default: {
            isStart = false;
            isEnd = false;
            break;
          }
        }

        const data: JSON = {
          x: point.position.x,
          y: point.position.y,
          start: isStart,
          end: isEnd,
          r: sentColor[0] ?? 0,
          g: sentColor[1] ?? 0,
          b: sentColor[2] ?? 0,
          a: sentColor[3] ?? 0,
          width: lastStroke.width,
          toolType: lastStroke.tool,
          gameUID: this.gameUID,
        } as unknown as JSON;

        this.streamingService.fillDrawingData(data);
      }
    });
  }

  ngOnInit(): void {
    this.gameService.currentGame.subscribe((game: Game) => {
      this.gameUID = game.gameUID ?? '';
    });
    this.undoRedoService.isUndo.subscribe((isUndo: boolean) => {
      if (isUndo) {
        console.log('isUndo');
        const data: JSON = {
          toolType: 'Undo',
          gameUID: this.gameUID,
        } as unknown as JSON;
        this.streamingService.sendDrawingData(data);
      }
    });
    this.undoRedoService.isRedo.subscribe((isRedo: boolean) => {
      if (isRedo) {
        console.log('isRedo');
        const data: JSON = {
          toolType: 'Redo',
          gameUID: this.gameUID,
        } as unknown as JSON;
        this.streamingService.sendDrawingData(data);
      }
    });
  }
}
