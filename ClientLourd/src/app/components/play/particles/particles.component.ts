import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-particles',
  templateUrl: './particles.component.html',
  styleUrls: ['./particles.component.scss']
})
export class ParticlesComponent {

  id = 'tsparticles';


  /* or the classic JavaScript object */
  particlesOptions: any = {
    fpsLimit: 60,
    particles: {
      number: {
        value: 0
      },
      color: {
        value: ['#ea00d9', '#0abdc6', '#ffffff', '#711c91', '#9358a8']
      },
      shape: {
        type: 'square',
      },
      opacity: {
        value: 5,
        animation: {
          enable: true,
          minimumValue: 0,
          speed: 2,
          startValue: 'max',
          destroy: 'min'
        }
      },
      size: {
        value: 7,
        random: {
          enable: true,
          minimumValue: 3
        }
      },
      links: {
        enable: false
      },
      life: {
        duration: {
          sync: true,
          value: 1
        },
        count: 1
      },
      move: {
        enable: true,
        speed: 7,
        direction: 'none',
        random: true,
        straight: false,
        outModes: {
          default: 'destroy',
          top: 'none'
        }
      }
    },
    interactivity: {
      detectsOn: 'canvas',
      events: {
        resize: true
      }
    },
    detectRetina: true,
    background: {
      color: '#091833'
    },
    emitters: {
      direction: 'none',
      life: {
        count: 0,
        duration: 0.1,
        delay: 0.4
      },
      rate: {
        delay: 0.1,
        quantity: 100
      },
      size: {
        width: 0,
        height: 0
      }
    }
  };

  particlesLoaded(container: any): void {

  }

  particlesInit(main: any): void {


    // Starting from 1.19.0 you can add custom presets or shape here, using the current tsParticles instance (main)
  }

}
