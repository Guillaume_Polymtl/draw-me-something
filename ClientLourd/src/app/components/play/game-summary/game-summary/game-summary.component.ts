import { Component, Input, OnInit } from '@angular/core';
import { Game } from '@models/game/game';
import { GameMode } from '@models/game/game-mode';
import { GroupGameSummary } from '@models/game/game-summary';
import { Team } from '@models/game/team';
import { AudioService } from '@services/audio/audio.service';
import { AuthenticationService } from '@services/authentication/authentication.service';
import { GameService } from '@services/play/game/game.service';

@Component({
  selector: 'app-game-summary',
  templateUrl: './game-summary.component.html',
  styleUrls: ['./game-summary.component.scss']
})
export class GameSummaryComponent implements OnInit {

  currentGame: Game = {};
  gameSummary: GroupGameSummary = {};
  teams: Team[] = [];
  isPersonnalRecord: boolean;
  isGlobalRecord: boolean;
  soloScore: number = 0;
  elapsedTime: number = 0;

  teamScore = 0;

  playerIsWinner: boolean;

  constructor(
    private gameService: GameService,
    private authService: AuthenticationService,
    private audioService: AudioService
  ) { }

  ngOnInit(): void {
    this.audioService.stopClock();
    this.gameService.gameSummary.subscribe((summary: GroupGameSummary) => {
      console.log(summary);
      this.gameSummary = summary;
      this.soloScore = summary.score as number;
      this.teams = summary.teams as Team[];
    });
    this.gameService.currentGame.subscribe((game: Game) => {
      this.currentGame = game;
    });
    if (this.currentGame.gameMode === GameMode.CLASSIC) {
      this.winner();
    } else if (this.currentGame.gameMode === GameMode.SPRINT_SOLO) {
      this.isGlobalRecord = this.gameSummary.isGlobalRecord as boolean;
      this.isPersonnalRecord = this.gameSummary.isPersonnalRecord as boolean;
    }
    this.elapsedTime = this.gameSummary.elapsedTime as number;

  }

  // test
  winner(): void {
    this.teams.forEach(t => {
      t.isWinner = this.gameSummary.winner.teamUID === t.teamUID;

      const hasPseudo = !!t.teamMembers?.find((user) => user.pseudo === this.authService.currentUserPseudo);

      if (t.isWinner && hasPseudo) {
        this.audioService.successEffect();
        this.playerIsWinner = true;
      } else if (!t.isWinner && hasPseudo) {
      }

      if (hasPseudo) {
        this.teamScore = t.points ?? 0;
      }
    });
  }

  formatElapsedTime(input: number): string {
    let totalMinutes;
    let totalSeconds;
    let minutes;
    let seconds;
    totalSeconds = input / 1000;
    totalMinutes = totalSeconds / 60;
    seconds = Math.floor(totalSeconds) % 60;
    minutes = Math.floor(totalMinutes) % 60;
    return (minutes + 'm:' + seconds + 's');
  }
}
