import { Game } from '@models/game/game';
import { GameService } from '@services/play/game/game.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { GameMode } from '@app/models/game/game-mode';
import RoundModel from '@models/game/round';
import { RouterService } from '@services/router/router.service';
import { GameDifficulty } from '@models/game/difficulty';
import { LobbyService } from '@services/play/lobby/lobby.service';
import { AudioService } from '@services/audio/audio.service';

@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.scss']
})
export class RoundComponent implements OnInit, OnDestroy {
  currentRound: any;
  currentGame: Game;
  wordToGuess: string;
  round: RoundModel;
  roundTime: number = 60000;

  isGuesser: boolean;
  isArtist: boolean;
  isGoodGuess: boolean;
  team1Score: number;
  team2Score: number;
  triesLeft: number = 1;
  previousRound: number = 0;

  constructor(
    private gameService: GameService,
    private lobbyService: LobbyService) { }

  ngOnInit(): void {
    this.gameService.currentGameStatus.subscribe((status: any) => {
      this.currentRound = status;
    });
    this.gameService.currentGame.subscribe((game: Game) => {
      this.currentGame = game;
      if (game.gameMode === GameMode.SPRINT_SOLO) {
        this.roundTime = 120000;
      }
    });
    this.gameService.isGuesser.subscribe((isGuesser: boolean) => {
      this.isGuesser = isGuesser;
    });
    this.gameService.isArtist.subscribe((isArtist: boolean) => {
      this.isArtist = isArtist;
    });
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      this.round = nextRound;
    });
    this.gameService.endGuessDraw().then((isGoodGuess) => {
      this.isGoodGuess = isGoodGuess;
    });
    this.gameService.team1Score.subscribe((score: number) => {
      this.team1Score = score;
    });
    this.gameService.team2Score.subscribe((score: number) => {
      this.team2Score = score;
    });
    this.gameService.triesLeft.subscribe((t: number) => {
      this.triesLeft = t;
    });
    this.gameService.nextRound.subscribe((nextRound: RoundModel) => {
      if (nextRound.roundNum !== this.previousRound) {
        if (this.currentGame.difficulty === GameDifficulty.NORMAL) {
          this.triesLeft = 3;
        } else {
          this.triesLeft = 1;
        }
        this.previousRound = nextRound.roundNum ?? 0;
      }
    });
    if (this.currentGame.difficulty === GameDifficulty.NORMAL) {
      this.triesLeft = 3;
    }
  }

  ngOnDestroy(): void {
    this.gameService.isGameStart.next(false);
  }

  leaveGame(): void {
    this.lobbyService.leaveGame();
  }
}
