import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { GameDifficulties, GameDifficulty } from '@app/models/game/difficulty';
import { Game } from '@app/models/game/game';
import { GameMode, GameModes } from '@app/models/game/game-mode';
import { HallService } from '@services/play/hall/hall.service';

@Component({
  selector: 'app-game-create',
  templateUrl: './game-create.component.html',
  styleUrls: ['./game-create.component.scss']
})
export class GameCreateComponent implements OnInit {

  sprintSolo = GameMode.SPRINT_SOLO;
  classic = GameMode.CLASSIC;


  private roundValidators = [
    Validators.required,
    Validators.min(1),
    Validators.max(10)
  ];

  gameForm = this.fb.group({
    name: ['', Validators.required],
    gameMode: ['', Validators.required],
    difficulty: ['', Validators.required],
    roundNum: ['', this.roundValidators],
  });


  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<GameCreateComponent>,
              private hallService: HallService) { }



  gameModes: GameMode[] = [];
  gameDifficulties: GameDifficulty[] = [];

  onSubmit(): void {
    this.dialogRef.close(this.gameForm.value);
  }

  onCancel(): void {
    this.gameForm.reset();
    this.dialogRef.close(false);
  }

  ngOnInit(): void {
    this.gameModes = GameModes;
    this.gameDifficulties = GameDifficulties;
    this.gameForm.get('gameMode')?.valueChanges.subscribe((form) => {
      this.gameForm.get('roundNum')?.disable();
      if (this.gameForm.get('gameMode')?.value === GameMode.CLASSIC) {
        this.gameForm.get('roundNum')?.enable();
      }
      this.gameForm.updateValueAndValidity();
    });
  }

  get mode(): GameMode {
    return this.gameForm.get('gameMode')?.value;
  }


  createGame(game: Game): void {
    this.hallService.createGame(game);
  }

  onclick(): void {
    Object.keys(this.gameForm.controls).forEach(key => {
      const controlErrors: ValidationErrors | null | undefined = this.gameForm.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log(keyError);
        });
      }
    });
  }

}
