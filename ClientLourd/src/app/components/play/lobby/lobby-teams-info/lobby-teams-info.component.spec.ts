import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyTeamsInfoComponent } from './lobby-teams-info.component';

describe('LobbyTeamsInfoComponent', () => {
  let component: LobbyTeamsInfoComponent;
  let fixture: ComponentFixture<LobbyTeamsInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LobbyTeamsInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyTeamsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
