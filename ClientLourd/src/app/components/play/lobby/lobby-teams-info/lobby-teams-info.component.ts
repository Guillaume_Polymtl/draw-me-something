import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Team } from '@models/game/team';
import { LobbyService } from '@services/play/lobby/lobby.service';
import { VirtualPlayerChoiceComponent } from '../virtual-player-choice/virtual-player-choice.component';

@Component({
  selector: 'app-lobby-teams-info',
  templateUrl: './lobby-teams-info.component.html',
  styleUrls: ['./lobby-teams-info.component.scss']
})
export class LobbyTeamsInfoComponent {
  @Input() teams: Team[] = [];

  constructor(private lobbyService: LobbyService,
              private dialog: MatDialog) { }

  public joinTeam(teamUID: string): void {
    this.lobbyService.joinTeam(teamUID);
  }

  addVPlayer(team: string): void {
    const dialogRef = this.dialog.open(VirtualPlayerChoiceComponent, {
      data: team
    });
  }
}
