import { Component, Inject, OnInit } from '@angular/core';
import { User } from '@models/user/user';
import { LobbyService } from '@services/play/lobby/lobby.service';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { VirtualUser } from '@models/user/virtual-user';

@Component({
  selector: 'app-virtual-player-choice',
  templateUrl: './virtual-player-choice.component.html',
  styleUrls: ['./virtual-player-choice.component.scss']
})
export class VirtualPlayerChoiceComponent implements OnInit {

  // temp
  bob: User = {pseudo: 'Bob', avatar: 'https://firebasestorage.googleapis.com/v0/b/projet3-log3900.appspot.com/o/bob.jpg?alt=media&token=3f93883f-76e9-4b05-8a22-7288eda51075'};
  alice: User = {pseudo: 'Alice', avatar: 'https://firebasestorage.googleapis.com/v0/b/projet3-log3900.appspot.com/o/alice.PNG?alt=media&token=ee4037f9-fff6-4a1a-bd16-b7d35d04030f'};

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private lobbyService: LobbyService,
    private dialogRef: MatDialogRef<VirtualPlayerChoiceComponent>) { }

  ngOnInit(): void {
  }

  addVPlayer(pseudo: string): void {
    this.lobbyService.addVirtualPlayer(this.data, pseudo);
    this.dialogRef.close();
  }

}
