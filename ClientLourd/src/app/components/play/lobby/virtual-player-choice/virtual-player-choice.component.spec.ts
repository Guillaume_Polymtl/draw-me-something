/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VirtualPlayerChoiceComponent } from './virtual-player-choice.component';

describe('VirtualPlayerChoiceComponent', () => {
  let component: VirtualPlayerChoiceComponent;
  let fixture: ComponentFixture<VirtualPlayerChoiceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualPlayerChoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualPlayerChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
