import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyGameInfoComponent } from './lobby-game-info.component';

describe('LobbyGameInfoComponent', () => {
  let component: LobbyGameInfoComponent;
  let fixture: ComponentFixture<LobbyGameInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LobbyGameInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyGameInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
