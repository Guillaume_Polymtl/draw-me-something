import { Component, Input, OnInit } from '@angular/core';
import { Game } from '@app/models/game/game';
import { GameMode } from '@models/game/game-mode';

@Component({
  selector: 'app-lobby-game-info',
  templateUrl: './lobby-game-info.component.html',
  styleUrls: ['./lobby-game-info.component.scss']
})
export class LobbyGameInfoComponent {
  @Input() game: Game;
}
