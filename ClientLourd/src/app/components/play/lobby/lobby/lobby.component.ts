import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Game } from '@app/models/game/game';
import RoundModel from '@app/models/game/round';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { LobbyService } from '@app/services/play/lobby/lobby.service';
import { RouterService } from '@app/services/router/router.service';
import { GameMode } from '@models/game/game-mode';
import { GameService } from '@services/play/game/game.service';


@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {

  currentUser: string;
  game: Game;
  players: string[] = [];
  canStartGame: boolean = false;
  nextRound: RoundModel;
  isClassic: boolean;

  constructor(
    private lobbyService: LobbyService,
    private authService: AuthenticationService,
    private changeDetector: ChangeDetectorRef, private routerService: RouterService,
    private gameService: GameService) {
    this.currentUser = this.authService.currentUserPseudo;

  }

  ngOnInit(): void {
    this.lobbyService.canStart.next(false);
    this.lobbyService.currentGame.subscribe((game: Game) => {
      this.game = game;
      this.isClassic = (game.gameMode === GameMode.CLASSIC);
      this.changeDetector.detectChanges();
    });
    this.lobbyService.canStart.subscribe((canStart: boolean) => {
      this.canStartGame = canStart;
    });
    this.gameService.isGameStart.subscribe((isStart: boolean) => {
      if (isStart) {
        this.toRoundPage();
      }
    });
  }

  leaveGame(): void {
    this.lobbyService.leaveGame();
  }

  startGroupGame(): void {
    this.lobbyService.startGame();
  }

  toRoundPage(): void {
    this.routerService.round();
  }
}
