import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Game } from '@models/game/game';
import { GameMode } from '@models/game/game-mode';
import { GameService } from '@services/play/game/game.service';
import { HallService } from '@services/play/hall/hall.service';
import { RouterService } from '@services/router/router.service';
import { GameCreateComponent } from '../game-create/game-create.component';


@Component({
  selector: 'app-hall',
  templateUrl: './hall.component.html',
  styleUrls: ['./hall.component.scss']
})
export class HallComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private hallService: HallService,
    private gameService: GameService,
    private routerService: RouterService) { }

  games: Game[] = [];

  // END DEBUG
  ngOnInit(): void {
    this.gameService.socket.emit('force-hall-emit');
    this.hallService.games.subscribe((games: Game[]) => {
      this.games = games;
    });
  }

  createGroup(): void {
    const dialogRef = this.dialog.open(GameCreateComponent);
    dialogRef.afterClosed().subscribe((game: Game) => {
      if (game) {
        // game created from the dialog
      }
    });
  }

  joinGame(game: Game): void {
    this.hallService.joinGame(game);
  }
}
