import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractConstructor } from '@angular/material/core/common-behaviors/constructor';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from '@app/models/user/user';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { PROFILE_EDITED_ERROR, PROFILE_EDITED_SUCCESS } from '@app/services/profile/profile-errors';
import { ProfileService } from '@app/services/profile/profile.service';
import { RouterService } from '@app/services/router/router.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {


  user: User;
  form: FormGroup;

  constructor(private profileService: ProfileService,
              private formBuilder: FormBuilder,
              private matDialogRef: MatDialogRef<EditProfileComponent>,
              private snackBarService: SnackBarService,
              private authService: AuthenticationService,
              private routerService: RouterService) {
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(30), Validators.minLength(1)]],
      lastName: ['', [Validators.required, Validators.maxLength(30), Validators.minLength(1)]],
      pseudo: ['', [Validators.required, Validators.maxLength(30), Validators.minLength(1)]],
      password: ['', [Validators.maxLength(128)]],
      confirmPassword: ['', [Validators.maxLength(128)]]
    });
  }

  ngOnInit(): void {
    this.authService.currentUser.subscribe({
      next: user => {
        this.user = user;
        this.initForm();
      },
      error: error => {
      }
    });
  }

  get firstName(): AbstractControl | null {
    return this.form.get('firstName');
  }

  get lastName(): AbstractControl | null {
    return this.form.get('lastName');
  }

  get pseudo(): AbstractControl | null {
    return this.form.get('pseudo');
  }

  get password(): AbstractControl | null {
    return this.form.get('password');
  }

  get confirmPassword(): AbstractControl | null {
    return this.form.get('confirmPassword');
  }

  initForm(): void {
    this.firstName?.setValue(this.user.firstName);
    this.lastName?.setValue(this.user.lastName);
    this.pseudo?.setValue(this.user.pseudo);
  }

  close(): void {
    this.matDialogRef.close();
  }

  onSubmit(): void {
    if (this.password?.value !== this.confirmPassword?.value) {
      this.snackBarService.error('Les mots de passent ne concordent pas.');
      return;
    }
    const user: User = {
      firstName: this.firstName?.value,
      lastName: this.lastName?.value,
      newPseudo: this.pseudo?.value,
      pseudo: this.user.pseudo
    };
    if (this.password?.value) {
      user.password = this.password.value;
    }
    this.profileService.editUser(user).subscribe({
      next: result => {
        if (result) {
          this.snackBarService.success(PROFILE_EDITED_SUCCESS);
          if (user.newPseudo) {
            this.authService.currentUserPseudo = user.newPseudo;
          }
          this.authService.updateCurrentUser();
          this.matDialogRef.close();
        }
      },
      error: error => {
        this.snackBarService.error(PROFILE_EDITED_ERROR);
      }
    });
  }

}
