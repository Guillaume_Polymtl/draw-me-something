import { Component, Input, OnInit } from '@angular/core';
import { User } from '@app/models/user/user';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { FriendsService } from '@services/friends/friends.service';
import { RouterService } from '@services/router/router.service';
import { SnackBarService } from '@services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {

  constructor(private authService: AuthenticationService,
              private friendsService: FriendsService,
              private snackbar: SnackBarService, private router: RouterService) { }

  @Input() user: User;
  // in pixels
  @Input() size: number = 50;
  @Input() overlay: boolean = true;

  currentUser: boolean = false;

  displayOptions: boolean = false;

  isFriend: boolean = false;
  isInboundRequest: boolean = false;
  isOutboundRequest: boolean = false;

  ngOnInit(): void {
    this.currentUser = this.user.pseudo === this.authService.currentUserPseudo;
    this.isFriend = this.friendsService.isFriend(this.user);
    this.isInboundRequest = this.friendsService.isInboundRequest(this.user);
    this.isOutboundRequest = this.friendsService.isOutboundRequest(this.user);
  }

  enter(): void {
    this.displayOptions = true && !this.currentUser;
  }

  leave(): void {
    this.displayOptions = false;
  }

  acceptRequest(pseudo: string): void {
    this.friendsService.answerRequest(pseudo, true).subscribe({
      next: res => {
        if (res) {
          this.snackbar.success('Demande acceptée');
          this.authService.updateCurrentUser();
        }
      },
      error: () => {
        this.snackbar.error('Erreur lors de l\'acceptation de la demande.');
      }
    });
  }

  sendFriendRequest(pseudo: string): void {
    this.friendsService.sendFriendRequest(pseudo).subscribe({
      next: result => {
        if (result) {
          this.snackbar.success('Demande envoyée');
          this.authService.updateCurrentUser();
        }
      },
      error: () => {
        this.snackbar.success(`Erreur lors de l'envoi de la demande`);
      }
    }
    );
  }

  profile(id: string): void {
    this.router.friendProfile(id);
  }
}
