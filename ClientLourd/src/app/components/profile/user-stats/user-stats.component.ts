import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { GameMode } from '@app/models/game/game-mode';
import { Statistics } from '@app/models/user/statistics';

@Component({
  selector: 'app-user-stats',
  templateUrl: './user-stats.component.html',
  styleUrls: ['./user-stats.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserStatsComponent {
  @Input() classicStats: Statistics;
  @Input() soloStats: Statistics;

  displayClassic: boolean = true;
  displaySprintSolo: boolean = false;

  selected = GameMode.CLASSIC;

  changeMode(event: MatSelectChange): void {
    if (event.value === GameMode.CLASSIC) {
      this.displayClassic = true;
      this.displaySprintSolo = false;
    } else {
      this.displayClassic = false;
      this.displaySprintSolo = true;
    }
  }
}
