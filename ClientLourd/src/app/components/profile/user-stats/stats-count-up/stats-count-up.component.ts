import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats-count-up',
  templateUrl: './stats-count-up.component.html',
  styleUrls: ['./stats-count-up.component.scss']
})
export class StatsCountUpComponent implements OnInit {
  @Input() icon: string = '';
  @Input() text: string = '';
  @Input() from: number = 0;
  @Input() to: number = 0;
  @Input() animation: 'spin' | 'rubber-band' | '' = '';
  @Input() decimals: number = 0;
  @Input() suffix: string = '';
  @Input() duration: number = 4000;
  @Input() frames: number = 100;
  @Input() type: 'number' | 'percent' | 'time' = 'number';
  current: number = 0;
  display: string = '';

  ngOnInit(): void {
    this.current = this.from;
    const delta = this.to - this.from;
    const animationDuration = this.duration;
    const frames = this.frames;
    const frameDuration = animationDuration / frames;
    const valuePerFrame = delta / frames;
    const interval = setInterval(() => {
      this.current += valuePerFrame;
      if (this.current >= this.to) {
        this.current = this.to;
      }
      if (this.type === 'time') {
        this.display = this.toTime(this.current);
      } else {
        this.display = this.current.toFixed(this.decimals) + this.suffix;
      }
      if (this.current >= this.to) {
        clearInterval(interval);
      }

    }, frameDuration);
  }

  private toTime(current: number): string {
    let time = '';
    let s = Math.floor(current / 1000);
    let m = Math.floor(s / 60);
    s = s % 60;
    let h = Math.floor(m / 60);
    m = m % 60;
    const d = Math.floor(h / 24);
    h = h % 24;
    // h += d * 24;
    const hours = ('0' + h).slice(-2);
    const mins = ('0' + m).slice(-2);
    const secs = ('0' + s).slice(-2);
    if (d) {
      time += `${d} jours `;
    }
    time += hours + ':' + mins + ':' + secs;
    return time;
  }

}
