import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsCountUpComponent } from './stats-count-up.component';

describe('StatsCountUpComponent', () => {
  let component: StatsCountUpComponent;
  let fixture: ComponentFixture<StatsCountUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatsCountUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsCountUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
