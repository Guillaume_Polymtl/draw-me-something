import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CANVAS_DEFAULT_HEIGHT, CANVAS_DEFAULT_WIDTH } from '@app/models/drawing/constants';
import { Drawing } from '@app/models/drawing/drawing';
import { User } from '@app/models/user/user';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { AVATAR_CHANGED, AVATAR_ERROR } from '@app/services/chat/errors';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { ProfileService } from '@app/services/profile/profile.service';
import { RouterService } from '@app/services/router/router.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { read } from 'node:fs';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-edit-avatar',
  templateUrl: './edit-avatar.component.html',
  styleUrls: ['./edit-avatar.component.scss'],
})
export class EditAvatarComponent implements OnInit {

  displayEditor: boolean = false;
  displayImage: boolean = false;

  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: any;
  downloadURL: Observable<any>;
  pseudo: string;

  selectedFile: any;

  drawing: Drawing;

  constructor(
    public authService: AuthenticationService,
    private afStorage: AngularFireStorage,
    private profileService: ProfileService,
    private drawingService: DrawingService,
    private snackBarService: SnackBarService,
    private routerService: RouterService,
    private matDialogRef: MatDialogRef<EditAvatarComponent>) {
    this.authService.connected.subscribe((data: boolean) => {
      if (data) {
        this.authService.currentUser.subscribe((user: User) => {
          this.pseudo = user.pseudo;
        });
      }
    });
  }

  ngOnInit(): void {
    this.drawingService.getCurrentDrawing().subscribe({
      next: drawing => {
        this.drawing = drawing;
      }
    });
  }

  processFile(event: any): void {
    const file = event.target.files[0];
    // file too large
    if (file.size > Math.pow(2, 20)) {
      return;
    }
    const randomId = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref('/images/' + this.pseudo + '/' + randomId);
    // tslint:disable-next-line: no-non-null-assertion
    this.task = this.ref.put((event.target as HTMLInputElement).files![0]);

    this.uploadProgress = this.task.snapshotChanges()
      .pipe(map(taskSnapshot => (
        // tslint:disable-next-line: no-non-null-assertion
        taskSnapshot!.bytesTransferred / taskSnapshot!.totalBytes) * 100
      ));

    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges()
      .pipe(
        finalize(() => this.downloadURL = this.ref.getDownloadURL())
      )
      .subscribe();
  }

  setUserAvatar(avatarURL: any): void {
    this.profileService.editAvatar(avatarURL).subscribe({
      next: result => {
        if (result) {
          this.snackBarService.success(AVATAR_CHANGED);
          this.authService.updateCurrentUser();
          this.matDialogRef.close();
        }
      },
      error: error => {
        this.snackBarService.error(AVATAR_ERROR);
      }
    }
    );
  }

  showEditor(): void {
    this.displayEditor = true;
    this.displayImage = false;
  }

  setAvatarFromEditor(): void {
    if (!this.drawing.strokes.length) {
      this.snackBarService.warning(`Vous n'avez rien dessiné!`);
      return;
    }
    const ghostCanvas = document.createElement('canvas');
    const ghostCtx = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
    ghostCanvas.width = CANVAS_DEFAULT_WIDTH;
    ghostCanvas.height = CANVAS_DEFAULT_HEIGHT;
    this.drawingService.draw(ghostCtx, ghostCtx, this.drawing, true);
    const img = ghostCanvas.toDataURL('image/png');
    const randomId = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref('/images/' + this.pseudo + '/' + randomId);

    const arr = img.split(',');
    const mime = (arr[0].match(/:(.*?);/) as string[])[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    const file = new File([u8arr], 'filename', { type: mime });

    this.task = this.ref.put(file);

    this.uploadProgress = this.task.snapshotChanges()
      .pipe(map(taskSnapshot => (
        // tslint:disable-next-line: no-non-null-assertion
        taskSnapshot!.bytesTransferred / taskSnapshot!.totalBytes) * 100
      ));

    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = this.ref.getDownloadURL();
          this.downloadURL.subscribe((url: string) => {
            this.setUserAvatar(url);
          });
        })
      )
      .subscribe();
  }

}
