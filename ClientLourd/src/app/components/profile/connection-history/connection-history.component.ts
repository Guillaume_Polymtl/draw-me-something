import { AfterContentInit, AfterViewInit, Component, Input, OnChanges, OnInit } from '@angular/core';
import { Connection } from '@app/models/user/connection';
import { ConnectionType } from '@app/models/user/connection-type';

@Component({
  selector: 'app-connection-history',
  templateUrl: './connection-history.component.html',
  styleUrls: ['./connection-history.component.scss']
})
export class ConnectionHistoryComponent implements OnChanges {
  @Input() logins: number[] = [];
  @Input() logouts: number[] = [];

  connections: Connection[] = [];

  ngOnChanges(): void {
    this.connections = [];
    if (this.logins && this.logouts) {
      for (const login of this.logins) {
        this.connections.push({ date: login, type: ConnectionType.CONNECTION });
      }
      for (const logout of this.logouts) {
        this.connections.push({ date: logout, type: ConnectionType.DISCONNECTION });
      }
      this.connections = this.connections.sort((connection1: Connection, connection2: Connection) => {
        return connection2.date - connection1.date;
      });
    }
  }
}
