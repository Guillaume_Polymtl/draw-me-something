import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { User } from '@app/models/user/user';
import { ProfileService } from '@app/services/profile/profile.service';
import { EditAvatarComponent } from '../edit-avatar/edit-avatar.component';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';
import { FriendsService } from '@services/friends/friends.service';
import { AuthenticationService } from '@services/authentication/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

  user: User;

  constructor(
    protected authService: AuthenticationService,
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
  ) { }

  ngOnInit(): void {
    this.authService.updateCurrentUser();
    this.authService.currentUser.subscribe((user: User) => {
      this.user = user;
    });
  }

  editAvatar(): void {
    this.dialog.open(EditAvatarComponent);
  }

  editProfile(): void {
    this.dialog.open(EditProfileComponent);
  }

  click(): void {
    console.log(this.user);
  }

}
