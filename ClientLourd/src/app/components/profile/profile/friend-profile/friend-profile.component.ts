import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { User } from '@models/user/user';
import { AuthenticationService } from '@services/authentication/authentication.service';
import { ProfileService } from '@services/profile/profile.service';
import { SnackBarService } from '@services/snack-bar/snack-bar.service';
import { ProfileComponent } from '../profile.component';

@Component({
  selector: 'app-friend-profile',
  templateUrl: './friend-profile.component.html',
  styleUrls: ['./friend-profile.component.scss']
})
export class FriendProfileComponent extends ProfileComponent implements OnInit {


  constructor(authService: AuthenticationService,
              dialog: MatDialog, snackBar: SnackBarService,
              private route: ActivatedRoute, private profileService: ProfileService) {
    super(authService, dialog, snackBar);
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;
    this.profileService.getUser(id).subscribe((user: User) => {
      this.user = user;
    });
  }

}
