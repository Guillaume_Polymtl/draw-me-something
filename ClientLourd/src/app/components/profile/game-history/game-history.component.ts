import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { GameMode } from '@models/game/game-mode';
import { GameResult } from '@models/game/game-result';
import { User } from '@models/user/user';
import { AuthenticationService } from '@services/authentication/authentication.service';

@Component({
  selector: 'app-game-history',
  templateUrl: './game-history.component.html',
  styleUrls: ['./game-history.component.scss']
})
export class GameHistoryComponent implements OnInit, OnChanges {

  private user: User;

  constructor(private authService: AuthenticationService) { }

  @Input() games: GameResult[] = [];
  @Input() soloGames: GameResult[] = [];

  displayClassic: boolean = true;
  displaySprintSolo: boolean = false;

  selected = GameMode.CLASSIC;

  bestSoloScore: number | undefined;

  ngOnInit(): void {
    this.authService.currentUser.subscribe((user: User) => {
      this.user = user;
    });
  }

  ngOnChanges(): void {
    this.initClassicGames();
    this.initSoloGames();
  }

  initClassicGames(): void {
    if (!this.games) {
      return;
    }

    this.games.forEach((game: GameResult) => {
      const maxScore = Math.max(game.winningTeam.score ?? 0, game.losingTeam.score ?? 0);
      const minScore = Math.min(game.winningTeam.score ?? 0, game.losingTeam.score ?? 0);
      game.result = `Défaite ${minScore}-${maxScore}`;
      if (maxScore === minScore) {
        game.result = `Égalité ${minScore}-${maxScore}`;
        return;
      }
      if (!game.winningTeam.players) {
        return;
      }
      if (!this.user.pseudo) {
        return;
      }
      for (const user of game.winningTeam.players) {
        if (user.pseudo === this.user.pseudo) {
          game.result = `Victoire ${maxScore}-${minScore}`;
          return;
        }
      }
    });
  }

  initSoloGames(): void {
    if (!this.soloGames) {
      return;
    }
    const soloGames = this.soloGames.map((game) => game);
    this.bestSoloScore = soloGames.sort((a, b) => {
      if (a.roundNum !== undefined && b.roundNum !== undefined) {
        console.log('sort');
        return b.roundNum - a.roundNum;
      }
      return 0;
    })[0]?.roundNum;
  }

  changeMode(event: MatSelectChange): void {
    if (event.value === GameMode.CLASSIC) {
      this.displayClassic = true;
      this.displaySprintSolo = false;
    } else {
      this.displayClassic = false;
      this.displaySprintSolo = true;
    }
  }
}
