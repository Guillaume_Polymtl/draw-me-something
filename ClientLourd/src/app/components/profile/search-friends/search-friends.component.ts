import { Component, OnInit } from '@angular/core';
import { User } from '@models/user/user';
import { FriendsService } from '@services/friends/friends.service';
import { FilterService } from '@services/helpers/filter/filter.service';

@Component({
  selector: 'app-search-friends',
  templateUrl: './search-friends.component.html',
  styleUrls: ['./search-friends.component.scss']
})
export class SearchFriendsComponent implements OnInit {

  users: User[] = [];

  search: string = '';

  constructor(private friendsService: FriendsService, private filterService: FilterService) { }

  ngOnInit(): void {
    this.friendsService.getAllUsers().subscribe({
      next: users => {
        this.users = users;
        this.filter();
      }
    });
  }

  filter(): void {
    this.filterService.filterUsersByPseudo(this.search, this.users);
  }

}
