import { AfterViewInit, Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FriendRequest } from '@models/user/friend-request';
import { FriendsAndRequests } from '@models/user/friends-and-requests';
import { User } from '@models/user/user';
import { AuthenticationService } from '@services/authentication/authentication.service';
import { FriendsService } from '@services/friends/friends.service';
import { FilterService } from '@services/helpers/filter/filter.service';
import { RouterService } from '@services/router/router.service';
import { SnackBarService } from '@services/snack-bar/snack-bar.service';
import { SearchFriendsComponent } from '../search-friends/search-friends.component';

@Component({
  selector: 'app-profile-friends',
  templateUrl: './profile-friends.component.html',
  styleUrls: ['./profile-friends.component.scss']
})
export class ProfileFriendsComponent implements OnInit, OnChanges {

  avatarSize: number = 30;
  showFilter: boolean = false;

  constructor(
    private filterService: FilterService,
    private friendsService: FriendsService,
    private snackbar: SnackBarService,
    private matDialog: MatDialog,
    private routerService: RouterService,
    private authService: AuthenticationService) { }

  @Input() friendsAndRequests: FriendsAndRequests = {
    friends: [],
    inbound: [],
    outbound: []
  };

  search: string = '';

  ngOnInit(): void {
    this.searchFilter();
  }

  ngOnChanges(): void {
    this.searchFilter();
  }

  toggleFilter(): void {
    this.showFilter = !this.showFilter;
  }

  searchFilter(): void {
    if (this.friendsAndRequests) {
      this.friendsAndRequests.friends = this.filterService.filterUsersByPseudo(this.search, this.friendsAndRequests.friends ?? []);
    }
  }

  searchFriends(): void {
    this.matDialog.open(SearchFriendsComponent);
  }

  acceptRequest(pseudo: string | undefined): void {
    if (!pseudo) {
      return;
    }
    this.friendsService.answerRequest(pseudo, true).subscribe({
      next: result => {
        if (result) {
          this.snackbar.success('Demande acceptée');
          this.authService.updateCurrentUser();
        }
      },
      error: () => {
        this.snackbar.error(`Erreur lors de l'acceptation de la demande.`);
      }
    });
  }

  declineRequest(pseudo: string | undefined): void {
    if (!pseudo) {
      return;
    }
    this.friendsService.answerRequest(pseudo, false).subscribe({
      next: result => {
        if (result) {
          this.snackbar.warning('Demande refusée');
          this.authService.updateCurrentUser();
        }
      },
      error: () => {
        this.snackbar.error(`Erreur lors du refus de la demande.`);
      }
    });
  }

}
