import { Component, Input, OnInit } from '@angular/core';
import { Statistics } from '@app/models/user/statistics';

@Component({
  selector: 'app-user-ranking',
  templateUrl: './user-ranking.component.html',
  styleUrls: ['./user-ranking.component.scss']
})
export class UserRankingComponent {
  @Input() soloStats: Statistics;
  @Input() classicStats: Statistics;
}
