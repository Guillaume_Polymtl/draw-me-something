import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { User } from '@app/models/user/user';
import { RankingService } from '@app/services/ranking/ranking.service';
import { FilterService } from '@services/helpers/filter/filter.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

  showClassic: boolean = true;
  showSprintSolo: boolean = false;

  showSearch: boolean = false;

  search: string = '';

  classicUsers: User[] = [];
  sprintSoloUsers: User[] = [];

  private ordered: boolean = true;

  constructor(private rankingService: RankingService, private filterService: FilterService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.rankingService.getClassicRanking().subscribe({
      next: users => {
        this.classicUsers = users;
        this.searchFilter();
        this.classicUsers.sort((user1: User, user2: User) => {
          const user1Score = user1.classicStats?.score ?? 0;
          const user2Score = user2.classicStats?.score ?? 0;
          if (user1Score < user2Score) {
            return 1;
          }
          return -1;
        });
        let i = 1;
        for (const user of this.classicUsers) {
          if (user.classicStats) {
            user.classicStats.rank = i;
          }
          i++;
        }
      }
    });
    this.rankingService.getSprintSoloRanking().subscribe({
      next: users => {
        this.sprintSoloUsers = users;
        this.searchFilter();
        this.sprintSoloUsers.sort((user1: User, user2: User) => {
          const user1Score = user1.soloStats?.score ?? 0;
          const user2Score = user2.soloStats?.score ?? 0;
          if (user1Score < user2Score) {
            return 1;
          }
          return -1;
        });
        let i = 1;
        for (const user of this.sprintSoloUsers) {
          if (user.soloStats) {
            user.soloStats.rank = i;
          }
          i++;
        }
      }
    });
  }

  toggleSearch(): void {
    this.showSearch = !this.showSearch;
  }

  displaySprintSolo(): void {
    this.showSprintSolo = true;
    this.showClassic = false;
  }

  displayClassic(): void {
    this.showSprintSolo = false;
    this.showClassic = true;
  }

  searchFilter(): void {
    this.classicUsers = this.filterService.filterUsersByPseudo(this.search, this.classicUsers);
    this.sprintSoloUsers = this.filterService.filterUsersByPseudo(this.search, this.sprintSoloUsers);
  }

  orderClassicByRank(): void {
    this.ordered = !this.ordered;
    this.classicUsers.sort((user1: User, user2: User) => {
      const user1Score = user1.classicStats?.score ?? 0;
      const user2Score = user2.classicStats?.score ?? 0;
      if (user1Score < user2Score && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderSoloByRank(): void {
    this.ordered = !this.ordered;
    this.sprintSoloUsers.sort((user1: User, user2: User) => {
      const user1Score = user1.soloStats?.score ?? 0;
      const user2Score = user2.soloStats?.score ?? 0;
      if (user1Score < user2Score && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderClassicByPseudo(): void {
    this.ordered = !this.ordered;
    this.classicUsers.sort((user1: User, user2: User) => {
      const user1Pseudo = user1.pseudo;
      const user2Pseudo = user2.pseudo;
      if (user1Pseudo < user2Pseudo && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderSoloByPseudo(): void {
    this.ordered = !this.ordered;
    this.sprintSoloUsers.sort((user1: User, user2: User) => {
      const user1Pseudo = user1.pseudo;
      const user2Pseudo = user2.pseudo;
      if (user1Pseudo < user2Pseudo && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderClassicByPlayedGames(): void {
    this.ordered = !this.ordered;
    this.classicUsers.sort((user1: User, user2: User) => {
      const user1NumberPlayed = user1.classicStats?.numberPlayed ?? 0;
      const user2NumberPlayed = user2.classicStats?.numberPlayed ?? 0;
      if (user1NumberPlayed < user2NumberPlayed && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderSoloByBestScore(): void {
    this.ordered = !this.ordered;
    this.sprintSoloUsers.sort((user1: User, user2: User) => {
      const user1BestScore = user1.soloStats?.bestScore ?? 0;
      const user2BestScore = user2.soloStats?.bestScore ?? 0;
      if (user1BestScore < user2BestScore && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderSoloByGamesPlayed(): void {
    this.ordered = !this.ordered;
    this.sprintSoloUsers.sort((user1: User, user2: User) => {
      const user1BestScore = user1.soloStats?.numberPlayed ?? 0;
      const user2BestScore = user2.soloStats?.numberPlayed ?? 0;
      if (user1BestScore < user2BestScore && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderClassicByWins(): void {
    this.ordered = !this.ordered;
    this.classicUsers.sort((user1: User, user2: User) => {
      const user1Wins = user1.classicStats?.wins ?? 0;
      const user2Wins = user2.classicStats?.wins ?? 0;
      if (user1Wins < user2Wins && this.ordered) {
        return 1;
      }
      return -1;
    });
  }

  orderClassicByWinPercentage(): void {
    this.ordered = !this.ordered;
    this.classicUsers.sort((user1: User, user2: User) => {
      const user1Wins = user1.classicStats?.wins ?? 0;
      const user1Games = user1.classicStats?.numberPlayed ?? 1;
      const user1Percentage = user1Wins / user1Games;

      const user2Wins = user2.classicStats?.wins ?? 0;
      const user2Games = user2.classicStats?.numberPlayed ?? 1;
      const user2Percentage = user2Wins / user2Games;

      if (user1Percentage < user2Percentage && this.ordered) {
        return 1;
      }
      return -1;
    });
  }
}
