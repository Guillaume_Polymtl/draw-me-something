import { Component, OnInit } from '@angular/core';
import { User } from '@app/models/user/user';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { IconRegistryService } from '@app/services/icon-registry/icon-registry.service';
import { RouterService } from '@app/services/router/router.service';
import { RouterInterceptorService } from '@services/router-interceptor/router-interceptor.service';
import { Observable } from 'rxjs/internal/Observable';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  authState: Observable<firebase.default.User | null>;
  currentUser: User;

  constructor(private routerService: RouterService,
              public authService: AuthenticationService,
              private interceptor: RouterInterceptorService,
              private iconRegistryService: IconRegistryService) { }

  ngOnInit(): void {
    this.iconRegistryService.addIconsToRegistry();
  }

  get displayHeader(): boolean {
    return this.routerService.canDisplayHeader();
  }

  goBack(): void {
    if (this.interceptor.previousUrl.includes('round')) {
      this.routerService.goHome();
    }
    if (this.canGoBack) {
      this.routerService.goBack();
    }
  }

  get canGoBack(): boolean {
    return this.routerService.canGoBack();
  }

  home(): void {
    this.routerService.goHome();
  }

  ranking(): void {
    this.routerService.ranking();
  }

  authentication(): void {
    this.routerService.authentication();
  }

  isUserSignedIn(): boolean {
    return this.authService.connected.value;
  }

  signOut(): void {
    this.authService.signOut();
  }

  play(): void {
    this.routerService.hall();
  }

  profile(): void {
    this.routerService.profile();
  }

  pairWordDrawing(): void {
    this.routerService.pairWordDrawingLibrary();
  }

  tutorial(): void {
    this.routerService.tutorial();
  }
}
