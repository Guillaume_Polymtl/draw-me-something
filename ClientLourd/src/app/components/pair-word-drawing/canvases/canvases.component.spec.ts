import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasesComponent } from './canvases.component';

describe('CanvasesComponent', () => {
  let component: CanvasesComponent;
  let fixture: ComponentFixture<CanvasesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CanvasesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
