import { Component, ElementRef, Input, OnChanges, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CanvasComponent } from '../canvas/canvas.component';

@Component({
  selector: 'app-canvases',
  templateUrl: './canvases.component.html',
  styleUrls: ['./canvases.component.scss']
})
export class CanvasesComponent implements OnInit, OnChanges {

  @ViewChildren(CanvasComponent) canvases: QueryList<CanvasComponent>;

  @Input() amount: number = 1;
  @Input() size: number | string;

  indexes: number[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.indexes = [];
    for (let i = 0; i < this.amount; i++) {
      this.indexes.push(i);
    }
  }

  get context(): CanvasRenderingContext2D {
    const canvas = this.canvases.toArray()[0];
    return canvas.context;
  }

  get contextes(): CanvasRenderingContext2D[] {
    const contextes = [];
    for (const canvas of this.canvases.toArray()) {
      contextes.push(canvas.context);
    }
    return contextes;
  }

  get background(): CanvasRenderingContext2D {
    const length = this.canvases.toArray().length;
    return this.canvases.toArray()[0].context;
    // return this.canvases.toArray()[length - 1].context;
  }

}
