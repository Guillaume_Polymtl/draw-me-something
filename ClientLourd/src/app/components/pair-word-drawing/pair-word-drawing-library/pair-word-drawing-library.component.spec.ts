import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PairWordDrawingLibraryComponent } from './pair-word-drawing-library.component';

describe('PairWordDrawingLibraryComponent', () => {
  let component: PairWordDrawingLibraryComponent;
  let fixture: ComponentFixture<PairWordDrawingLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PairWordDrawingLibraryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PairWordDrawingLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
