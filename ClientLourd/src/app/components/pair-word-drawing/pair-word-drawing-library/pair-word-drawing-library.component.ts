import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Drawing } from '@app/models/drawing/drawing';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { PairWordDrawingService } from '@app/services/pair-word-drawing/pair-word-drawing.service';
import { RouterService } from '@app/services/router/router.service';
import { CanvasComponent } from '../canvas/canvas.component';
import { CanvasesComponent } from '../canvases/canvases.component';

@Component({
  selector: 'app-pair-word-drawing-library',
  templateUrl: './pair-word-drawing-library.component.html',
  styleUrls: ['./pair-word-drawing-library.component.scss']
})
export class PairWordDrawingLibraryComponent implements OnInit, AfterViewInit {

  canvasSize: number = 300;
  private numberOfDrawings = 6;
  drawingIds: number[] = [];

  currentPage: number = 1;
  pages: number[] = [];

  @ViewChildren(CanvasesComponent) private canvases: QueryList<CanvasesComponent>;
  drawings: Drawing[] = [];

  constructor(
    private router: RouterService,
    private pairWordDrawingService: PairWordDrawingService,
    private drawingService: DrawingService) { }

  ngOnInit(): void {


    for (let i = 0; i < this.numberOfDrawings; i++) {
      this.drawingIds.push(i);
    }
  }


  ngAfterViewInit(): void {
    this.goToPage(this.currentPage);
  }

  goToPage(page: number): void {
    this.currentPage = page;
    this.drawings = [];
    this.pairWordDrawingService.getDrawings(this.numberOfDrawings, this.currentPage).subscribe({
      next: result => {
        if (page > result.numberOfPages) {
          return;
        }
        this.pages = [];
        for (let p = 0; p < result.numberOfPages; p++) {
          this.pages.push(p + 1);
        }

        for (const canvas of this.canvases.toArray()) {
          this.drawingService.clearCanvas(canvas.context);
        }

        let i = 0;
        for (const drawing of result.drawings) {
          const canvas = this.canvases.toArray()[i];
          this.drawingService.clearCanvas(canvas.context);
          const size = this.drawingService.getCanvasSize(canvas.context);
          this.drawingService.resizeStrokes(drawing, size);
          this.drawings.push(drawing);
          this.draw(canvas, drawing, true);
          i++;
        }
      },
      error: error => {
      }
    });
  }

  draw(canvas: CanvasesComponent, drawing: Drawing, instant: boolean): void {
    /*for (const stroke of drawing.strokes) {
      if (stroke.index) {
        this.drawingService.drawWithIndexes(canvas.contextes, drawing, instant);
        return;
      }
    }*/
    this.drawingService.draw(canvas.context, canvas.context, drawing, instant);
  }

  createPairWordDrawing(): void {
    this.router.pairWordDrawingCreate();
  }

}
