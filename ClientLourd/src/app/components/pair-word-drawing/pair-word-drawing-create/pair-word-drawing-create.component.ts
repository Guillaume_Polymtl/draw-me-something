import {
  Component, Directive, ElementRef, Input, OnDestroy,
  OnInit, ViewChild,
} from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { DynamicContentDirective } from '@app/components/shared/dynamic-content/dynamic-content.directive';
import { CANVAS_DEFAULT_HEIGHT, CANVAS_DEFAULT_WIDTH } from '@app/models/drawing/constants';
import { CreationMode } from '@app/models/drawing/creation-mode';
import { Drawing } from '@app/models/drawing/drawing';
import { DrawingMode } from '@app/models/drawing/drawing-mode';
import { PotracePreview } from '@app/models/drawing/potrace/potrace-preview';
import { QuickDraw } from '@app/models/drawing/quick-draw';
import { Stroke } from '@app/models/drawing/stroke';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { PairWordDrawingService } from '@app/services/pair-word-drawing/pair-word-drawing.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { Subscription } from 'rxjs';
import { CanvasComponent } from '../canvas/canvas.component';
import { Vec2 } from '@app/models/drawing/vec2';
import { BLACK, PURPLE, RED } from '@models/drawing/color';
import { ToolName } from '@models/drawing/tool-name';
import { GameDifficulty } from '@models/game/difficulty';

@Directive({ selector: 'quick-draw-canvas' })
export class QuickDrawCanvasDirective {
  @Input() id!: string;
}
@Component({
  selector: 'app-pair-word-drawing-create',
  templateUrl: './pair-word-drawing-create.component.html',
  styleUrls: ['./pair-word-drawing-create.component.scss']
})
export class PairWordDrawingCreateComponent implements OnInit, OnDestroy {

  canvasSize = { x: CANVAS_DEFAULT_WIDTH, y: CANVAS_DEFAULT_HEIGHT };

  canRefreshQuickDraw: boolean = false;

  currentForm: FormGroup;
  previousForm: FormGroup;

  potraceInProgress = false;

  potraceSize: Vec2;

  currentQuickDraw: QuickDraw;


  constructor(
    private fb: FormBuilder,
    private drawingService: DrawingService,
    public pairWordDrawingService: PairWordDrawingService,
    private snackBarService: SnackBarService,
  ) {
    this.currentForm = this.fb.group({
      creationMode: ['', Validators.required],
      word: ['', [Validators.required, Validators.maxLength(30)]],
      difficulty: ['', Validators.required],
      drawingMode: ['', Validators.required],
      panoramicMode: ['', Validators.required],
      centerMode: ['', Validators.required],
      hints: this.fb.array([
        this.fb.control('', Validators.required)
      ], Validators.required),
      potrace: this.fb.group({
        fileControl: ['', Validators.required],
        potraceImage: ['', Validators.required],
        turdSize: [2, Validators.required],
        alphaMax: [1, Validators.required],
        optCurve: [true, Validators.required],
        optTolerance: [0.2, Validators.required],
        threshold: [''],
        blackOnWhite: [false, Validators.required],
        color: ['#000000', Validators.required],
        background: ['#ffffff', Validators.required]
      }),
      strokes: ['', Validators.required]
    });

    // this.drawingMode.disable();
    this.panoramicMode.disable();
    this.centerMode.disable();
    this.potrace.disable();
  }

  @ViewChild(DynamicContentDirective, { static: true }) drawModeHost: DynamicContentDirective;

  @ViewChild('file') fileSelector: ElementRef;

  @ViewChild('quickDrawCanvas') quickDrawCanvas: CanvasComponent;

  @ViewChild('potracePreview') potracePreview: CanvasComponent;

  private creationModeSubscription: Subscription;

  private drawingModeSubscription: Subscription;


  accept: string = '.png, .jpg, .jpeg, .bmp';

  files: any;

  imagePath: any;


  imgURL: any;

  ngOnInit(): void {
    this.previousForm = this.currentForm;
    this.onChanges();
  }


  setStrokes(strokes: Stroke[]): void {
    this.currentForm.get('strokes')?.setValue(strokes);
    this.currentForm.updateValueAndValidity();
  }

  preview(files: any): void {
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const image = new Image();

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      image.src = _event.target?.result as string;
      image.onload = (e) => {
        const height = image.height;
        const width = image.width;
        this.potraceSize = { x: width, y: height };
      };
      this.imgURL = reader.result;
      this.potraceImage?.setValue(reader.result);
      this.refreshPotracePreview();

      if (this.getBase64().length > Math.pow(2, 20)) {
        this.imgURL = '';
        this.potraceImage?.setValue('');
        files = [];
        this.snackBarService.error(`La taille de l'image est trop grande.`);
      }
    };
  }

  private getBase64(): string {
    return this.potraceImage?.value.split(';base64,')[1];
  }

  onChanges(): void {
    this.creationModeSubscription = this.creationMode.valueChanges.subscribe((creationMode: CreationMode) => {
      if (creationMode === CreationMode.MANUAL1) {
        this.drawingMode.disable();
        this.panoramicMode.disable();
        this.centerMode.disable();
      } else {
        this.drawingMode.enable();
      }
      if (creationMode === CreationMode.ASSISTED1) {
        this.potrace.enable();
        this.strokes.disable();
      } else {
        this.potrace.disable();
        this.strokes.enable();
      }
      // quick draw
      if (creationMode === CreationMode.ASSISTED2) {
        this.newQuickDraw();
      } else {
        this.word.enable();
        this.word.setValue('');
      }
    });
    this.drawingModeSubscription = this.drawingMode.valueChanges.subscribe((drawingMode: DrawingMode) => {
      if (drawingMode === DrawingMode.PANORAMIC) {
        this.panoramicMode.enable();
      } else {
        this.panoramicMode.disable();
      }
      if (drawingMode === DrawingMode.CENTER) {
        this.centerMode.enable();
      } else {
        this.centerMode.disable();
      }
    });
    this.currentForm.updateValueAndValidity();
  }

  private getPotracePayLoad(): PotracePreview {
    const imageBase64 = this.getBase64();
    const potraceOptions = this.currentForm.value.potrace;
    for (const key in potraceOptions) {
      if (potraceOptions[key] === '') {
        delete potraceOptions[key];
      }
    }
    const drawing = new Drawing(this.currentForm.value);
    drawing.strokes = [];
    return { potraceOptions, imageBase64, drawing };
  }

  refreshPotracePreview(): void {
    if (this.fileControl?.value && this.potraceImage?.value) {
      const previewPayload = this.getPotracePayLoad();
      this.potraceInProgress = true;
      this.pairWordDrawingService.getPotracePreview(previewPayload).subscribe({
        next: potrace => {
          const canvasSize = this.drawingService.getCanvasSize(this.potracePreview.context);
          this.drawingService.resizeStrokes(potrace, canvasSize);
          for (const stroke of potrace.strokes) {
            stroke.width = 1;
          }
          const ctx = this.potracePreview.context;
          this.drawingService.draw(ctx, ctx, potrace, false);
          const drawingTime = this.drawingService.getTotalDrawingTime(potrace);
          setTimeout(() => {
            this.potraceInProgress = false;
          }, drawingTime);
        },
        error: error => {
          this.potraceInProgress = false;
        }
      });
    }
  }

  get fileControl(): AbstractControl | null | undefined {
    return this.potrace?.get('fileControl');
  }

  get potraceImage(): AbstractControl | null | undefined {
    return this.potrace?.get('potraceImage');
  }

  get optCurve(): boolean {
    return this.potrace?.get('optCurve')?.value;
  }

  ngOnDestroy(): void {
    this.creationModeSubscription.unsubscribe();
    this.drawingModeSubscription.unsubscribe();
  }

  strokesFromEditor(strokes: Stroke[]): void {
    this.setStrokes(strokes);
  }

  getMode(form: FormGroup): CreationMode {
    return (form.get('creationMode') as AbstractControl).value;
  }

  refreshQuickDraw(): void {
    this.canRefreshQuickDraw = false;
    const ctx = this.quickDrawCanvas.context;
    const drawing = this.drawingService.quickDrawToDrawing(ctx, this.currentQuickDraw);
    this.drawingMode.value ? drawing.drawingMode = this.drawingMode.value : drawing.drawingMode = undefined;
    this.centerMode.value ? drawing.centerMode = this.centerMode.value : drawing.centerMode = undefined;
    this.panoramicMode.value ? drawing.panoramicMode = this.panoramicMode.value : drawing.panoramicMode = undefined;
    this.strokes.setValue(drawing.strokes);
    const drawingTime = this.drawingService.getTotalDrawingTime(drawing);
    setTimeout(() => {
      this.canRefreshQuickDraw = true;
    }, drawingTime);
    this.word.setValue(this.currentQuickDraw.frenchWord);
    this.word.disable();
    this.drawingService.clearCanvas(ctx);
    this.drawingService.draw(ctx, ctx, drawing, false);
  }

  newQuickDraw(): void {
    this.getRandomQuickDraw();
    this.refreshQuickDraw();

    // this.drawingService.quickDraw(ctx, blanket, quickDraw);
  }

  private getRandomQuickDraw(): void {
    this.currentQuickDraw = this.pairWordDrawingService.getRandomQuickDraw();
  }


  getErrors(): void {
    Object.keys(this.currentForm.controls).forEach(key => {
      const controlErrors: ValidationErrors | null | undefined = this.currentForm.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
        });
      }
    });
    const errors = this.findInvalidControlsRecursive(this.currentForm);
  }

  public findInvalidControlsRecursive(formToInvestigate: FormGroup | FormArray): string[] {
    const invalidControls: string[] = [];
    const recursiveFunc = (form: FormGroup | FormArray) => {
      Object.keys(form.controls).forEach(field => {
        const control = form.get(field);
        if (control?.invalid) { invalidControls.push(field); }
        if (control instanceof FormGroup) {
          recursiveFunc(control);
        } else if (control instanceof FormArray) {
          recursiveFunc(control);
        }
      });
    };
    recursiveFunc(formToInvestigate);
    return invalidControls;
  }

  get isEditor(): boolean {
    if (this.creationMode.value === CreationMode.MANUAL1) {
      return true;
    }
    if (this.creationMode.value === CreationMode.MANUAL2) {
      return true;
    }
    return false;
  }

  get isPotrace(): boolean {
    return this.creationMode.value === CreationMode.ASSISTED1 ? true : false;
  }

  get isQuickDraw(): boolean {
    return this.creationMode.value === CreationMode.ASSISTED2 ? true : false;
  }

  get difficulty(): AbstractControl {
    return this.currentForm.get('difficulty') as AbstractControl;
  }

  get creationMode(): AbstractControl {
    return this.currentForm.get('creationMode') as AbstractControl;
  }

  get strokes(): AbstractControl {
    return this.currentForm.get('strokes') as AbstractControl;
  }

  get drawingMode(): AbstractControl {
    return this.currentForm.get('drawingMode') as AbstractControl;
  }

  setDrawingMode(drawingMode: DrawingMode): void {
    this.currentForm.get('drawingMode')?.setValue(drawingMode);
  }

  get centerMode(): AbstractControl {
    return this.currentForm.get('centerMode') as AbstractControl;
  }

  get word(): AbstractControl {
    return this.currentForm.get('word') as AbstractControl;
  }

  get panoramicMode(): AbstractControl {
    return this.currentForm.get('panoramicMode') as AbstractControl;
  }

  get potrace(): AbstractControl {
    return this.currentForm.get('potrace') as AbstractControl;
  }

  addPanoramicMode(): void {
    this.currentForm.addControl('panoramicMode', new FormControl('', Validators.required));
  }

  removePanoramicMode(): void {
    this.currentForm.removeControl('panoramicMode');
  }

  addCenterMode(): void {
    this.currentForm.addControl('centerMode', new FormControl('', Validators.required));
  }

  removeCenterMode(): void {
    this.currentForm.removeControl('centerMode');
  }

  get hints(): FormArray {
    return this.currentForm.get('hints') as FormArray;
  }

  addHint(): void {
    this.hints.push(this.fb.control(''));
    this.currentForm.updateValueAndValidity();
  }

  removeHint(index: number): void {
    this.hints.removeAt(index);
  }

  selectFile(): void {
    this.fileSelector.nativeElement.click();
  }


  onSubmit(): void {
    if (this.currentForm.invalid) {
      return;
    }
    this.word.enable();
    const drawing = new Drawing(this.currentForm.value);
    if (this.creationMode.value === CreationMode.ASSISTED1) {
      const potracePayLoad = this.getPotracePayLoad();
      drawing.potraceOptions = potracePayLoad.potraceOptions;
      drawing.imageBase64 = potracePayLoad.imageBase64;
      drawing.size = this.potraceSize;
    } else {
      drawing.size = undefined;
    }
    this.pairWordDrawingService.create(drawing);
  }
}
