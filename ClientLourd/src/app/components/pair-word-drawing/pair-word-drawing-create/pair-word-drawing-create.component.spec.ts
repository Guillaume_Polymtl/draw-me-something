import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PairWordDrawingCreateComponent } from './pair-word-drawing-create.component';

describe('PairWordDrawingCreateComponent', () => {
  let component: PairWordDrawingCreateComponent;
  let fixture: ComponentFixture<PairWordDrawingCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PairWordDrawingCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PairWordDrawingCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
