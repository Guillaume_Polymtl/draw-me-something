import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent {

  @ViewChild('canvas', { static: false }) canvas!: ElementRef<HTMLCanvasElement>;

  @Input() width: number | string = 0;
  @Input() height: number | string = 0;
  get context(): CanvasRenderingContext2D {
    return this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
  }
}
