import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { User } from '@app/models/user/user';
import { PURPLE } from '@models/drawing/color';
@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  errors: string[] = [];
  signUpForm = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.maxLength(128), Validators.minLength(1)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(128), Validators.minLength(1)]),
    pseudo: new FormControl('', [Validators.required, Validators.maxLength(30), Validators.minLength(1)])
  });

  constructor(private dialogRef: MatDialogRef<LoginDialogComponent>, public authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  get pseudo(): string {
    return this.signUpForm.get('pseudo')?.value;
  }

  get password(): string {
    return this.signUpForm.get('password')?.value;
  }

  get confirmPassword(): string {
    return this.signUpForm.get('confirmPassword')?.value;
  }


  onSubmit(): void {
    if (this.hasErrors()) {
      return;
    }
    this.authService.signUp(this.signUpForm.value);
    this.dialogRef.close();

  }

  onCancel(): void {
    this.signUpForm.reset();
    this.dialogRef.close(false);
  }

  hasErrors(): boolean {
    this.errors = [];
    if (this.pseudo.length < 1) {
      this.errors.push('Le pseudo doit avoir au moins 1 caractère.');
    }
    if (this.pseudo.length > 30) {
      this.errors.push('Le pseudo doit avoir au maximum 30 caractères.');
    }
    if (this.password.length < 1) {
      this.errors.push('Le mot de passe doit avoir au moins 1 caractère.');
    }
    if (this.pseudo.length > 128) {
      this.errors.push('Le pseudo doit avoir au maximum 128 caractères.');
    }
    if (this.password !== this.confirmPassword) {
      this.errors.push('Les mots de passes ne correspondent pas.');

    }

    return this.errors.length > 0;
  }
}
