import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { LoginDialogComponent } from '@app/components/authentication/login-dialog/login-dialog.component';
import { User } from '@app/models/user/user';
import { BackgroundDrawingService } from '@services/background-drawing/background-drawing.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements AfterViewInit {

  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;

  constructor(public authService: AuthenticationService,
              private dialog: MatDialog, private backgroundDrawing: BackgroundDrawingService) { }
  password: string;
  pseudo: string;
  loginForm = new FormGroup({
    password: new FormControl('', [Validators.required]),
    pseudo: new FormControl('')
  });

  @Input() error: string | null;

  @Output() submitForm = new EventEmitter();

  ngAfterViewInit(): void {
    const context = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.backgroundDrawing.execute(context);
  }

  signIn(user: User): void {
    this.authService.signIn(user);
  }

  submit(): void {
    if (this.loginForm.valid) {
      this.submitForm.emit(this.loginForm.value);
    }
  }

  openDialog(): void {
    this.dialog.open(LoginDialogComponent);
  }

  hasError = (controlName: string, errorName: string) => {
    return this.loginForm.controls[controlName].hasError(errorName);
  }
}
