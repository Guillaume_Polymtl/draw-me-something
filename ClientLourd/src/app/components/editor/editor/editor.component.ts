import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Drawing } from '@app/models/drawing/drawing';
import { Stroke } from '@app/models/drawing/stroke';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  @Output() strokes: EventEmitter<Stroke[]> = new EventEmitter<Stroke[]>();

  constructor(private drawingService: DrawingService) {

  }


  @HostListener('mouseleave', ['$event'])
  onMouseMove(event: MouseEvent): void {
    this.drawingService.getCurrentDrawing().subscribe((drawing: Drawing) => {
      this.strokes.next(drawing.strokes);

    }).unsubscribe();
  }

  ngOnInit(): void {
  }

  isDrawer(): boolean {
    return true;
  }

}
