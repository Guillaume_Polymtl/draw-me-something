// code from Lukas Marx at https://malcoded.com/posts/angular-color-picker/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Color, BLACK } from '@app/models/drawing/color';
import { Subscription } from 'rxjs';
import { ColorService } from '../../services/color.service';

@Component({
    selector: 'app-color-picker',
    templateUrl: './color-picker.component.html',
    styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent implements OnInit, OnDestroy {

    public hue: Color = BLACK;
    public color: Color = BLACK;

    @Output() closed: EventEmitter<boolean> = new EventEmitter();

    @Input() size: string;

    @Input() show: boolean = false;

    private colorSub: Subscription;

    constructor(private colorService: ColorService) { }

    ngOnInit(): void{
        this.colorSub = this.colorService.getColor().subscribe((color: Color) => {
            this.color = color;
        });
    }

    ngOnDestroy(): void{
        this.colorSub.unsubscribe();
    }

    close(): void {
        this.show = false;
        this.closed.next(true);
    }

    selectColor(color: Color): void {
        this.colorService.setColor(color);
    }

    changeHue(color: Color): void {
        this.hue = color;
        this.colorService.setColor(color);
    }
}
