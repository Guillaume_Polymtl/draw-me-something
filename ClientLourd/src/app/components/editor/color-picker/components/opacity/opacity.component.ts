import { Component, OnInit } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { DEFAULT_OPACITY, MAX_OPACITY, MIN_OPACITY, STEP_OPACITY } from '@app/models/drawing/constants';
import { ColorService } from '../../services/color.service';

@Component({
  selector: 'app-opacity',
  templateUrl: './opacity.component.html',
  styleUrls: ['./opacity.component.scss']
})
export class OpacityComponent {

  readonly maxOpacity = MAX_OPACITY;
  readonly minOpacity = MIN_OPACITY;
  readonly defaultOpacity = DEFAULT_OPACITY;
  readonly stepOpacity = STEP_OPACITY;

  constructor(private colorService: ColorService) { }

  setOpacity(change: MatSliderChange): void{
    const opacity = change.value ?? MAX_OPACITY;
    this.colorService.setOpacity(opacity / MAX_OPACITY);
  }

}
