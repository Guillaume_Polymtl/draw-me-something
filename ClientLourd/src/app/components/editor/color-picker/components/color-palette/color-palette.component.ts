import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Color, BLACK, WHITE } from '@app/models/drawing/color';
import { Vec2 } from '@app/models/drawing/vec2';


@Component({
    selector: 'app-color-palette',
    templateUrl: './color-palette.component.html',
    styleUrls: ['./color-palette.component.scss'],
})
export class ColorPaletteComponent implements AfterViewInit, OnChanges {

    @Input() hue: Color = BLACK;
    @Output() color: EventEmitter<Color> = new EventEmitter(true);
    @ViewChild('canvas', { static: true }) canvas!: ElementRef<HTMLCanvasElement>;

    private ctx!: CanvasRenderingContext2D;
    private mousedown = false;
    public selectedPosition: Vec2;

    ngAfterViewInit(): void {
        this.draw();
    }

    draw(): void {
        if (!this.ctx && this.canvas) {
            this.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        }
        const width = this.canvas.nativeElement.width;
        const height = this.canvas.nativeElement.height;

        this.ctx.fillStyle = this.hue.value || 'rgba(255,255,255,1)';
        this.ctx.fillRect(0, 0, width, height);

        const whiteGrad = this.ctx.createLinearGradient(0, 0, width, 0);
        whiteGrad.addColorStop(0, 'rgba(255,255,255,1)');
        whiteGrad.addColorStop(1, 'rgba(255,255,255,0)');

        this.ctx.fillStyle = whiteGrad;
        this.ctx.fillRect(0, 0, width, height);

        const blackGrad = this.ctx.createLinearGradient(0, 0, 0, height);
        blackGrad.addColorStop(0, 'rgba(0,0,0,0)');
        blackGrad.addColorStop(1, 'rgba(0,0,0,1)');

        this.ctx.fillStyle = blackGrad;
        this.ctx.fillRect(0, 0, width, height);

        if (this.selectedPosition) {
            this.ctx.strokeStyle = WHITE.value;
            this.ctx.fillStyle = WHITE.value;
            this.ctx.beginPath();
            this.ctx.arc(
                this.selectedPosition.x,
                this.selectedPosition.y,
                10,
                0,
                2 * Math.PI
            );
            this.ctx.lineWidth = 5;
            this.ctx.stroke();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hue) {
            this.draw();
            if (this.selectedPosition) {
                this.color.emit(this.getColorAtPosition(this.selectedPosition));
            }
        }
    }

    @HostListener('window:mouseup', ['$event'])
    onMouseUp(evt: MouseEvent): void {
        this.mousedown = false;
    }

    onMouseDown(evt: MouseEvent): void {
        this.mousedown = true;
        this.selectedPosition = { x: evt.offsetX, y: evt.offsetY };
        this.draw();
        this.color.emit(this.getColorAtPosition(this.selectedPosition));
    }

    onMouseMove(evt: MouseEvent): void {
        if (this.mousedown) {
            this.selectedPosition = { x: evt.offsetX, y: evt.offsetY };
            this.draw();
            this.emitColor(this.selectedPosition);
        }
    }

    emitColor(position: Vec2): void {
        const rgbaColor = this.getColorAtPosition(position);
        this.color.emit(rgbaColor);
    }

    getColorAtPosition(position: Vec2): Color {
        const imageData = this.ctx.getImageData(position.x, position.y, 1, 1).data;
        return new Color(imageData[0], imageData[1], imageData[2], 1);
    }
}
