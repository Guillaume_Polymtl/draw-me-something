import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Output, ViewChild } from '@angular/core';
import { BLUE, Color, CYAN, GREEN, PURPLE, RED, YELLOW } from '@app/models/drawing/color';

@Component({
    selector: 'app-color-slider',
    templateUrl: './color-slider.component.html',
    styleUrls: ['./color-slider.component.scss'],
})
export class ColorSliderComponent implements AfterViewInit {

    @ViewChild('canvas')
    canvas!: ElementRef<HTMLCanvasElement>;

    @Output()
    color: EventEmitter<Color> = new EventEmitter();

    private ctx!: CanvasRenderingContext2D;
    private mousedown = false;
    private selectedHeight = 0;

    ngAfterViewInit(): void {
        this.draw();
    }

    draw(): void {
        if (!this.ctx) {
            this.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        }
        const width = this.canvas.nativeElement.width;
        const height = this.canvas.nativeElement.height;

        this.ctx.clearRect(0, 0, width, height);

        const gradient = this.ctx.createLinearGradient(0, 0, 0, height);
        gradient.addColorStop(0, RED.value);
        gradient.addColorStop(0.17, YELLOW.value);
        gradient.addColorStop(0.34, GREEN.value);
        gradient.addColorStop(0.51, CYAN.value);
        gradient.addColorStop(0.68, BLUE.value);
        gradient.addColorStop(0.85, PURPLE.value);
        gradient.addColorStop(1, RED.value);

        this.ctx.beginPath();
        this.ctx.rect(0, 0, width, height);

        this.ctx.fillStyle = gradient;
        this.ctx.fill();
        this.ctx.closePath();

        if (this.selectedHeight) {
            this.ctx.beginPath();
            this.ctx.strokeStyle = 'white';
            this.ctx.lineWidth = 5;
            this.ctx.rect(0, this.selectedHeight - 5, width, 10);
            this.ctx.stroke();
            this.ctx.closePath();
        }
    }

    @HostListener('window:mouseup', ['$event'])
    onMouseUp(evt: MouseEvent): void {
        this.mousedown = false;
    }

    onMouseDown(evt: MouseEvent): void {
        this.mousedown = true;
        this.selectedHeight = evt.offsetY;
        this.draw();
        this.emitColor(evt.offsetX, evt.offsetY);
    }

    onMouseMove(evt: MouseEvent): void {
        if (this.mousedown) {
            this.selectedHeight = evt.offsetY;
            this.draw();
            this.emitColor(evt.offsetX, evt.offsetY);
        }
    }

    emitColor(x: number, y: number): void {
        const rgbaColor = this.getColorAtPosition(x, y);
        this.color.emit(rgbaColor);
    }

    getColorAtPosition(x: number, y: number): Color {
        const imageData = this.ctx.getImageData(x, y, 1, 1).data;
        return new Color(imageData[0], imageData[1], imageData[2], 1);
    }
}
