import { Injectable } from '@angular/core';
import { BLACK, Color } from '@app/models/drawing/color';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ColorService {

    private currentColor: BehaviorSubject<Color> = new BehaviorSubject<Color>(BLACK);

    getColor(): Observable<Color>{
        return this.currentColor;
    }

    setColor(color: Color): void{
        const currentColor = this.currentColor.value;
        const newColor = new Color(color.red, color.green, color.blue, currentColor.alpha);
        this.currentColor.next(newColor);
    }

    setOpacity(opacity: number): void{
        const currentColor = this.currentColor.value;
        const newColor = new Color(currentColor.red, currentColor.green, currentColor.blue, opacity);
        this.currentColor.next(newColor);
    }
}
