import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { MAX_WIDTH, MIN_WIDTH, STEP_WIDTH } from '@app/models/drawing/constants';


@Component({
  selector: 'app-width',
  templateUrl: './width.component.html',
  styleUrls: ['./width.component.scss']
})
export class WidthComponent {

  readonly maxWidth = MAX_WIDTH;
  readonly minWidth = MIN_WIDTH;
  readonly defaultWidth = (this.maxWidth - this.minWidth) / 2;
  readonly stepWidth = STEP_WIDTH;
  @Input() width: number = this.defaultWidth;
  @Output() valueChange: EventEmitter<number> = new EventEmitter(true);

  constructor() { }

  setWidth(change: MatSliderChange): void {
    if (change.value) {
      this.valueChange.next(change.value);
    }
  }

}
