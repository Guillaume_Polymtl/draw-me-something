import { Component, AfterViewInit, OnInit, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import { WHITE } from '@app/models/drawing/color';
import { CANVAS_DEFAULT_WIDTH, CANVAS_DEFAULT_HEIGHT } from '@app/models/drawing/constants';
import { Tool } from '@app/models/drawing/tool';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { ToolsService } from '@app/services/editor/tools/tools.service';
import { UndoRedoService } from '@app/services/editor/undo-redo/undo-redo.service';
import { Subject, Subscription, BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-drawing',
  templateUrl: './drawing.component.html',
  styleUrls: ['./drawing.component.scss'],
})
export class DrawingComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('canvasContainer', { static: false }) canvasContainer!: ElementRef<HTMLDivElement>;
  @ViewChild('baseCanvas', { static: false }) baseCanvas!: ElementRef<HTMLCanvasElement>;
  // On utilise ce canvas pour dessiner sans affecter le dessin final
  @ViewChild('grid', { static: false }) gridCanvas!: ElementRef<HTMLCanvasElement>;
  @ViewChild('previewCanvas', { static: false }) previewCanvas!: ElementRef<HTMLCanvasElement>;

  private baseCtx!: CanvasRenderingContext2D;
  private previewCtx!: CanvasRenderingContext2D;
  private gridCtx !: CanvasRenderingContext2D;


  ctxLoaded!: Subject<void>;
  tool!: Tool;

  private toolChangeSub: Subscription;
  private canvasLoadedSub: Subscription;

  private drawingStarted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private timeStarted: number;

  constructor(
    private drawingService: DrawingService,
    public toolsService: ToolsService,
    private undoRedoService: UndoRedoService,
  ) { }

  ngOnInit(): void {
    this.drawingService.resetCurrentDrawing();
    this.toolChangeSub = this.toolsService.getTool().subscribe((tool: Tool) => {
      this.tool = tool;
    });
    this.ctxLoaded = new Subject<void>();
    this.canvasLoadedSub = this.ctxLoaded.subscribe(() => {
      this.drawingService.isLoaded = true;
    });
    this.drawingStarted.subscribe((value: boolean) => {
      if (value) {
        this.timeStarted = Date.now();
      }
    });
    this.drawingService.drawingReseted.subscribe((value: boolean) => {
      if (value) {
        this.drawingStarted.next(false);
      }
    });
  }

  ngOnDestroy(): void {
    this.toolChangeSub.unsubscribe();
    this.canvasLoadedSub.unsubscribe();
  }

  ngAfterViewInit(): void {
    const parent = document.getElementById('parent') as HTMLDivElement;
    parent.style.height = this.height + 'px';
    parent.style.width = this.width + 'px';

    this.baseCtx = this.baseCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.baseCtx.canvas.width = parent?.offsetWidth;
    this.baseCtx.canvas.height = parent?.offsetHeight;
    this.baseCtx.fillStyle = WHITE.value;
    this.baseCtx.fillRect(0, 0, this.width, this.height);
    this.drawingService.baseCtx = this.baseCtx;

    this.previewCtx = this.previewCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.previewCtx.canvas.width = parent?.offsetWidth;
    this.previewCtx.canvas.height = parent?.offsetHeight;
    this.drawingService.previewCtx = this.previewCtx;

    this.gridCtx = this.gridCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.gridCtx.canvas.width = parent?.offsetWidth;
    this.gridCtx.canvas.height = parent?.offsetHeight;
    this.drawingService.gridCtx = this.gridCtx;

    this.ctxLoaded.next();
  }

  get timeOfDrawing(): number {
    return Date.now() - this.timeStarted;
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent): void {
    this.tool.onMouseMove(event, this.timeOfDrawing);
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: MouseEvent): void {
    if (!this.drawingStarted.value) {
      this.drawingStarted.next(true);
    }
    this.tool.onMouseDown(event, this.timeOfDrawing);
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp(event: MouseEvent): void {
    this.tool.onMouseUp(event, this.timeOfDrawing);
  }

  @HostListener('wheel', ['$event'])
  onMouseWheel(event: WheelEvent): void {
    this.tool.onMouseWheel(event);
  }

  @HostListener('contextmenu', ['$event'])
  onContextMenu(event: MouseEvent): void {
    event.preventDefault();
    this.tool.onContextMenu(event);
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave(event: MouseEvent): void {
    this.tool.onMouseLeave(event, this.timeOfDrawing);
  }

  @HostListener('mouseenter', ['$event'])
  onMouseEnter(event: MouseEvent): void {
    this.tool.onMouseEnter(event);
  }

  @HostListener('click', ['$event'])
  onMouseClick(event: MouseEvent): void {
    this.tool.onMouseClick(event);
  }

  @HostListener('dblclick', ['$event'])
  onMouseDblClick(event: MouseEvent): void {
    this.tool.onMouseDblClick(event);
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): void {
    this.tool.onKeyDown(event);
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    this.tool.onKeyUp(event);
  }

  get width(): number {
    return CANVAS_DEFAULT_WIDTH;
  }

  get height(): number {
    return CANVAS_DEFAULT_HEIGHT;
  }
}
