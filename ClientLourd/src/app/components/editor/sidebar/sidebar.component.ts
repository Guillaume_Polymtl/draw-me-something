import { Component, OnInit, OnDestroy } from '@angular/core';
import { CANVAS_DEFAULT_HEIGHT } from '@app/models/drawing/constants';
import { Tool } from '@app/models/drawing/tool';
import { ToolName } from '@app/models/drawing/tool-name';
import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { GridService } from '@app/services/editor/grid/grid.service';
import { EraserService } from '@app/services/editor/tools/eraser/eraser.service';
import { PencilService } from '@app/services/editor/tools/pencil/pencil.service';
import { ToolsService } from '@app/services/editor/tools/tools.service';
import { UndoRedoService } from '@app/services/editor/undo-redo/undo-redo.service';
import { Color } from '@models/drawing/color';
import { Subscription } from 'rxjs';
import { ColorService } from '../color-picker/services/color.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {

  iconColor: string = 'white';
  iconSize: string = '40';

  height = CANVAS_DEFAULT_HEIGHT;

  gridActivated: boolean = false;

  tool: ToolName;
  pencil: ToolName.PENCIL;
  eraser: ToolName.ERASER;

  showColorPicker: boolean = false;

  displayPencilPanel: boolean = false;
  displayEraserPanel: boolean = false;
  displayGridPanel: boolean = false;

  private toolSub: Subscription;

  color: Color;

  constructor(
    private toolsService: ToolsService,
    private undoRedoService: UndoRedoService,
    private gridService: GridService,
    private drawingService: DrawingService,
    private pencilService: PencilService,
    private eraserService: EraserService,
    private colorService: ColorService) { }

  ngOnInit(): void {
    this.toolSub = this.toolsService.getTool().subscribe((tool: Tool) => {
      this.tool = tool.name;
    });
    this.colorService.getColor().subscribe((color: Color) => {
      this.color = color;
    });
    this.gridService.isVisible.subscribe((visible: boolean) => {
      this.gridActivated = visible;
    });
  }

  ngOnDestroy(): void {
    this.toolSub.unsubscribe();
  }

  closeColorPicker(): void {
    this.showColorPicker = false;
  }

  toggleColorPicker(): void {
    this.showColorPicker = !this.showColorPicker;
  }

  get pencilWidth(): number {
    return this.pencilService.getWidth();
  }

  get isPencil(): boolean {
    return this.tool === ToolName.PENCIL;
  }

  get isEraser(): boolean {
    return this.tool === ToolName.ERASER;
  }

  setPencilWidth(width: number): void {
    this.pencilService.setWidth(width);
  }

  get eraserWidth(): number {
    return this.eraserService.getWidth();
  }

  setEraserWidth(width: number): void {
    this.eraserService.setWidth(width);
  }

  showPencilPanel(): void {
    this.displayPencilPanel = true;
  }
  hidePencilPanel(): void {
    this.displayPencilPanel = false;
  }

  showEraserPanel(): void {
    this.displayEraserPanel = true;
  }

  hideEraserPanel(): void {
    this.displayEraserPanel = false;
  }

  showGridPanel(): void {
    this.displayGridPanel = true;
  }

  preventDefault(event: Event): void {
    event.preventDefault();
  }

  hideGridPanel(): void {
    this.displayGridPanel = false;
  }

  get gridWidth(): number {
    return this.gridService.getWidth();
  }

  setGridWidth(width: number): void {
    this.gridService.setWidth(width);
  }

  canRedo(): boolean {
    return this.undoRedoService.canRedo();
  }

  canUndo(): boolean {
    return this.undoRedoService.canUndo();
  }

  setPencil(): void {
    this.toolsService.setTool(ToolName.PENCIL);
  }

  setEraser(): void {
    this.toolsService.setTool(ToolName.ERASER);
  }

  clear(): void {
    this.undoRedoService.undoAll();
  }

  undo(): void {
    this.undoRedoService.undo();
  }

  redo(): void {
    this.undoRedoService.redo();
  }

  grid(): void {
    this.gridService.toggle();
  }

}
