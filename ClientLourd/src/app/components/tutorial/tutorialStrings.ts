const tutorialText = [
  {
    title: 'Bienvenue!',
    text: `Salut! Je m'appelle Alice! Tout d'abord, je te souhaite la bienvenue dans Guess It. Je te guiderai afin de te montrer les
    fonctionnalités intéressantes du jeu. Clique sur une option de navigation à gauche afin de changer de page.`,
    imgSrc: [],
  },
  {
    title: 'Profil',
    text: `Ton profil affiche plusieurs informations pertinentes qui te donneront un visage unique dans l'application. Il est possible d'y ajouter ta propre image
    comme avatar ou mieux encore, le dessiner toi-même! Dans la section Profil, tu pourras personnaliser les différents aspects de ton compte, consulter ton classement,
    l'historique des parties jouées et plus encore. `,
    imgSrc: [
      { path: 'assets/img/tutorial/profile.png', title: 'Menu du profil' },
      { path: 'assets/img/tutorial/custom-avatar.png', title: 'Modifier l\'avatar' }
    ],
  },
  {
    title: 'Canaux',
    text: `Les canaux de communication sont une partie importante du jeu. Ils te permettront de clavarder en direct avec d'autres utilisateurs en ligne.
    Il est possible de joindre un canal en cliquant sur son nom. La barre d'options permet aussi de télécharger son historique ou de quitter celui-ci. Il est même possible de créer son
    propre canal pour y regrouper plusieurs amis! D'ailleurs, le canal Général regroupe tous les utilisateurs de l'application.`,
    imgSrc: [
      { path: 'assets/img/tutorial/channel-view.png', title: 'Joindre un canal' },
      { path: 'assets/img/tutorial/create-channel.png', title: 'Créer un canal' },
    ],
  },
  {
    title: 'Au jeu!',
    text: `Le bouton "Jouer" permet de rentrer dans le vif de l'action. Il se peut que plusieurs parties en cours y soient déjà. Tu peux facilement rejoindre une partie
    de groupe dans ce menu en cliquant sur l'option "Rejoindre". Tu y verras entres autres les joueurs connectés et le niveau de difficulté de la partie. Une fois la partie rejointe,
    tu auras accès au canal privé de la partie en tout temps afin d'échanger avec les autres joueurs dans la même partie.`,
    imgSrc: [
      { path: 'assets/img/tutorial/play-btn.png', title: 'Entrer dans le jeu' },
      { path: 'assets/img/tutorial/game-hall.png', title: 'Tableau des parties' },
    ],
  },
  {
    title: 'Créer une partie',
    text: `Il est aussi possible de créer une partie personnalisée. Tu pourras choisir entre le mode classique et sprint solo (j'y arrive!), le niveau de difficulté de la partie
    son nom afin qu'elle puisse être retrouvée et les options spécifiques telles que le nombre de manches pour une partie de groupe.`,
    imgSrc: [
      { path: 'assets/img/tutorial/create-game.png', title: 'Créer une partie' },
      { path: 'assets/img/tutorial/created-game.png', title: 'Nouvelle partie créée' }
    ],
  },
  {
    title: 'Vestibule',
    text: `Lorsqu'une partie est rejointe, tu auras accès au vestibule (lobby) de la partie. Une nouveau canal de discussion propre à la partie sera alors créé.
    Au même endroit, tu pourras choisir l'équipe à joindre!`,
    imgSrc: [
      { path: 'assets/img/tutorial/game-lobby.png', title: 'Vestibule de la partie' },
      { path: 'assets/img/tutorial/join-team.png', title: 'Équipe jointe' }
    ],
  },
  {
    title: 'Mode classique',
    text: `Dans ce mode multijoueurs, on y retrouve deux équipes (Rose et Bleue) composées chacune de deux joueurs. Lorsque la partie démarre, un joueur dessine
    tandis que son partenaire tente de deviner le mot associé au dessin en 60 secondes. En cas de bonne réponse, l'équipe a un point. Si le coéquipier n'a pu deviner, l'autre équipe a un
    droit de réplique de 15 secondes afin de récupérer le point. Au tour suivant, les rôles sont inversés. Puis c'est au tour de l'autre équipe, et ainsi de suite.`,
    imgSrc: [
      { path: 'assets/img/tutorial/artist.png', title: 'Partie classique: Artiste' },
      { path: 'assets/img/tutorial/guesser-layout.png', title: 'Partie classique: Devineur' }
    ],
  },
  {
    title: 'Artiste',
    text: `En tant qu'artiste, ton rôle sera de faire deviner ton ou ta partenaire le mot apparaissant à l'écran. Pour ce faire, tu devras sortir ton meilleur coup
    de crayon! Tu as à ta disposition un crayon, une palette de couleurs, une efface, ainsi que les possibilités de revenir en arrière ou même effacer le dessin.`,
    imgSrc: [
      { path: 'assets/img/tutorial/tools.png', title: 'Vue de l\'artiste' },
    ],
  },
  {
    title: 'Devineur',
    text: `En tant que devineur, ton rôle sera de deviner quel est le mot associé au dessin que ton ou ta partenaire trace à l'écran. Pour t'aider, tu auras droit à un certain nombre
    d'indices.`,
    imgSrc: [
      { path: 'assets/img/tutorial/guess.png', title: 'Vue du devineur' },
      { path: 'assets/img/tutorial/guess-box.png', title: 'Champ d\'entrée pour deviner' },
    ],
  },
  {
    title: 'Mode classique - Joueurs virtuels',
    text: `Si 1 ou 2 joueurs humains manquent à l'appel dans la partie multijoueur, il est possible de choisir un joueur virtuel accompagnant chaque humain dans la partie.
    Tu auras le choix entre moi ou encore mon cousin intempestif Bob. Puisque nous sommes incapables de deviner (nous sommes des robots, nous n'avons pas d'yeux!), nous dessinerons
    afin de faire deviner un mot aux joueurs humains. Reste à l'affut de nos messages dans le canal du jeu!:)`,
    imgSrc: [
      { path: 'assets/img/tutorial/vpchoice.png', title: 'Choix du joueur virtuel' },
      { path: 'assets/img/tutorial/vplayer-speech.png', title: 'Message envoyé par mon cousin Bob' },
    ],
  },
  {
    title: 'Sprint Solo',
    text: `Il est aussi possible de jouer seul! Lors de la création d'une partie, tu pourras choisir le mode Sprint solo. Ici, une série d'images seront dessinées et ton but sera de
    deviner les mot associés le plus rapidement possible! Prend garde, tu as un nombre d'essais limité pour chaque dessin et un temps initial de 2 minutes. Heureusement, si tu
    réussis, tu auras un bonus de temps! `,
    imgSrc: [
      { path: 'assets/img/tutorial/guess-bar.png', title: 'Pointage, temps et essais restants' },
      { path: 'assets/img/tutorial/sprint-view.png', title: 'Vue du sprint solo' },
    ],
  },
  {
    title: 'Paire mot-image',
    text: `Une paire mot-image correspond à un mot associé à un dessin comprenant un ou plusieurs indices. La gallerie regroupe tous les dessins et leurs informations, incluant ceux faits par les autres
    utilisateurs. Tu peux toi-même créer ta propre paire mot-image afin de contribuer à l'archive, et possiblement reconnaître ton dessin dans une partie!`,
    imgSrc: [
      { path: 'assets/img/tutorial/pair-lib.png', title: 'Librairie des paires' }
    ],
  },
  {
    title: 'Paire mot-image - Création',
    text: `La création d'une paire mot-image passe par l'éditeur. Ici, tu pourras choisir entre différents modes de création en plus de choisir la manière dont il
    apparaîtra à l'écran. Le mode Assisté 1 permet même de transformée une image téléversée en un joli dessin!`,
    imgSrc: [
      { path: 'assets/img/tutorial/pair-create.png', title: 'Création d\'une nouvelle paire' },
      { path: 'assets/img/tutorial/new-pair.png', title: 'Nouvelle paire générée' },
    ],
  },
  {
    title: 'Classement',
    text: `Le classement regroupe les scores de tous les joueurs de Guess It! Ici, tu pourras jeter un coup d'oeil aux meilleurs joueurs selon le mode et
    en rechercher un par son pseudo. En espérant de retrouver dans les top 5!`,
    imgSrc: [
      { path: 'assets/img/tutorial/rank.png', title: 'Classement du mode classique' }
    ],
  },
  {
    title: 'Amis',
    text: `Savais-tu qu'il était possible de se faire des amis via l'application? Tu peux envoyer une demande d'amititié et en recevoir dans la section "Amis" de ton profil.
    Une fois une demande acceptée, tu auras la chance de consulter son historique et des informations plus spécifiques sur son compte, cachées des autres utilisateurs.
    En plus, un canal de communication privé sera créé entre toi et un ami!`,
    imgSrc: [
      { path: 'assets/img/tutorial/friendList.png', title: 'Liste d\'amis et demandes' },
      { path: 'assets/img/tutorial/friend-get.png', title: 'Recherche des autres joueurs' },
    ],
  },
];

export default tutorialText;
