import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-single-image',
  templateUrl: './single-image.component.html',
  styleUrls: ['./single-image.component.scss']
})
export class SingleImageComponent {

  @Input() source: string;
  @Input() title: string;

}
