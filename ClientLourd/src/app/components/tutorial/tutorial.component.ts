import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { RouterService } from '@services/router/router.service';
import { TypingEffectService } from '@services/typing-effect/typing-effect.service';
import tutorialText from './tutorialStrings';
@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit, AfterViewInit {

  currentPage: number = 1;

  tutorialStr = tutorialText;

  menuElements = [''];

  @ViewChild('typewritertitle') titleElemRef: ElementRef;

  titleElement: HTMLElement;
  currentTitleTimeout: NodeJS.Timeout;

  constructor(private typingEffect: TypingEffectService, private router: RouterService) { }

  ngOnInit(): void {

    this.tutorialStr.forEach((elem) => {
      this.menuElements.push(elem.title);
    });
  }

  ngAfterViewInit(): void {
    this.titleElement = this.titleElemRef.nativeElement;
    this.goToPage(this.currentPage, true);

  }

  previous(): void {
    this.currentPage = Math.max(this.currentPage - 1, 1);
    this.goToPage(this.currentPage);
  }

  next(): void {
    this.currentPage = Math.min(this.currentPage + 1, this.menuElements.length - 1);
    this.goToPage(this.currentPage);
  }

  home(): void {
    this.router.goHome();
  }

  goToPage(page: number, isFirstView?: boolean): void {
    // Reset content and effect
    this.titleElement.innerHTML = '';
    clearInterval(this.currentTitleTimeout);

    const currentTutorialPortion = this.tutorialStr[page - 1];

    if (page === this.currentPage && !isFirstView) {
      this.titleElement.innerHTML = currentTutorialPortion.title;
    } else {
      this.currentTitleTimeout = this.typingEffect.typewriterEffect(this.titleElement, currentTutorialPortion.title, 75);
    }
    this.currentPage = page;
  }


}
