import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RouterService } from '@app/services/router/router.service';
import { BackgroundDrawingService } from '@services/background-drawing/background-drawing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;

  constructor(
    private routerService: RouterService,
    private backgroundDrawing: BackgroundDrawingService) { }

  ngOnInit(): void {

  }

  resize(): void {
    this.canvas.nativeElement.height = window.innerHeight - 30;
    this.canvas.nativeElement.width = window.innerWidth - 30;
  }

  ngAfterViewInit(): void {
    const context = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
    this.backgroundDrawing.execute(context);
  }

  play(): void {
    this.routerService.hall();
  }

  pairWordDrawing(): void {
    this.routerService.pairWordDrawingLibrary();
  }

  profile(): void {
    this.routerService.profile();
  }

  ranking(): void {
    this.routerService.ranking();
  }

  tutorial(): void {
    this.routerService.tutorial();
  }
}
