import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/services/authentication/authentication.service';
import { ChatIntegrationService } from '@app/services/chat/chat-integration/chat-integration.service';
import { AudioService } from '@services/audio/audio.service';
import { GameService } from '@services/play/game/game.service';
import { RouterInterceptorService } from '@services/router-interceptor/router-interceptor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  userConnect: boolean = false;

  constructor(
    private chatIntegrationService: ChatIntegrationService,
    private authService: AuthenticationService,
    private routerInterceptor: RouterInterceptorService,
    private audioService: AudioService,
    private gameService: GameService) {
  }

  isWindowedChat(): boolean {
    return this.routerInterceptor.isWindowedChat();
  }

  ngOnInit(): void {
    this.chatIntegrationService.attach();
    this.authService.connected.subscribe((value: boolean) => {
      this.userConnect = value;
    });
  }
}
