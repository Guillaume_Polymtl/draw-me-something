import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-thrash-icon',
  templateUrl: './thrash-icon.component.html',
  styleUrls: ['./thrash-icon.component.scss']
})
export class ThrashIconComponent {
  @Input() size: string;
  @Input() color: string;
}
