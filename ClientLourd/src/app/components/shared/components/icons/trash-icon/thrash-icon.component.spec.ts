import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThrashIconComponent } from './thrash-icon.component';

describe('TrashIconComponent', () => {
  let component: ThrashIconComponent;
  let fixture: ComponentFixture<ThrashIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThrashIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThrashIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
