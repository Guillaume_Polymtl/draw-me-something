import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pencil-icon',
  templateUrl: './pencil-icon.component.html',
  styleUrls: ['./pencil-icon.component.scss']
})
export class PencilIconComponent {
  @Input() size: string;
  @Input() color: string;
}
