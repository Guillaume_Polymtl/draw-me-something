import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupPlusComponent } from './group-plus.component';

describe('GroupPlusComponent', () => {
  let component: GroupPlusComponent;
  let fixture: ComponentFixture<GroupPlusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupPlusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
