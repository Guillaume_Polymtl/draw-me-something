import { Component, OnInit } from '@angular/core';
import { WHITE } from '@models/drawing/color';
import { IconComponent } from '../icon/icon.component';

@Component({
  selector: 'app-group-plus',
  templateUrl: './group-plus.component.html',
  styleUrls: ['./group-plus.component.scss']
})
export class GroupPlusComponent extends IconComponent implements OnInit {
    ngOnInit(): void {
      this.color = WHITE;
    }
}
