import { Component, Input } from '@angular/core';
import { Color, WHITE } from '@app/models/drawing/color';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent {
  @Input() size: number = 20;
  @Input() color: Color = WHITE;
}
