import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-undo-icon',
  templateUrl: './undo-icon.component.html',
  styleUrls: ['./undo-icon.component.scss']
})
export class UndoIconComponent{
  @Input() size: string;
  @Input() color: string;
}
