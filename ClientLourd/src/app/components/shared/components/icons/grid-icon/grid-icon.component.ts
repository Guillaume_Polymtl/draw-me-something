import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-grid-icon',
  templateUrl: './grid-icon.component.html',
  styleUrls: ['./grid-icon.component.scss']
})
export class GridIconComponent {
  @Input() size: string;
  @Input() color: string;
}
