import { Component, Input, OnInit } from '@angular/core';
import { BLACK } from '@models/drawing/color';

@Component({
  selector: 'app-eraser-icon',
  templateUrl: './eraser-icon.component.html',
  styleUrls: ['./eraser-icon.component.scss']
})
export class EraserIconComponent {
  @Input() size: string = '100%';
  @Input() color: string = BLACK.value;
}
