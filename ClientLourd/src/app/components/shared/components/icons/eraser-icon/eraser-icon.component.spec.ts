import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EraserIconComponent } from './eraser-icon.component';

describe('EraserIconComponent', () => {
  let component: EraserIconComponent;
  let fixture: ComponentFixture<EraserIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EraserIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EraserIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
