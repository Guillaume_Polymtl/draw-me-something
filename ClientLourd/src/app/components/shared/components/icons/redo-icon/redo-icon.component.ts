import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-redo-icon',
  templateUrl: './redo-icon.component.html',
  styleUrls: ['./redo-icon.component.scss']
})
export class RedoIconComponent {
  @Input() size: string;
  @Input() color: string;
}
