import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[app-dynamic-content]'
})
export class DynamicContentDirective {

  constructor(public viewContrainerRef: ViewContainerRef) { }

}
