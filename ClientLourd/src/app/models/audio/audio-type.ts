export enum AudioType {
    MUSIC,
    EFFECTS,
    CLOCK
}
export const audioTypes: AudioType[] = [AudioType.MUSIC, AudioType.EFFECTS, AudioType.CLOCK];
