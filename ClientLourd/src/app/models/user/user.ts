import { GameDifficulty } from '@models/game/difficulty';
import { GameMode } from '@models/game/game-mode';
import { GameResult } from '@models/game/game-result';
import { Game } from '../game/game';
import { FriendsAndRequests } from './friends-and-requests';
import { Statistics } from './statistics';

export interface User {
  pseudo: string;
  newPseudo?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  logged?: boolean;
  loginHistory?: number[];
  logoutHistory?: number[];
  avatar?: string;
  description?: string;
  authData?: string;
  gameHistory?: GameResult[];
  soloGameHistory?: GameResult[];
  classicStats?: Statistics;
  soloStats?: Statistics;

  friendsAndRequests?: FriendsAndRequests;

  display?: boolean;
}


export const MOCK_USER: User = {
  firstName: 'Guillaume',
  lastName: 'Rompre',
  avatar: 'https://static.scientificamerican.com/sciam/cache/file/92E141F8-36E4-4331-BB2EE42AC8674DD3_source.jpg',
  pseudo: 'my_pseudo',
  password: 'a',
  logged: true,
  loginHistory: [
    100000000,
    200000000,
  ],
  logoutHistory: [
    150000000,
    250000000,
  ],
  description: 'this is my super awesome description',
  gameHistory: [

  ],
  soloStats: {
    numberPlayed: 40,
    score: 500,
    wins: 40,
    timePlayedMs: 400000,
    bestScore: 27
  },
  classicStats: {
    numberPlayed: 45,
    score: 670,
    wins: 20,
    timePlayedMs: 400000,
    bestScore: 20,
  },
  friendsAndRequests: {
    friends: [
      {
        pseudo: 'friend 1',
        avatar: 'https://cdn.pixabay.com/photo/2013/07/13/01/24/bunny-155674_960_720.png',
      },
      {
        pseudo: 'friend 2',
        avatar: 'https://media.istockphoto.com/vectors/earth-earth-environment-icon-earth-day-icon-earth-day-vector-earth-vector-id1203947402?s=612x612'
      },
      {
        pseudo: 'friend 3',
        avatar: 'https://i.pinimg.com/originals/7e/27/a8/7e27a8bfbf03bf27ef73b728dd75cb0d.png'
      },
      {
        pseudo: 'friend 4',
        avatar: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgVFRUYGBgYGBgYGhgaGBgYGhgYGBwaGRgYHBgcIS4lHB4rIRgZJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QGBISGDQhGCE0MTQ0NDQ0MTQ0NDExMTQxMTQxNDQ0NDQ0NDExNDQ0MTQ0NDQ/NDQ0PzE0NDE0NDE0Mf/AABEIAQwAvAMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAQUGAgMEB//EAD4QAAIBAgQDBQUFBwMFAQAAAAECEQADBBIhMQVBUQYiYXGBEzJCkaEUUnKx0QcjYoLB4fAzkvEkQ1Oi0sL/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAGhEBAQEBAQEBAAAAAAAAAAAAAAERQTEhYf/aAAwDAQACEQMRAD8A9iooFBqAoiiigcUUUUBRSoNAUE1z4rFpbUu7BVHMmKqGP7eJJXD22uQYzsGVCfCQJ8/A0TV2msc1ebPx7HuJzIgJ92JKruScokx013rWnGMcCCbiAdSYU84IYaHwq4a9PFOqHhu1t9P9S3nEAysbGNQVJEVYeF9orN8wrQ33W0b06/nUNTJrJRWKODtWwUUqKdKgVBpmigVI06KAopTToooopCiHTNIU6ApU6RoEajeN8WTDJnfc6KoiWboJ/Ou+9cCqWYwACSTyA3NeT8Ux7Yq+bjGLakqgmIGs7/Gdj4dauJbjVjMfexdzO7aKSVXTIsEER946bnTUVJWMEgGgBYTLsS0f8RsNfKajuFpmcBFJYnuqgBLbMAJ0/m5QTPS88O7MggNiIOgi0p7i8+8wguxMzy1571Un1S72JCOZbM06gCRr4D8o6zXSmLtXRGZAdANTprI1OsfXSvTMNhkRcqIqL0VQo+QrTjOHWbv+pbR/xKCRy0O4oYoNpZWCIOi6TEDr+nKekitfEsOjgHVSvuugyuDIIJYe/wCVdvHuGnCkBG/d3D3ZJLIygk2w25BGo56MDuI5bDAoTpCgKVnoT0PM8t9qI39ne1D2mWzi2zI/+negLPRHA58wRp+Veho0ivGsfaLAqwiR3ZMFOWfTaD9QPCrh2A469wNhr8e1tAQeVy2fdYCd/wC1MWXi70UUVGhSIp0VBiadI06ArGsqxoMqKcUUBRRRQFI06xagp/b/AIkUtrZQwXBZj0RfLqfyqiIS0JoO7mOuuvvDXp+tSHbPFe0x9xJn2YtjwChVckjn3i1c/CML7a8LZUd9wjGR7kB3AA2kZt+h6VuMW/V77GcNCW/bMO9cUZdu5bHugHx3Pp0qUxPH8OhIZ5I5L3vqNKrHbDtJ7Nlw9sEsxyqie85G6qADCjrEfKu/snxiwzvhxa9jetkhlMEvliSGBkkSNPHTSou8WLA8Rt3hKOGjccx5iuo1C9oMMFU4hAFuWu+GGhIA1VuqkaetS9i7mRWHxKG/3CaKrXbsfuUIX3byHNOXKCCp15SDHrVWsMxOUga67aIJlSByERp+tTn7QsWP3Fjm7m43OEtDcjn3nUgc8p8aj+FWs4nSGZsuxg7Cep/U0S+smwuYQIAGwgSAJlp66A+EeVRl+42HvW8QNMsK2oj2RIgRvsfoatj21QZSNI1PIgA6dd59Yqr8fIa20gyVB2nQg5V/hG9CvT7DyOvj18a21BdksQbmFsvrqigz/CAP6VOioooooAoopU6dBiaVOlFBkKKKxJqDKkTXHxDiVuwjPdYIqiSSYqutxjHYhZwmGFtCNL2JlT5rYEMRH3iPKgtmaonjvaHDYRC+Iuqg5Lu7eCoNSahrnZnFXRF/H3cp3VD7MeMZAhjzJrowHYbA2yGNkXH+/cJcmd5LbjwM1UeYfbjicTiMUyMiXyotgxJCKEUGOZAnTSX30qzdikC3mc7W7d64BGgIITT0Lf2q0cd7IWLyH2SJZughkdUA7y8mCwWU9PXlVO4VdfD/AGi3eTJcGHugoSJJZpBVp7ykRBHrrpV4z1GcH43ZGKxmKuMM62GWzP3iWzATsxgecn1g+xWJYY6zc1Z2uLO8lnMP56E/OtJwhLOCpzE5VMECSBMnqATOvKrB2GsrZcX3tl3UEW10VVJOrFmPjGn6CteRn7Xq3ai/lsMg964QijrJGaI8J+lSllciKpPuqAT5D+1U1cViblwXsluVUhQzkhV3JAUHU6anoOlZ8ax2La2VKLkfRntzmCR3u6dRO0idJ8xl0Vzj2P8AtGJa4uqlclvTQoD3SDzzNmbpAANWDhbBEQR7vdWdpEifIQfWoLh+GLXJLIyqCBlBERoNDygAenlE8LWquuggBQdvOJmP0oy6rrqok78iSOm++/8ASq/xESjH3tmiDqTy8u7HpU07KBE7666mCNTljnt6VXuLXQlvYmCCBPvQAFXXckwP8mjS2/s+ecEnQPcA8g5Ajwqz1DdleHGxhbVs+8Flvxt3m+pNTQFSk8FFBoopGmTSrGgypRTp0CFI06BUFJ43wjFvivaqtu4iqDbW4JS22stkzDM8aSduXOtz4TiryTibNudgtotG2+b9auBFartsMpVtQwIO40Om4q6mPOeM8TxOGGW5xO0WAJyC0FeI/hkr110rj4b+0e8GCsqX15uqum3iwA/4qxXP2b4MgqGuqGJJCsm55klJY6fETXFif2a247l0vyi6CZHTMhX8qvxMqU4Z26wt1lRybTtsHiDvsw0O1b+1PZi1jkUhyjj3LqQ2m4DCRmWfEESYIkzTsd2TWxJuI6qQQbtsi4onbMjqSvLUfOpvhXArgX2mDx3dbWCmZSdwCA2n+aUPyqhj+xXEbLEIq3UJ962FLZR4NBB0HXzNHDxew8e1W5baYIuJAjXY7HfSOnhVu41xHH+za29i6jTIv4Mpc0E6G28MAfAVj2a4/jYC4uxcayBBxDWWtNJMDPab6soI8qpka+GcSLxIciDtrrJ302ifn61ZbatymTO8aHmdJ8q18b4Rh1tvchbLLLB17ozRABVdGnQdawF9jbtTIdkQsIGjMstJPOsqjV4aqYlyp7jjPA0hwTmjlB/Oa6baAzvlJAA6jWB4DQ6Vvxo0mYG2+4JAAGlCDJLFZPJZ/MGqOW60Avsdx1B2kH/N6gsBhjicWlsA+ztkO/SVJYDzzH/1qQ45i/ZWsxgE6Acp325gCSfKpbsFwn2WHFxh37oDGdwvwg+MQaC0qIp0UCoCiilRRRFFFAUUUUADQKQpioHSNOigUUopmiqMGTlVP7T8PbDAYrDEIyuougAlWRzlzZQQCQzKfKaucVw8WwouWbiH40ZfUqQPrVSq+/aC/h1DYm0hRioV7bAA5tpVyNT00rRje2Le7aw5YsPjIAg6SVmSP71NcGyYnB2/aIrh7arcUgEEgZXBB8QaovaPgZwLqUBbCuwAksfszGTAaZCH6H0ol1I28S964GxDC6VMogAFpIGpKndt9TUv7dXEjnsJ3iASY32qsI2VNNZJMkRCwANeQ2+XpXdw68TKsNACPXTMSdNJEUVYMPZDCW7uUyIGkxo3jBB+U1rdhEmAsZmJjfcfr/zWVrFZVkjukEnTmdI/Oqn2w42wHsMOJdgCzHZByDcmcyTl5GOmgZYW2eJYzJqcPaILmIkDUp4FjlB8PI16iAAIGkbeFV/sTwRcJhkQauwDu25YnbXyqw0SCnWIrKjQoNI06BUU6VAUUGigKKZpVkFFFFAxSimKDVCrXdIAJO0GfKtlRfaHFezw9xuZXKv4m0H+eFBx9i9cGkbZnjyzsQak+I4JL1t7TiUdSrDwI5TzrHgmGNqxbQ7qonwJ1I+tdhqo8gGfDu2DuEkoYR+qDvAQT8tpFS+BkO07iQNiBoM2upO4/vWz9pmCh7N9RqJXQayNRJHy8qjsNiyVXqe8Btppv5//AD0qsrZhrcjVu6AMonlzJPzNQWOwAOIw+HT43Nx+W4gsecAK3PmetT2BvAW0g6sBp56CT1OtcnBjm4i/8Foj1/dj/wDRqNLqopxSFOiiKZopUQUU6VFFBoooEadFFAUU6VZBTFAooClRNBNUJqrHGbvtsTZw6mQje0uATplEgE/SP46sWIvqilmMBQST4DeoHsrYze0xBWDecsvXIDoT4kyfKKJVkikaYoNVVX7e2c2FYgSVZSPWRy8xVK4fBQtMkTmgwVAIKqfHYevlV77cPGDuRuSgH+4dPKqNgLQS2qmWEyBPvGRvHLQf5NGb6sWBeACZBOUAkKR4x/DOgHhT4C4HEbwkElNwZ3FpvTQR6VhhFJgHfRt4A3AMeX9K18JfLxED79vfnqs6eWX6mir+adIU6KBQKBRRBSNOlRRRRRUBRRRQOlToqApU6VUBrFzTJqB49xIqDbtnvkd5oJFtebHqegnUkegRPanGm+64S28Z3VLjLvrqUB5aSxPIDxFW7C2EtoqIoVVAVVA0AGwqj9mLGbFwNVsWiSY3e8RE+OXPV8WqkZig1iGoJoqqdu7xNtbSnV2k+Q0Hpv8AKoHCYcqABBKb9M3OI15tpXRx7GC5iWbVktSBGxIGoEbmQaywdlSpMQBAPLxOvw6RH+GjNdGGmF1gaSd4iJJ/Oua6QmLw1wHRibZ0g6nMCfmflXZhdQDELPdSN4B/tUZ2mQm3nGrWnV5GkahSB5CTPgOlFekIayrg4LjBeso/3lBPnsakKKVBNE0poA0UU6BUUUCgKKKKAooorIBQxoFJ6Dh4rjhZtO7fCJHidgPmQPWqfbDNLvo7tncHTme6TroqiPVj8Rrt7RYk3LoQTkt5bjabkE5FG25E+lcOOuhUJ3AQsW0jvaQQDrpGmu+1aZqU7EWiUu3T/wBy60fhTujXzzVaBUT2Ut5cJZERKZo/Exb+tTGWizwjXFxjF+xsu86hTH4joK7SKqfbbEZjaw4Pvtnb8K/4fpQVi2kWu+ScwLsBvrzJGuxHzrdavmCrxkgkSY1GxII6xW3HpqVEKuZWJB11MR5b1g1mHmNNAs6E7QZ6Adf6ijLobG5ELkE5Ed4BAIyqSO6B73d+RFaOF403sMjPBLrLiNxEQQOZBOs7DzrT2hv5cMcs5rgFpBzJcnM3lBb0A3ms+HoyW0Tdo8RAMKN/FvrQ6m+wGLyq+GYktbbQnmOo9IPqau1eb8SU4Z7eJQe4QlwdUMDMfEa+hFehYa8HUMpkEAg+Bo1G2inSiinRQDRQKgUjToCiiigKBTpVkFacS8KT0Brc1RXH72TD3G6IaorPDpcM5Gru9wa65V7iKI3EAn+aseKgLbcxqQBPjmgQPL6KK68NhwiokSURRt90a+tcC4dr6Xrv/btqxUbZ3XvM0xqBB8KqLdwA/wDTWeX7tNP5RUnUT2ecNhrRH3FH+0ZT9QalKBNXnfF8T7THXI+BcizrLDKG8ohq9Ec15PZuZr19u7Id2E+LPPnv+lEqcs2AzkmDrq2kQJnTlXILjO+YbErAG7RspgbCQfEmK22LYhnBIUgEs3whTBAPWA2o6/PnxOKFi21xffcgW1ICgBtgAOepn670RH8QurcxS2y2ZbUCR7puEQQv4ZIqbwdiWBOxaBqPdB5cz09Kg8JgTbUSwle+55lmGY6+f0XlrU5wlJhj8JLHTouf5Sw67eVFiRdFdDmAKuHhTHuE6k9JE/StPZLiRsXfsV06RmsuToyfc8xqNOnlWy1caSDocgOumh2Hlt8608b4aLiBVIV0IdHMyjn7sciBr8+VFXqgioLsrxr7Ta7wy3LZyXFO4bkfI71O0CooooETTrFhRRWVFFY0GdKnSrIKi+0FucPcG/cb8pqVrVfthlKkSCCD5HegomMvM6LkJz3mRVJ+EuAAdNogfKrlhsCiWxaA7gXJrzBEGfE6/OoDBdnrqYi2WZTYtZnWCczOS2UFTsFzE6dBVrFVIgOyDn2TWzvbd06aBjr8yan4qucPYWsddtna8i3FnmQSGUf+xqyGqRrc15lgECtikI7q3NephmgTvGgmvTnFec8RsmzjLqPol9S6nlmgZgY5yCfUUSunE2VZAHRiBCqm4JEAac4qHxeBuYl7jpqlgMqmAQXABc5eZ1qaQtAfaEMCTKyZ1HXT6coqT7EWgcKpPxszGddz9dIoeqxhsQHWRsFKkd2CSCCCT4xtFb+DuyMQ7MQ/diBBZ4jTyWtHF8N9mxJSIS4MyNr3TBlZHj/Su2xEiPhaR55mHKiupmDtcdVPd7hk76gAjw6+Zra+IJeCIObKFmZgDY85kT5Vx4a8VDb5fuR7xUgiR13MeNdDI2XPmCkBifAup3JmfntQasRc+zXUxSAkQEurEZkmJ8SraTV6w95XUMplWAIPUHUVTFuIy5CJUIVaZYQfeAjxnXyrZ2O4iUuPg3M+zYm23Moe9l+RHqDRIuhoNIUzRojToooEadFFA6VOishU6VZUCpUzSAqiq9t0KImKQS+HdXgGMySM6nz0+tWezcV1VlMqwDA9QdQa1Y7CrcR0YSrqVI8CIqB7GubavhWOth8qTubR9w/OR8qqdWRhVf7V8G+0W+4ctxDmtt0YbDyOx855VYqwZaDzXAYpnDoQy3FlHVjqGOYEqDuDIM+NWXsMYw2Q7o5Xy0ViPGCTXTxXgCXWW4vcuJ7rrEx0IIIPhINdXB+FrYVoJZnbO7GJZvQAfSiSNHaPgaYq3kbRlko33W/TQfIVRMPddH+z3pW4un4wDuu0k6n1r1OKi+NcBs4kDOuo2dTldfJqLYpi3O6SY8j73e90R866sBf1yMBkALMNCSIIj8qiOMcPxGBYC4zXLDEZbgJzL/C6+u+vPrUhhroZSVIhgIgzpEkzy8QOtGXeyjLmBgEIAAIAl4+cmajscCl+zfXunOEIGndnMoMb6g1I4a8CrAwQp0jn3RqRGusVHY8H2KvrpdDamY/eQBHrFGnpCHSnNaMC8op6qD9BXRRRRRQaAooooGaKKVZGJrZWArI1QUhTFFAjVe45YNq4mKQSU7rj71toDeogEeMVYq1XVBBBEjp16iqgs3AyhgZBAIPIg6gj0rMioLhb/Z3+zOe4dbLE7ySTb8xuPXwqdmisKypxSoAikazrGg0YiyrqUdQysCGUiQQdwRXlnEcG2CxBs6+xYl7cnQqVIyk/wkn5TXrJqs9ueHC5hy4WXtS465Tow8tm/lozVewj98SBqBOomJzCPOAPGKfGGDWHAWBntiZnUvmJ13Mn0ri4MAyo53adANpKrEz0U124/veyQAy+IQEEAfCeXIaiPwmiPQcEDkSd8o/Kt1FtIUKOQA+QisqNkKdIVlQKiiigDRQaKyHRTrGgdFBoFUFIinSoIvjPDheQrOVhqjCZVuR05f50rl4Txds3sMRCXhoD8NwfeUkDU/r0IE4aheMphnUpcdAV1BzDOnj1G30qonKRqlWO0jYeBdYX7Y09oiPnVeRZdc3TTXz2q2YPG27y57bq69VMx4Ebg+Boa6qxpUiaKYFY37YZWVhIYEEdQRBFOaSuCJBny1oPLmsnD3blqQWtuGEg95GJYHQagwa60MXsKT/5wSdOsAabDXn0rt7T8Iv4rE5sOVRsOhy3GBi4zwfZE9NJ8PWq9e4gwi3iUfD3kBcKyMyMw+46SMpjTyqsPXxRUdwHiS4iwl1Z7wgyIIZdG089fWpGo2KdKigKKKKB0hTNArIdI0UVQiaYpU6ANRXF+MphwMwZnacqKJJiPluPnUoah+OcEXEhf3l206E5XtOUaD7yk81MDTwFERRfE4gTcYWrZ+BTLEc8ziB8vlW7DcHQCMgjnOvzY/0itVvscVn/AKzFExAJcaDpAFYvwDGASMYxI2DDT1MSPTxqiYtYZF+FfQD/AA1x43hFp2zKpRvv2zkafEjf1BqDxF3G4cfvblsknu6ZmIG0ARI9BvXfauYm6AQFt7Sx3Ompg6xvFB0JgsSnuYlo6Oocj+b+1aMWmKCl/tIlVLZQqrmInuz410LwrN/qXrjeWgrIcHsjUozHTVnP9KCMvoblgOmJLuyhwjuoBEarA1Xp5iprAOotoULBGAYCR8Xe1PrrvUenCbJ0W2ANdZM+QM7eVSdiwoACqABEbk6bb0Gzs9cLWQG95HuI/wCJHZT/AEPrUjdw6OIZFYdGUMPkah+At+/xSggqHQg/xZMj6ctUA9DU7QYWraqAqqFA2AAAHkBWylTFAqKKdFKiiigypU6VZBTpUVQRRRRRCop0UGJFa7kgGBJgwNpPIVtrGqqj2uJDMWe1cN0SWGXYwdJJMDSuZOP+0JBLqnS2h84Zj4V6Aa1i2o2UDyAomKxguLYa2oRM++aMjk68yY8OVZPx5G0Fq6w/COXLVhVkNtZ91fkK25ANqhitJi7jaJhnnTVyBv5T+ddK4TEvo7pbU/8Aj96OmYzr4iKm6ZqmObhvD0sJkQQOZ3JPUmu2sRTFFFOiigKKKKAopU6D/9k='
      }, {
        pseudo: 'friend 5',
        avatar: 'https://i.pinimg.com/originals/95/f4/1f/95f41f5579d1f7cbea4a2c18e2de03c6.jpg'
      },
      {
        pseudo: 'friend 6',
        avatar: 'https://i.pinimg.com/originals/16/43/75/164375ad7eea4e9ea28aafafeff98822.jpg'
      }
    ], inbound: [
      {
        from: { pseudo: 'request 1', avatar: 'https://www.seekpng.com/png/detail/416-4169979_easy-simple-drawings-in-flower.png' },
        date: 100000
      },
      {
        from: { pseudo: 'request 2', avatar: 'https://lh3.googleusercontent.com/proxy/AexRURzIY3H1Rp5mw6MDLHaSX6FNCjiR0CJ9ei90jnal-CItquNMbcA76s_qkR0L528mDrIwasU5JKnbaAL8polrrVnTUJwL0bqFzaR2' },
      }
    ]
  }
};
