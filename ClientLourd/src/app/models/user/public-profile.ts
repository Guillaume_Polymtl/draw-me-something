import { PrivateProfile } from './private-profile';

export interface PublicProfile {
    pseudo: string;
}
