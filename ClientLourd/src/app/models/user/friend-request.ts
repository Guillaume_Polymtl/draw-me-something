import { User } from './user';

export interface FriendRequest {
    from?: User;
    to?: User;
    date?: number;
}
