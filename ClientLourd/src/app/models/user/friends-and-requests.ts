import { FriendRequest } from './friend-request';
import { User } from './user';

export interface FriendsAndRequests {
    friends?: User[];
    inbound?: FriendRequest[];
    outbound?: FriendRequest[];
}
