export interface Statistics {
    score: number;
    timePlayedMs: number;
    numberPlayed: number;
    wins: number;
    bestScore: number;
    rank?: number;
}
