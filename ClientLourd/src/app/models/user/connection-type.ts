export enum ConnectionType {
    CONNECTION = 'Connexion',
    DISCONNECTION = 'Déconnexion'
}
