import { ConnectionType } from './connection-type';

export interface Connection {
    type: ConnectionType;
    date: number;
}
