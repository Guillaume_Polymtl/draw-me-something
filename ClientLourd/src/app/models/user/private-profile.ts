import { PublicProfile } from './public-profile';

export interface PrivateProfile extends PublicProfile {
    firstName: string;
    lastName: string;
}
