export interface Guess {
    guessContent?: string;
    gameUID?: string;
    pseudo?: string;
    isGoodGuess?: boolean;
    proximity?: number;
    triesLeft?: number;
}
