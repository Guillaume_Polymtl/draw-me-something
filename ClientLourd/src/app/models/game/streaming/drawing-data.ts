export class DrawingData {
  x: any[];
  y: any[];
  start: any[];
  end: any[];
  width: any[];
  toolType: any[];
  r: any[];
  g: any[];
  b: any[];
  a: any[];
  gameUID: string;

  constructor() {
    this.x = [];
    this.y = [];
    this.start = [];
    this.end = [];
    this.r = [];
    this.g = [];
    this.b = [];
    this.a = [];
    this.width = [];
    this.toolType = [];
    this.gameUID = '';
  }
}
