import { Guess } from './streaming/guess';

export class GameStatus {

    constructor() {
        this.guesses = [];
    }

    UID?: string;
    roundTimeLeft?: number;
    guesses?: Guess[];
}
