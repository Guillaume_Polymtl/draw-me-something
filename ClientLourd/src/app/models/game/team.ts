import { User } from '../user/user';

export interface Team {
  teamMembers?: User[];
  teamUID: string;
  score?: number;
  points?: number;
  isWinner?: boolean;
}
export default interface TeamMember {
  pseudo: string;
  teamUID: string;
  isVirtual?: boolean;
}
