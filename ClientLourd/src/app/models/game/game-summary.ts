import { Team } from './team';

export interface GroupGameSummary {
  gameUID?: string;
  name?: string;
  teams?: Team[];
  winner?: any;
  elapsedTime?: number;
  isEnded?: boolean;
  isPersonnalRecord?: boolean;
  isGlobalRecord?: boolean;
  score?: number;
}
