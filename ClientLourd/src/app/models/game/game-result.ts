import { GameDifficulty } from './difficulty';
import { GameMode } from './game-mode';
import { Team } from './team';

export interface GameResult {
    startTime?: number;
    name: string;
    gameMode: GameMode;
    difficulty: GameDifficulty;

    winningTeam: {
        players: { pseudo: string; avatar: string, isVirtual?: boolean }[];
        score: number;
        name: string;
    };
    losingTeam: {
        players: { pseudo: string; avatar: string, isVirtual?: boolean }[];
        score: number;
        name: string;
    };

    roundNum?: number;

    result?: string;
}
