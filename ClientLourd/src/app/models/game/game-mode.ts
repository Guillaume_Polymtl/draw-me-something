export enum GameMode {
    CLASSIC = 'Classique',
    SPRINT_SOLO = 'Sprint solo',
}

export const GameModes = [GameMode.CLASSIC, GameMode.SPRINT_SOLO];
