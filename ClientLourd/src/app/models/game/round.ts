export default interface RoundModel {
  artist?: string; // empty string if none
  guessers?: string[];

  roundNum?: number;
  wordToGuess?: string;
  timeToGuess?: number;
  soloSprintScore?: number;
}
