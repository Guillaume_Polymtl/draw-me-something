export enum GameDifficulty {
    NORMAL = 'Normale',
    DIFFICULT = 'Difficile'
}

export const GameDifficulties = [GameDifficulty.NORMAL, GameDifficulty.DIFFICULT];
