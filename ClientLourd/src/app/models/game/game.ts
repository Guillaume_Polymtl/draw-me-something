import { GameDifficulty } from './difficulty';
import { GameMode } from './game-mode';
import { Team } from './team';

export interface Game {
  gameMode?: GameMode;
  id?: number;
  date?: number;
  name?: string;
  difficulty?: GameDifficulty;
  result?: string;
  players?: string[];
  gameUID?: string;
  teams?: Team[];
  roundNum?: number;
  timeToGuess?: number;
}
