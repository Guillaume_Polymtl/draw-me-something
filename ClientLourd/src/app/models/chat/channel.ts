import { Message } from './message';

export class Channel {

    constructor(name: string, pseudo: string = '', messages: Message[] = [], unseenMessages: number = 0, isGameChannel: boolean = false) {
        this.channelName = name;
        this.messages = [];
        this.unseenMessages = unseenMessages;
        this.pseudo = pseudo;
        this.isGameChannel = isGameChannel;
    }

    channelName: string;
    messages: Message[];
    unseenMessages: number;
    pseudo: string;
    isOwner: boolean;
    isGameChannel?: boolean;
}
