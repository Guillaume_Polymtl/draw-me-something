export interface Message {
    pseudo: string;
    message: string;
    date: number;
    channelName: string;
    fromCurrent: boolean;
}
