import { Vec2 } from './vec2';

export interface Point {
    position: Vec2;
    time: number;
}
