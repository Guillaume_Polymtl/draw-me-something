export default interface PotraceOptions {
  alphaMax?: number;
  background?: string;
  blackOnWhite?: boolean;
  color?: string;
  optCurve?: boolean;
  optTolerance?: number;
  threshold?: number;
  turdSize?: number;
}
