import { Drawing } from '../drawing';
import PostraceSettingsModel from './potrace-options';

export interface PotracePreview {
  potraceOptions?: PostraceSettingsModel;
  imageBase64: string;
  drawing: Drawing;
}
