export enum CenterMode {
    INWARDS = `Vers l'intérieur`,
    OUTWARDS = `Vers l'extérieur`
}

export const CenterModes: CenterMode[] = [
    CenterMode.INWARDS,
    CenterMode.OUTWARDS
];
