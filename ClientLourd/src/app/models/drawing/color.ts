export const MIN_COLOR_INTENSITY = 0;
export const MAX_COLOR_INTENSITY = 255;

export const MAX_TOLRERANCE = 100;
export const MIN_TOLERANCE = 0;

export enum ColorType {
    PRIMARY = 'Primary',
    SECONDARY = 'Secondary',
}

export class Color {
    red: number;
    green: number;
    blue: number;
    alpha: number;

    constructor(r: number, g: number, b: number, a: number){
        this.red = r;
        this.green = g;
        this.blue = b;
        this.alpha = a;
    }

    get value(): string{
        return `rgba(${this.red},${this.green},${this.blue},${this.alpha})`;
    }

}

export const INVISIBLE = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY);
export const WHITE = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const BLACK = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);

export const RED = new Color(MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const YELLOW = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const GREEN = new Color(MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const CYAN = new Color(MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const BLUE = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const PURPLE = new Color(MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);


export const DEFAULT_COLOR_HISTORY: Color[] = [YELLOW, YELLOW, RED, PURPLE, BLUE, CYAN, GREEN, BLACK, WHITE, YELLOW];
