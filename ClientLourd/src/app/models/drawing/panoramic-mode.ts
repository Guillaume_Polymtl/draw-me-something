export enum PanoramicMode{
    LEFT_RIGHT = 'Gauche à droite',
    RIGHT_LEFT = 'Droite à gauche',
    TOP_BOTTOM = 'Haut en bas',
    BOTTOM_TOP = 'Bas en haut',
}

export const PanoramicModes: PanoramicMode[] = [
    PanoramicMode.LEFT_RIGHT,
    PanoramicMode.RIGHT_LEFT,
    PanoramicMode.TOP_BOTTOM,
    PanoramicMode.BOTTOM_TOP
];
