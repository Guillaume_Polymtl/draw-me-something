interface Point {
  x: number;
  y: number;
}

export default interface DrawingBroadcast {
  gameUID?: string;

  toolSize: number;

  // Right now, the drawing is transmitted point-by-point
  position: Point;
  color: string;
  start: boolean;
  end: boolean;
}
