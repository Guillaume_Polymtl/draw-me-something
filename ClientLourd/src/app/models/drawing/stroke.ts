import { Color } from './color';
import { Point } from './point';
import { ToolName } from './tool-name';

export interface Stroke {
    tool: ToolName;
    points: Point[];
    color: Color;
    width: number;
    index?: number;
}
