export interface QuickDraw {
    frenchWord: string;
    word: string;
    countrycode: string;
    timestamp: string;
    recognized: boolean;
    key_id: string;
    drawing: number[][][];
}
