export enum CreationMode {
    MANUAL1 = 'Manuel 1',
    MANUAL2 = 'Manuel 2',
    ASSISTED1 = 'Assisté 1',
    ASSISTED2 = 'Assisté 2',
    // ASSISTED3 = 'Assisté 3'
}

export const CreationModes = [
    CreationMode.MANUAL1,
    CreationMode.MANUAL2,
    CreationMode.ASSISTED1,
    CreationMode.ASSISTED2,
    // CreationMode.ASSISTED3
];
