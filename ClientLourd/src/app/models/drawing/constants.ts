export const CANVAS_DEFAULT_WIDTH = 500;
export const CANVAS_DEFAULT_HEIGHT = 500;


export const DEFAULT_WIDTH = 20;
export const MIN_WIDTH = 1;
export const MAX_WIDTH = 100;
export const STEP_WIDTH = 5;

export const ERASER_LINE_WIDTH = 2;

export const DEFAULT_HEX = '00';

export const MIN_OPACITY = 0;
export const MAX_OPACITY = 100;
export const DEFAULT_OPACITY = 100;
export const STEP_OPACITY = 5;

export const DEFAULT_GRID_OPACITY = 0.5;
export const MAX_GRID_OPACITY = 1;
export const MIN_GRID_OPACITY = 0.2;

export const DEFAULT_GRID_THICKNESS = 20;
