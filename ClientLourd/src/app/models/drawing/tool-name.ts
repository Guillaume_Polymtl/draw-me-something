export enum ToolName {
    PENCIL = 'Pencil',
    ERASER = 'Eraser',
}
