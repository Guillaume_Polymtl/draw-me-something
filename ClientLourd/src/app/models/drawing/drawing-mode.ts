export enum DrawingMode {
    CONVENTIONAL = 'Conventionnel',
    RANDOM = 'Aléatoire',
    PANORAMIC = 'Panoramique',
    CENTER = 'Centré'
}

export const DrawingModes: DrawingMode[] = [
    DrawingMode.RANDOM,
    DrawingMode.CENTER,
    DrawingMode.CONVENTIONAL,
    DrawingMode.PANORAMIC,
];

