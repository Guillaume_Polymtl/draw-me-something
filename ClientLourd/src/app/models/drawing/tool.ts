import { DrawingService } from '@app/services/editor/drawing/drawing.service';
import { UndoRedoService } from '@app/services/editor/undo-redo/undo-redo.service';
import { Color } from './color';
import { DEFAULT_WIDTH, MAX_WIDTH, MIN_WIDTH } from './constants';
import { MouseButton } from './mouse-buttons';
import { Point } from './point';
import { ToolName } from './tool-name';
import { Vec2 } from './vec2';



export class Tool {
    name: ToolName;
    mouseDownCoord: Vec2 = { x: 0, y: 0 };
    mouseDown = false;


    protected color: Color;

    constructor(protected drawingService: DrawingService, undoredoService: UndoRedoService) { }

    protected width = DEFAULT_WIDTH;

    protected currentStroke: Point[] = [];

    getWidth(): number {
        return this.width;
    }

    setWidth(width: number): void {
        width > MAX_WIDTH ? this.width = MAX_WIDTH : this.width = width;
        width < MIN_WIDTH ? this.width = MIN_WIDTH : this.width = width;
    }

    reset(): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.currentStroke = [];
    }

    onToolChange(): void { }

    onMouseDown(event: MouseEvent, time: number): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            // this.clearPath();
            // this.stroke.points = [];
            this.mouseDownCoord = this.getPositionFromMouse(event);
            // this.pathData.push(this.mouseDownCoord);
            this.currentStroke.push({
                position: this.mouseDownCoord,
                time
            });
        }
    }

    onMouseUp(event: MouseEvent, time: number): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            // this.pathData.push(mousePosition);
            this.currentStroke.push({
                position: mousePosition,
                time
            });
            this.draw(this.drawingService.baseCtx, this.currentStroke);
            this.drawingService.addStrokeToCurrentDrawing({
                width: this.width,
                color: this.color,
                points: this.currentStroke,
                tool: this.name
            });
            this.resetCurrentStroke();
        }
        this.mouseDown = false;
        // this.clearPath();
        this.resetCurrentStroke();
    }

    protected draw(ctx: CanvasRenderingContext2D, drawingPoints: Point[]): void { }

    protected resetCurrentStroke(): void {
        this.currentStroke = [];
    }

    onMouseMove(event: MouseEvent, time: number): void { }

    onContextMenu(event: MouseEvent): void { }

    onMouseLeave(event: MouseEvent, time: number): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.onMouseUp(event, time);
        /*this.draw(this.drawingService.baseCtx, this.currentStroke);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);*/
    }

    onMouseEnter(event: MouseEvent): void {
        this.resetCurrentStroke();
        // this.clearPath();
        if (event.buttons) {
            this.mouseDown = true;
        } else {
            this.mouseDown = false;
        }
    }

    onMouseClick(event: MouseEvent): void { }

    onMouseDblClick(event: MouseEvent): void { }

    onKeyDown(event: KeyboardEvent): void { }

    onKeyUp(event: KeyboardEvent): void { }

    onMouseWheel(event: WheelEvent): void { }

    getPositionFromMouse(event: MouseEvent): Vec2 {
        return { x: event.offsetX, y: event.offsetY };
    }
}
