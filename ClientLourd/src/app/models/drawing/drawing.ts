import { GameDifficulty } from '../game/difficulty';
import { CenterMode } from './center-mode';
import { Color } from './color';
import { DrawingMode } from './drawing-mode';
import { PanoramicMode } from './panoramic-mode';
import PotraceOptions from './potrace/potrace-options';
import PostraceSettingsModel from './potrace/potrace-options';
import { Stroke } from './stroke';
import { Vec2 } from './vec2';

export class Drawing {
    public constructor(init?: Partial<Drawing>) {
        Object.assign(this, init);
        if (!this.strokes) {
            this.strokes = [];
        }
    }
    word?: string;
    hints?: string[];
    difficulty?: GameDifficulty;
    drawingMode?: DrawingMode;
    panoramicMode?: PanoramicMode;
    centerMode?: CenterMode;
    strokes: Stroke[];
    background?: string;
    size?: Vec2;
    creatorPseudo?: string;
    creationTime?: number;
    imageBase64?: string;
    potraceOptions?: PotraceOptions;
}
