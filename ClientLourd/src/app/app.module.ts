import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication/authentication.service';
import { HomeComponent } from './components/home/home.component';
import { GameCreateComponent } from './components/play/game-create/game-create.component';
import { AppComponent } from './components/app/app.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TextButtonComponent } from '@components/shared/components/text-button/text-button.component';
import { TitleComponent } from '@components/shared/components/title/title.component';
import { MatInputModule } from '@angular/material/input';
import { LoginDialogComponent } from './components/authentication/login-dialog/login-dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthPageComponent } from './components/authentication/auth-page/auth-page.component';
import { MatCardModule } from '@angular/material/card';
import { ProfileComponent } from './components/profile/profile/profile.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IntegratedChatComponent } from './components/chat/integrated-chat/integrated-chat.component';
import { WindowedChatComponent } from './components/chat/windowed-chat/windowed-chat.component';
import { ChatComponent } from './components/chat/chat/chat.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { IconComponent } from './components/shared/components/icons/icon/icon.component';
import { MoveComponent } from './components/shared/components/icons/move/move.component';
import { MatTabsModule } from '@angular/material/tabs';
import { GroupPlusComponent } from './components/shared/components/icons/group-plus/group-plus.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateChannelComponent } from './components/chat/create-channel/create-channel.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { ChannelComponent } from './components/chat/channel/channel.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ChatBodyComponent } from './components/chat/chat-body/chat-body.component';
import { ChatSideBarComponent } from './components/chat/chat-side-bar/chat-side-bar.component';
import { PairWordDrawingCreateComponent } from './components/pair-word-drawing/pair-word-drawing-create/pair-word-drawing-create.component';
import { PairWordDrawingLibraryComponent } from './components/pair-word-drawing/pair-word-drawing-library/pair-word-drawing-library.component';
import { CanvasComponent } from './components/pair-word-drawing/canvas/canvas.component';
import { EditorComponent } from './components/editor/editor/editor.component';
import { SidebarComponent } from './components/editor/sidebar/sidebar.component';
import { DrawingComponent } from './components/editor/drawing/drawing.component';
import { WidthComponent } from './components/editor/width/width.component';
import { MatSliderModule } from '@angular/material/slider';
import { ColorPickerComponent } from './components/editor/color-picker/components/color-picker/color-picker.component';
import { ColorPaletteComponent } from './components/editor/color-picker/components/color-palette/color-palette.component';
import { OpacityComponent } from './components/editor/color-picker/components/opacity/opacity.component';
import { ColorSliderComponent } from './components/editor/color-picker/components/color-slider/color-slider.component';
import { ThrashIconComponent } from './components/shared/components/icons/trash-icon/thrash-icon.component';
import { RedoIconComponent } from './components/shared/components/icons/redo-icon/redo-icon.component';
import { UndoIconComponent } from './components/shared/components/icons/undo-icon/undo-icon.component';
import { GridIconComponent } from './components/shared/components/icons/grid-icon/grid-icon.component';
import { EraserIconComponent } from './components/shared/components/icons/eraser-icon/eraser-icon.component';
import { PencilIconComponent } from './components/shared/components/icons/pencil-icon/pencil-icon.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TokenInterceptor } from './services/authentication/token-interceptor';
import { AvatarComponent } from './components/profile/avatar/avatar.component';
import { GameHistoryComponent } from './components/profile/game-history/game-history.component';
import { ConnectionHistoryComponent } from './components/profile/connection-history/connection-history.component';
import { EditAvatarComponent } from './components/profile/edit-avatar/edit-avatar.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { UserStatsComponent } from './components/profile/user-stats/user-stats.component';
import { EditProfileComponent } from './components/profile/edit-profile/edit-profile.component';
import { StatsCountUpComponent } from './components/profile/user-stats/stats-count-up/stats-count-up.component';
import { AudioComponent } from '@components/audio/audio.component';
import { WatchStreamComponent } from './components/play/stream/watch-stream/watch-stream.component';
import { StreamComponent } from './components/play/stream/stream/stream.component';
import { NgParticlesModule } from 'ng-particles';
import { ParticlesComponent } from './components/play/particles/particles.component';
import { RankingComponent } from '@components/ranking/ranking.component';
import { UserRankingComponent } from '@components/profile/user-ranking/user-ranking.component';
import { GameInfoComponent } from './components/play/stream/shared/game-info/game-info.component';
import { LobbyComponent } from './components/play/lobby/lobby/lobby.component';
import { GuessComponent } from './components/play/stream/shared/guess/guess.component';
import { HintsComponent } from './components/play/stream/shared/hints/hints.component';
import { LobbyGameInfoComponent } from './components/play/lobby/lobby-game-info/lobby-game-info.component';
import { HallComponent } from './components/play/hall/hall.component';
import { LobbyTeamsInfoComponent } from './components/play/lobby/lobby-teams-info/lobby-teams-info.component';
import { StreamTeamsInfoComponent } from './components/play/stream/shared/stream-teams-info/stream-teams-info.component';
import { ProfileFriendsComponent } from './components/profile/profile-friends/profile-friends.component';
import { SearchFriendsComponent } from './components/profile/search-friends/search-friends.component';
import { RoundComponent } from '@components/play/round/round/round.component';
import { VirtualPlayerChoiceComponent } from '@components/play/lobby/virtual-player-choice/virtual-player-choice.component';
import { GameSummaryComponent } from '@components/play/game-summary/game-summary/game-summary.component';
import { CanvasesComponent } from './components/pair-word-drawing/canvases/canvases.component';
import { FriendProfileComponent } from './components/profile/profile/friend-profile/friend-profile.component';
import { MatListModule } from '@angular/material/list';
import { TutorialComponent } from '@components/tutorial/tutorial.component';
import { SingleImageComponent } from './components/tutorial/single-image/single-image.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    GameCreateComponent,
    TextButtonComponent,
    TitleComponent,
    LoginDialogComponent,
    HeaderComponent,
    GameCreateComponent,
    AuthPageComponent,
    ProfileComponent,
    IntegratedChatComponent,
    WindowedChatComponent,
    ChatComponent,
    IconComponent,
    MoveComponent,
    GroupPlusComponent,
    CreateChannelComponent,
    ChannelComponent,
    ChatBodyComponent,
    ChatSideBarComponent,
    PairWordDrawingCreateComponent,
    PairWordDrawingLibraryComponent,
    CanvasComponent,
    EditorComponent,
    SidebarComponent,
    DrawingComponent,
    WidthComponent,
    ProfileComponent,
    ColorPickerComponent,
    ColorPaletteComponent,
    OpacityComponent,
    ColorSliderComponent,
    TitleComponent,
    ThrashIconComponent,
    RedoIconComponent,
    UndoIconComponent,
    GridIconComponent,
    EraserIconComponent,
    PencilIconComponent,
    AvatarComponent,
    GameHistoryComponent,
    ConnectionHistoryComponent,
    UserStatsComponent,
    EditAvatarComponent,
    StatsCountUpComponent,
    EditProfileComponent,
    AudioComponent,
    StreamComponent,
    WatchStreamComponent,
    ParticlesComponent,
    RankingComponent,
    UserRankingComponent,
    GameInfoComponent,
    LobbyComponent,
    GameCreateComponent,
    GuessComponent,
    HintsComponent,
    LobbyGameInfoComponent,
    HallComponent,
    LobbyTeamsInfoComponent,
    StreamTeamsInfoComponent,
    RoundComponent,
    VirtualPlayerChoiceComponent,
    GameSummaryComponent,
    ProfileFriendsComponent,
    SearchFriendsComponent,
    RoundComponent,
    CanvasesComponent,
    FriendProfileComponent,
    TutorialComponent,
    SingleImageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireStorageModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    HttpClientModule,
    MatTooltipModule,
    MatButtonModule,
    MatTabsModule,
    MatDialogModule,
    MatExpansionModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatSnackBarModule,
    MatSliderModule,
    MatFormFieldModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    NgParticlesModule,
    MatListModule
  ],
  providers: [
    AngularFirestore,
    // { provide: StorageBucket }
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
