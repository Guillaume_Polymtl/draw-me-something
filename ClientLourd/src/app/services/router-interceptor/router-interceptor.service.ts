import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RouterInterceptorService {

  private _previousUrl: string = '';
  private _currentUrl: string = '';
  private _routeHistory: string[];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this._routeHistory = [];
    this.router.events
      .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
      .subscribe((events: RoutesRecognized[]) => {
        this.setUrls(events[1].urlAfterRedirects);
      });
  }

  public canGoBack(): boolean {
    /*if (this.currentUrl.includes('/home')) {
      return false;
    }*/
    if (this._routeHistory.length === 0) {
      return false;
    }
    return true;
  }

  canDisplayToggleChat(): boolean {
    const current = document.location.href;
    if (current.includes('windowed')) {
      return false;
    }
    return true;
  }

  isWindowedChat(): boolean {
    const current = document.location.href;
    if (current.includes('windowed')) {
      return true;
    }
    return false;
  }

  canDisplayHeader(): boolean {
    const current = document.location.href;
    if (current.includes('auth')) {
      return false;
    }
    if (current.includes('windowed')) {
      return false;
    }
    if (current.includes('home')) {
      // return false;
    }
    if (current.includes('watch')) {
      return true;
    }
    if (current.includes('stream')) {
      return true;
    }
    return true;
  }

  private setUrls(currentUrl: string): void {
    const tempUrl = this._currentUrl;
    this._previousUrl = tempUrl;
    this._currentUrl = currentUrl;
    this._routeHistory.push(this._currentUrl);
  }

  get previousUrl(): string {
    return this._previousUrl;
  }

  get currentUrl(): string {
    return this._currentUrl;
  }

  get routeHistory(): string[] {
    return this._routeHistory;
  }

}
