import { Injectable } from '@angular/core';
import { User } from '@models/user/user';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor() { }

  filterUsersByPseudo(search: string, users: User[], caseSensitive: boolean = false): User[] {
    if (!users) {
      return users;
    }
    users.forEach((user: User) => {
      caseSensitive ?
        user.display = user.pseudo.includes(search) :
        user.display = user.pseudo.toLowerCase().includes(search.toLowerCase());
    });
    return users;
  }

}
