import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TypingEffectService {

  constructor() { }

  typewriterEffect(element: HTMLElement, textToBeTyped: string, typeSpeed: number): NodeJS.Timeout {
    let i = 0;
    const interval = setInterval(() => {
      element.innerHTML += textToBeTyped.charAt(i);
      i++;
      if (i >= textToBeTyped.length) {
        clearInterval(interval);
      }
    }, typeSpeed);
    return interval;
  }

}
