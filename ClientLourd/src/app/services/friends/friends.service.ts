import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FriendRequest } from '@models/user/friend-request';
import { FriendsAndRequests } from '@models/user/friends-and-requests';
import { User } from '@models/user/user';
import { AuthenticationService } from '@services/authentication/authentication.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment';
import { initPjs } from 'tsparticles/dist/pjs';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {

  private currentUser: User;

  constructor(private authService: AuthenticationService, private http: HttpClient) {
    this.init();
  }

  private init(): void {
    this.authService.currentUser.subscribe((user: User) => {
      this.currentUser = user;
    });
  }

  sendFriendRequest(to: string): Observable<boolean> {
    return this.http.patch<boolean>(environment.server.friends.sendRequest, { to });
  }

  answerRequest(to: string, accepted: boolean): Observable<boolean> {
    return this.http.patch<boolean>(environment.server.friends.answerRequest, { to, accepted });
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(environment.server.users.all);
  }


  // determines wether or not an other user is a friend
  isFriend(user: User): boolean {
    if (!this.currentUser?.friendsAndRequests?.friends) {
      return false;
    }
    for (const friend of this.currentUser.friendsAndRequests.friends) {
      if (friend.pseudo === user.pseudo) {
        return true;
      }
    }
    return false;
  }

  isInboundRequest(user: User): boolean {
    if (!this.currentUser?.friendsAndRequests?.inbound) {
      return false;
    }
    for (const request of this.currentUser.friendsAndRequests.inbound) {
      if (request.from?.pseudo === user.pseudo) {
        return true;
      }
    }
    return false;
  }

  isOutboundRequest(user: User): boolean {
    if (!this.currentUser?.friendsAndRequests?.outbound) {
      return false;
    }
    for (const request of this.currentUser.friendsAndRequests.outbound) {
      if (request.to?.pseudo === user.pseudo) {
        return true;
      }
    }
    return false;
  }

}
