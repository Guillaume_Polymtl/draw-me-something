import { getLocaleCurrencyCode } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MOCK_USER, User } from '@app/models/user/user';
import { Game } from '@models/game/game';
import { GameResult } from '@models/game/game-result';
import { FriendsAndRequests } from '@models/user/friends-and-requests';
import { FriendsService } from '@services/friends/friends.service';
import { env } from 'node:process';

import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { AVATAR_CHANGED, AVATAR_ERROR } from '../chat/errors';
import { RouterService } from '../router/router.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    apikey: environment.firebase.apiKey,
    appkey: environment.firebase.appId,
  }),
};
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  profileSocket: Socket;

  private user: BehaviorSubject<User> = new BehaviorSubject<User>({ pseudo: '' });
  private firstLogin = false;

  connected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    private http: HttpClient,
    private routerService: RouterService,
    private snackbar: SnackBarService,
  ) {
    this.init();
  }

  private init(): void {
    if (localStorage.getItem('currentUser')) {
      this.connected.next(true);
    }
  }

  get currentUserPseudo(): string {
    if (localStorage.getItem('currentUser')) {
      return (localStorage.getItem('currentUser') as string).replace(/"/g, '');
    }
    return '';
  }

  set currentUserPseudo(pseudo: string) {
    localStorage.setItem('currentUser', JSON.stringify(pseudo));
  }

  get currentUser(): Observable<User> {
    return this.user;
  }

  updateCurrentUser(): void {
    this.http.get<User>(`${environment.server.users.account}/${this.currentUserPseudo}`).subscribe((user: User) => {
      user.authData = this.getToken();
      this.user.next(user);
      this.getFriendsAndRequests().subscribe((res) => {
        user.friendsAndRequests = res;
        this.user.next(user);
      });

      this.getClassicGamesHistory().subscribe((games: GameResult[]) => {
        user.gameHistory = games;
        this.user.next(user);
      });

      this.getSoloGamesHistory(user.pseudo).subscribe((soloGames: GameResult[]) => {
        user.soloGameHistory = soloGames;
      });
    });
  }

  getFriendsAndRequests(): Observable<FriendsAndRequests> {
    return this.http.get<FriendsAndRequests>(environment.server.friends.friends);
  }

  getClassicGamesHistory(): Observable<GameResult[]> {
    return this.http.get<GameResult[]>(environment.server.users.classicHistory);
  }

  getSoloGamesHistory(pseudo: string): Observable<GameResult[]> {
    return this.http.get<GameResult[]>(`${environment.server.users.soloHistory}/?searchpseudo=${pseudo}`);
  }

  signUp(user: User): void {
    console.log('sign up post');
    this.http.post<User>(environment.server.users.singUp, user, httpOptions).subscribe({
      next: (result) => {
        if (result) {

          this.snackbar.success('Compte créé avec succès!');
          this.firstLogin = true;
        }
      },
      error: (error) => {
        this.snackbar.error('Erreur lors de la création du compte');

      },
    });
  }

  signIn(user: User): void {
    const signInObservable = this.http.patch<{ authToken: string }>(environment.server.users.signIn, user);
    signInObservable.subscribe({
      next: (result) => {
        if (result) {
          localStorage.setItem('currentUser', JSON.stringify(user.pseudo));
          localStorage.setItem('token', result.authToken);
          this.connected.next(true);
          sessionStorage.setItem('isLogged', 'true');
          this.currentUserPseudo = user.pseudo;
          this.routerService.goHome();
          this.snackbar.success(`Bienvenue ${user.pseudo}`);
          this.updateCurrentUser();
          // Show tutorial if first login
          if (this.firstLogin) {
            this.routerService.tutorial();
            this.firstLogin = false;
          }
        } else {
          this.snackbar.error('Erreur lors de la connexion.');
        }

      },
      error: (error) => {
        if (error.status === 409) {
          this.snackbar.error('Vous êtes déjà connecté(e).');
        } else {
          this.snackbar.error('Erreur lors de la connexion');
        }
      },
    });
  }

  getToken(): string {
    const token = localStorage.getItem('token');
    return token ?? '';
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    return token !== '';
  }

  signOut(): void {
    // this.router.navigate(['/']);
    this.http.patch<User>(environment.server.users.logOut, {}).subscribe({
      next: (result) => {
        if (result) {
          this.snackbar.success(`Au revoir ${this.currentUserPseudo}`);
          localStorage.removeItem('currentUser');
          localStorage.removeItem('token');
          this.connected.next(false);
          this.routerService.authentication();
        }
      },
      error: (error) => {
        this.connected.next(false);
        this.snackbar.error(`Erreur lors de la déconnexion.`);
      },
    });
  }
}
