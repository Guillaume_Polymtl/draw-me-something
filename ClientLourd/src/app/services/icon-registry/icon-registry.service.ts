import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Icons } from '../../models/icons/icons';

@Injectable({
  providedIn: 'root'
})
export class IconRegistryService {

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer) {
                this.addIconsToRegistry();
  }

  addIconsToRegistry(): void {
    const iconsFolder = 'assets/icons';
    this.loadIcons(Object.values(Icons), iconsFolder);
  }

  loadIcons(keys: string[], urls: string): void {
    keys.forEach(key => {
      this.matIconRegistry.addSvgIcon(key, this.domSanitizer.bypassSecurityTrustResourceUrl(`${urls}/${key}.svg`));
    });
  }
}
