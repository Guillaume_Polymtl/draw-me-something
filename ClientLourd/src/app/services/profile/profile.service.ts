import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GameDifficulty } from '@app/models/game/difficulty';
import { GameMode } from '@app/models/game/game-mode';
import { MOCK_USER, User } from '@app/models/user/user';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { AVATAR_CHANGED, AVATAR_ERROR } from '../chat/errors';
import { SnackBarService } from '../snack-bar/snack-bar.service';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient, private authService: AuthenticationService, private snackBarService: SnackBarService) { }

  editAvatar(avatarUrl: string): Observable<boolean> {
    // this.router.navigate(['/']);
    return this.http.patch<boolean>(environment.server.users.avatar, { avatar: avatarUrl });
  }

  editUser(user: User): Observable<boolean> {
    return this.http.patch<boolean>(environment.server.users.account, user);
  }

  getUser(pseudo: string): Observable<User> {
    return this.http.get<User>(`${environment.server.users.account}/${pseudo}`);
  }

}
