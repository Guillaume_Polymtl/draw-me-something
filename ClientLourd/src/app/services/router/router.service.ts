import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RouterInterceptorService } from '@services/router-interceptor/router-interceptor.service';

@Injectable({
  providedIn: 'root',
})
export class RouterService {

  constructor(private router: Router, private location: Location, private routerInterceptorService: RouterInterceptorService) { }

  reloadCurrent(): void {

    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  canGoBack(): boolean {
    return this.routerInterceptorService.canGoBack();
  }

  goBack(): void {
    this.location.back();
  }

  goHome(): void {

    this.router.navigate(['/home']);
  }

  authentication(): void {
    this.router.navigate(['/auth']);
  }

  ranking(): void {
    this.router.navigate(['/ranking']);
  }

  pairWordDrawingLibrary(): void {
    this.router.navigate(['pair-word-drawing']);
  }

  canDisplayHeader(): boolean {
    return this.routerInterceptorService.canDisplayHeader();
  }

  pairWordDrawingCreate(): void {
    this.router.navigate(['pair-word-drawing/create']);
  }

  profile(): void {
    this.router.navigate(['/profile']);
  }

  hall(): void {
    this.router.navigate(['/hall']);
  }

  lobby(): void {
    this.router.navigate(['/lobby']);
  }

  stream(): void {
    this.router.navigate(['/stream']);
  }

  watch(): void {
    this.router.navigate(['/watch']);
  }

  round(): void {
    this.router.navigate(['/round']);
  }

  gameSummary(): void {
    this.router.navigate(['/game-summary']);
  }

  friendProfile(id: string): void {
    this.router.navigate([`/friend-profile/${id}`]);
  }

  tutorial(): void {
    this.router.navigate(['tutorial']);
  }

}
