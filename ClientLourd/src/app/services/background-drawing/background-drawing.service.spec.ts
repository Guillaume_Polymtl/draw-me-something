import { TestBed } from '@angular/core/testing';

import { BackgroundDrawingService } from './background-drawing.service';

describe('BackgroundDrawingService', () => {
  let service: BackgroundDrawingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackgroundDrawingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
