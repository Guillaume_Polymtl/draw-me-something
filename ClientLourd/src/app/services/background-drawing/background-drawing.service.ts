import { Injectable } from '@angular/core';
import { WHITE } from '@models/drawing/color';
import { Drawing } from '@models/drawing/drawing';
import { QuickDraw } from '@models/drawing/quick-draw';
import { DrawingService } from '@services/editor/drawing/drawing.service';
import { PairWordDrawingService } from '@services/pair-word-drawing/pair-word-drawing.service';
import { QuickDraws } from '@services/pair-word-drawing/quick-draws';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackgroundDrawingService {


  constructor(private drawingService: DrawingService, private pairWordDrawingService: PairWordDrawingService) { }

  execute(context: CanvasRenderingContext2D): void {
    const canvas = context.canvas;
    canvas.height = window.innerHeight - 40;
    canvas.width = window.innerWidth - 40;
    this.draw(context);
  }

  private draw(context: CanvasRenderingContext2D): void {
    const canvas = context.canvas;
    this.getNewDrawing(context).subscribe((drawing: Drawing) => {
      const timer = this.drawingService.getTotalDrawingTime(drawing) + 3000;
      setTimeout(() => {
        this.draw(context);
      }, timer);
      const width = drawing.size?.x ?? 0;
      const height = drawing.size?.y ?? 0;
      const maxX = canvas.width - width;
      const maxY = canvas.height - height;
      let randomX = Math.max(canvas.width * Math.random() - width, 0);
      let randomY = Math.max(canvas.height * Math.random() - height, 0);
      if (randomX > maxX) {
        randomX = maxX;
      }
      if (randomY > maxY) {
        randomY = maxY;
      }
      for (const stroke of drawing.strokes) {
        stroke.color = WHITE;
        for (const point of stroke.points) {
          point.position.x += randomX;
          point.position.y += randomY;
        }
      }
      this.drawingService.drawStrokesOnCanvas(context, drawing.strokes, false);
    });

  }

  private getNewDrawing(context: CanvasRenderingContext2D): Observable<Drawing> {
    return this.pairWordDrawingService.getRandomQuickDrawForHome().pipe(map((quickDraw: QuickDraw) => {
      let randomSize = Math.min(context.canvas.width, context.canvas.height) * Math.random();
      randomSize = Math.max(randomSize, 200);
      const ghostCanvas = document.createElement('canvas');
      ghostCanvas.height = randomSize;
      ghostCanvas.width = randomSize;
      const ghostCtx = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
      const drawing = this.drawingService.quickDrawToDrawing(ghostCtx, quickDraw);
      drawing.size = { x: randomSize, y: randomSize };
      this.drawingService.clearCanvas(context);
      return drawing;
    }));
  }

}
