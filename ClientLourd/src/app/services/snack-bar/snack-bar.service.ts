import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  private DEFAULT_ACTION = 'OK';
  private DEFAULT_DURATION = 3000;
  private DEFAULT_INFO_MESSAGE = 'Information';
  private DEFAULT_WARNING_MESSAGE = ' Avertissement!';
  private DEFAULT_ERROR_MESSAGE = 'Erreur!';
  private DEFAULT_SUCCESS_MESSAGE = 'Succès';

  constructor(private snackbar: MatSnackBar) { }

  info(
    message: string = this.DEFAULT_INFO_MESSAGE,
    action: string = this.DEFAULT_ACTION,
    durationInMS: number = this.DEFAULT_DURATION): void {
    const classes = ['info'];
    this.openSnackbar(message, action, durationInMS, classes);
  }

  error(
    message: string = this.DEFAULT_ERROR_MESSAGE,
    action: string = this.DEFAULT_ACTION,
    durationInMS: number = this.DEFAULT_DURATION): void {
    const classes = ['error'];
    this.openSnackbar(message, action, durationInMS, classes);
  }

  warning(
    message: string = this.DEFAULT_WARNING_MESSAGE,
    action: string = this.DEFAULT_ACTION,
    durationInMS: number = this.DEFAULT_DURATION): void {
    const classes = ['warning'];
    this.openSnackbar(message, action, durationInMS, classes);
  }

  success(
    message: string = this.DEFAULT_SUCCESS_MESSAGE,
    action: string = this.DEFAULT_ACTION,
    durationInMS: number = this.DEFAULT_DURATION): void {
    const classes = ['success'];
    this.openSnackbar(message, action, durationInMS, classes);
  }

  private openSnackbar(message: string, action: string, durationInMS: number, classes: string[]): void {
    const snackbar = this.snackbar.open(message, action, {
      duration: durationInMS,
      panelClass: classes.concat('snack-bar'),
    });
  }

}
