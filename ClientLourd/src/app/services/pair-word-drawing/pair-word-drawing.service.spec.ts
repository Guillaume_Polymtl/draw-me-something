import { TestBed } from '@angular/core/testing';

import { PairWordDrawingService } from './pair-word-drawing.service';

describe('PairWordDrawingService', () => {
  let service: PairWordDrawingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PairWordDrawingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
