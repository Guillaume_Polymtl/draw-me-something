import { PotracePreview } from '../../models/drawing/potrace/potrace-preview';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CenterMode, CenterModes } from '@app/models/drawing/center-mode';
import { CreationMode, CreationModes } from '@app/models/drawing/creation-mode';
import { Drawing } from '@app/models/drawing/drawing';
import { DrawingMode, DrawingModes } from '@app/models/drawing/drawing-mode';
import { PanoramicMode, PanoramicModes } from '@app/models/drawing/panoramic-mode';
import { QuickDraw } from '@app/models/drawing/quick-draw';
import { GameDifficulty, GameDifficulties } from '@app/models/game/difficulty';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SERVER_ERROR } from '../chat/chat-errors';
import { DrawingService } from '../editor/drawing/drawing.service';
import { RouterService } from '../router/router.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';
import { QuickDraws } from './quick-draws';
import { CANVAS_DEFAULT_HEIGHT, CANVAS_DEFAULT_WIDTH } from '@app/models/drawing/constants';
@Injectable({
  providedIn: 'root'
})
export class PairWordDrawingService {

  creationModes: CreationMode[] = CreationModes;
  difficulties: GameDifficulty[] = GameDifficulties;
  drawingModes: DrawingMode[] = DrawingModes;
  panoramicModes: PanoramicMode[] = PanoramicModes;
  centerModes: CenterMode[] = CenterModes;

  randomDrawings: BehaviorSubject<Drawing[]> = new BehaviorSubject<Drawing[]>([]);
  potracedBase64: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(
    private snackbarService: SnackBarService,
    private router: RouterService,
    private http: HttpClient,
    private drawingService: DrawingService) { }

  create(drawing: Drawing): void {
    if (!drawing.drawingMode) {
      drawing.drawingMode = DrawingMode.CONVENTIONAL;
    }
    if (!drawing.size) {
      drawing.size = { x: CANVAS_DEFAULT_WIDTH, y: CANVAS_DEFAULT_HEIGHT };
    }
    this.http.post<boolean>(`${environment.server.drawings.create}`, drawing).subscribe({
      next: data => {
        this.snackbarService.success('Paire mot image créée avec succès!');
        this.drawingService.resetCurrentDrawing();
        this.router.pairWordDrawingLibrary();
      },
      error: error => {
        if (error.status === 413) {
          this.snackbarService.error(`L'image est trop grande.`);
        } else {

          this.snackbarService.error(SERVER_ERROR);
        }
      }
    });
  }

  getRandomQuickDraw(): QuickDraw {
    const rand = Math.round(Math.random() * (QuickDraws.length - 1));
    return QuickDraws[rand];
  }

  getRandomDrawing(difficulty?: GameDifficulty): Observable<Drawing> {
    if (!difficulty) {
      if (Math.round(Math.random())) {
        difficulty = GameDifficulty.DIFFICULT;
      } else {
        difficulty = GameDifficulty.NORMAL;
      }
    }
    return this.http.get<Drawing>(`${environment.server.drawings.getRandom}/${difficulty}`);
  }

  getRandomQuickDrawForHome(): Observable<QuickDraw> {
    const randomIndex = Math.round(QuickDraws.length * Math.random());
    return of(QuickDraws[randomIndex]);
  }

  getDrawings(amount: number, page: number): Observable<{
    drawings: Drawing[],
    pagesLeft: number, numberOfPages: number
  }> {
    return this.http.get<{ drawings: Drawing[], pagesLeft: number, numberOfPages: number }>
      (`${environment.server.drawings.page}/${amount}/${page}`, {});
  }

  getPotracePreview(previewModel: PotracePreview): Observable<Drawing> {
    return this.http.post<Drawing>(`${environment.server.drawings.potracePreview}`, previewModel);
  }
}
