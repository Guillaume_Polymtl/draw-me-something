export const CHANNEL_ALREADY_EXISTS = `Le canal existe déjà.`;
export const CHANNEL_DOESNT_EXIST = `Le canal n'existe pas.`;
export const CHANNEL_CREATED = 'Le canal a été créé.';
export const CHANNEL_DELETED = 'Le canal a été supprimé.';
export const SERVER_ERROR = 'Erreur du serveur.';
export const CHANNEL_LEFT = 'Vous avez quittez le canal.';
export const HISTORY_OBTAINED = 'Historique téléchargé.';
export const CHANNEL_JOINED = 'Vous avez rejoint le canal.';
export const INBOUND_MESSAGE_ERROR = 'Erreur de réception de message.';
export const CHANNEL_HAS_NO_HISTORY = `Le canal n'a pas d'historique.`;

export const GET_CHANNELS_ERROR = 'Erreur lors de la mise à jour des canaux.';
