

export const CHANNEL_ALREADY_EXISTS = `Le canal existe déjà.`;

export const CHANNEL_CREATED = 'Le canal a été créé.';

export const CHANNEL_DELETED = 'Le canal a été supprimé.';

export const SERVER_ERROR = 'Erreur du serveur.';

export const CHANNEL_LEFT = 'Vous avez quittez le canal.';

export const AVATAR_CHANGED = 'Votre avatar a été modifié!';

export const AVATAR_ERROR = 'Une erreur s\'est produite, votre avatar n\'a pu être modifié.';
