import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ChatIntegrationService {

  private ipcRenderer: IpcRenderer;

  private isAttached: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private isOpened: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(){
    this.handleChatIntegration();
  }

  private handleChatIntegration(): void {
    this.ipcRenderer = (window as any).require('electron').ipcRenderer;
    this.ipcRenderer.on('detatch-chat-complete', () => {
      this.isAttached.next(false);
    });
    this.ipcRenderer.on('attach-chat-complete', () => {
      this.isAttached.next(true);
    });
  }


  detatch(): void {
    this.ipcRenderer.send('detatch-chat');
    // this.currentChannel.next(undefined);
    this.isAttached.next(false);
  }

  attach(): void {
    if (this.ipcRenderer) {
      this.ipcRenderer.send('attach-chat');
      // this.currentChannel.next(undefined);
      this.isAttached.next(true);
      this.isOpened.next(true);
    }
  }

  getIsAttached(): Observable<boolean> {
    return this.isAttached;
  }
}
