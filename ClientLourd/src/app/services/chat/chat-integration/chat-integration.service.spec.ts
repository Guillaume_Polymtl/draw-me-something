import { TestBed } from '@angular/core/testing';

import { ChatIntegrationService } from './chat-integration.service';

describe('ChatIntegrationService', () => {
  let service: ChatIntegrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatIntegrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
