import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Channel } from '@app/models/chat/channel';
import { Message } from '@app/models/chat/message';
import { User } from '@app/models/user/user';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { ProfileService } from '../profile/profile.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';
import {
  CHANNEL_ALREADY_EXISTS, CHANNEL_CREATED,
  CHANNEL_DELETED, CHANNEL_DOESNT_EXIST, CHANNEL_HAS_NO_HISTORY, CHANNEL_JOINED,
  CHANNEL_LEFT, GET_CHANNELS_ERROR, HISTORY_OBTAINED, INBOUND_MESSAGE_ERROR, SERVER_ERROR
} from './chat-errors';
import { ChatIntegrationService } from './chat-integration/chat-integration.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private socket: Socket;
  private subscribedChannels: BehaviorSubject<Channel[]> = new BehaviorSubject<Channel[]>([]);
  private unsubscribedChannels: BehaviorSubject<Channel[]> = new BehaviorSubject<Channel[]>([]);
  private currentChannelName: BehaviorSubject<string> = new BehaviorSubject<string>('');
  constructor(
    private http: HttpClient,
    private authService: AuthenticationService,
    private snackbar: SnackBarService,
    private chatIntegrationService: ChatIntegrationService,
    private profileService: ProfileService
  ) {
    this.authService.connected.subscribe((data: boolean) => {
      if (data) {
        this.authService.currentUser.subscribe((user: User) => {
          this.connect();
        });
      } else {
        this.disconnect();
        this.chatIntegrationService.attach();
      }
    });
  }

  private disconnect(): void {
    this.socket?.disconnect();
    this.subscribedChannels.next([]);
    this.unsubscribedChannels.next([]);
  }

  private connect(): void {
    this.socket = io(`${environment.server.channels.ws}`);
    this.updateChannels();
    this.handleInboundMessage();
  }

  private handleInboundMessage(): void {
    this.socket.on('room-message', (message: Message) => {
      try {
        if (message.pseudo === this.authService.currentUserPseudo) {
          message.fromCurrent = true;
        } else {
          message.fromCurrent = false;
        }
        const channel = this.subscribedChannels.value.find((c: Channel) => c.channelName === message.channelName);
        if (channel) {
          if (!channel.messages) {
            channel.messages = [];
          }
          channel.messages.push(message);
          this.sortMessagesByDate(channel);
          if (channel.channelName !== this.currentChannelName.value) {
            channel.unseenMessages++;
          }
        }
      } catch (err) {
        this.snackbar.error(INBOUND_MESSAGE_ERROR);
      }
    });
  }

  processRecievedMessage(message: Message): void {
    try {
      if (message.pseudo === this.authService.currentUserPseudo) {
        message.fromCurrent = true;
      } else {
        message.fromCurrent = false;
      }
      const channel = this.subscribedChannels.value.find((c: Channel) => c.channelName === message.channelName);
      if (channel) {
        if (!channel.messages) {
          channel.messages = [];
        }
        channel.messages.push(message);
        this.sortMessagesByDate(channel);
        if (channel.channelName !== this.currentChannelName.value) {
          channel.unseenMessages++;
        }
      }
    } catch (err) {
      this.snackbar.error(INBOUND_MESSAGE_ERROR);
    }
  }

  public updateChannels(): void {
    this.http.get<Channel[]>(`${environment.server.users.subscribedChannels}`)
      .subscribe(
        {
          next: subscribedChannels => {
            this.handleChannelsUpdate(subscribedChannels, this.subscribedChannels);
          },
          error: () => {
            this.snackbar.error(GET_CHANNELS_ERROR);

          }
        },
      );

    this.http.get<Channel[]>(`${environment.server.users.unsubscribedChannels}`)
      .subscribe(
        {
          next: unsubscribedChannels => {
            this.handleChannelsUpdate(unsubscribedChannels, this.unsubscribedChannels);
          },
          error: () => {
            this.snackbar.error(GET_CHANNELS_ERROR);
          }
        }
      );
  }

  private handleChannelsUpdate(channels: Channel[], currentChannels: BehaviorSubject<Channel[]>): void {
    // Add new channels
    let newChannels = currentChannels.value;
    const channelNames: string[] = [];
    for (const channel of channels) {
      const name = channel.channelName;
      channelNames.push(name);
      if (this.getChannelByName(currentChannels.value, name) === undefined) {
        const newChannel = new Channel(name);
        this.socket.emit('join-room', name);

        if (channel.pseudo === this.authService.currentUserPseudo) {
          newChannel.isOwner = true;
        }

        if (channel.isGameChannel) {
          newChannel.isGameChannel = true;
        }
        newChannels.push(newChannel);
      }
    }
    currentChannels.next(newChannels);
    newChannels = currentChannels.value;
    // Remove channels that are not in the incoming array
    for (const channel of currentChannels.value) {
      if (!channelNames.includes(channel.channelName)) {
        newChannels = currentChannels.value.filter((c: Channel) => c.channelName !== channel.channelName);
      }
    }
    currentChannels.next(newChannels);
  }

  orderChannelsByName(channels: Channel[]): Channel[] {
    const result = channels.sort((channel1: Channel, channel2: Channel) => {
      let r = 0;
      channel1.channelName > channel2.channelName ? r = 1 : r = -1;
      return r;
    });
    return result;
  }


  getUnsubscribedChannels(): Observable<Channel[]> {
    return this.unsubscribedChannels;
  }

  getSubscribedChannels(): Observable<Channel[]> {
    return this.subscribedChannels;
  }

  getCurrentChannelName(): Observable<string> {
    return this.currentChannelName;
  }

  selectChannel(channelName: string): void {
    const channel = this.getChannelByName(this.subscribedChannels.value, channelName);
    if (channel) {
      channel.unseenMessages = 0;
      this.currentChannelName.next(channelName);
      /*this.currentChannelName = channelName;
      this.currentChannel.next(channel);*/
    } else {
      this.snackbar.error(CHANNEL_DOESNT_EXIST);
    }
  }

  createChannel(channelName: string): void {
    this.http.post<boolean>(environment.server.channels.http, { channelName }).subscribe({
      next: data => {
        if (data) {
          this.socket.emit('create-room', channelName);
          this.joinChannel(channelName);
          this.snackbar.success(CHANNEL_CREATED);
        } else {
          this.snackbar.error(CHANNEL_ALREADY_EXISTS);
        }
      },
      error: error => {
        this.snackbar.error(CHANNEL_ALREADY_EXISTS);
      }
    }
    );
  }

  deleteChannel(channelName: string): void {
    this.http.delete<boolean>(`${environment.server.channels.http}/${channelName}`, {}).subscribe({
      next: data => {
        if (data) {
          this.snackbar.error(CHANNEL_DELETED);
          this.updateChannels();
        } else {
          this.snackbar.error(SERVER_ERROR);
        }
      },
      error: error => {
        this.snackbar.error(SERVER_ERROR);
      }
    });
  }

  joinChannel(channelName: string): void {
    this.http.patch<boolean>(`${environment.server.channels.http}/join/${channelName}`, {}).subscribe({
      next: data => {
        if (data) {
          this.socket.emit('join-room', channelName);
          this.updateChannels();
          setTimeout(() => {
            this.selectChannel(channelName);
          }, 500);
          this.snackbar.success(CHANNEL_JOINED);
        }
      },
      error: error => {
        this.snackbar.error();
      }
    }
    );
  }

  leaveChannel(channelName: string): void {
    this.http.patch<boolean>(`${environment.server.channels.http}/leave/${channelName}`, {}).subscribe({
      next: data => {
        if (data) {
          const channels = this.subscribedChannels.value
            .filter((channel: Channel) => channel.channelName !== channelName);
          this.subscribedChannels.next(channels);
          this.snackbar.success(CHANNEL_LEFT);
          this.updateChannels();
        } else {
          this.snackbar.error();
        }
      },
      error: error => {
        this.snackbar.error(SERVER_ERROR);
      }
    });
  }

  getChannelHistory(channelName: string): void {
    this.http.get<Message[]>(`${environment.server.channels.http}/chat/${channelName}`)
      .subscribe({
        next: data => {
          if (!data.length) {
            this.snackbar.warning(CHANNEL_HAS_NO_HISTORY);
            return;
          }
          const channel = this.getChannelByName(this.subscribedChannels.value, channelName);
          if (channel) {
            data.forEach((message: Message) => {
              message.pseudo === this.authService.currentUserPseudo ? message.fromCurrent = true : message.fromCurrent = false;
            });
            channel.messages = data;
            this.sortMessagesByDate(channel);
            this.snackbar.success(HISTORY_OBTAINED);
          }
        },
        error: error => {
          this.snackbar.error(SERVER_ERROR);
        }
      });
  }

  private sortMessagesByDate(channel: Channel): void {
    channel.messages.sort((m1: Message, m2: Message) => {
      return m1.date - m2.date;
    });

  }

  private getChannelByName(channels: Channel[], channelName: string): Channel | undefined {
    return channels.find((c: Channel) => c.channelName === channelName);
  }


  sendMessage(message: string, channelName: string): void {
    console.log(`pseudo: ${this.authService.currentUserPseudo}`);
    if (this.isBlank(message)) {
      return;
    }
    this.http.patch<boolean>(`${environment.server.channels.http}/chat/${channelName}`, { message }).subscribe({
      next: data => {
        if (data) {
          this.socket.emit('room-message', { message, pseudo: this.authService.currentUserPseudo, channelId: channelName });
        } else {
          this.snackbar.error(SERVER_ERROR);
        }
      },
      error: error => {
        this.snackbar.error(SERVER_ERROR);
      }
    });
  }

  private isBlank(message: string): boolean {
    return (message.length === 0 || !message.trim());
  }

}
