import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@app/models/user/user';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor(private http: HttpClient) { }

  mockUsers: User[] = [
    {
      pseudo: '123',
    },
    {
      pseudo: '124'
    },
    {
      pseudo: 'vanille'
    },
    {
      pseudo: 'bonjour'
    },
    {
      pseudo: 'bon van'
    }
  ];

  getClassicRanking(): Observable<User[]> {
    return this.http.get<User[]>(environment.server.ranking.classic);
  }

  getSprintSoloRanking(): Observable<User[]> {
    return this.http.get<User[]>(environment.server.ranking.sprintSolo);
  }

}
