import { ElementRef, Injectable } from '@angular/core';
import { AudioType, audioTypes } from '@models/audio/audio-type';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  private musicFolder = 'assets/sound/music/';
  private effectsFolder = 'assets/sound/effects/';

  readonly currentMusic: BehaviorSubject<string> = new BehaviorSubject<string>('');
  readonly currentEffect: BehaviorSubject<string> = new BehaviorSubject<string>('');
  readonly currentClock: BehaviorSubject<string> = new BehaviorSubject<string>('');

  readonly audioEnabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  enableAudio(): void {
    this.audioEnabled.next(true);
  }

  disableAudio(): void {
    this.audioEnabled.next(false);
  }

  toggleAudio(): void {
    this.audioEnabled.next(!this.audioEnabled.value);
  }

  startBackgroundMusic(): void {
    this.currentMusic.next(`${this.musicFolder}background.mp3`);
  }

  startClock(): void {
    this.currentClock.next(`${this.effectsFolder}clock-ticking.mp3`);
  }

  stopEffect(): void {
    this.currentEffect.next('');
  }

  stopClock(): void {
    this.currentClock.next('');
  }

  wrongAnswerEffect(): void {
    this.currentEffect.next(`${this.effectsFolder}wrong-answer.wav`);
  }

  successEffect(): void {
    this.currentEffect.next(`${this.effectsFolder}success.wav`);
  }

  startEffectSuccessTimed(timeMs: number): void {
    this.successEffect();
    setTimeout(() => {
      this.stopEffect();
    }, timeMs);
  }

  startWrongAnsTimed(timeMs: number): void {
    this.wrongAnswerEffect();
    setTimeout(() => {
      this.stopEffect();
    }, timeMs);
  }
}
