import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CenterMode } from '@app/models/drawing/center-mode';
import { BLACK, WHITE, Color } from '@app/models/drawing/color';
import { Drawing } from '@app/models/drawing/drawing';
import { DrawingMode } from '@app/models/drawing/drawing-mode';
import { PanoramicMode } from '@app/models/drawing/panoramic-mode';
import { Point } from '@app/models/drawing/point';
import { QuickDraw } from '@app/models/drawing/quick-draw';
import { Stroke } from '@app/models/drawing/stroke';
import { ToolName } from '@app/models/drawing/tool-name';
import { Vec2 } from '@app/models/drawing/vec2';
import { GameDifficulty } from '@app/models/game/difficulty';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
    providedIn: 'root',
})
export class DrawingService {


    baseCtx!: CanvasRenderingContext2D;
    previewCtx!: CanvasRenderingContext2D;
    gridCtx!: CanvasRenderingContext2D;

    isLoaded: boolean;

    private NUMBER_OF_FRAMES = 1000;

    private currentDrawing: BehaviorSubject<Drawing> = new BehaviorSubject<Drawing>(
        { word: '', hints: [], strokes: [], difficulty: GameDifficulty.NORMAL }
    );

    drawingReseted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private router: Router) {
        this.isLoaded = false;
    }

    addStrokeToCurrentDrawing(stroke: Stroke): void {
        this.currentDrawing.value.strokes?.push(stroke);
        this.currentDrawing.next(this.currentDrawing.value);
    }

    removeLastStrokeFromCurrentDrawing(): Stroke | undefined {
        if (this.currentDrawing.value.strokes?.length) {
            const lastStroke = this.currentDrawing.value.strokes.pop() as Stroke;
            this.currentDrawing.next(this.currentDrawing.value);
            return lastStroke;
        }
        return undefined;
    }

    getCurrentDrawing(): Observable<Drawing> {
        return this.currentDrawing.asObservable();
    }

    get time(): number {
        return Date.now();
    }

    resetCurrentDrawing(): void {
        this.currentDrawing.value.strokes = [];
        this.currentDrawing.next(this.currentDrawing.value);
        this.drawingReseted.next(true);
    }

    loadSavedDrawing(source: string, width: number, height: number): void {
        const img = new Image(width, height);
        img.addEventListener('load', this.drawSavedDrawing.bind(this, img));
        img.src = source;
    }

    drawSavedDrawing(img: HTMLImageElement): void {
        this.baseCtx.drawImage(img, 0, 0);
    }

    getCanvasBase64(): string {
        /*toDataURL returns a bunch of informations.
        Only the content after ";base64," is needed.*/
        return this.baseCtx.canvas.toDataURL().split(';base64,')[1];
    }

    clearCanvas(context: CanvasRenderingContext2D): void {
        const width = context.canvas.width;
        const height = context.canvas.height;
        context.clearRect(0, 0, width, height);
        // Reset attributes
        context.setLineDash([0]);
        context.strokeStyle = BLACK.value;
    }

    quickDrawToDrawing(ctx: CanvasRenderingContext2D, quickDraw: QuickDraw): Drawing {
        if (quickDraw) {
            const drawing: Drawing = {
                word: quickDraw.frenchWord,
                strokes: [],
            };
            const canvasSize: Vec2 = { x: ctx.canvas.width, y: ctx.canvas.height };
            const width = Math.ceil(Math.min(canvasSize.x, canvasSize.y) / 100);
            // 1. Convert the quickdraw into a drawing object
            for (const stroke of quickDraw.drawing) {
                const newStroke: Stroke = { points: [], width, color: BLACK, tool: ToolName.PENCIL };
                for (let i = 0; i < stroke[0].length; i++) {
                    const x = stroke[0][i];
                    const y = stroke[1][i];
                    const t = stroke[2][i];
                    const point: Point = {
                        position: { x, y },
                        time: t
                    };
                    newStroke.points.push(point);
                }
                drawing.strokes?.push(newStroke);
            }
            const size = this.getCanvasSize(ctx);
            drawing.strokes = this.resizeQuickDrawStrokes(drawing.strokes as Stroke[], size, 5);
            return drawing;
        }
        return { strokes: [] };
    }

    resizeQuickDrawStrokes(strokes: Stroke[], canvasSize: Vec2, padding: number): Stroke[] {
        const resizedStrokes: Stroke[] = [];
        // ratio of padding around the image
        // Step 1: Get smallest box containing the drawing
        let minX = 10000000;
        let maxX = 0;
        let minY = 10000000;
        let maxY = 0;
        for (const stroke of strokes) {
            for (const point of stroke.points) {
                const x = point.position.x;
                const y = point.position.y;
                minX = Math.min(x, minX);
                maxX = Math.max(x, maxX);
                minY = Math.min(y, minY);
                maxY = Math.max(y, maxY);
            }
        }
        const originalSize: Vec2 = { x: maxX - minX, y: maxY - minY };

        canvasSize.x = canvasSize.x - 2 * padding;
        canvasSize.y = canvasSize.y - 2 * padding;

        const xRatio = canvasSize.x / originalSize.x;
        const yRatio = canvasSize.y / originalSize.y;
        const ratio = Math.min(xRatio, yRatio);
        const newDrawingMiddle: Vec2 = { x: originalSize.x * ratio / 2, y: originalSize.y * ratio / 2 };
        const middleCanvas: Vec2 = { x: canvasSize.x / 2, y: canvasSize.y / 2 };
        for (const stroke of strokes) {

            // 1% of the biggest side
            const newWidth = Math.max(canvasSize.x, canvasSize.y) * 0.01;

            /*const widthRatio = stroke.width / Math.max(originalSize.x, originalSize.y);
            const newWidth = widthRatio * Math.min(canvasSize.x, canvasSize.y);*/

            const newStroke: Stroke = { tool: stroke.tool, width: newWidth, points: [], color: stroke.color };

            for (const point of stroke.points) {
                const x = point.position.x;
                const y = point.position.y;
                const newX = ((x - minX) * ratio + padding) - newDrawingMiddle.x + middleCanvas.x;
                const newY = ((y - minY) * ratio + padding) - newDrawingMiddle.y + middleCanvas.y;
                newStroke.points.push({ position: { x: newX, y: newY }, time: point.time });
            }
            resizedStrokes.push(newStroke);
        }
        return resizedStrokes;
    }

    resizeStrokes(drawing: Drawing, canvasSize: Vec2): Drawing {
        const resizedStrokes: Stroke[] = [];
        if (drawing.size) {
            const xRatio = canvasSize.x / drawing.size.x;
            const yRatio = canvasSize.y / drawing.size.y;
            const ratio = Math.min(xRatio, yRatio);
            for (const stroke of drawing.strokes) {
                const widthRatio = stroke.width / Math.max(drawing.size.x, drawing.size.y);
                const newWidth = widthRatio * Math.max(canvasSize.x, canvasSize.y);
                const color = new Color(stroke.color.red, stroke.color.green, stroke.color.blue, stroke.color.alpha);
                const newStroke: Stroke = { tool: stroke.tool, width: newWidth, points: [], color, index: stroke.index };
                for (const point of stroke.points) {
                    const x = point.position.x;
                    const y = point.position.y;
                    const newX = x * ratio;
                    const newY = y * ratio;
                    newStroke.points.push({ position: { x: newX, y: newY }, time: point.time });
                }
                resizedStrokes.push(newStroke);
            }
            drawing.strokes = resizedStrokes;

        } else {
            drawing.strokes = this.resizeQuickDrawStrokes(drawing.strokes, canvasSize, 0);
        }
        return drawing;
    }

    /**
     * Draws a quick draw as big as possible on the given ctx.canvas
     * @description Draws a quick draw object as big as possible on the given ctx.canvas
     * @param ctx ctx in which to draw
     * @param quickDraw QuickDraw object to draw
     */
    quickDraw(ctx: CanvasRenderingContext2D, blanketCtx: CanvasRenderingContext2D, quickDraw: QuickDraw): void {
        const drawing = this.quickDrawToDrawing(ctx, quickDraw);
        this.draw(ctx, ctx, drawing, false);
    }

    getCanvasSize(ctx: CanvasRenderingContext2D): Vec2 {
        return { x: ctx.canvas.width, y: ctx.canvas.height };
    }

    // from a drawing, get the total time it took to draw it
    getTotalDrawingTime(drawing: Drawing): number {
        return this.getStrokesDrawingTime(drawing.strokes);
    }

    // from strokes, get the total time it took to draw them
    private getStrokesDrawingTime(strokes: Stroke[]): number {
        try {
            const numberOfStrokes = strokes.length;
            const lastStroke = strokes[numberOfStrokes - 1];
            const numberOfPoints = lastStroke.points.length;
            const lastPoint = lastStroke.points[numberOfPoints - 1];
            const time = lastPoint.time;
            return time;
        } catch {
            return 0;
        }
    }

    draw(ctx: CanvasRenderingContext2D, background: CanvasRenderingContext2D, drawing: Drawing, instant: boolean): void {
        const size = this.getCanvasSize(ctx);
        background.fillStyle = WHITE.value;
        if (drawing.background) {
            background.fillStyle = drawing.background;
        }
        background.fillRect(0, 0, size.x, size.y);


        if (!drawing.strokes) {
            drawing.strokes = [];
        }

        if (instant) {
            this.drawStrokesOnCanvas(ctx, drawing.strokes, true);
            return;
        }

        if (!drawing.drawingMode) {
            this.drawStrokesOnCanvas(ctx, drawing.strokes, false);
            return;
        }
        this.drawStrokesOnCanvas(ctx, drawing.strokes, false);
    }

    drawStrokesOnCanvas(ctx: CanvasRenderingContext2D, strokes: Stroke[], instant: boolean): void {
        for (const stroke of strokes) {
            ctx.strokeStyle = stroke.color.value ?? stroke.color;
            ctx.lineWidth = stroke.width;
            if (stroke.tool === ToolName.PENCIL) {
                this.pencil(ctx, stroke, instant);
            } else if (stroke.tool === ToolName.ERASER) {
                this.eraser(ctx, stroke, instant);
            }
        }
    }


    pencil(ctx: CanvasRenderingContext2D, stroke: Stroke, instant: boolean): void {
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        this.drawStrokeOnCanvas(ctx, stroke, instant);
    }

    eraser(ctx: CanvasRenderingContext2D, stroke: Stroke, instant: boolean): void {
        ctx.lineJoin = 'round';
        this.drawStrokeOnCanvas(ctx, stroke, instant);
    }

    drawStrokeOnCanvas(ctx: CanvasRenderingContext2D, stroke: Stroke, instant: boolean): void {
        ctx.beginPath();
        const color = new Color(stroke.color.red, stroke.color.green, stroke.color.blue, stroke.color.alpha);
        for (let i = 0; i < stroke.points.length; i++) {
            const point = stroke.points[i];
            const x = point.position.x;
            const y = point.position.y;
            let previousX = x;
            let previousY = y;
            if (i > 0) {
                previousX = stroke.points[i - 1].position.x;
                previousY = stroke.points[i - 1].position.y;
            }
            if (instant) {
                ctx.beginPath();
                ctx.strokeStyle = color.value;
                ctx.lineWidth = stroke.width;
                ctx.moveTo(previousX, previousY);
                ctx.lineTo(x, y);
                ctx.stroke();
            } else {
                setTimeout(() => {
                    ctx.beginPath();
                    ctx.strokeStyle = color.value;
                    ctx.lineWidth = stroke.width;
                    ctx.moveTo(previousX, previousY);
                    ctx.lineTo(x, y);
                    ctx.stroke();
                }, point.time);
            }
            ctx.stroke();
        }
        ctx.stroke();
    }

    drawWithIndexes(contextes: CanvasRenderingContext2D[], drawing: Drawing, instant: boolean): void {
        const background = contextes[contextes.length - 1];
        for (const context of contextes) {
            this.clearCanvas(context);
        }
        for (let i = 0; i < drawing.strokes.length; i++) {
            const stroke = drawing.strokes[i];
            const fakeDrawing = JSON.parse(JSON.stringify(drawing));
            fakeDrawing.strokes = [stroke];
            this.draw(contextes[i], background, fakeDrawing, instant);
        }
    }
}
