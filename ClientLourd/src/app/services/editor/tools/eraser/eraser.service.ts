import { Injectable } from '@angular/core';
import { Color, WHITE, BLACK } from '@app/models/drawing/color';
import { ERASER_LINE_WIDTH } from '@app/models/drawing/constants';
import { Point } from '@app/models/drawing/point';
import { Stroke } from '@app/models/drawing/stroke';
import { Tool } from '@app/models/drawing/tool';
import { ToolName } from '@app/models/drawing/tool-name';
import { Vec2 } from '@app/models/drawing/vec2';
import { DrawingService } from '../../drawing/drawing.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';



@Injectable({
    providedIn: 'root',
})
export class EraserService extends Tool {
    readonly name: ToolName = ToolName.ERASER;
    protected color: Color = WHITE;

    constructor(drawingService: DrawingService, undoRedoService: UndoRedoService) {
        super(drawingService, undoRedoService);
        this.onInit();
    }



    onInit(): void {
        // this.clearPath();
        this.resetCurrentStroke();
    }

    onMouseMove(event: MouseEvent, time: number): void {
        const mousePosition = this.getPositionFromMouse(event);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        if (this.mouseDown) {
            // this.pathData.push(mousePosition);
            this.draw(this.drawingService.baseCtx, this.currentStroke);
            const drawingPoint: Point = {
                position: mousePosition,
                time
            };
            this.currentStroke.push(drawingPoint);
            // this.drawingService.addPointToCurrentDrawing(drawingPoint);
        }
        const topLeftPosition: Vec2 = { x: mousePosition.x - this.width / 2, y: mousePosition.y - this.width / 2 };
        this.drawEraser(topLeftPosition);
    }

    private drawEraser(position: Vec2): void {
        const ctx = this.drawingService.previewCtx;
        ctx.beginPath();
        ctx.fillStyle = WHITE.value;
        ctx.strokeStyle = BLACK.value;
        ctx.lineWidth = ERASER_LINE_WIDTH;
        ctx.arc(position.x + this.width / 2, position.y + this.width / 2, this.width / 2, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.fill();
    }

    protected draw(ctx: CanvasRenderingContext2D, drawingPoints: Point[]): void {
        const stroke: Stroke = { tool: ToolName.ERASER, points: drawingPoints, color: WHITE, width: this.width };
        this.drawingService.eraser(ctx, stroke, true);
    }
}
