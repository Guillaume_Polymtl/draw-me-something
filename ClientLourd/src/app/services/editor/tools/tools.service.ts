import { Injectable } from '@angular/core';
import { Tool } from '@app/models/drawing/tool';
import { ToolName } from '@app/models/drawing/tool-name';
import { BehaviorSubject, Observable } from 'rxjs';
import { EraserService } from './eraser/eraser.service';
import { PencilService } from './pencil/pencil.service';

@Injectable({
    providedIn: 'root',
})
export class ToolsService {


    private width = 0;

    constructor(
        private pencilService: PencilService,
        private eraserService: EraserService,
    ) {
        this.init();
    }

    // tools: BehaviorSubject<ToolButton[]> = new BehaviorSubject<ToolButton[]>(toolButtons);
    tools: Map<ToolName, Tool> = new Map();
    private tool: BehaviorSubject<Tool> = new BehaviorSubject<Tool>(this.pencilService);

    init(): void {
        this.tools = this.tools
            .set(ToolName.ERASER, this.eraserService)
            .set(ToolName.PENCIL, this.pencilService);
    }

    getTool(): Observable<Tool> {
        return this.tool;
    }

    setTool(toolName: ToolName): void {
        const service = this.tools.get(toolName);
        if (service) {
            // Reset the previous tool before changing it
            this.tool.getValue().onToolChange();
            this.tool.next(service);
        }
    }
}
