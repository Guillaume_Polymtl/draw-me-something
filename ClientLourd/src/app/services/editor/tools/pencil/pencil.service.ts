import { Injectable } from '@angular/core';
import { ColorService } from '@app/components/editor/color-picker/services/color.service';
import { Color, BLACK } from '@app/models/drawing/color';
import { ERASER_LINE_WIDTH } from '@app/models/drawing/constants';
import { Point } from '@app/models/drawing/point';
import { Stroke } from '@app/models/drawing/stroke';
import { Tool } from '@app/models/drawing/tool';
import { ToolName } from '@app/models/drawing/tool-name';
import { Vec2 } from '@app/models/drawing/vec2';
import { DrawingService } from '../../drawing/drawing.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';

@Injectable({
    providedIn: 'root',
})
export class PencilService extends Tool {
    readonly name: ToolName = ToolName.PENCIL;

    protected color: Color = BLACK;

    constructor(drawingService: DrawingService, undoredoService: UndoRedoService, private colorService: ColorService) {
        super(drawingService, undoredoService);
        this.onInit();
    }

    onInit(): void {
        // this.clearPath();
        this.resetCurrentStroke();
        this.colorService.getColor().subscribe((color: Color) => {
            this.color = color;
        });
    }

    onMouseMove(event: MouseEvent, time: number): void {
        const mousePosition = this.getPositionFromMouse(event);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        if (this.mouseDown) {
            this.draw(this.drawingService.previewCtx, this.currentStroke);
            const drawingPoint: Point = {
                position: mousePosition,
                time
            };
            this.currentStroke.push(drawingPoint);
        }
        this.drawPencil(mousePosition);
    }

    private drawPencil(position: Vec2): void {
        const ctx = this.drawingService.previewCtx;
        const radius = this.width / 2;
        const startAngle = 0;
        const endAngle = 2 * Math.PI;
        ctx.beginPath();
        ctx.strokeStyle = BLACK.value;
        ctx.fillStyle = this.color.value;
        ctx.lineWidth = ERASER_LINE_WIDTH;
        ctx.arc(position.x, position.y, radius, startAngle, endAngle);
        ctx.stroke();
        ctx.fill();
    }

    protected draw(ctx: CanvasRenderingContext2D, drawingPoints: Point[]): void {
        const stroke: Stroke = { tool: ToolName.PENCIL, points: drawingPoints, color: this.color, width: this.width };
        this.drawingService.pencil(ctx, stroke, true);
    }

}
