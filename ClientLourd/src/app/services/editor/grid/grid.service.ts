
import { Injectable } from '@angular/core';
import { DEFAULT_GRID_OPACITY, DEFAULT_GRID_THICKNESS } from '@app/models/drawing/constants';
import { BehaviorSubject, Observable } from 'rxjs';
import { DrawingService } from '../drawing/drawing.service';

@Injectable({
  providedIn: 'root'
})
export class GridService {

  private opacity: number = DEFAULT_GRID_OPACITY;
  private thickness: number = DEFAULT_GRID_THICKNESS;

  private visible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private drawingService: DrawingService) { }

  get isVisible(): Observable<boolean> {
    return this.visible;
  }

  setWidth(width: number): void {
    this.thickness = width;
    if (this.visible.value) {
      this.drawingService.clearCanvas(this.drawingService.gridCtx);
      this.drawGrid();
    }
  }

  getWidth(): number {
    return this.thickness;
  }

  toggle(): void {
    if (this.visible.value) {
      this.drawingService.clearCanvas(this.drawingService.gridCtx);
    } else {
      this.drawGrid();
    }
    this.visible.next(!this.visible.value);
  }

  private drawGrid(): void {
    const currentThickness = this.thickness;
    for (let i = 1; i < this.drawingService.previewCtx.canvas.width / currentThickness; i++) {
      this.drawingService.gridCtx.beginPath();
      this.drawingService.gridCtx.globalAlpha = this.opacity;
      this.drawingService.gridCtx.moveTo(i * currentThickness, 0);
      this.drawingService.gridCtx.lineTo(i * currentThickness, this.drawingService.previewCtx.canvas.height);
      this.drawingService.gridCtx.stroke();
    }
    for (let j = 1; j < this.drawingService.previewCtx.canvas.height / currentThickness; j++) {
      this.drawingService.gridCtx.beginPath();
      this.drawingService.gridCtx.globalAlpha = this.opacity;
      this.drawingService.gridCtx.moveTo(0, j * currentThickness);
      this.drawingService.gridCtx.lineTo(this.drawingService.previewCtx.canvas.width, j * currentThickness);
      this.drawingService.gridCtx.stroke();
    }
  }



}
