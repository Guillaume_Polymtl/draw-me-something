import { Injectable } from '@angular/core';
import { Drawing } from '@app/models/drawing/drawing';
import { Stroke } from '@app/models/drawing/stroke';
import { BehaviorSubject } from 'rxjs';
import { DrawingService } from '../drawing/drawing.service';

@Injectable({
  providedIn: 'root'
})
export class UndoRedoService {

  isUndo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isRedo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private drawingService: DrawingService) {
    this.onInit();
  }

  private undoneStrokes: Stroke[] = [];

  private currentDrawing: Drawing;

  onInit(): void {
    this.drawingService.getCurrentDrawing().subscribe((drawing: Drawing) => {
      this.currentDrawing = drawing;
    });
  }

  undoAll(): void {
    while (this.currentDrawing.strokes.length > 0) {
      this.undo();
    }
    this.drawingService.resetCurrentDrawing();
    this.undoneStrokes = [];
  }

  undo(): void {
    if (this.currentDrawing.strokes.length > 0) {
      this.isUndo.next(true);
      const stroke = this.drawingService.removeLastStrokeFromCurrentDrawing() as Stroke;
      this.drawingService.draw(this.drawingService.baseCtx, this.drawingService.baseCtx, this.currentDrawing, true);
      this.undoneStrokes.unshift(stroke);
      this.isUndo.next(false);
    }
  }

  canRedo(): boolean {
    if (this.undoneStrokes.length > 0) { return true; }
    return false;
  }

  canUndo(): boolean {
    if (this.currentDrawing.strokes.length > 0) { return true; }
    return false;
  }

  redo(): void {
    if (this.undoneStrokes.length > 0) {
      this.isRedo.next(true);
      const stroke = this.undoneStrokes.shift() as Stroke;
      this.drawingService.addStrokeToCurrentDrawing(stroke);
      this.drawingService.draw(this.drawingService.baseCtx, this.drawingService.baseCtx, this.currentDrawing, true);
      this.isRedo.next(false);
    }
  }

}
