import { Injectable } from '@angular/core';
import { Game } from '@app/models/game/game';
import { GameStatus } from '@app/models/game/game-status';
import RoundModel from '@app/models/game/round';
import { Guess } from '@app/models/game/streaming/guess';
import { BehaviorSubject } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '@services/authentication/authentication.service';
import { GroupGameSummary } from '@models/game/game-summary';
import { Team } from '@models/game/team';
import { RouterService } from '@services/router/router.service';
import { SnackBarService } from '@services/snack-bar/snack-bar.service';
import { GameMode } from '@models/game/game-mode';
import { ChatService } from '@services/chat/chat.service';
import { Message } from '@models/chat/message';
import { AudioService } from '@services/audio/audio.service';

const cloneDeep = require('lodash/cloneDeep');
@Injectable({
  providedIn: 'root'
})
export class GameService {

  socket: Socket;

  currentGame: BehaviorSubject<Game> = new BehaviorSubject<Game>({});
  currentGameStatus: BehaviorSubject<GameStatus> = new BehaviorSubject<any>({});
  currentTeam: BehaviorSubject<string> = new BehaviorSubject<string>('');

  isGuesser: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isArtist: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isGameStart: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  team1Score: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  team2Score: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  hints: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(['']);
  isRoundEnded: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  currentGuess: Guess = {};
  lastGuess: Guess = {};
  isGoodGuess: boolean;
  nextRound: BehaviorSubject<RoundModel> = new BehaviorSubject<RoundModel>({});
  gameSummary: BehaviorSubject<GroupGameSummary> = new BehaviorSubject<GroupGameSummary>({});

  triesLeft: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  constructor(
    private authService: AuthenticationService,
    private routerService: RouterService,
    private snackbar: SnackBarService,
    private chatService: ChatService,
    private audioService: AudioService) {
    this.onInit();
  }

  onInit(): void {
    this.socket = io(`${environment.server.gaming.ws}`);
    this.handleSocket();
    this.handleNextRound();
    this.handleRightOfReply();
    this.onGameStart();
    this.currentPlayerTeam();
    this.getHints();
    this.endOfRound();
    this.endGame();

  }

  get currentUserPseudo(): string {
    return this.authService.currentUserPseudo;
  }

  private handleSocket(): void {
    this.socket.on('player-guessing', (guess: Guess) => {
      const newStatus = this.currentGameStatus.value;
      if (!newStatus.guesses) {
        newStatus.guesses = [];
      }
      newStatus.guesses?.push(guess);
      this.currentGameStatus.next(newStatus);

      if (this.isGuesser) {
        this.guessProximity(guess.proximity as number);
      }
      if (this.currentGame.value.gameMode === GameMode.SPRINT_SOLO) {
        this.triesLeft.next(guess.triesLeft as number);
      }
    });

    this.socket.on('game-clock', (roundTimeLeft: number) => {
      const gameStatus = this.currentGameStatus.value;
      gameStatus.roundTimeLeft = roundTimeLeft;
      this.currentGameStatus.next(gameStatus);
      if (roundTimeLeft === 10000) {
        this.audioService.startClock();
      }
    });

    this.socket.on('room-message', (message: Message) => {
      this.chatService.processRecievedMessage(message);
    });
  }

  public guess(guess: Guess): void {
    this.socket.emit('player-guessing', {
      guessContent: guess.guessContent ?? '',
      gameUID: guess.gameUID,
      pseudo: guess.pseudo
    });
  }

  public endGuessDraw(): Promise<boolean> {
    return new Promise((resolve, _) => {
      this.socket.on('end-guess-draw', (goodGuess: boolean) => {
        resolve(this.isGoodGuess = goodGuess);
      });
    });
  }

  getGameUID(soc: Socket): Promise<string> {
    return new Promise((resolve, _) => {
      soc.on('create-game', (obj: any) => resolve(obj.gameUID));
    });
  }

  public isplayerDrawing(): boolean {
    const isDrawing = (this.nextRound.value.artist === this.currentUserPseudo);
    if (isDrawing) {
    }
    return (this.nextRound.value.artist === this.currentUserPseudo);
  }

  public isPlayerGuessing(): boolean {
    const isGuesser = this.nextRound.value.guessers?.some(g => g === this.currentUserPseudo);
    if (isGuesser) {
    }
    return isGuesser as boolean;
  }

  public handleNextRound(): void {
    this.socket.on('next-round', (round: RoundModel) => {
      this.audioService.stopClock();
      this.isRoundEnded.next(false);
      if (this.currentGame.value.gameMode === GameMode.SPRINT_SOLO) {
        this.team1Score.next(round.soloSprintScore as number);
      }
      this.nextRound.next(round);
      this.playerRoles();
    });
  }

  public handleRightOfReply(): void {
    this.socket.on('right-of-reply', (round: RoundModel) => {
      this.nextRound.next(round);
      this.playerRoles();
    });
  }

  public playerRoles(): void {
    this.isGuesser.next(this.isPlayerGuessing());
    this.isArtist.next(this.isplayerDrawing());
  }

  private onGameStart(): void {
    this.gameSummary.next({});
    this.socket.on('start-game', (isStart: boolean) => {
      this.isGameStart.next(isStart);
      this.currentGameStatus.next({});
    });
  }

  private getHints(): void {
    this.socket.on('send-hint', (hints: string[]) => {
      this.hints.next(hints);
    });
  }

  public hintResponse(hintNum: number): void {
    this.socket.emit('hint-response', {
      gameUID: this.currentGame.value.gameUID,
      pseudo: this.currentUserPseudo,
      hintNum,
    });
  }

  private endOfRound(): void {
    this.socket.on('team-point', (team: { teamUID: string, points: number }) => {
      this.updateTeamScores(team.teamUID, team.points);
      this.isRoundEnded.next(true);
      // updates the actual game object(teams) with the new points, but is not used at this point...
      const updatedTeams = this.currentGame.value.teams?.map(t => {
        const updatedTeam = { ...t };
        if (t.teamUID === team.teamUID) {
          updatedTeam.score = team.points;
        }
        return updatedTeam;
      });

      const updatedCurrentGame = cloneDeep(this.currentGame.value);
      updatedCurrentGame.teams = updatedTeams;
      this.currentGame.next(updatedCurrentGame);
    });
  }

  private updateTeamScores(teamUID: string, score: number): void {
    if (teamUID === 'Rose') {
      this.team1Score.next(score);
    } else if (teamUID === 'Bleue') {
      this.team2Score.next(score);
    }
  }

  private endGame(): void {
    this.socket.on('end-game', (gameSummary: any) => {
      this.gameSummary.next(gameSummary);
      this.routerService.gameSummary();
      this.isGameStart.next(false);
      this.currentTeam.next('');
    });
  }

  private guessProximity(proximity: number): void {
    const SOUND_TIME_MS = 2000;
    switch (proximity) {
      case -1: {
        break;
      }
      case 0: {
        this.snackbar.info('Vous êtes loin de la réponse...');
        this.audioService.startWrongAnsTimed(SOUND_TIME_MS);
        break;
      }
      case 1: {
        this.snackbar.info('Vous y êtes presque!');
        this.audioService.startWrongAnsTimed(SOUND_TIME_MS);

        break;
      }
      case 2: {
        this.snackbar.info('Bonne réponse!');
        this.audioService.startEffectSuccessTimed(SOUND_TIME_MS);
        this.audioService.stopClock();
        break;
      }
      default: {
        break;
      }
    }
  }

  private currentPlayerTeam(): void {
    this.currentGame.value.teams?.forEach(team => {
      team.teamMembers?.forEach(member => {
        if (member.pseudo === this.currentUserPseudo) { this.currentTeam.next(team.teamUID); }
        console.log(team.teamUID);
      });
    });
  }
}
