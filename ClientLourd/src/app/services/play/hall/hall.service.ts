import { Injectable } from '@angular/core';
import { Game } from '@models/game/game';
import { GameMode } from '@models/game/game-mode';
import { RouterService } from '@services/router/router.service';
import { SnackBarService } from '@services/snack-bar/snack-bar.service';
import { BehaviorSubject } from 'rxjs';
import { GameService } from '../game/game.service';


@Injectable({
  providedIn: 'root'
})
export class HallService {

  readonly games: BehaviorSubject<Game[]> = new BehaviorSubject<Game[]>([]);

  constructor(private gameService: GameService, private snackbar: SnackBarService,
              private routerService: RouterService) {
    this.handleHallUpdate();
  }

  private handleHallUpdate(): void {
    this.gameService.socket.on('update-hall', (games: Game[]) => {
      this.games.next(games);
    });
  }

  createGame(game: Game): void {
    this.gameService.socket.emit('create-game', game);
  }

  public joinGame(game: Game): void {
    const index = this.games.value.findIndex(g => g.gameUID === game.gameUID);

    this.gameService.socket.emit('join-game', {
      gameUID: game.gameUID,
      pseudo: this.gameService.currentUserPseudo
    });

    this.gameService.currentGame.next(this.games.value[index]);
    this.routerService.lobby();
  /*   if (this.games.value[index].gameMode === GameMode.CLASSIC) {
      this.routerService.lobby();
    } else {
      this.routerService.round();
    } */
  }
}
