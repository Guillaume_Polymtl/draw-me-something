import { Injectable } from '@angular/core';
import { BLACK, Color, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY } from '@app/models/drawing/color';
import { Drawing } from '@app/models/drawing/drawing';
import { Stroke } from '@app/models/drawing/stroke';
import { ToolName } from '@app/models/drawing/tool-name';
import { DrawingData } from '@models/game/streaming/drawing-data';
import { UndoRedoService } from '@services/editor/undo-redo/undo-redo.service';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { DrawingService } from '../../editor/drawing/drawing.service';
import { GameService } from '../game/game.service';
@Injectable({
  providedIn: 'root'
})
export class StreamingService {
  private currentDrawing: Drawing;
  private socket: Socket;
  private stroke: Stroke;
  public context: CanvasRenderingContext2D;
  public drawingData: DrawingData = new DrawingData();

  constructor(private drawingService: DrawingService,
              private gameService: GameService,
              private undoRedoService: UndoRedoService) {
    this.connect();
    this.onInit();
    this.stroke = { tool: ToolName.PENCIL, points: [], color: BLACK, width: 5 };
  }

  onInit(): void {
    this.drawingService.getCurrentDrawing().subscribe((drawing: Drawing) => {
      this.currentDrawing = drawing;
    });

  }

  connect(): void {
    this.socket = io(`${environment.server.gaming.ws}`);
    this.handleInboundDrawing();
  }

  sendDrawingData(drawingData: any): void {
    this.gameService.socket.emit('player-drawing', drawingData);
  }

  handleInboundDrawing(): void {
    this.gameService.socket.on('player-drawing', (drawingData: any) => {
      if (drawingData.toolType === 'Undo') {
        this.undoRedoService.undo();
        console.log('called undo');
      } else if (drawingData.toolType === 'Redo') {
        this.undoRedoService.redo();
        console.log('called redo');
      } else {
        this.parseInboundDrawing(drawingData);
      }
    });
  }

  parseInboundDrawing(drawingData: any): void {
    let end = false;
    let data;
    console.log(drawingData);
    for (let i = 0; i <= 10 && !end; i++) {
      data = {
        x: drawingData.x[i],
        y: drawingData.y[i],
        start: drawingData.start[i],
        end: drawingData.end[i],

        r: drawingData?.r[i] ?? MIN_COLOR_INTENSITY,
        g: drawingData?.g[i] ?? MIN_COLOR_INTENSITY,
        b: drawingData?.b[i] ?? MIN_COLOR_INTENSITY,
        a: drawingData?.a[i] ?? MAX_COLOR_INTENSITY,

        width: drawingData.width[i],
        toolType: drawingData.toolType[i],
        gameUID: drawingData.gameUID,
      };
      if (data?.x) {
        this.addPointToStroke(data);
        this.stroke.width = data.width;
        this.stroke.color = new Color(data.r, data.g, data.b, data.a); // this.toColor(data.color);
      }
      end = data.end;

    }
    this.drawStroke(data);

  }

  drawStroke(drawingData: any): void {
    if (drawingData.end) {
      switch (drawingData.toolType) {
        case ToolName.PENCIL: {
          this.drawingService.pencil(this.context, this.stroke, false);
          break;
        }
        case ToolName.ERASER: {
          this.drawingService.eraser(this.context, this.stroke, false);
          break;
        }
        default: {
          break;
        }
      }
      this.stroke = { tool: ToolName.PENCIL, points: [], color: new Color(0, 0, 0, 0), width: 0 };
    }
  }

  addPointToStroke(drawingData: any): void {
    const position = { x: drawingData.x, y: drawingData.y };
    const point = { position, time: 0 };
    this.stroke.points.push(point);
  }

  fillDrawingData(data: any): void {
    const length = this.drawingData.x?.length ?? 10;

    if (length === 10) {
      this.drawingData.end[this.drawingData.end.length - 1] = true;
      this.sendDrawingData(this.drawingData);
      this.drawingData = new DrawingData();
    } else if (length < 10) {
      this.drawingData.x?.push(data.x);
      this.drawingData.y?.push(data.y);
      this.drawingData.start?.push(data.start);
      this.drawingData.end?.push(data.end);

      this.drawingData.r?.push(data.r);
      this.drawingData.g?.push(data.g);
      this.drawingData.b?.push(data.b);

      // Optionnal alpha value
      this.drawingData.a?.push(data.a ?? 0);

      this.drawingData.width?.push(data.width);
      this.drawingData.toolType?.push(data.toolType);
      this.drawingData.gameUID = data.gameUID;
    } else {
      this.drawingData = new DrawingData();
    }
  }
}
