import { Injectable } from '@angular/core';
import { Game } from '@models/game/game';
import RoundModel from '@models/game/round';
import { RouterService } from '@services/router/router.service';
import { BehaviorSubject } from 'rxjs';
import { GameService } from '@services/play/game/game.service';
import { ChatService } from '@services/chat/chat.service';

@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  currentGame: BehaviorSubject<Game> = new BehaviorSubject<Game>({});
  canStart: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(private gameService: GameService, private routerService: RouterService, private chatService: ChatService) {
    this.gameService.currentGame.subscribe((game: Game) => {
      this.currentGame.next(game);
    });
    this.handleCanStart();
    this.handleLobbyUpdate();
    this.handleGameChannelUpdate();
  }

  private handleCanStart(): void {
    this.gameService.socket.on('can-start', (canStart: boolean) => {
      this.canStart.next(canStart);
    });
  }

  private handleLobbyUpdate(): void {
    this.gameService.socket.on('update-lobby', (game: Game) => {
      this.gameService.currentGame.next(game);
    });
  }

  private handleGameChannelUpdate(): void {
    this.gameService.socket.on('game-chat-ack', (response: { update: boolean }) => {
      if (response.update) {
        this.chatService.updateChannels();
      }
    });
  }

  public joinTeam(team: string): void {
    this.gameService.socket.emit('join-team', {
      gameUID: this.currentGame.value.gameUID,
      teamMember: { pseudo: this.gameService.currentUserPseudo, teamUID: team },
    });
  }

  public addVirtualPlayer(team: string, vPlayer: string): void {
    this.gameService.socket.emit('join-team', {
      gameUID: this.currentGame.value.gameUID,
      teamMember: { pseudo: vPlayer, teamUID: team, isVirtual: true },
    });
  }

  public leaveGame(): void {
    this.gameService.socket.emit('leave-game', {
      gameUID: this.currentGame.value.gameUID,
      pseudo: this.gameService.currentUserPseudo,
    });
    this.currentGame.next({});
    this.routerService.hall();
  }

  public startGame(): void {
    this.gameService.currentGameStatus.next({});
    this.gameService.socket.emit('start-game', this.currentGame.value.gameUID);
  }
}
