import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AuthPageComponent } from './components/authentication/auth-page/auth-page.component';
import { AuthGuard } from './services/guard/auth.guard';
import { ProfileComponent } from './components/profile/profile/profile.component';
import { IntegratedChatComponent } from './components/chat/integrated-chat/integrated-chat.component';
import { WindowedChatComponent } from './components/chat/windowed-chat/windowed-chat.component';
import { PairWordDrawingLibraryComponent } from './components/pair-word-drawing/pair-word-drawing-library/pair-word-drawing-library.component';
import { PairWordDrawingCreateComponent } from './components/pair-word-drawing/pair-word-drawing-create/pair-word-drawing-create.component';
import { WatchStreamComponent } from './components/play/stream/watch-stream/watch-stream.component';
import { StreamComponent } from './components/play/stream/stream/stream.component';
import { LobbyComponent } from './components/play/lobby/lobby/lobby.component';
import { RankingComponent } from '@components/ranking/ranking.component';
import { HallComponent } from './components/play/hall/hall.component';
import { RoundComponent } from '@components/play/round/round/round.component';
import { GameSummaryComponent } from '@components/play/game-summary/game-summary/game-summary.component';
import { FriendProfileComponent } from '@components/profile/profile/friend-profile/friend-profile.component';
import { TutorialComponent } from '@components/tutorial/tutorial.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent
  },
  {
    path: 'hall',
    canActivate: [AuthGuard],
    component: HallComponent
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    component: ProfileComponent
  },
  {
    path: 'friend-profile/:id',
    canActivate: [AuthGuard],
    component: FriendProfileComponent
  },
  {
    path: 'chat',
    canActivate: [AuthGuard],
    component: IntegratedChatComponent,
  },
  {
    path: 'windowed-chat',
    canActivate: [AuthGuard],
    component: WindowedChatComponent
  },
  {
    path: 'pair-word-drawing',
    canActivate: [AuthGuard],
    component: PairWordDrawingLibraryComponent
  },
  {
    path: 'pair-word-drawing/create',
    component: PairWordDrawingCreateComponent
  },
  {
    path: 'ranking',
    canActivate: [AuthGuard],
    component: RankingComponent
  },
  {
    path: 'stream',
    canActivate: [AuthGuard],
    component: StreamComponent
  },
  {
    path: 'watch',
    canActivate: [AuthGuard],
    component: WatchStreamComponent
  },
  {
    path: 'lobby',
    canActivate: [AuthGuard],
    component: LobbyComponent
  },
  {
    path: 'round',
    component: RoundComponent
  },
  {
    path: 'game-summary',
    component: GameSummaryComponent
  },
  {
    path: 'tutorial',
    component: TutorialComponent
  },
  {
    path: '*',
    redirectTo: 'auth/login',
  },
  {
    path: '*',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }
