// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// const HTTP = 'http://localhost:5000';
// const WS = 'ws://localhost:5000';
const HTTP = 'http://log3990-110.canadaeast.cloudapp.azure.com:80';
const WS = 'ws://log3990-110.canadaeast.cloudapp.azure.com:80';

// hello world

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBNxOLxH0kbIFYbvfhDrd-PikTs5LdY9Bo',
    authDomain: 'projet3-log3900.firebaseapp.com',
    projectId: 'projet3-log3900',
    storageBucket: 'projet3-log3900.appspot.com',
    messagingSenderId: '871333797731',
    appId: '1:871333797731:web:a19970d3a007f79e4fbd56',
    measurementId: 'G-KGVVDV55RT',
  },
  server: {
    base: WS,
    channels: {
      http: `${HTTP}/channels`,
      ws: `${WS}/channel`,
    },
    users: {
      subscribedChannels: `${HTTP}/users/subscribedChannels`,
      unsubscribedChannels: `${HTTP}/users/unsubscribedChannels`,
      classicHistory: `${HTTP}/game/history/classic`,
      soloHistory: `${HTTP}/game/history/solosprint`,
      singUp: `${HTTP}/users/signup`,
      signIn: `${HTTP}/users/login`,
      logOut: `${HTTP}/users/logout`,
      account: `${HTTP}/users/account`,
      avatar: `${HTTP}/users/account`,
      all: `${HTTP}/users`
    },
    drawings: {
      create: `${HTTP}/pair-word/create`,
      getRandom: `${HTTP}/pair-word/random`,
      potracePreview: `${HTTP}/pair-word/potrace-preview`,
      page: `${HTTP}/pair-word/page`
    },
    gaming: {
      ws: `${WS}/game`,
    },
    ranking: {
      classic: `${HTTP}/game/ranking/classic`,
      sprintSolo: `${HTTP}/game/ranking/soloSprint`,
      user: {
        classic: `${HTTP}/users/gamestats/classic`,
        sprintSolo: `${HTTP}/users/gamestats/soloStats`,
      },
    },
    friends: {
      sendRequest: `${HTTP}/friends/request`,
      answerRequest: `${HTTP}/friends/acknowledge`,
      friends: `${HTTP}/friends`
    }
  },
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
