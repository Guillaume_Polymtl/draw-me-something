const { app, BrowserWindow } = require('electron')

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      nativeWindowOpen: true,
    },
  })
  // win.setMenu(null);

  // and load the index.html of the app.
  win.loadURL('http://localhost:4200')
}

app.whenReady().then(createWindow)
